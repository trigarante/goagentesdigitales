import { Component, OnInit } from '@angular/core';
import { T2020FilesByAreasInterface } from 'src/app/_interfaces';
import { T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  apiLoaded = false;
  loaderGo: boolean = false;
  trainningInfo: T2020FilesByAreasInterface[];
  googleEndpoint: string = environment.endpoint_t2020_drive;
  constructor(
    private t2020Service: T2020Service
  ) { }

  ngOnInit(): void {
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
    if(this.apiLoaded){
      setTimeout(() => {
          this.loaderGo = true;
        }, 1000);
    }
    this._getTrainningInfo();
  }

  private _getTrainningInfo(): void {
    this.t2020Service.getFilesByArea(27).subscribe(
      succ => {
        this.trainningInfo = succ;
      },
      err => {
        console.error(err.messageError);
      }
    );
  }
}