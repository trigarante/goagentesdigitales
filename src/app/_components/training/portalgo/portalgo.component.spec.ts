import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PortalgoComponent } from './portalgo.component';

describe('PortalgoComponent', () => {
  let component: PortalgoComponent;
  let fixture: ComponentFixture<PortalgoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PortalgoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
