import { Component, OnInit } from '@angular/core';
import { T2020FilesByAreasInterface } from 'src/app/_interfaces';
import { T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-portalgo',
  templateUrl: './portalgo.component.html',
  styleUrls: ['./portalgo.component.css']
})
export class PortalgoComponent implements OnInit {
  apiLoaded = false;
  loaderGo = false;
  trainningInfo: T2020FilesByAreasInterface[];
  // googleEndpoint: string = environment.endpoint_t2020_drive;
 public imgPortalGO: any;

  constructor(private capacitacion:CapacitacionService ) { }

  ngOnInit(): void {
    this._getImgPortalGO();
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
    if(this.apiLoaded){
          setTimeout(() => {
              this.loaderGo = true;
            }, 1000);
    }
    // this._getTrainningInfo();

  }

  _getImgPortalGO(){
    this.capacitacion.getImgPortalGo().subscribe((resp)=>{
      this.imgPortalGO=resp;
    })
  }

}
