import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CobranzaGOComponent } from './cobranza-go.component';

describe('CobranzaGOComponent', () => {
  let component: CobranzaGOComponent;
  let fixture: ComponentFixture<CobranzaGOComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CobranzaGOComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CobranzaGOComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
