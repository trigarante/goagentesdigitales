import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-cobranza-go',
  templateUrl: './cobranza-go.component.html',
  styleUrls: ['./cobranza-go.component.css']
})
export class CobranzaGOComponent implements OnInit {
    public imgCobranza: any;

  constructor(private capacitacion:CapacitacionService) { }

  ngOnInit(): void {
    this._getImgCobranza();
  }
_getImgCobranza(){
    this.capacitacion.getImgCobranza().subscribe((resp)=>{
        this.imgCobranza= resp;
      console.log(resp);
    })
}
}
