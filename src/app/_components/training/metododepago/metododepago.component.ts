import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-metododepago',
  templateUrl: './metododepago.component.html',
  styleUrls: ['./metododepago.component.css']
})
export class MetododepagoComponent implements OnInit {
  public imgMetodoPago: any;

  constructor(private capacitacionService:CapacitacionService) { }

  ngOnInit(): void {
    this.getImgMetodoPago();
  }
  getImgMetodoPago(){
    this.capacitacionService.getImgMetodoPago().subscribe(resp =>{
      this.imgMetodoPago =resp;
    })
  }
}
