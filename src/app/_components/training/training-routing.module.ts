import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';

import { TrainingComponent } from './training.component';
import { ProductComponent } from './product/product.component';
import { CarsComponent } from './cars/cars.component';
import { PortalgoComponent } from './portalgo/portalgo.component';
import { MetododepagoComponent } from './metododepago/metododepago.component';
import {CobranzaGOComponent} from "./cobranza-go/cobranza-go.component";
import {ArgumentosVentaComponent} from "./argumentos-venta/argumentos-venta.component";
import {ProspectarComponent} from "./prospectar/prospectar.component";

const routes: Routes = [
  { path: '', component: TrainingComponent, children: [
    { path: 'producto', component: ProductComponent, canActivate: [AuthGuardGuard] },
    { path: 'autos', component: CarsComponent, canActivate: [AuthGuardGuard] },
    { path: 'portal-go', component: PortalgoComponent, canActivate: [AuthGuardGuard] },
    { path: 'metodo-de-pago', component: MetododepagoComponent, canActivate: [AuthGuardGuard] },
    { path: 'cobranza-go', component: CobranzaGOComponent, canActivate: [AuthGuardGuard] },
    { path: 'argumentos-ventas', component: ArgumentosVentaComponent, canActivate: [AuthGuardGuard] },
    { path: 'prospectar', component: ProspectarComponent, canActivate: [AuthGuardGuard] },

    { path: ':any', redirectTo: '/admin,producto', pathMatch: 'full' },
    { path: '', redirectTo: '/admin/producto', pathMatch: 'full' }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingRoutingModule { }
