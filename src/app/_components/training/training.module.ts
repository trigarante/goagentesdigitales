import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainingRoutingModule } from './training-routing.module';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';

import { TrainingComponent } from './training.component';
import { ProductComponent } from './product/product.component';
import { CarsComponent } from './cars/cars.component';
import { PortalgoComponent } from './portalgo/portalgo.component';
import { MetododepagoComponent } from './metododepago/metododepago.component';
import { CobranzaGOComponent } from './cobranza-go/cobranza-go.component';
import { ArgumentosVentaComponent } from './argumentos-venta/argumentos-venta.component';
import {QRCodeModule} from "angularx-qrcode";
import { ProspectarComponent } from './prospectar/prospectar.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    TrainingComponent,
    ProductComponent,
    CarsComponent,
    PortalgoComponent,
    MetododepagoComponent,
    CobranzaGOComponent,
    ArgumentosVentaComponent,
    ProspectarComponent
  ],
  imports: [
    CommonModule,
    TrainingRoutingModule,
    CommonLocalModule,
    QRCodeModule,
    ReactiveFormsModule
  ]
})
export class TrainingModule { }
