import { Component, OnInit } from '@angular/core';
import { T2020FilesByAreasInterface } from 'src/app/_interfaces';
import { T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  trainningInfo: T2020FilesByAreasInterface[];
  googleEndpoint: string = environment.endpoint_t2020_drive;

  constructor(
    private t2020Service: T2020Service
  ) { }

  ngOnInit(): void {
    this._getTrainningInfo();
  }

  private _getTrainningInfo(): void {
    this.t2020Service.getFilesByArea(25).subscribe(
      succ => {
        this.trainningInfo = succ.sort();
      }, 
      err => {
        console.error(err.messageError);
      }
    );
  }
}
