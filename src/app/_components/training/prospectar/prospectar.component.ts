import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-prospectar',
  templateUrl: './prospectar.component.html',
  styleUrls: ['./prospectar.component.css']
})
export class ProspectarComponent implements OnInit {
lista:string[]=[];
  constructor(private capacitacionService:CapacitacionService ) { }

  ngOnInit(): void {
    this.getClientes();
  }
  getClientes(){
    this.capacitacionService.getClientes().subscribe(resp =>{
      this.lista=resp;
    })
  }


}
