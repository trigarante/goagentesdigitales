import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgumentosVentaComponent } from './argumentos-venta.component';

describe('ArgumentosVentaComponent', () => {
  let component: ArgumentosVentaComponent;
  let fixture: ComponentFixture<ArgumentosVentaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArgumentosVentaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgumentosVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
