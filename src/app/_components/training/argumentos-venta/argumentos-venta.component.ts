import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-argumentos-venta',
  templateUrl: './argumentos-venta.component.html',
  styleUrls: ['./argumentos-venta.component.css']
})
export class ArgumentosVentaComponent implements OnInit {
lista:string[]=[];
  constructor(private capacitacionService:CapacitacionService) { }

  ngOnInit(): void {
    this.getArgumetosVentas();
  }
  getArgumetosVentas(){
    this.capacitacionService.getArgumentosVentas().subscribe(resp=>{
      this.lista = resp;

      console.log(this.lista)
    })
  }

}
