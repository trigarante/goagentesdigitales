import { Component, OnInit } from '@angular/core';
import { MenuToggleService } from 'src/app/_services';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
toggleMenu: boolean = false;
  constructor(  private menuToggleService: MenuToggleService
  ) { }

  ngOnInit(): void {
  this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {this.toggleMenu = item;});
  }

}
