import { Component, OnInit } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { element } from 'protractor';
import { T2020AreasInterface, T2020FilesByAreasInterface } from 'src/app/_interfaces';
import { T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import {ImagenesService} from '../../../../_services/imagenes.service';

@Component({
  selector: 'phone-book',
  templateUrl: './phone-book.component.html',
  styleUrls: ['./phone-book.component.css']
})
export class PhoneBookComponent implements OnInit {
  phoneBook=[];

  constructor(
    private dbService: NgxIndexedDBService,
    private imagenesService:ImagenesService,
  ){}

  ngOnInit(): void {
    this._getServiceDirectorio();
  }
_getServiceDirectorio():void{
  this.imagenesService.getDirectorioGo().subscribe((resp:[])=>{
      this.phoneBook = resp;
       //console.log('Se esta consumiendo con exito imagenesService', this.phoneBook);
    })
}
  // private _getPhoneBook(): void {
  //   this.t2020Service.getFilesByArea(24).subscribe(
  //     succ => {
  //       this.phoneBook = environment.endpoint_t2020_drive + succ[0].url;
  //     }, 
  //     err => {
  //       console.error(err.messageError);
  //     }
  //   );
  // }
}
