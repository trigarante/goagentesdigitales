import { Component, OnInit } from '@angular/core';
import {ImagenesService} from '../../../_services/imagenes.service';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

// @ts-ignore
@Component({
  selector: 'inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [NgbCarouselConfig]
})
export class InicioComponent implements OnInit {
  imagenes:[];
  private ngCarousel: any;
  constructor(private imageBanner:ImagenesService,config: NgbCarouselConfig) {
    config.interval = 1000;
    config.keyboard = true;
    config.pauseOnHover = true;
  }
  sliderView: boolean = false;
changeClass(id){
  //console.log(id);
  for (let i = 1; i <= this.imagenes.length; i++) {
  document.getElementById('item'+i).className='d-none';
}
  let elm = document.getElementById('item'+id);
  if(elm.className === 'd-none'){
      elm.className = 'active';
    } else {
      elm.className = 'd-none';
    }
}

  ngOnInit() {
      this.imageBanner.getImgBanner().subscribe((resp:[])=>{
        this.imagenes = resp;
        // console.log('Ses esta consumiendo con exito', this.imagenes);
      })
    }
}




