import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';

import { DashboardComponent } from './dashboard.component';
import { PrincipalComponent } from './principal/principal.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, children: [
    { path: 'principal', component: PrincipalComponent, canActivate: [AuthGuardGuard] },

    // { path: ':any', redirectTo: '/admin/dashboard/principal', pathMatch: 'full' },
    { path: '**', redirectTo: '/admin/dashboard/principal', pathMatch: 'full' }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
