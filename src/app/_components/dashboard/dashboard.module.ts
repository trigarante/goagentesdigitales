import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';

import { DashboardComponent } from './dashboard.component';
import { PrincipalComponent } from './principal/principal.component';
import { InicioComponent } from './inicio/inicio.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthBarrierInterceptor } from 'src/app/_interceptors';
import { PhoneBookComponent } from './inicio/phone-book/phone-book.component';
import {InicioModule} from "../inicio/inicio.module";
import {NgbCarouselModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    DashboardComponent, 
    PrincipalComponent, 
    InicioComponent, PhoneBookComponent
  ],
    imports: [
        CommonModule,
        DashboardRoutingModule,

        CommonLocalModule,
        InicioModule,
        NgbCarouselModule
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthBarrierInterceptor,
      multi: true
    }
  ]
})
export class DashboardModule { }
