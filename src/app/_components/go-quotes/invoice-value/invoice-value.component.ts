import { Component, OnInit } from '@angular/core';
import {
  AseguradoraInterface,
  CatalogosInterface, CodPostalInterface,
  CotizacionRequestInterface,
  CotizacionResponseInterface
} from "../../../_interfaces";
import {ActivatedRoute, Router} from "@angular/router";
import {
  CatalogosService, CotizacionService,
  DataComunicationsService,
  OnlyNumbersService,
  RemoveStorageQuotationService, ToLowerCaseKeyService
} from "../../../_services";
import {environment as GENERALS} from "../../../../environments/environment.generals";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import jwt_decode from "jwt-decode";
import Swal from "sweetalert2";

@Component({
  selector: 'app-invoice-value',
  templateUrl: './invoice-value.component.html',
  styleUrls: ['./invoice-value.component.css']
})
export class InvoiceValueComponent implements OnInit {
  aseguradoraSelected: string = '';
  submitted: boolean = false;
  cotizacionResponse: CotizacionResponseInterface = { Aseguradora: '' };
  cotizacionRequest: CotizacionRequestInterface = {};
  descuento: number = 0;
  precioGral: number = 0;
  precioDescuento: number = 0;
  precioFin: number = 0;
  dropdownList: CatalogosInterface[] = [];
  selected: any;
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  fechaNacimientoL: string = '';
  modelos: CatalogosInterface[] = [];
  descripciones: CatalogosInterface[] = [];
  subdescripciones: CatalogosInterface[] = [];
  detalles: CatalogosInterface[] = [];
  selectedDetalles: any;
  colonia: string = '';
  colonias: CodPostalInterface[] = [];
  cotizo: boolean = false;
  // cotizacion: any; // FormGroup;
  hiddenData: boolean = false;
  emisionRequest: CotizacionResponseInterface = { Aseguradora: '' };
  idAgente: any;
  tokenData: string = '';
  isLoading: any; // : Promise<boolean>;
  messageError: string = '';
  loaderGo: boolean = false;
  typeSingleMulti: string = '';
  urlType: string = '';
  tmpQuotation: any[] = [];
  generlsDiscount = GENERALS.discount;
  bancsMSI;
  cotizacionUpperCase:any = { };
  descuentoAfirme:any;
  ciudad:string;
   modeloFactura: CatalogosInterface[] = [];

  cotizacion = new FormGroup({
    selectMarca: new FormControl('', [Validators.required]),
    selectModelo: new FormControl('', [Validators.required]),
    selectDescripcion: new FormControl('', [Validators.required]),
    selectSubDescripcion: new FormControl('', [Validators.required]),
    //selectDetalle: new FormControl('', [Validators.required]),
    edad: new FormControl('', [Validators.required]),
    valorFactura: new FormControl('', [Validators.required,Validators.minLength(6), Validators.maxLength(6), Validators.min(100000), Validators.max(850000)]),
    genero: new FormControl('', [Validators.required]),
    cp: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]),
  });


  constructor(
      private route: ActivatedRoute,
      private catalogosService: CatalogosService,
      private autosService: CatalogosService,
      private onlyNumber: OnlyNumbersService,
      private toLowerCaseKeyService: ToLowerCaseKeyService,
      private cotizacionService: CotizacionService
  ) {
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    });
  }

  ngOnInit(): void {
    let _tokenAs = localStorage.getItem('tokenAs') || '';
    this.aseguradoraSelected = _tokenAs;
    let aseguradora = _tokenAs.toUpperCase();
    switch (aseguradora){
      case "LA_LATINO": aseguradora = 'LATINO'; break;
      case "GENERAL_DE_SEGUROS": aseguradora = 'GENERAL'; break;
      case "EL_POTOSI": aseguradora = 'ELPOTOSI'; break;
      case "EL_AGUILA": aseguradora = 'ELAGUILA'; break;
        // case "EL_AGUILA": aseguradora = 'ELAGUILA'; break;
        //case "PRIMERO": aseguradora = 'PRIMERO_SEGUROS'; break;
      default: aseguradora = _tokenAs.toUpperCase(); break;
    }

    this.bancsMSI = this.generlsDiscount.find(el => el.insurance.toUpperCase() === aseguradora).bancs;
    this._getMarcas(this.aseguradoraSelected.toLowerCase());
  }

  private _getMarcas(aseguradora: any): void {
    this.modelos = [];
    this.descripciones = [];
    this.subdescripciones = [];
    this.selectedDetalles = [];
    this.cotizacion.controls['selectModelo'].setValue('');
    this.cotizacion.controls['selectDescripcion'].setValue('');
    this.cotizacion.controls['selectSubDescripcion'].setValue('');

    this.autosService.getMarcas(aseguradora === 'primero'? 'primero_seguros':aseguradora.toString().toLowerCase(), this.urlType).subscribe(
        resp => {
          this.dropdownList = resp;
          console.log( 'valor-factura',resp)
        },
        err => {
          console.error('ERROR: ', err);
        }
    );
  }

  getModelos(): void {
    this.descripciones = [];
    this.subdescripciones = [];
    this.selectedDetalles = [];
    this.cotizacion.controls['selectDescripcion'].setValue('');
    this.cotizacion.controls['selectSubDescripcion'].setValue('');
    // console.log("getModelos")
    // console.log(this.cotizacion.controls.selectModelo.value)
    // console.log(this.aseguradoraSelected, 'aseguradora');

    this.autosService.getModelos(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.urlType).subscribe(
        resp => {
          this.modelos = resp;
          this.modeloFactura= [];
          if(this.aseguradoraSelected==='HDI'){
            for (let i = 0 ; i < 1; i++){
              // if(parseInt(this.modelos[i]['text'])>=2021){
              //   this.modeloFactura.push(this.modelos[i]);
              // }
              this.modeloFactura.push(this.modelos[i]);
            }
          }else{
            for (let i = 0 ; i <= 2; i++){
              this.modeloFactura.push(this.modelos[i])
            }
          }

        },
        (err: HttpErrorResponse) => {
          if (err.status === 401) {
            console.error(err.status);
            console.log('Sesion Caducada');
          }
        });
  }
//OBTINE LOS CATALODOS DE LAS COLOBNIAS
  getColonias(): void {
    if(this.aseguradoraSelected === 'Afirme' && this.cotizacion.controls['cp'].value.length === 5){
      this.autosService.getColonias(this.cotizacion.controls['cp'].value).subscribe(
          data => {
            this.colonias = data;
            this.ciudad=this.colonias[0].ciudad;
            this.getDescuentoAfirme(this.ciudad);
          },
          err => {
            console.error('ERROR: ', err);
          }
      );

    }

  }
  getDescuentoAfirme(ciudad:string){
    this.autosService.getDescuentoAfirme(ciudad).subscribe(
        data => {
          this.descuentoAfirme = data;
          this.descuento = this.descuentoAfirme.descuento;
          localStorage.setItem('descAfi', this.descuento.toString());
        },
        err => {
          console.error('ERROR: ', err);
        }
    );
  }

  get f() { return this.cotizacion.controls; }

  onSubmit() {

    // console.log('primero descuento', this.descuento)
    // this.getColonias();
    this.submitted = true;
    this.loaderGo = true;
    if (this.cotizacion.invalid) {
      console.log('Formulario no validado correctamente');
      this.loaderGo = false;
      return; //lo quite para no llenar el form y revisar el form
    }
    localStorage.setItem('_t', (this.cotizacion.controls['genero'].value === 'MASCULINO') ? 'H' : 'M');
    let _insurance = this.aseguradoraSelected.split('_').join('')===  "generalDeSeguros" ? 'GENERAL':this.aseguradoraSelected.split('_').join('');
    this.descuento = this.descuento === 0 ? GENERALS.discount.find(el => el.insurance.toUpperCase() === _insurance.toUpperCase()).discount : this.descuento;
    let _typeQuote = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase()).disabled;
    const fechaN = new Date().getFullYear() - this.cotizacion.controls['edad'].value;
    //FORMATO DE FECHA PARA MAPFRE
    if(this.aseguradoraSelected==='mapfre'){
      this.fechaNacimientoL = fechaN + '-01-01';
    }else{
      this.fechaNacimientoL = '01/01/' + fechaN;
    }

    let _typePack = 'AMPLIA';
    // if (this.urlType === 'privado') {
    //   _typePack = 'AMPLIA';
    // } else {
    //   _typePack = (_typeQuote) ? 'AMPLIA' : (this.aseguradoraSelected.toUpperCase() === 'QUALITAS' || this.aseguradoraSelected.toUpperCase() === 'GNP' || this.aseguradoraSelected.toUpperCase() === 'ZURICH' || this.aseguradoraSelected.toUpperCase() === 'AXA' || this.aseguradoraSelected.toUpperCase() === 'MIGO') ? 'ALL' : 'ALL'; // eldd change to mayus when back change
    // }
    // console.log('descuentoooo',this.descuento)
    this.cotizacionRequest = {
      aseguradora: this.aseguradoraSelected.toUpperCase(),
      clave: this.selectedDetalles +'-'+ this.cotizacion.controls['valorFactura'].value,
      cp: this.cotizacion.controls['cp'].value,
      descripcion: this.cotizacion.controls['selectDescripcion'].value,
      descuento: this.descuento,
      edad: this.cotizacion.controls['edad'].value.toString(),
      fechaNacimiento: this.fechaNacimientoL,
      genero: this.cotizacion.controls['genero'].value,
      marca: this.cotizacion.controls['selectMarca'].value,
      modelo: this.cotizacion.controls['selectModelo'].value,
      movimiento: 'cotizacion',
      paquete: _typePack,
      servicio: 'PARTICULAR',
    };
    // this.cotizacionRequest.descuento = this.descuento;
    if (this.aseguradoraSelected.toUpperCase() === 'MIGO') {
      this.cotizacionRequest.submarca = this.cotizacion.controls['selectSubDescripcion'].value;
    }
    let _token = localStorage.getItem('token') || '';
    this.tokenData = _token;
    this.idAgente = jwt_decode(this.tokenData);
    const dataSaveCot = {
      idAgente: +this.idAgente['sub'].split(':')[1],
      peticion: JSON.stringify(this.cotizacionRequest),
      respuesta: "[]"
    };
    if (!_typeQuote) {
      sessionStorage.setItem('baseQuotation', JSON.stringify(this.cotizacionRequest));
      if (this.urlType === 'particular') {
        this._quotations3Ways(dataSaveCot, true);
      }
      else {
        this.autosService.cotizarValorFactura(this.cotizacionRequest, this.urlType).subscribe(
            resp => {
              this.isLoading = false;
              this.loaderGo = false;

              if(resp.coberturas){
                if (resp.coberturas.length > 0) {

                  localStorage.setItem('tmpQuotation', JSON.stringify(resp))
                  this._saveQuote(dataSaveCot);
                  this._validateResult(resp);
                } else {
                  this.loaderGo = false;
                  if (resp.codigoError) {
                    Swal.fire({
                      title: resp.codigoError,
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      showConfirmButton: true
                    });
                  }
                }
              }

              if(resp.Coberturas){
                if (resp.Coberturas.length > 0) {
                  localStorage.setItem('tmpQuotation', JSON.stringify(resp))
                  this._saveQuote(dataSaveCot);
                  this._validateResult(resp);
                } else {
                  this.loaderGo = false;
                  if (resp.CodigoError) {
                    Swal.fire({
                      title: resp.CodigoError,
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      showConfirmButton: true
                    });
                  }
                }
              }


            },
            error => {
              console.log('18.2.1');
              Swal.fire({
                title: 'Error',
                text: 'Hubo un error, favor de volver a intentar la cotización. 5',
                icon: 'error',
              });
              this.isLoading = false;
              this.loaderGo = false;
              console.error(`Error al cotizar ${error}`);
            }
        );
      }
    }
    else {

      this.autosService.cotizarValorFactura(this.cotizacionRequest, this.urlType).subscribe(
          resp => {
            if (resp.Coberturas.length > 0) {
              localStorage.setItem('tmpQuotation', JSON.stringify(resp))
              this._saveQuote(dataSaveCot);
              this._validateResult(resp);
            } else {
              this.loaderGo = false;
              if (resp.CodigoError) {
                Swal.fire({
                  title: resp.CodigoError,
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  showConfirmButton: true
                });
              }
            }
          },
          error => {
            console.log('18.2.1');
            Swal.fire({
              title: 'Error',
              text: 'Hubo un error, favor de volver a intentar la cotización. 6',
              icon: 'error',
            });
            this.isLoading = false;
            this.loaderGo = false;
            console.error(`Error al cotizar ${error}`);
          }
      );
    }
  }

  private _quotations3Ways(dataSaveCot: any, type: boolean): void {
    this.autosService.cotizar3Ways(this.cotizacionRequest).subscribe(
        succ => {
          if(dataSaveCot.peticion){
            let peticion = JSON.parse(dataSaveCot.peticion);
            if(peticion.aseguradora.toString().toUpperCase() === 'MIGO'){ succ.aseguradora = peticion.aseguradora.toString().toUpperCase(); }
          }
          if (succ.codigoError) {
            this.isLoading = false;
            this.loaderGo = false;
            Swal.fire({
              title: 'Error',
              text: 'Hubo un error, favor de volver a intentar la cotización. 7',
              icon: 'error',
            });
          } else {
            console.log('18.1');
            this.tmpQuotation.push(succ);
            if (type)
              localStorage.setItem('lastCotizacion', JSON.stringify(succ));
            this._makeQuotations3Types(this.cotizacionRequest, dataSaveCot, succ);
          }

        },
        err => {
          Swal.fire({
            title: 'Error',
            text: 'Hubo un error, favor de volver a intentar la cotización. 8',
            icon: 'error',
          });
          this.isLoading = false;
          this.loaderGo = false;
          console.error(`Error al cotizar ${err}`);
          console.log('ERROR 18.1.1');
        }
    );
  }

  private _makeQuotations3Types(request: any, dataSaveCot: any, _3ways): void {
    request.paquete = 'AMPLIA';
    let dataCotizacionAmplia:any = [];
    if(this.tmpQuotation[0]){
      dataCotizacionAmplia = this.tmpQuotation[0];
      if(dataCotizacionAmplia.cotizacion && dataCotizacionAmplia.coberturas){
        let nodoCotizacion = dataCotizacionAmplia.cotizacion[0].amplia[0].anual;
        let nodoCoberturas = dataCotizacionAmplia.coberturas[0].amplia;
        dataCotizacionAmplia.cotizacion = nodoCotizacion;
        dataCotizacionAmplia.coberturas = nodoCoberturas;
        dataCotizacionAmplia.paquete = 'AMPLIA';
        this.tmpQuotation.push(JSON.parse(localStorage.getItem('lastCotizacion')));
        _3ways = JSON.parse(localStorage.getItem('lastCotizacion'));
      }
    }
    if(dataCotizacionAmplia.aseguradora){
      localStorage.setItem('tmpQuotation', JSON.stringify(dataCotizacionAmplia))
      this._saveQuote(dataSaveCot);
      this._validateResult(_3ways);
    }
    // else {
    //
    //   this.cotizacionService.cotizar(request).subscribe(
    //       resp => {
    //         if(request.aseguradora.toString().toUpperCase() === 'PRIMERO'){ resp = this.cotizacionUpperCase;}
    //         localStorage.setItem('tmpQuotation', JSON.stringify(resp))
    //         this._saveQuote(dataSaveCot);
    //         this._validateResult(_3ways);
    //       },
    //       error => {
    //         Swal.fire({
    //           title: 'Error',
    //           text: 'Hubo un error, favor de volver a intentar la cotización.',
    //           icon: 'error',
    //         });
    //         this.isLoading = false;
    //         this.loaderGo = false;
    //         console.error(`Error al cotizar ${error}`);
    //       });
    // }

  }

  private _validateResult(result: CotizacionResponseInterface): void {
    let _token = localStorage.getItem('token') || '';
    this.cotizo = true;
    /**
     * Funcion para validar que traiga precio
     */
    let _insurance = localStorage.getItem('tokenAs');
    this.cotizacionResponse = result;
    this.emisionRequest = result;
    this.typeSingleMulti = (GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase()).disabled) ? 'single' : (this.urlType === 'privada') ? 'single' : 'multi';
    this.tokenData = _token;
    this.idAgente = jwt_decode(this.tokenData);
    this.loaderGo = false;

    if (this.cotizacionResponse && (this.cotizacionResponse.cotizacion || this.cotizacionResponse.Cotizacion)) {
      // && this.cotizacionResponse.Cotizacion.PrimaTotal && Number(this.cotizacionResponse.Cotizacion.PrimaTotal) > 500) {
      localStorage.setItem('lastCotizacion', JSON.stringify(result));
      this.messageError = '';
      this.isLoading = true; // Promise.resolve(true);
      console.log('typeSingleMulti', this.typeSingleMulti)
      if (this.typeSingleMulti === 'single') {



        // this._limpiarCoberturas();
        this.precioFin = Number((this.cotizacionResponse.cotizacion) ? this.cotizacionResponse.cotizacion.primaTotal : this.cotizacionResponse.Cotizacion.PrimaTotal);
        this.precioDescuento = (Number((this.cotizacionResponse.cotizacion) ? this.cotizacionResponse.cotizacion.primaTotal : this.cotizacionResponse.Cotizacion.PrimaTotal) * this.descuento) / 100;
        this.precioGral = this.precioFin * 1.30;//es estático para todas en lo que se desarrolla un servicio.
      }
    } else {
      if (this.cotizacionResponse.codigoError === undefined) {
        this.messageError = 'Error al cotizar';
      } else {
        this.messageError = this.cotizacionResponse.codigoError;
      }
    }
  }

  private _saveQuote(dataSaveCot: any): void {
    this.catalogosService.guardarCotizacion(dataSaveCot).subscribe(
        respuesta => {
          localStorage.setItem('cotizacionSaved', JSON.stringify(respuesta));
          this._saveNew(respuesta);
        },
        err => {
          console.error(err);
        }
    );
  }

  private _saveNew(respuesta: any): void {
    let _petition = this._makeJSONSave(respuesta.peticion);
    this.catalogosService.guardarNewFormat(respuesta.id, _petition).subscribe(
        respuesta => {
          localStorage.setItem('cotizacionSavedNew', JSON.stringify(respuesta));
        },
        err => {
          console.error(err);
        }
    );
  }

  private _limpiarCoberturas(): void {
    if (this.cotizacionResponse !== undefined) {
      if (this.cotizacionResponse.coberturas) {
        for (const c in this.cotizacionResponse.coberturas[0]) {
          if (this.cotizacionResponse.coberturas[0][c] === '-') {
            delete this.cotizacionResponse.coberturas[0][c];
          }
        }
      }
    }

    let _coverage = (localStorage.getItem('tokenAs').toUpperCase() === GENERALS.discount[0].insurance.toUpperCase()) ? this.cotizacionResponse.Coberturas[0] : this.cotizacionResponse.coberturas;
    this.cotizacionResponse.coberturas = Object.values(_coverage);
  }

  getDetalles(): void {

    console.log("getDetalles")
    this.detalles = this.subdescripciones;
    const detallesDisp = this.detalles.length - 1;
    for (let i = 0; i <= detallesDisp; i++) {
      if (this.cotizacion.controls['selectSubDescripcion'].value === this.detalles[i].text) {
        this.selectedDetalles = this.detalles[i].id;
      }
    }
  }

  getDescripciones() {
    this.subdescripciones = [];
    this.selectedDetalles = [];
    console.log("getDescripciones")
    console.log(this.cotizacion.controls['selectDescripcion'].value)

    this.autosService.getDescripciones(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.cotizacion.controls['selectModelo'].value, this.urlType).subscribe(
        resp => {
          this.descripciones = resp;

        },
        err => {
          console.error(err);
        }
    );
  }

  getSubdescripciones() {

    console.log("getSubdescripciones")
    this.autosService.getSubdescripciones(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.cotizacion.controls['selectModelo'].value, this.cotizacion.controls['selectDescripcion'].value, this.urlType).subscribe(
        resp => {
          this.subdescripciones = resp;
        },
        err => {
          console.error(err);
        }
    );
  }

  recotizar(setValue: boolean): void {
    this.cotizo = setValue;
  }

  private _makeJSONSave(petition: any): object {
    let _saved = JSON.parse(localStorage.getItem('tmpQuotation'));
    return _saved;
  }

  numberOnly(event: any): boolean {
    console.log(this.onlyNumber.validateInput(event))
    return this.onlyNumber.validateInput(event);
  }

}
