import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceValueComponent } from './invoice-value.component';

describe('InvoiceValueComponent', () => {
  let component: InvoiceValueComponent;
  let fixture: ComponentFixture<InvoiceValueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceValueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
