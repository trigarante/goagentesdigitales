import { Component, OnInit } from '@angular/core';
import { CatalogosService, MenuToggleService, RemoveStorageQuotationService } from 'src/app/_services';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Component({
  selector: 'app-go-quotes',
  templateUrl: './go-quotes.component.html',
  styleUrls: ['./go-quotes.component.css']
})
export class GoQuotesComponent implements OnInit {
  toggleMenu: boolean = false;

  constructor(
    private menuToggleService: MenuToggleService,
    private dbService: NgxIndexedDBService,
    private catalogosService: CatalogosService,
    private removeStorageServices: RemoveStorageQuotationService
  ) { }

  ngOnInit(): void {
    this._setStorage();
    this._getBrands();
    this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {
        this.toggleMenu = item; });
  }

  private _setStorage(): void {
    this.removeStorageServices.removeStorage(1);
  }

  private _getBrands(): void {
    // if (!localStorage.getItem('brandQualitas')) {
    //   this.catalogosService.getMarcas(aseguradora).subscribe(
    //     resp => {
    //       resp.forEach(el => {
    //         // this._getModels(el.text);
    //         this.dbService
    //           .add('brandQualitas', {
    //             id: el.id,
    //             text: el.text
    //           })
    //           .subscribe((key) => {
    //             console.log('key: ', key);
    //           });
    //       });
    //       localStorage.setItem('brandQualitas', 'true');
    //     }
    //   );
    // }
  }
}



