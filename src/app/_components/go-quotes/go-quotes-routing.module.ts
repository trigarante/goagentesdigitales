import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';
import { GenericPayComponent } from '../generals';

import { GoQuotesComponent } from './go-quotes.component';
import { MultiQuotesComponent } from './multi-quotes/multi-quotes.component';
import { ParticularComponent } from './particular/particular.component';
import { QuotesComponent } from './quotes/quotes.component';
import { ResponseTerminalComponent } from './response-terminal/response-terminal.component';
import {InvoiceValueComponent} from "./invoice-value/invoice-value.component";
import {FakePayComponent} from "../generals/fake-pay/fake-pay.component";
import {PayGnpComponent} from "../generals/generic-pay/pay-gnp/pay-gnp.component";
import {PayQualitasComponent} from "../generals/generic-pay/pay-qualitas/pay-qualitas.component";
import {PayMapfreComponent} from "../generals/generic-pay/pay-mapfre/pay-mapfre.component";
import {GraciasPagoComponent} from "../generals/generic-pay/gracias-pago/gracias-pago.component";
import {PayBanorteComponent} from "../generals/generic-pay/pay-banorte/pay-banorte.component";
import {PayAfirmeComponent} from "../generals/generic-pay/pay-afirme/pay-afirme.component";
import {PayAxaComponent} from "../generals/generic-pay/pay-axa/pay-axa.component";
import {PaySuraComponent} from "../generals/generic-pay/pay-sura/pay-sura.component";
import {PayAbaComponent} from "../generals/generic-pay/pay-aba/pay-aba.component";

const routes: Routes = [
  {
    path: '', component: GoQuotesComponent, children: [
      { path: 'multicotizador', component: MultiQuotesComponent, canActivate: [AuthGuardGuard] },
      { path: 'multicotizador/:cache', component: MultiQuotesComponent, canActivate: [AuthGuardGuard] },
      { path: 'terminal/:responseTerminal', component: ResponseTerminalComponent, canActivate: [AuthGuardGuard] },

      { path: ':typeQuote', component: ParticularComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/cotizador-valor-factura', component: InvoiceValueComponent, canActivate: [AuthGuardGuard]  },
      { path: ':typeQuote/cotizador', component: QuotesComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision-valor-factura', component: FakePayComponent, canActivate: [AuthGuardGuard] },

      { path: ':typeQuote/emision', component: GenericPayComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/mapfre', component: PayMapfreComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/qualitas', component: PayQualitasComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/gnp', component: PayGnpComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/banorte', component: PayBanorteComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/aba', component: PayAbaComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/afirme', component: PayAfirmeComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/axa', component: PayAxaComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/sura', component: PaySuraComponent, canActivate: [AuthGuardGuard] },

      { path: ':typeQuote/gracias', component: GraciasPagoComponent, canActivate: [AuthGuardGuard] },


      { path: ':typeQuote/emision/virtual', component: GenericPayComponent, canActivate: [AuthGuardGuard] },
      { path: ':typeQuote/emision/virtual/:terminalResponse', component: GenericPayComponent, canActivate: [AuthGuardGuard] },
      

      { path: ':any', redirectTo: '/admin/particular', pathMatch: 'full' },
      { path: '', redirectTo: '/admin/particular', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoQuotesRoutingModule { }
