import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiQuotesComponent } from './multi-quotes.component';

describe('MultiQuotesComponent', () => {
  let component: MultiQuotesComponent;
  let fixture: ComponentFixture<MultiQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
