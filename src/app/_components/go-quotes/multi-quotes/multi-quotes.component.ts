import { Component, HostListener, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CatalogosInterface, CotizacionResponseInterface } from 'src/app/_interfaces';
import {
  AuthenticationService,
  CatalogosService,
  CotizacionService,
  DatosServiceService,
  EvetnsEmiterLocalService,
  OnlyNumbersService,
  RemoveStorageQuotationService
} from 'src/app/_services';
import jwt_decode from 'jwt-decode';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { ModalService } from '../../generals/modal-dialog/_services';
import Swal from 'sweetalert2';
import { NgOverlayContainerService } from 'ng-overlay-container';
import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas';
import * as moment from "moment";
import {ImagenesService} from '../../../_services/imagenes.service';
import {faOtter} from "@fortawesome/free-solid-svg-icons";
import {marginTop} from "html2canvas/dist/types/css/property-descriptors/margin";
import {AseguradorasOnOffService} from "../../../_services/aseguradoras-on-off.service";

@Component({
  selector: 'app-multi-quotes',
  templateUrl: './multi-quotes.component.html',
  styleUrls: ['./multi-quotes.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MultiQuotesComponent implements OnInit {
  imagenesMulti:any;
  mensajeDeNoDisponible = 'PRECIO NO DISPONIBLE';
  @ViewChild('origin', { static: false, read: ViewContainerRef }) origin: ViewContainerRef;
  loaderGo: boolean = false;
  loaderGoEmision: boolean = false;
  user: any;
  currentDate = moment().locale('es').format('LLLL');
  marcas; // : CatalogosInterface[];
  submitted: boolean = false;
  modelos: CatalogosInterface[] = [];
  descripciones: CatalogosInterface[] = [];
  subdescripciones: CatalogosInterface[] = [];
  detalles: CatalogosInterface[] = [];
  versiones: CatalogosInterface[] = [];
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  request = [];
  cotizacionResponses: CotizacionResponseInterface[] = [];
  isReady: Promise<boolean> = Promise.resolve(false);
  btnCotizarDisabled: boolean = false;
  cotizacion = new FormGroup({
    selectDescripcion: new FormControl('', [Validators.required]),
    selectMarca: new FormControl('', [Validators.required]),
    selectModelo: new FormControl('', [Validators.required]),
    selectSubmarca: new FormControl('', [Validators.required]),
    edad: new FormControl('', [Validators.required]),
    genero: new FormControl('', [Validators.required]),
    cp: new FormControl('', [Validators.required])
  });
  idPushResponse: number = 0;
  dataSource: CotizacionResponseInterface[] = [];
  displayedColumns = ['insurance', 'goCost', 'Coberturas',  'emitir',]; //'model','materialDamage', 'totalTiff', 'legalAsistant',  'partialCost', 
  // expandedElement: null;
  expandedElement: CotizacionResponseInterface | null;
  selectedID: string;
  valueLoader: boolean = false;
  scrHeight: any;
  scrWidth: any;
  displayExpads: boolean = true;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    this.displayExpads = (this.scrWidth <= GENERALS.responsive.width) ? false : true;
  }

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    public auth: AuthenticationService,
    private catalogosService: CatalogosService,
    private cotizacionService: CotizacionService,
    private dbService: NgxIndexedDBService,
    private onlyNumber: OnlyNumbersService,
    private removeStorageServices: RemoveStorageQuotationService,
    private evetnsEmiterLocalService: EvetnsEmiterLocalService,
    private modalService: ModalService,
    private overlayContainerService: NgOverlayContainerService,
    private imagenesService: ImagenesService,
    private datosService: DatosServiceService,
    private servicesOnOff: AseguradorasOnOffService
  ) {
    this.getScreenSize();
  }

  ngOnInit(): void {
    this._getData();
    this._getMarcasComparador();
    this.obtenerDescuentos();
    let _cache = this.activatedRoute.snapshot.params.cache;
    if (!_cache) {
      this._deleteLastStorage();
    } else {
      let _cotizacionResponses = JSON.parse(localStorage.getItem('multiQuote'));
      this.loaderGo = false;
      this.isReady = Promise.resolve(true);
      this.dataSource = _cotizacionResponses;
    };
    // 
    this._getServiceBannerMulti();
  }
_getServiceBannerMulti():void{
    let multi =[];
  this.imagenesService.getImgBannerMulti().subscribe((resp:[])=>{
       multi = resp;
      this.imagenesMulti = multi[0].url
    })
}

  openInfoModal(coverage: any, opt?: any): void {
    this.getScreenSize();
    if (!this.displayExpads) {
      this.modalService.openInfoQuotationModal(opt, coverage);
    }
  }

  private _eventLocal(active: boolean, insurance: string): void {
    this.evetnsEmiterLocalService.emitLoader(JSON.stringify({ active: active, insurance: insurance }));
  }

  _deleteLastStorage(): void {
    this.removeStorageServices.removeStorage();
  }

  private _getMarcasComparador(): void {
    let _getFromService: boolean = false;
    if (localStorage.getItem('brandsMulty')) {
      this.dbService.getAll('brandMulty').subscribe((brands) => {
        _getFromService = (brands.length > 1) ? false : true;
        this.marcas = brands;
      });
    } else { _getFromService = true; }
    if (_getFromService) {
      this.catalogosService.getMarcasComparador().subscribe(
        resp => {
          this.marcas = resp;
        },
        err => {
          console.error(err);
        }
      );
    }
  }

  get multiAseg() { return this.cotizacion.controls; }

  getModelos(marca: any): void {
    let _getFromService: boolean = false;
    if (localStorage.getItem('brandsMulty')) {
      this.dbService.getAll('modelMulty').subscribe((models) => {
        let _models = [];
        models.forEach(el => {
          if (el['brand'] === marca) {
            _models.push(el);
          }
        });
        _getFromService = (models.length > 1) ? false : true;
        this.modelos = _models;
      });
    } else { _getFromService = true; }
    if (_getFromService) {
      this.catalogosService.getModelosComparador(marca).subscribe(
        resp => {
          this.modelos = resp;
        },
        err => {
          console.error(err);
        }
      );
    }
  }

  getSubmarcas(marca: string, modelo: string): void {
    let _getFromService: boolean = false;
    if (localStorage.getItem('brandsMulty')) {
      this.dbService.getAll('subBrandMulty').subscribe((models) => {
        let _subBrands = [];
        models.forEach(el => {
          if (el['brand'] === marca && el['model'] === modelo) {
            _subBrands.push(el);
          }
        });
        _getFromService = (models.length > 1) ? false : true;
        this.descripciones = _subBrands;
      });
    } else { _getFromService = true; }
    
    if (_getFromService) {
      this.catalogosService.getSubmarcaComparador(marca, modelo).subscribe(
        resp => {
          this.descripciones = resp;
        },
        err => {
          console.error(err);
        }
      );
    }
  }

  getDescripciones(marca: string, modelo: string, submarca: string): void {

    this.catalogosService.getDescripcionesComparador(marca, modelo, submarca).subscribe(
      resp => {
        this.subdescripciones = resp;
      },
      err => {
        console.error(err);
      }
    );
  }

  getDetalles(marca: string, modelo: string, submarca: string, descripcion: string): void {
    this.catalogosService.getDetallesComparador(marca, modelo, submarca, descripcion).subscribe(
      resp => {
        this.detalles = resp;
        this.descuentosService(this.detalles)
      },
      err => {
        console.error(err);
      }
    );
  }

  private _saveRequest(): void {
    // let _insurance = this.aseguradoraSelected.split('_').join('')===  "generalDeSeguros" ? 'GENERAL':this.aseguradoraSelected === 'la_latino' ? 'LATINO':this.aseguradoraSelected.split('_').join('');
    // this.descuento = this.descuento === 0 ? GENERALS.discount.find(el => el.insurance.toUpperCase() === _insurance.toUpperCase()).discount : this.descuento;

    this.detalles;
    let _cotizacionRequest = {
      aseguradora: '',
      clave: '',
      cp: this.cotizacion.controls['cp'].value,
      descripcion: this.cotizacion.controls['selectDescripcion'].value,
      descuento: '',
      edad: this.cotizacion.controls['edad'].value.toString(),
      fechaNacimiento: '01/01/' + (new Date().getFullYear() - Number(this.cotizacion.controls['edad'].value)),
      genero: this.cotizacion.controls['genero'].value,
      marca: this.cotizacion.controls['selectMarca'].value,
      modelo: this.cotizacion.controls['selectModelo'].value,
      detalle:this.cotizacion.controls['selectSubmarca'].value,
      movimiento: 'cotizacion',
      paquete: 'AMPLIA',
      servicio: 'PARTICULAR',
      // periodicidadDePago: 'All'
    };
    let _token = localStorage.getItem('token') || '';
    let _tokenData = _token;
    let _idAgente = jwt_decode(_tokenData);
    const dataSaveCot = {
      idAgente: +_idAgente['sub'].split(':')[1],
      peticion: JSON.stringify(_cotizacionRequest),
      respuesta: "[]"
    };
    this.catalogosService.guardarCotizacion(dataSaveCot).subscribe(
      succ => {
        this.idPushResponse = succ.id;
        localStorage.setItem('_tmpMultyPush', JSON.stringify(succ));
      },
      err => {
        console.error(err);
      }
    );
  }

  private _pushRequest(resp: CotizacionResponseInterface): void {
    this.catalogosService.guardarNewFormat(this.idPushResponse, resp).subscribe(
      succ => { },
      err => {
        console.error(err);
      }
    );
  }
//Funcion para convertir de mayusculas a minusculas las iniciales del json
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
        }
      }
    }
    return json;
  }
  descuentoAseguradoraMulty2 = []
  descuentoAseguradoraMulty22 = []
  descuentoAseguradoraMulty: any;
  descuentoAseguradoraMulty1: any;
  descuentoDeServicio = []

  obtenerDescuentos(){
    this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
      dataAseg.forEach((dataDescuento, asegu) => {
        this.descuentoDeServicio.push({insurance: dataDescuento.insuranceCompanyID, business: 'Go Agentes', discount: dataDescuento.discount})
      })
    })
  }

  descuentosService(detalesAseg){
    for(let des = 0 ; des < detalesAseg.length; des++){
      for (let selectCot = 0; selectCot < this.descuentoDeServicio.length; selectCot++){
        if(detalesAseg[des].aseguradora === this.descuentoDeServicio[selectCot].insurance){
          this.descuentoAseguradoraMulty2.push(this.descuentoDeServicio[selectCot])
          this.descuentoAseguradoraMulty = {...this.descuentoAseguradoraMulty2}
        }
      }

    }
  }
  armarRequest() {
    this._saveRequest();
    this.loaderGo = true;
    this.valueLoader = true;
    localStorage.setItem('_t', this.multiAseg.genero.value);
    localStorage.setItem('cpAfirme',this.cotizacion.controls['cp'].value);
    // aqui va uno
    this.cotizacionService.armarRequest(this.cotizacion.controls, this.detalles).then(
      requests => {
        let _size = this.detalles.length;
        let _errSize = 0;
        requests.forEach(req => {
          if(req.aseguradora !== "CRABI"){
                if(req.aseguradora.toUpperCase() ==='MAPFRE'){  req.fechaNacimiento = moment(req.fechaNacimiento).format('YYYY-MM-DD'); }
                if(req.aseguradora.toUpperCase() ==='AFIRME'){
                  req.fechaNacimiento = moment(req.fechaNacimiento).format('DD-MM-YYYY');
                  req.detalle = this.cotizacion.controls['selectDescripcion'].value
                  req.descripcion = this.cotizacion.controls['selectSubmarca'].value
                }
                for (let prueba = 0; prueba < this.detalles.length; prueba++){
                  if((this.descuentoAseguradoraMulty[prueba].insurance).toUpperCase() === req.aseguradora.toUpperCase()){
                        req.descuento = (this.descuentoAseguradoraMulty[prueba].discount == 'Descuento no configurado para esa Empresa.') ? '0' : this.descuentoAseguradoraMulty[prueba].discount;
                  }
                }

            this.cotizacionService.cotizar(req).subscribe(
            resp => {
              resp= this.parseToLower(JSON.stringify(resp));
              _size -= 1;
              this.valueLoader = (_size === 0) ? false : true;
                  if (resp) {

                    if(resp.aseguradora === 'GENERALDESEGUROS'){
                      resp.aseguradora = 'GS';
                    }
                    if(typeof resp.aseguradora !== 'undefined'){
                      if(resp.cotizacion[0] && resp.coberturas[0]){
                        resp.cotizacion = resp.cotizacion[0].amplia[0].anual;
                        resp.coberturas = resp.coberturas[0];
                      }
                      //resp = this.toCamel(resp);
                    }

                    this._pushRequest(resp);
                  }
                  this.isReady = Promise.resolve(true);
                  if (resp) {
                    if(resp.cotizacion.primaTotal){
                      resp.cotizacion.primaTotal = resp.cotizacion.primaTotal.replace('mxn', '')
                      resp.cotizacion.primaTotal = resp.cotizacion.primaTotal.replace(',', '')
                    }
                    this.cotizacionResponses.push(new CotizacionResponseInterface(resp.paquete === 'MIGO' ? 'MIGO' : (req.aseguradora) ? req.aseguradora : '', resp, [] ));
                     this.cotizacionResponses = this.cotizacionResponses.sort(function (a, b) {
                        let datoA=0;
                        let datoB=0
                        if(a['cotizacion']){
                            if(a['cotizacion']['cotizacion']){
                            if(a['cotizacion']['cotizacion']['primaTotal'] === '' || a['cotizacion']['cotizacion']['primaTotal'] === '0.0'){
                              datoA = 100000
                            }else{
                              datoA = a['cotizacion']['cotizacion']['primaTotal'];

                            }
                          }
                        }
                        if(b['cotizacion']){
                            if(b['cotizacion']['cotizacion']){
                            if(b['cotizacion']['cotizacion']['primaTotal'] === '' || b['cotizacion']['cotizacion']['primaTotal'] === '0.0'){
                              datoB = 1000000
                            }else{
                              datoB = b['cotizacion']['cotizacion']['primaTotal'];

                            }
                          }
                        }
                         return Number(datoA) - Number(datoB);
                     });
                    this.loaderGo = false;
                    this.dataSource = this.cotizacionResponses;
                    localStorage.setItem('multiQuote', JSON.stringify(this.cotizacionResponses));
                  }
                }, err => { 
                _size -= 1;
                this.valueLoader = (_size === 0) ? false : true;
                
                _errSize += 1;
                if(_errSize === this.detalles.length){
                  Swal.fire({
                    icon: 'warning',
                    title: 'por el momento ninguna aseguradora cotiza',
                    text: 'por favor intenta mas tarde',
                    footer: 'Gracias!'
                  });
                  this.loaderGo = false;
                }
            });
          }

        });
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.cotizacion.invalid) {
      console.log('Formulario no validado correctamente');
      return; //lo quite para no llenar el form y revisar el form
    } else {
      this.btnCotizarDisabled = true;
    }
    this.armarRequest();
  }

  showForm() {
    // this.cotizacion.reset();
    this.request = [];
    this.cotizacionResponses = [];
    this.isReady = Promise.resolve(false);
    this.btnCotizarDisabled = false;

    this.submitted = false;
    this.cotizacion = new FormGroup({
      selectDescripcion: new FormControl('', [Validators.required]),
      selectMarca: new FormControl('', [Validators.required]),
      selectModelo: new FormControl('', [Validators.required]),
      selectSubmarca: new FormControl('', [Validators.required]),
      edad: new FormControl('', [Validators.required]),
      genero: new FormControl('', [Validators.required]),
      cp: new FormControl('', [Validators.required])
    });

  }

  jumpWizard(insurance: string): void {
    this.loaderGoEmision = true
    localStorage.setItem('tmpMultiSelected', insurance);
    this._eventLocal(true, insurance);
    this._setCotizacionSaved(insurance);
  }

  private _setCotizacionSaved(insurance: string): void {
    let _tmp = JSON.parse(localStorage.getItem('tmpMultAskQuotation'));
    let _quotation = _tmp.filter(el => el.aseguradora.toLowerCase() === insurance.toLowerCase())[0];
        localStorage.removeItem('cotizacionSaved');
          let _cotizacionRequest = _tmp.find(el => el.aseguradora.toLowerCase() === insurance.toLowerCase());
    let _multyInsurance = localStorage.getItem('tmpMultiSelected') || null;
    let _tmpQuotation2 = JSON.parse(localStorage.getItem('multiQuote'));
    let _quotation2 = _tmpQuotation2.find(el => el.aseguradora.toLowerCase() === _multyInsurance.toLowerCase()).cotizacion;
    let nuevaPet = [];
    nuevaPet.push(_quotation2)
    console.log('hola2', _quotation2)
          let _token = localStorage.getItem('token') || '';
          let tokenData = _token;
          let idAgente = jwt_decode(tokenData);
          const dataSaveCot = {
            idAgente: +idAgente['sub'].split(':')[1],
            peticion: JSON.stringify(_cotizacionRequest),
            // respuesta: "[]" // hola2
            respuesta: _quotation2 ? JSON.stringify(nuevaPet) : ''
          };
          this.catalogosService.guardarCotizacion(dataSaveCot).subscribe(
            respuesta => {
              this.loaderGoEmision = false;
              localStorage.setItem('cotizacionSaved', JSON.stringify(respuesta));
              localStorage.setItem('cotizacionSaved', JSON.stringify(respuesta));
              if(localStorage.getItem('tmpMultiSelected') === 'MAPFRE'){
                this.router.navigate(['admin/cotiza-go/particular/emision/mapfre']);
              }else if(localStorage.getItem('tmpMultiSelected') === 'GNP'){
                this.router.navigate(['admin/cotiza-go/particular/emision/gnp']);
              }else if(localStorage.getItem('tmpMultiSelected') === 'QUALITAS'){
                this.router.navigate(['admin/cotiza-go/particular/emision/qualitas']);
              }else if(localStorage.getItem('tmpMultiSelected') === 'BANORTE'){
                this.router.navigate(['admin/cotiza-go/particular/emision/banorte']);
              }else if(localStorage.getItem('tmpMultiSelected') === 'ABA'){
                this.router.navigate(['admin/cotiza-go/particular/emision/aba']);
              }
              else if(localStorage.getItem('tmpMultiSelected') === 'AFIRME'){
                    this.router.navigate(['admin/cotiza-go/particular/emision/afirme']);
                  }
              //     else if(localStorage.getItem('tmpMultiSelected') === 'AXA'){
              //       this.router.navigate(['admin/cotiza-go/particular/emision/axa']);
              //     }
                  // else if(localStorage.getItem('tokenAs') === 'Sura'){
                  //   this.router.navigate(['admin/cotiza-go/particular/emision/sura']);
              // }
              else {
                this.router.navigate(['admin/cotiza-go/particular/emision/']);
              }
            },
            err => {
              this.loaderGoEmision = false;
              console.error(err);
            }
          );
          localStorage.setItem('cotizacionSaved', JSON.stringify(_quotation));

  }

  getRowClass(row): string {
    return this.selectedID && this.selectedID.toUpperCase() === row.aseguradora.toUpperCase() ? 'selected-row' : null;
  }

  highlight(row, detailRow?) {
    this.selectedID = (this.selectedID !== row.aseguradora) ? row.aseguradora : null;
  }

  camelize(indexData){
    return  indexData.charAt(0).toUpperCase() + indexData.substring(1,indexData.length);
  }

  toCamel(dataJson){
    if (typeof dataJson === 'object'){
      if(Object.keys(dataJson).length){
        for (var indexData in dataJson){
          var newIndexData = '';
          if(indexData.length>0){ newIndexData = this.camelize(indexData); }
          dataJson[newIndexData] =  dataJson[indexData];
          delete dataJson[indexData];
          if (Object.keys(dataJson[newIndexData]).length>0 && newIndexData !== 'Coberturas'){
           dataJson[newIndexData] = this.toCamelSubNivel(dataJson[newIndexData]);
          }
        }
        return dataJson;
      }
    }else { return dataJson; }
  }

  toCamelSubNivel(jsonData){
    if (typeof jsonData === 'object'){
      for (var key in jsonData){
        var newKey = '';
        if(key.length>0){ newKey =  this.camelize(key);}
        jsonData[newKey] = jsonData[key];
        delete jsonData[key];
        if(typeof jsonData[newKey] !== 'undefined'){
          if (Object.keys(jsonData[newKey]).length>1){
            this.toCamelSubNivel(jsonData[newKey]);
          }
        }
      }
      return jsonData;
    }else { return jsonData; }
  }

  numberOnly(event: any): boolean {
    return this.onlyNumber.validateInput(event);
  }

  //--------------------------
  public showTemplate(content: TemplateRef<any>, insurance: string): void {
    const ngPopoverRef = this.overlayContainerService.open<string, { id: number }>({
      content,
      data: insurance,
      origin: this.origin.element.nativeElement
    });

    ngPopoverRef.afterClosed$.subscribe(result => {
    });
  }

  close(variables: any): void {
  }
  private _getData(): void {
    this.datosService.get().subscribe(
        data => {
          this.user = data;
        },
        err => {
          console.error(err);
        }
    );
  }

  htmltoPDF(): void {
    const input = document.getElementById("quotation-R");
    const divHeight = input.clientHeight;
    const divWidth = input.clientWidth;
    const ratio = divHeight / divWidth;

    html2canvas(input, {scale:1}).then((canvas) => {
      let imgData = canvas.toDataURL("image/jpeg", 1.0);
      let pdf = new jsPDF('p', 'px', [canvas.width, canvas.height]);
      const width = pdf.internal.pageSize.getWidth();
      let height = pdf.internal.pageSize.getHeight();
        height = ratio * width;    
          pdf.addImage(imgData, 0, 0, width, height);
          pdf.save('converteddoc.pdf');
        }
    );
  }
}
