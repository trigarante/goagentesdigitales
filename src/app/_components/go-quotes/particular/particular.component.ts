import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AseguradoraInterface } from 'src/app/_interfaces';
import { DataComunicationsService, RemoveStorageQuotationService } from 'src/app/_services';
import { environment as GENERALS } from 'src/environments/environment.generals';
import {AseguradorasOnOffService} from "../../../_services/aseguradoras-on-off.service";

@Component({
  selector: 'particular',
  templateUrl: './particular.component.html',
  styleUrls: ['./particular.component.css']
})
export class ParticularComponent implements OnInit {
  contactoData: AseguradoraInterface = {};
  disbaled: boolean = true;
  insuranceCarriers: any[] = [];
  url: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataComunicactionService: DataComunicationsService,
    private removeStorageServices: RemoveStorageQuotationService,
    private servicesOnOff: AseguradorasOnOffService
  ) {

    // VALIDA QUE OPCION FUE SELECCIONADA EN EL MENÚ PARA ASI MANDAR LA LISTA DE LAS ASEGURADORAS
    // CON LAS QUE SE PUEDE COTIZAR
    this.route.params.subscribe(params => {
      this.url = params.typeQuote;
      if (params.typeQuote === 'privado') {
        this.insuranceCarriers = [];
        GENERALS.particual_quote.forEach(el => {
          if (el.private.has) {
            this.insuranceCarriers.push(el)
          }
        });
      }else if (params.typeQuote === 'valor-factura') {
        this.insuranceCarriers = [];
        GENERALS.particual_quote.forEach(el => {
          if (el.invoice.has) {
            this.insuranceCarriers.push(el)
          }else {}
        });
      } else {
        this.insuranceCarriers = GENERALS.particual_quote;
      }
    });
  }
estadoAseguradoraService
  ngOnInit(): void {
    this._deleteLastStorage();
      this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
        this.estadoAseguradoraService = dataAseg
        for(let recorAseg = 0 ; recorAseg < GENERALS.particual_quote.length; recorAseg++ ){
          if( GENERALS.particual_quote[recorAseg].insurance_carrier && this.estadoAseguradoraService[recorAseg].isEnabledGeneral === false) {
            if(this.insuranceCarriers[recorAseg]){
              this.insuranceCarriers[recorAseg].disabled = !this.estadoAseguradoraService[recorAseg].isEnabledGeneral;
            }
          }
        }


      })
  }

//ELIMINA DEL LOCALSTORAGE
  _deleteLastStorage(): void {
    this.removeStorageServices.removeStorage(1);
  }
//AQUI DESACTIVAS LA LIGA DE COTIZACION POR ASEGURADORA.
  sendCotizacionAseguradora(aseguradoraSelected: any): void {
  console.log(this.url, 'url')
    if (this.url ==='particular'){
      for(let recorAseg = 0 ; recorAseg < GENERALS.particual_quote.length; recorAseg++ ){
        if( aseguradoraSelected === GENERALS.particual_quote[recorAseg].insurance_carrier && this.estadoAseguradoraService[recorAseg].isEnabledGeneral === false) {
          this.insuranceCarriers[recorAseg].disabled = !this.estadoAseguradoraService[recorAseg].isEnabledGeneral;
          return;
        }
      }
    }
    this.contactoData.selectAseguradora = aseguradoraSelected
    this.dataComunicactionService.sendDatos(this.contactoData);
    localStorage.setItem('tokenAs', this.contactoData.selectAseguradora);
    if(this.url==='valor-factura'){
      this.router.navigate(['admin/cotiza-go/' + this.url + '/cotizador-valor-factura']);
    }else{
       this.router.navigate(['admin/cotiza-go/' + this.url + '/cotizador']);
    }
  }

  toCamelSubNivel(jsonData: any):any{
    if (typeof jsonData === 'object'){
      for (var key in jsonData){
        var newKey = '';
        if(key.length>0){ newKey =  this.camelize(key);}
        jsonData[newKey] = jsonData[key];
        delete jsonData[key];
        if(typeof jsonData[newKey] !== 'undefined'){
          if (Object.keys(jsonData[newKey]).length>1){
            this.toCamelSubNivel(jsonData[newKey]);
          }
        }
      }
      return jsonData;
    }else { return jsonData; }
  }

  camelize(indexData: any):any{
    if(indexData == 'MSI'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'RFC'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'CURP'){ indexData = indexData.toString().toLowerCase();
    }else { indexData = indexData.charAt(0).toLowerCase() + indexData.substring(1,indexData.length); }
    return  indexData;
  }

  toCamel(dataJson: any): any{
    if (typeof dataJson === 'object'){
      if(Object.keys(dataJson).length){
        for (var indexData in dataJson){
          var newIndexData = '';
          if(indexData.length>0){ newIndexData = this.camelize(indexData); }
          dataJson[newIndexData] =  dataJson[indexData];
          delete dataJson[indexData];
          if(typeof dataJson[newIndexData] !== 'undefined') {
            if (Object.keys(dataJson[newIndexData]).length > 0 && newIndexData !== 'coberturas' && newIndexData !== 'Coberturas' && newIndexData !== 'cotizacion') {
              dataJson[newIndexData] = this.toCamelSubNivel(dataJson[newIndexData]);
            }
          }
        }
        return dataJson;
      }
    }else { return dataJson; }
  }

  available(insruance: any, type: string){
    // console.log(insruance)
    if (type === 'particular') {
      return insruance.disabled;
      // return (insruance.insurance_carrier.toUpperCase() === 'ANA' || insruance.insurance_carrier.toUpperCase() === 'ATLAS') ? false : insruance.disabled;
    }else if( type === 'valor-factura'){
      return !insruance.invoice.active;
    } else {
      return !insruance.private.active;
    }
  }
}
