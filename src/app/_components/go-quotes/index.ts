export * from './particular/particular.component';
export * from './invoice-value/invoice-value.component';
export * from './quotes/quotes.component';
export * from './multi-quotes/multi-quotes.component';
export * from './quotes/result-quote/result-quote.component';
export * from './quotes/multy-result/multy-result.component';
//export * from './quotes/multy-result-lower/multy-result-lower.component';
// export * from './quotes/multi-result/multi-result.component';
export * from './response-terminal/response-terminal.component';
