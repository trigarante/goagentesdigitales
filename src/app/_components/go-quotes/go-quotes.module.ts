import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoQuotesRoutingModule } from './go-quotes-routing.module';
import { GoQuotesComponent } from './go-quotes.component';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';
import { ParticularComponent } from './particular/particular.component';
import { AuthenticationService, CotizacionService, DataComunicationsService } from 'src/app/_services';
import { QuotesComponent } from './quotes/quotes.component';
import { MultiQuotesComponent } from './multi-quotes/multi-quotes.component';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthBarrierInterceptor } from 'src/app/_interceptors';
import { ResultQuoteComponent } from './quotes/result-quote/result-quote.component';
import { MultyResultComponent } from './quotes/multy-result/multy-result.component';
import { ResponseTerminalComponent } from './response-terminal/response-terminal.component';
import { PdfMultyComponent } from './quotes/multy-result/pdf-multy/pdf-multy.component';
import { InvoiceValueComponent } from './invoice-value/invoice-value.component';
import { InvoiceResultComponent } from './quotes/invoice-result/invoice-result.component';
import {FakePayComponent} from "../generals/fake-pay/fake-pay.component";
import { NoticeOfPrivacyComponent } from './notice-of-privacy/notice-of-privacy.component';

@NgModule({
  declarations: [
    GoQuotesComponent,
    ParticularComponent,
    QuotesComponent,
    MultiQuotesComponent,
    ResultQuoteComponent,
    MultyResultComponent,
    ResponseTerminalComponent,
    PdfMultyComponent,
    InvoiceValueComponent,
    InvoiceResultComponent,
    FakePayComponent,
    NoticeOfPrivacyComponent
  ],
  imports: [
    CommonModule,
    GoQuotesRoutingModule,

    CommonLocalModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthBarrierInterceptor,
      multi: true
    },
    DataComunicationsService,
    AuthenticationService,
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService,
    CotizacionService
  ]
})
export class GoQuotesModule { }
