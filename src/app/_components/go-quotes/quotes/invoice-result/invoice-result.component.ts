import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {CotizacionResponseInterface} from "../../../../_interfaces";
import {ActivatedRoute, Router} from "@angular/router";
import {environment as GENERALS} from "../../../../../environments/environment.generals";
import {HomologationParseResultService} from "../../../../_services";
import html2canvas from "html2canvas";
import {jsPDF} from "jspdf";

@Component({
  selector: 'app-invoice-result',
  templateUrl: './invoice-result.component.html',
  styleUrls: ['./invoice-result.component.css','../multy-result/multy-result.component.css']
})
// export class InvoiceResultComponent implements OnInit {
//   @Input() aseguradoraSelected: string;
//   @Input() cotizacion: FormGroup;
//   @Input() precioFin: any;
//   @Input() cotizacionResponse: CotizacionResponseInterface;
//   @Input() messageError: string;
//   @Output() cotizoEvent = new EventEmitter<boolean>();
//   quotations = [];
//   urlType: string = '';
//
//   constructor(
//       private router: Router,
//       private route: ActivatedRoute
//   ) {
//     this.route.params.subscribe(params => {
//       this.urlType = params.typeQuote;
//     });
//   }
//
//   ngOnInit(): void {
//     this._setValues();
//     this.recotizar(true);
//   }
//
//   private _setValues(): void {
//     // if (this.urlType === 'particular') {
//     //   if (this.cotizacionResponse.Coberturas[0].AsitenciaCompleta && this.cotizacionResponse.Coberturas[0].AsitenciaCompleta.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].AsitenciaCompleta);
//     //   if (this.cotizacionResponse.Coberturas[0].Cristales && this.cotizacionResponse.Coberturas[0].Cristales.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].Cristales);
//     //   if (this.cotizacionResponse.Coberturas[0].DanosMateriales && this.cotizacionResponse.Coberturas[0].DanosMateriales.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DanosMateriales);
//     //   if (this.cotizacionResponse.Coberturas[0].DanosMaterialesPP && this.cotizacionResponse.Coberturas[0].DanosMaterialesPP.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DanosMaterialesPP);
//     //   if (this.cotizacionResponse.Coberturas[0].DefensaJuridica && this.cotizacionResponse.Coberturas[0].DefensaJuridica.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DefensaJuridica);
//     //   if (this.cotizacionResponse.Coberturas[0].GastosMedicosEvento && this.cotizacionResponse.Coberturas[0].GastosMedicosEvento.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].GastosMedicosEvento);
//     //   if (this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes && this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes);
//     //   if (this.cotizacionResponse.Coberturas[0].MuerteAccidental && this.cotizacionResponse.Coberturas[0].MuerteAccidental.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].MuerteAccidental);
//     //   if (this.cotizacionResponse.Coberturas[0].RC && this.cotizacionResponse.Coberturas[0].RC.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RC);
//     //   if (this.cotizacionResponse.Coberturas[0].RCBienes && this.cotizacionResponse.Coberturas[0].RCBienes.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCBienes);
//     //   if (this.cotizacionResponse.Coberturas[0].RCExtension && this.cotizacionResponse.Coberturas[0].RCExtension.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCExtension);
//     //   if (this.cotizacionResponse.Coberturas[0].RCExtranjero && this.cotizacionResponse.Coberturas[0].RCExtranjero.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCExtranjero);
//     //   if (this.cotizacionResponse.Coberturas[0].RCFamiliar && this.cotizacionResponse.Coberturas[0].RCFamiliar.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCFamiliar);
//     //   if (this.cotizacionResponse.Coberturas[0].RCPExtra && this.cotizacionResponse.Coberturas[0].RCPExtra.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCPExtra);
//     //   if (this.cotizacionResponse.Coberturas[0].RCPersonas && this.cotizacionResponse.Coberturas[0].RCPersonas.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCPersonas);
//     //   if (this.cotizacionResponse.Coberturas[0].RoboTotal && this.cotizacionResponse.Coberturas[0].RoboTotal.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RoboTotal);
//     // } else {
//       if(this.cotizacionResponse.coberturas){
//         this.quotations = this.cotizacionResponse.coberturas;
//       }else if(this.cotizacionResponse.Coberturas){
//         this.quotations = this.cotizacionResponse.Coberturas;
//       }
//     // }
//   }
//
//   recotizar(returnLocal: boolean): void {
//     this.cotizoEvent.emit(returnLocal);
//   }
//
//   emitir(aseguradora: any): void {
//     if (aseguradora.toUpperCase() === GENERALS.particual_quote[0].insurance_carrier.toUpperCase()) {
//       this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/']);
//     }
//         // else if (aseguradora.toUpperCase() === GENERALS.particual_quote[1].insurance_carrier.toUpperCase()) {
//         //   this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/virtual/']);
//     // }
//     else if (aseguradora.toUpperCase() === GENERALS.particual_quote[1].insurance_carrier.toUpperCase()) {
//       this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/']);
//     }
//     else {
//       this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/']);
//     }
//   }
//
//   validateButton(): boolean {
//     if (this.aseguradoraSelected.toUpperCase() === 'ATLAS' || this.aseguradoraSelected.toUpperCase() === 'ANA') return true;
//     let _insurance = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase());
//     return (!_insurance.disabled) ? true : false;
//   }
//   htmltoPDF(): void {
//     html2canvas(document.querySelector("#quotationPDF")).then(
//         canvas => {
//           let pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
//           let _coverage = this.cotizacion.controls['selectMarca'].value + '_' + this.cotizacion.controls['selectModelo'].value + '_' + this.cotizacion.controls['selectSubDescripcion'].value;
//
//           let imgData = canvas.toDataURL("image/jpeg", 1.0);
//           pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
//           pdf.save('cotizacion_' + _coverage + '.pdf');
//         }
//     );
//   }
// }

export class InvoiceResultComponent implements OnInit{
  @Input() aseguradoraSelected: string;
  @Input() cotizacion: FormGroup;
  @Input() precioFin: any;
  @Input() cotizacionResponse: CotizacionResponseInterface;
  @Input() messageError: string;
  @Output() cotizoEvent = new EventEmitter<boolean>();
  finalPrice: any;
  quotationID: string;
  optSelected;
  insurance: string;
  TabIndex: string = 'amplia';
  coverages: any;
  coveragesAnual;
  coveragesLimitada;
  coveragesRC;
  defaultChecked: boolean = true;
  typeRadio: string = 'anual';
  pays: any[];
  generlsDiscount = GENERALS.discount;
  generlsParticualQuote = GENERALS.particual_quote;
  bancsMSI;
  coveragesList;

  constructor(
      private router: Router,
      private homologationParseResultService: HomologationParseResultService,
  ) { }

  ngOnInit(): void {
    localStorage.setItem('coverageSelected','AMPLIA');
    localStorage.setItem('payFormSelected','ANUAL');
    this.coveragesList = this.cotizacionResponse;
    console.log('this.coverageList', this.coveragesList);
    // this.coveragesList = this.homologationParseResultService.getCoveragesList(this.cotizacionResponse, 'amplia');
    // this.cotizacionResponse
    console.log(this.cotizacionResponse)
    this.insurance = localStorage.getItem('tokenAs');
    console.log('this.generlsDiscount',this.generlsDiscount)
    this.recotizar(true);
    this.changeSelected();
    // this.radioChange('anual');
    // this.btnClick(1);
    console.log('this.generlsDiscount',this.generlsDiscount)
    let _insurance = (this.optSelected.quotation.aseguradora.toUpperCase() === 'LA_LATINO') ? 'latino' : (this.optSelected.quotation.aseguradora.toUpperCase() === 'GENERALDESEGUROS') ? 'general' : this.optSelected.quotation.aseguradora;
    this.bancsMSI = this.generlsDiscount.find(el => el.insurance.toUpperCase() === _insurance.toUpperCase()).bancs;

    //Mandamos el valor de la cotizacion al localStorage
    localStorage.setItem('totalP', this.coveragesList.cotizacion.primaTotal);

  }

  btnClick(tabIndex: number): void {
    this.TabIndex = (tabIndex === 1) ? 'amplia' : (tabIndex === 2) ? 'limitada' : 'rc';
  }

  // onValChange(event: any): void {
  //   this.defaultChecked = false;
  //   let _event = { index: event === 'rc' ? 0 : (event === 'limitada') ? 1 : 2 };
  //   this.btnClick((event === 'amplia') ? 1 : (event === 'limitada') ? 2 : 0);
  //   setTimeout(() => {
  //     this.changeSelected(_event);
  //     this.radioChange('anual');
  //     this.defaultChecked = true;
  //   }, 10);
  // }

  recotizar(returnLocal: boolean): void {
    this.cotizoEvent.emit(returnLocal);
  }

  emitir(): void {
    // this._makeQuotations3Types();

    this._setCotizacionSaved();
    let _insurance = (this.optSelected.quotation.aseguradora.toUpperCase() === 'ELPOTOSI')?'EL_POTOSI':this.optSelected.quotation.aseguradora.toUpperCase();

    if (_insurance === GENERALS.particual_quote[0].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[2].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[3].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[5].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[7].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[8].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[11].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[12].insurance_carrier.split('_').join('').toUpperCase() ||
        _insurance === GENERALS.particual_quote[17].insurance_carrier.toUpperCase() ||
        _insurance === GENERALS.particual_quote[1].insurance_carrier.toUpperCase() ||
        _insurance === this.generlsParticualQuote[14].insurance_carrier.toUpperCase() ||
        _insurance === this.generlsParticualQuote[13].insurance_carrier.toUpperCase() ||
        _insurance === this.generlsParticualQuote[4].insurance_carrier.toUpperCase() ||
        _insurance === this.generlsParticualQuote[16].insurance_carrier.toUpperCase() ||
        _insurance === this.generlsParticualQuote[19].insurance_carrier.toUpperCase() ||
        _insurance === 'MIGO' || // migo
        _insurance === 'AFIRME' // afirme
        // _insurance === 'GENERAL'  // afirme
    ) {
      console.log('_insurance',_insurance);
      this.router.navigate(['admin/cotiza-go/particular/emision-valor-factura/']);
    }
    /* Caso en el que una aseguradora debera usar terminal virtual*/
    // if (//_insurance === this.generlsParticualQuote[1].insurance_carrier.toUpperCase() ||
    //   _insurance === this.generlsParticualQuote[16].insurance_carrier.toUpperCase() ) {
    //   this.router.navigate(['admin/cotiza-go/particular/emision/virtual/']);
    // }
  }

  private _setCotizacionSaved(): void {
    let _local = JSON.parse(localStorage.getItem('cotizacionSaved'));
    let _json, _coberturas, _cotizacion;
    if (this.displayValidate(this.insurance, 0)) {
      console.log(this.coveragesList);
      _coberturas = this.coveragesList.coberturas;
      _cotizacion = this.coveragesList.cotizacion;

      _json = {
        fechaActualizacion: _local.fechaActualizacion,
        fechaCreacion: _local.fechaCreacion,
        id: _local.id,
        idAgente: _local.idAgente,
        idSubRamo: _local.idSubRamo,
        peticion: _local.peticion,
        respuesta: JSON.stringify({
          aseguradora: this.optSelected.quotation.aseguradora,
          cliente: this.optSelected.quotation.cliente,
          codigoError: this.optSelected.quotation.codigoError,
          descuento: this.optSelected.quotation.descuento,
          paquete: this.optSelected.insurance,
          periodicidadDePago: this.optSelected.quotation.periodicidadDePago,
          urlRedireccion: this.optSelected.quotation.urlRedireccion,
          vehiculo: this.optSelected.quotation.vehiculo,
          coberturas: _coberturas,
          cotizacion: _cotizacion
        })
      }
    } else {
      if (this.insurance === this.generlsParticualQuote[0].insurance_carrier.toUpperCase()) {
        _coberturas = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.coberturas[0].rc : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.coberturas[1].limitada : this.optSelected.quotation.aoberturas[2].amplia;
        _cotizacion = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.cotizacion[0].rc[0].anual : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.cotizacion[1].limitada[0].anual : this.optSelected.quotation.cotizacion[2].amplia[0].anual;
      } else {
        _coberturas = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.coberturas[2].rc : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.coberturas[1].limitada : this.optSelected.quotation.coberturas[0].amplia;
        _cotizacion = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.cotizacion[2].rc[2].anual : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.cotizacion[1].limitada[0].anual : this.optSelected.quotation.cotizacion[0].amplia[0].anual;
      }

      _json = {
        fechaActualizacion: _local.fechaActualizacion,
        fechaCreacion: _local.fechaCreacion,
        id: _local.id,
        idAgente: _local.idAgente,
        idSubRamo: _local.idSubRamo,
        peticion: _local.peticion,
        respuesta: JSON.stringify({
          aseguradora: this.optSelected.quotation.aseguradora,
          cliente: this.optSelected.quotation.cliente,
          codigoError: this.optSelected.quotation.codigoError,
          descuento: this.optSelected.quotation.descuento,
          paquete: this.optSelected.insurance,
          periodicidadDePago: this.optSelected.quotation.periodicidadDePago,
          urlRedireccion: this.optSelected.quotation.urlRedireccion,
          vehiculo: this.optSelected.quotation.vehiculo,
          coberturas: _coberturas,
          cotizacion: _cotizacion
        })
      }
    }
    localStorage.removeItem('cotizacionSaved');
    localStorage.setItem('cotizacionSaved', JSON.stringify(_json));
  }

  canEmmit(aseguradoraSelected: string): boolean {
    let _insurance = this.generlsParticualQuote.find(el => {
      el.insurance_carrier.toUpperCase() === aseguradoraSelected.toUpperCase() && el.disabled === false
    });

    return (_insurance) ? true : false;
  }

  changeSelected(event?: any): void {
    this.finalPrice = this.cotizacionResponse.cotizacion.primaTotal ;
    console.log('PRECIO FINAL DE INVOICE-RESULT',this.cotizacionResponse.cotizacion.primaTotal);
    this.quotationID = this.cotizacionResponse.cotizacion.idCotizacion;
    this.optSelected = { insurance: this.homologationParseResultService.getOpt(event), quotation: this.cotizacionResponse };
    // this.finalPrice = this.homologationParseResultService.getTotal(event, this.insurance.toUpperCase(), this.cotizacionResponse, 'amplia', 'anual');
    // this.quotationID = this.homologationParseResultService.getIDQuotation(event, this.insurance, this.cotizacionResponse, 'amplia', 'anual');
    // this.optSelected = { insurance: this.homologationParseResultService.getOpt(event), quotation: this.cotizacionResponse };
    if(typeof event !== 'undefined') {
      if (typeof event.index !== 'undefined') {
        switch (event.index) {
          case 0:
            //cobertura RC
            localStorage.setItem('coverageSelected', 'RC')
            break;
          case 1:
            //cobertura LIMITADA
            localStorage.setItem('coverageSelected', 'LIMITADA')
            break;
          case 2:
            //cobertura AMPLIA
            localStorage.setItem('coverageSelected', 'AMPLIA')
            break;
          default:
            //default cobertura AMPLIA
            localStorage.setItem('coverageSelected', 'AMPLIA')
            break

        }
      }
    }
  }

  displayValidate(insurance: string, type?: number): boolean {
    let _return;
    if (!type) {
      insurance = (insurance === 'general_De_Seguros') ? 'general' : (insurance === 'la_latino') ? 'latino' : (insurance === 'el_potosi') ? 'elpotosi' : insurance;
      let data = this.generlsDiscount.find(el => el.insurance.toUpperCase() === insurance.split('_').join('').toUpperCase() && el.active == true).active;
      _return = data;
    } else {
      _return = (insurance.toUpperCase() === this.generlsDiscount[type].insurance.toUpperCase()) ? true : false;
    }

    return _return;
  }

  // radioChange(event?: any): void {
  //   this.typeRadio = event;
  //   let _total: number = 0;
  //   let _quotation = this.cotizacionResponse.cotizacion;
  //
  //   let _coverages = [];
  //   this.insurance = (this.insurance === 'general_De_Seguros') ? 'general' : (this.insurance === 'la_latino') ? 'latino' : this.insurance;
  //
  //   _total = _total = this.homologationParseResultService.getTotal({index: 5}, this.insurance.toUpperCase(), this.cotizacionResponse, this.optSelected.insurance, event);
  //   localStorage.setItem('totalP', _total.toString());
  //   sessionStorage.setItem('customQuotation', JSON.stringify({ radio: event, selected: this.optSelected.insurance }));
  //
  //   this.finalPrice = _total;
  //   this.coverages = this.homologationParseResultService.getCoverages(this.optSelected.insurance, this.insurance.toUpperCase(), _quotation);
  //   this.coveragesAnual = this.homologationParseResultService.getCoverages('amplia', this.insurance.toUpperCase(), _quotation);
  //   this.coveragesLimitada = this.homologationParseResultService.getCoverages('limitada', this.insurance.toUpperCase(), _quotation);
  //   this.coveragesRC = this.homologationParseResultService.getCoverages('rc', this.insurance.toUpperCase(), _quotation);
  //   this.pays = this.homologationParseResultService.getPays(this.optSelected.insurance, this.insurance.toUpperCase(), _quotation, event);
  //   if(typeof event !== 'undefined'){
  //     switch (event){
  //       case 'anual':
  //         localStorage.setItem('payFormSelected','ANUAL');
  //         break;
  //       case 'semestral':
  //         localStorage.setItem('payFormSelected','SEMESTRAL');
  //         break;
  //       case 'trimestral':
  //         localStorage.setItem('payFormSelected','TRIMESTRAL');
  //         break;
  //       case 'mensual':
  //         localStorage.setItem('payFormSelected','MENSUAL');
  //         break;
  //       default:
  //         localStorage.setItem('payFormSelected','ANUAL');
  //         break;
  //     }
  //   }
  //
  // }

  htmltoPDF(): void {
    html2canvas(document.querySelector("#quotationPDF")).then(
        canvas => {
          let pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
          let _coverage = this.cotizacion.controls['selectMarca'].value + '_' + this.cotizacion.controls['selectModelo'].value + '_' + this.cotizacion.controls['selectSubDescripcion'].value;

          let imgData = canvas.toDataURL("image/jpeg", 1.0);
          pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
          pdf.save('cotizacion_' + _coverage + '.pdf');
        }
    );
  }
}
