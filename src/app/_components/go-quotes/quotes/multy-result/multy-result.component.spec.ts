import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultyResultComponent } from './multy-result.component';

describe('MultyResultComponent', () => {
  let component: MultyResultComponent;
  let fixture: ComponentFixture<MultyResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultyResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultyResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
