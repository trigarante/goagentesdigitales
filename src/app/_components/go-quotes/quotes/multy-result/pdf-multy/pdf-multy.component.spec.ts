import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfMultyComponent } from './pdf-multy.component';

describe('PdfMultyComponent', () => {
  let component: PdfMultyComponent;
  let fixture: ComponentFixture<PdfMultyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdfMultyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfMultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
