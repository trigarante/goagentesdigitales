import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as moment from 'moment';
import {DatosServiceService} from "../../../../../_services";
import {CotizacionResponseInterface} from "../../../../../_interfaces";

@Component({
  selector: 'pdf-multy',
  templateUrl: './pdf-multy.component.html',
  styleUrls: ['./pdf-multy.component.css']
})
export class PdfMultyComponent implements OnInit {
  @Input() aseguradoraSelected: string;
  @Input() cotizacion: FormGroup;
  @Input() bancsMSI: any;
  @Input() coveragesList: any;
  @Input() coveragesAnual: any;
  @Input() coveragesLimitada: any;
  @Input() coveragesRC: any;
  @Input() coveragesEspecial: any;
  @Input() coveragesGold: any;
  @Input() quotationID: string;
  currentDate = moment().locale('es').format('LLLL');
  user: any;
  quotationType: string;
  lastCotizacion:any;
  lastCotizacionParce:any

  constructor(private datosService: DatosServiceService) { }

  ngOnInit(): void {
    this._getData();
    this.quotationType = localStorage.getItem('quotationType');
    this.lastCotizacion = localStorage.getItem('lastCotizacion');
    this.lastCotizacionParce = JSON.parse(this.lastCotizacion)
    console.log( 'soy el pdf --------------------------------> xxxxxxxxxxxxxxx',this.coveragesLimitada);
    console.log('xxxxxx',this.lastCotizacionParce);

  }
  
  private _getData(): void {
    this.datosService.get().subscribe(
        data => {
          this.user = data;
        },
        err => {
          console.error(err);
        }
    );
  }
}
