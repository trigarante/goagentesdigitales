import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CotizacionResponseInterface } from 'src/app/_interfaces';
// import { ShowDeduciblePipe, ShowNamePipe, ShowSumaRcPipe } from 'src/app/_pipes';
import { HomologationParseResultService } from 'src/app/_services';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas';

@Component({
  selector: 'multy-result',
  templateUrl: './multy-result.component.html',
  styleUrls: ['./multy-result.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MultyResultComponent implements OnInit {
  @Input() aseguradoraSelected: string;
  @Input() cotizacion: FormGroup;
  @Input() precioFin: any;
  @Input() cotizacionResponse: CotizacionResponseInterface;
  @Input() messageError: string;
  @Input() descuentoAseguradora: number;
  @Output() cotizoEvent = new EventEmitter<boolean>();

  finalPrice: any;
  quotationID: string;
  optSelected;
  insurance: string;
  TabIndex: string = 'amplia';
  coverages: any;
  coveragesAnual;
  coveragesLimitada;
  coveragesRC;
  coveragesEspecial;
  coveragesGold;
  defaultChecked: boolean = true;
  typeRadio: string = 'anual';
  pays: any[];
  generlsDiscount = GENERALS.discount;
  generlsParticualQuote = GENERALS.particual_quote;
  bancsMSI;
  coveragesList;
  quotationType:string;
  chofPrivado:string = ''
  constructor(
    private router: Router,
    private homologationParseResultService: HomologationParseResultService,
  ) { }

  ngOnInit(): void {
    this.chofPrivado = localStorage.getItem('servicio');
    if(this.chofPrivado == undefined){
      this.chofPrivado = 'particular'
    }
    console.log('----->this.chofPrivado',this.chofPrivado);
    localStorage.setItem('coverageSelected','AMPLIA');
    localStorage.setItem('payFormSelected','ANUAL');
    
    console.log(this.cotizacionResponse);
    this.coveragesList = this.homologationParseResultService.getCoveragesList(this.cotizacionResponse, 'amplia');
    console.log(this.coveragesList)
    
    this.cotizacionResponse
    this.insurance = localStorage.getItem('tokenAs');
    
    this.quotationType = localStorage.getItem('quotationType');
    // console.log('id ada',this.insurance)
    this.recotizar(true);
    this.changeSelected();

    console.log(this.optSelected);


    this.radioChange('anual');
    this.btnClick(1);

    console.log(this.optSelected);
    let _insurance = (this.optSelected.quotation.aseguradora.toUpperCase() === 'LA_LATINO') ? 'latino' : (this.optSelected.quotation.aseguradora.toUpperCase() === 'GENERALDESEGUROS') ? 'general' : this.optSelected.quotation.aseguradora;
    this.bancsMSI = this.generlsDiscount.find(el => el.insurance.toUpperCase() === _insurance.toUpperCase()).bancs;
  }

  btnClick(tabIndex: number): void {
    this.TabIndex = (tabIndex === 1) ? 'amplia' : (tabIndex === 2) ? 'limitada' : (tabIndex === 3) ? 'especial' : (tabIndex === 4) ? 'gold' : 'rc';
  }

  onValChange(event: any): void {
    this.defaultChecked = false;
    let _event = { index: event === 'rc' ? 0 : (event === 'limitada') ? 1 : (event === 'especial') ? 3 : (event === 'gold') ? 4 : 2 };
    this.btnClick((event === 'amplia') ? 1 : (event === 'limitada') ? 2 : (event === 'especial') ? 3 : (event === 'gold') ? 4 : 0);
    setTimeout(() => {
      this.changeSelected(_event);
      this.radioChange('anual');
      this.defaultChecked = true;
    }, 10);
  }

  recotizar(returnLocal: boolean): void {
    this.cotizoEvent.emit(returnLocal);
  }

  emitir(): void {
    // this._makeQuotations3Types();

    this._setCotizacionSaved();
    let _insurance = (this.optSelected.quotation.aseguradora.toUpperCase() === 'ELPOTOSI')?'EL_POTOSI':this.optSelected.quotation.aseguradora.toUpperCase();

    if (_insurance === GENERALS.particual_quote[0].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[2].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[3].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[5].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[7].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[8].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[11].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[12].insurance_carrier.split('_').join('').toUpperCase() ||
      _insurance === GENERALS.particual_quote[17].insurance_carrier.toUpperCase() ||
      _insurance === GENERALS.particual_quote[1].insurance_carrier.toUpperCase() ||
      _insurance === this.generlsParticualQuote[14].insurance_carrier.toUpperCase() ||
      _insurance === this.generlsParticualQuote[13].insurance_carrier.toUpperCase() ||
      _insurance === this.generlsParticualQuote[4].insurance_carrier.toUpperCase() ||
      _insurance === this.generlsParticualQuote[16].insurance_carrier.toUpperCase() ||
      _insurance === this.generlsParticualQuote[19].insurance_carrier.toUpperCase() ||
      _insurance === 'MIGO' || // migo
      _insurance === 'AFIRME'|| // afirme
      _insurance === 'AIG'
      // _insurance === 'GENERAL'  // afirme
      ) {
     // this.router.navigate(['admin/cotiza-go/particular/emision/']);
      if(localStorage.getItem('tokenAs') === 'mapfre'){
        this.router.navigate(['admin/cotiza-go/particular/emision/mapfre']);
      }else if(localStorage.getItem('tokenAs') === 'GNP'){
        this.router.navigate(['admin/cotiza-go/particular/emision/gnp']);
      }else if(localStorage.getItem('tokenAs') === 'Qualitas'){
        this.router.navigate(['admin/cotiza-go/particular/emision/qualitas']);
      }else if(localStorage.getItem('tokenAs') === 'Banorte'){
        this.router.navigate(['admin/cotiza-go/particular/emision/banorte']);
      }else if(localStorage.getItem('tokenAs') === 'Aba'){
        this.router.navigate(['admin/cotiza-go/particular/emision/aba']);
      }
      else if(localStorage.getItem('tokenAs') === 'Afirme'){
        this.router.navigate(['admin/cotiza-go/particular/emision/afirme']);
      }
      else if(localStorage.getItem('tokenAs') === 'AXA') {
        this.router.navigate(['admin/cotiza-go/particular/emision/axa']);
        console.log('estas en Axa')
      }
      // }else if(localStorage.getItem('tokenAs') === 'Sura'){
      //   this.router.navigate(['admin/cotiza-go/particular/emision/sura']);
      //   console.log('estas en Axa')
      // }
      else {
         this.router.navigate(['admin/cotiza-go/particular/emision/']);
      }
    }
    /* Caso en el que una aseguradora debera usar terminal virtual*/
    // if (//_insurance === this.generlsParticualQuote[1].insurance_carrier.toUpperCase() ||
    //   _insurance === this.generlsParticualQuote[16].insurance_carrier.toUpperCase() ) {
    //   this.router.navigate(['admin/cotiza-go/particular/emision/virtual/']);
    // }
  }

  private _setCotizacionSaved(): void {
    let _local = JSON.parse(localStorage.getItem('cotizacionSaved'));
    let _json, _coberturas, _cotizacion;
    if (this.displayValidate(this.insurance, 0)) {
      _coberturas = this.homologationParseResultService.getCoveragesList(this.optSelected.quotation, '');
      _cotizacion = this.homologationParseResultService.getQuotationList(this.optSelected.quotation, '');
      _json = {
        fechaActualizacion: _local.fechaActualizacion,
        fechaCreacion: _local.fechaCreacion,
        id: _local.id,
        idAgente: _local.idAgente,
        idSubRamo: _local.idSubRamo,
        peticion: _local.peticion,
        respuesta: JSON.stringify({
          aseguradora: this.optSelected.quotation.aseguradora,
          cliente: this.optSelected.quotation.cliente,
          codigoError: this.optSelected.quotation.codigoError,
          descuento: this.optSelected.quotation.descuento,
          paquete: this.optSelected.insurance,
          periodicidadDePago: this.optSelected.quotation.periodicidadDePago,
          urlRedireccion: this.optSelected.quotation.urlRedireccion,
          vehiculo: this.optSelected.quotation.vehiculo,
          coberturas: _coberturas,
          cotizacion: _cotizacion
        })
      }
    } else {
      if (this.insurance === this.generlsParticualQuote[0].insurance_carrier.toUpperCase()) {
        _coberturas = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.coberturas[0].rc : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.coberturas[1].limitada : this.optSelected.quotation.aoberturas[2].amplia;
        _cotizacion = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.cotizacion[0].rc[0].anual : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.cotizacion[1].limitada[0].anual : this.optSelected.quotation.cotizacion[2].amplia[0].anual;
      } else {
        _coberturas = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.coberturas[2].rc : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.coberturas[1].limitada : this.optSelected.quotation.coberturas[0].amplia;
        _cotizacion = (this.optSelected.insurance === 'rc') ? this.optSelected.quotation.cotizacion[2].rc[2].anual : (this.optSelected.insurance === 'limitada') ? this.optSelected.quotation.cotizacion[1].limitada[0].anual : this.optSelected.quotation.cotizacion[0].amplia[0].anual;
      }

      _json = {
        fechaActualizacion: _local.fechaActualizacion,
        fechaCreacion: _local.fechaCreacion,
        id: _local.id,
        idAgente: _local.idAgente,
        idSubRamo: _local.idSubRamo,
        peticion: _local.peticion,
        respuesta: JSON.stringify({
          aseguradora: this.optSelected.quotation.aseguradora,
          cliente: this.optSelected.quotation.cliente,
          codigoError: this.optSelected.quotation.codigoError,
          descuento: this.optSelected.quotation.descuento,
          paquete: this.optSelected.insurance,
          periodicidadDePago: this.optSelected.quotation.periodicidadDePago,
          urlRedireccion: this.optSelected.quotation.urlRedireccion,
          vehiculo: this.optSelected.quotation.vehiculo,
          coberturas: _coberturas,
          cotizacion: _cotizacion
        })
      }
    }
    localStorage.removeItem('cotizacionSaved');
    localStorage.setItem('cotizacionSaved', JSON.stringify(_json));
  }

  canEmmit(aseguradoraSelected: string): boolean {
    let _insurance = this.generlsParticualQuote.find(el => {
      el.insurance_carrier.toUpperCase() === aseguradoraSelected.toUpperCase() && el.disabled === false
    });

    return (_insurance) ? true : false;
  }

  changeSelected(event?: any): void {
   
    this.finalPrice = this.homologationParseResultService.getTotal(event, this.insurance.toUpperCase(), this.cotizacionResponse, 'amplia', 'anual');
    // if(this.insurance.toUpperCase() === "AIG"){
    //   let a = this.finalPrice.toString().split(",");
    //   this.finalPrice  = a[0]+a[1];
    // }
    this.quotationID = this.homologationParseResultService.getIDQuotation(event, this.insurance, this.cotizacionResponse, 'amplia', 'anual');
    this.optSelected = { insurance: this.homologationParseResultService.getOpt(event), quotation: this.cotizacionResponse };
    if(typeof event !== 'undefined') {
      if (typeof event.index !== 'undefined') {
        switch (event.index) {
          case 0:
            //cobertura RC
            localStorage.setItem('coverageSelected', 'RC')
            break;
          case 1:
            //cobertura LIMITADA
            localStorage.setItem('coverageSelected', 'LIMITADA')
            break;
          case 2:
            //cobertura AMPLIA
            localStorage.setItem('coverageSelected', 'AMPLIA')
            break;
          case 3:
            //cobertura ESPECIAL
            localStorage.setItem('coverageSelected', 'ESPECIAL')
            break;
          case 4:
            //cobertura ESPECIAL
            localStorage.setItem('coverageSelected', 'GOLD')
            break;
          default:
            //default cobertura AMPLIA
            localStorage.setItem('coverageSelected', 'AMPLIA')
            break

        }
      }
    }
  }

  displayValidate(insurance: string, type?: number): boolean {
    let _return;
    if (!type) {
      insurance = (insurance === 'general_De_Seguros') ? 'general' : (insurance === 'la_latino') ? 'latino' : (insurance === 'el_potosi') ? 'elpotosi' : insurance;
        let data = this.generlsDiscount.find(el => el.insurance.toUpperCase() === insurance.split('_').join('').toUpperCase() && el.active == true).active;
      _return = data;
    } else {
      _return = (insurance.toUpperCase() === this.generlsDiscount[type].insurance.toUpperCase()) ? true : false;
    }

    return _return;
  }

  radioChange(event?: any): void {
    this.typeRadio = event;
    let _total: any = 0;
    let _quotation = this.cotizacionResponse.cotizacion;
    let _coverages = [];
    this.insurance = (this.insurance === 'general_De_Seguros') ? 'general' : (this.insurance === 'la_latino') ? 'latino' : this.insurance;

    _total = _total = this.homologationParseResultService.getTotal({index: 5}, this.insurance.toUpperCase(), this.cotizacionResponse, this.optSelected.insurance, event);
    /*Funcion que se usa para eliminar las comas en los precios para AIG*/
    if(this.insurance.toUpperCase() === "AIG"){
      if(_total.toString().includes(",")){
        let a = _total.toString().split(",");
        _total  = a[0]+a[1];
      }
    }
    // console.log(_total);
    localStorage.setItem('totalP', _total.toString());
    sessionStorage.setItem('customQuotation', JSON.stringify({ radio: event, selected: this.optSelected.insurance }));

    this.finalPrice = _total;
    this.coverages = this.homologationParseResultService.getCoverages(this.optSelected.insurance, this.insurance.toUpperCase(), _quotation);
    this.coveragesAnual = this.homologationParseResultService.getCoverages('amplia', this.insurance.toUpperCase(), _quotation);
    /*Funcion que se usa para eliminar las comas en los precios para AIG*/
    if(this.insurance.toUpperCase() === "AIG"){
      this.coveragesAnual.map(x => {
        for (const key in x.values) {
            let element = x.values[key];
            element = element.toString().replace(',','');
            element = element.toString().replace('mxn','');
            element.trim();
            x.values[key] = element;
        }
      });
    }
    this.coveragesLimitada = this.homologationParseResultService.getCoverages('limitada', this.insurance.toUpperCase(), _quotation);
    /*Funcion que se usa para eliminar las comas en los precios para AIG*/
    if(this.insurance.toUpperCase() === "AIG"){
      this.coveragesLimitada.map(x => {
        for (const key in x.values) {
            let element = x.values[key];
            element = element.toString().replace(',','');
            element = element.toString().replace('mxn','');
            element.trim();
            x.values[key] = element;
        }
      });
    }
    if (this.insurance != 'Atlas'){
      this.coveragesRC = this.homologationParseResultService.getCoverages('rc', this.insurance.toUpperCase(), _quotation);
      /*Funcion que se usa para eliminar las comas en los precios para AIG*/
      if(this.insurance.toUpperCase() === "AIG"){
        this.coveragesRC.map(x => {
          for (const key in x.values) {
              let element = x.values[key];
              element = element.toString().replace(',','');
              element = element.toString().replace('mxn','');
              element.trim();
              x.values[key] = element;
          }
        });
      }
    }
    this.coveragesEspecial = this.homologationParseResultService.getCoverages('especial', this.insurance.toUpperCase(), _quotation);
    console.log('coverturas especial', this.coveragesEspecial);
    this.coveragesGold = this.homologationParseResultService.getCoverages('gold', this.insurance.toUpperCase(), _quotation);
    console.log('coverturas gold', this.coveragesGold);
    /*Funcion que se usa para eliminar las comas en los precios para AIG*/
    if(this.insurance.toUpperCase() === "AIG"){
      this.coveragesGold.map(x => {
        for (const key in x.values) {
            let element = x.values[key];
            element = element.toString().replace(',','');
            element = element.toString().replace('mxn','');
            element.trim();
            x.values[key] = element;
        }
      });
    }


    this.pays = this.homologationParseResultService.getPays(this.optSelected.insurance, this.insurance.toUpperCase(), _quotation, event);
    if(this.insurance.toUpperCase() === "AIG"){
        for (const key in this.pays[0]) {
            let element = this.pays[0][key];
            if(element.toString().includes(",")){
              element = element.toString().replace(',','');
            }
            this.pays[0][key] = element;
        }
    }
    if(typeof event !== 'undefined'){
      switch (event){
        case 'anual':
          localStorage.setItem('payFormSelected','ANUAL');
          break;
        case 'semestral':
          localStorage.setItem('payFormSelected','SEMESTRAL');
          break;
        case 'trimestral':
            localStorage.setItem('payFormSelected','TRIMESTRAL');
            break;
        case 'mensual':
          localStorage.setItem('payFormSelected','MENSUAL');
          break;
        default:
          localStorage.setItem('payFormSelected','ANUAL');
          break;
      }
    }

  }

  htmltoPDF(): void {
    html2canvas(document.querySelector("#quotationPDF")).then(
      canvas => {
        let pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
        let _coverage = this.cotizacion.controls['selectMarca'].value + '_' + this.cotizacion.controls['selectModelo'].value + '_' + this.cotizacion.controls['selectSubDescripcion'].value;

        let imgData = canvas.toDataURL("image/jpeg", 1.0);
        pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
        pdf.save('cotizacion_' + _coverage + '.pdf');
      }
    );
  }
}
