import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CotizacionResponseInterface } from 'src/app/_interfaces';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Component({
  selector: 'result-quote',
  templateUrl: './result-quote.component.html',
  styleUrls: ['./result-quote.component.css']
})
export class ResultQuoteComponent implements OnInit {
  @Input() aseguradoraSelected: string;
  @Input() cotizacion: FormGroup;
  @Input() precioFin: any;
  @Input() cotizacionResponse: CotizacionResponseInterface;
  @Input() messageError: string;
  @Output() cotizoEvent = new EventEmitter<boolean>();
  quotations = [];
  urlType: string = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    });
  }

  ngOnInit(): void {
    this._setValues();
    this.recotizar(true);
  }
  private _setValues(): void {
    if (this.urlType === 'particular') {

      for(let i = 0; i < this.cotizacionResponse.coberturas.length; i++){
        // console.log(typeof this.cotizacionResponse.coberturas[i],'hola')
        if(this.cotizacionResponse.coberturas[i].sumaAsegurada === '0'){
          this.cotizacionResponse.coberturas[i].sumaAsegurada = 'NO APLICA';
        }
        if(this.cotizacionResponse.coberturas[i].deducible === '0'){
          this.cotizacionResponse.coberturas[i].deducible = 'NO APLICA';
        }
        this.quotations.push( this.cotizacionResponse.coberturas[i]);
        // console.log(this.cotizacionResponse.coberturas[i])
      }
      // if (this.cotizacionResponse.Coberturas[0].AsitenciaCompleta && this.cotizacionResponse.Coberturas[0].AsitenciaCompleta.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].AsitenciaCompleta);
      // if (this.cotizacionResponse.Coberturas[0].Cristales && this.cotizacionResponse.Coberturas[0].Cristales.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].Cristales);
      // if (this.cotizacionResponse.Coberturas[0].DanosMateriales && this.cotizacionResponse.Coberturas[0].DanosMateriales.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DanosMateriales);
      // if (this.cotizacionResponse.Coberturas[0].DanosMaterialesPP && this.cotizacionResponse.Coberturas[0].DanosMaterialesPP.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DanosMaterialesPP);
      // if (this.cotizacionResponse.Coberturas[0].DefensaJuridica && this.cotizacionResponse.Coberturas[0].DefensaJuridica.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].DefensaJuridica);
      // if (this.cotizacionResponse.Coberturas[0].GastosMedicosEvento && this.cotizacionResponse.Coberturas[0].GastosMedicosEvento.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].GastosMedicosEvento);
      // if (this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes && this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].GastosMedicosOcupantes);
      // if (this.cotizacionResponse.Coberturas[0].MuerteAccidental && this.cotizacionResponse.Coberturas[0].MuerteAccidental.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].MuerteAccidental);
      // if (this.cotizacionResponse.Coberturas[0].RC && this.cotizacionResponse.Coberturas[0].RC.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RC);
      // if (this.cotizacionResponse.Coberturas[0].RCBienes && this.cotizacionResponse.Coberturas[0].RCBienes.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCBienes);
      // if (this.cotizacionResponse.Coberturas[0].RCExtension && this.cotizacionResponse.Coberturas[0].RCExtension.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCExtension);
      // if (this.cotizacionResponse.Coberturas[0].RCExtranjero && this.cotizacionResponse.Coberturas[0].RCExtranjero.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCExtranjero);
      // if (this.cotizacionResponse.Coberturas[0].RCFamiliar && this.cotizacionResponse.Coberturas[0].RCFamiliar.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCFamiliar);
      // if (this.cotizacionResponse.Coberturas[0].RCPExtra && this.cotizacionResponse.Coberturas[0].RCPExtra.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCPExtra);
      // if (this.cotizacionResponse.Coberturas[0].RCPersonas && this.cotizacionResponse.Coberturas[0].RCPersonas.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RCPersonas);
      // if (this.cotizacionResponse.Coberturas[0].RoboTotal && this.cotizacionResponse.Coberturas[0].RoboTotal.length > 2) this.quotations.push(this.cotizacionResponse.Coberturas[0].RoboTotal);
    } else {
      if(this.cotizacionResponse.coberturas){
        this.quotations = this.cotizacionResponse.coberturas;
      }
      if(this.cotizacionResponse.Coberturas){
        this.quotations = this.cotizacionResponse.Coberturas;
      }
    }
  }

  recotizar(returnLocal: boolean): void {
    this.cotizoEvent.emit(returnLocal);
  }

  emitir(aseguradora: any): void {
    // for(let i = 0; i < GENERALS.particual_quote.length ; i++){
    //   console.log(GENERALS.particual_quote[i],'GENERALS.particual_quote[0]')
    // }
    if (aseguradora.toUpperCase() === GENERALS.particual_quote[0].insurance_carrier.toUpperCase()) {
      this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/']);
    }
    // else if (aseguradora.toUpperCase() === GENERALS.particual_quote[1].insurance_carrier.toUpperCase()) {
    //   this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/virtual/']);
    // }
    else if (aseguradora.toUpperCase() === GENERALS.particual_quote[1].insurance_carrier.toUpperCase()) {
      this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/']);
    }
    else {
      this.router.navigate(['admin/cotiza-go/' + this.urlType + '/emision/aba']);
    }
  }

  validateButton(): boolean {
    if (this.aseguradoraSelected.toUpperCase() === 'ATLAS' || this.aseguradoraSelected.toUpperCase() === 'ANA') return true;
    let _insurance = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase());
    return (!_insurance.disabled) ? true : false;
  }
}
