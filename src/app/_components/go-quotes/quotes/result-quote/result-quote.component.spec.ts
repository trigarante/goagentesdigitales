import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultQuoteComponent } from './result-quote.component';

describe('ResultQuoteComponent', () => {
  let component: ResultQuoteComponent;
  let fixture: ComponentFixture<ResultQuoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultQuoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
