import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CatalogosInterface, CotizacionResponseInterface, CodPostalInterface, CotizacionRequestInterface } from 'src/app/_interfaces';
import {CatalogosService, CotizacionService, DatosServiceService, OnlyNumbersService, ToLowerCaseKeyService} from 'src/app/_services';
import jwt_decode from 'jwt-decode';
import Swal from 'sweetalert2';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { ActivatedRoute } from '@angular/router';
import {AseguradorasOnOffService} from "../../../_services/aseguradoras-on-off.service";

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.css']
})
export class QuotesComponent implements OnInit {
  aseguradoraSelected: string = '';
  submitted: boolean = false;
  cotizacionResponse: CotizacionResponseInterface = { Aseguradora: '' };
  cotizacionRequest: CotizacionRequestInterface = {};
  descuento: number = 0;
  precioGral: number = 0;
  precioDescuento: number = 0;
  precioFin: number = 0;
  dropdownList: CatalogosInterface[] = [];
  selected: any;
  edades = Array.from(Array(80).keys(), n => n + 17 + 1);
  fechaNacimientoL: string = '';
  modelos: CatalogosInterface[] = [];
  descripciones: CatalogosInterface[] = [];
  subdescripciones: CatalogosInterface[] = [];
  detalles: CatalogosInterface[] = [];
  selectedDetalles: any;
  colonia: string = '';
  colonias: CodPostalInterface[] = [];
  cotizo: boolean = false;
  // cotizacion: any; // FormGroup;
  hiddenData: boolean = false;
  emisionRequest: CotizacionResponseInterface = { Aseguradora: '' };
  idAgente: any;
  tokenData: string = '';
  isLoading: any; // : Promise<boolean>;
  messageError: string = '';
  loaderGo: boolean = false;
  typeSingleMulti: string = '';
  urlType: string = '';
  tmpQuotation: any[] = [];
  generlsDiscount = GENERALS.discount;
  bancsMSI;
  cotizacionUpperCase:any = { };
  descuentoAfirme:any;
  ciudad:string;
  descuentoAseguradora: any;
  dataSaveCot:{idAgente:number,peticion:string, respuesta:string};
  
  cotizacion = new FormGroup({
    selectMarca: new FormControl('', [Validators.required]),
    selectModelo: new FormControl('', [Validators.required]),
    selectDescripcion: new FormControl('', [Validators.required]),
    selectSubDescripcion: new FormControl('', [Validators.required]),
    //selectDetalle: new FormControl('', [Validators.required]),
    edad: new FormControl('', [Validators.required]),
    genero: new FormControl('', [Validators.required]),
    cp: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]),
  });

  constructor(
    private route: ActivatedRoute,
    private catalogosService: CatalogosService,
    private autosService: CatalogosService,
    private onlyNumber: OnlyNumbersService,
    private toLowerCaseKeyService: ToLowerCaseKeyService,
    private cotizacionService: CotizacionService,
    private servicesOnOff: AseguradorasOnOffService,
    private DatosServiceService:DatosServiceService
    
  ) {
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    });
  }

  ngOnInit(): void {
    let descuentoAseguradora = localStorage.getItem('tokenAs');
    // this.autosService.getDescuentosGenerales(descuentoAseguradora).subscribe((dataDescuento) => {
    //   this.descuentoAseguradora = dataDescuento;
    //   this.descuento = this.descuento === 0 ?
    //                         (dataDescuento.discount === 'Descuento no configurado para esa Empresa.')
    //                             ? 0 :  dataDescuento.discount : this.descuento;
    // })

    this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
      (descuentoAseguradora === 'la_latino') ? descuentoAseguradora= 'latino' : descuentoAseguradora;
      dataAseg.forEach((dataDescuento, asegu) => {
        if((descuentoAseguradora).toUpperCase() === dataDescuento.insuranceCompanyID)
        this.descuento = dataDescuento.discount
      })
    })

    //obtinen el nombre de la aseguradora almacenada en el  localStorage
    let _tokenAs = localStorage.getItem('tokenAs') || '';
    this.aseguradoraSelected = _tokenAs;
    let aseguradora = _tokenAs.toUpperCase();
    switch (aseguradora){
      case "LA_LATINO": aseguradora = 'LATINO'; break;
      case "GENERAL_DE_SEGUROS": aseguradora = 'GENERAL'; break;
      case "EL_POTOSI": aseguradora = 'ELPOTOSI'; break;
      case "EL_AGUILA": aseguradora = 'ELAGUILA'; break;
      // case "EL_AGUILA": aseguradora = 'ELAGUILA'; break;
      //case "PRIMERO": aseguradora = 'PRIMERO_SEGUROS'; break;
      default: aseguradora = _tokenAs.toUpperCase(); break;
    }
    //se obtiene los banco a msi con los que tiene promocion la aseguradora
    this.bancsMSI = this.generlsDiscount.find(el => el.insurance.toUpperCase() === aseguradora).bancs;
    // se ejecuta la funcion que extrae las marcas
    this._getMarcas(this.aseguradoraSelected.toLowerCase());
  }
  //funcion que hace la peticion al servicio para extraer el catalogo de marcas
  private _getMarcas(aseguradora: any): void {
    this.modelos = [];
    this.descripciones = [];
    this.subdescripciones = [];
    this.selectedDetalles = [];
    this.cotizacion.controls['selectModelo'].setValue('');
    this.cotizacion.controls['selectDescripcion'].setValue('');
    this.cotizacion.controls['selectSubDescripcion'].setValue('');

    this.autosService.getMarcas(aseguradora === 'primero'? 'primero_seguros':aseguradora.toString().toLowerCase(), this.urlType).subscribe(
      resp => {
        this.dropdownList = resp;
      },
      err => {
        console.error('ERROR: ', err);
      }
    );
  }

  //funcion que hace la peticion al servicio para extraer el catalogo de modelos
  getModelos(): void {
    this.descripciones = [];
    this.subdescripciones = [];
    this.selectedDetalles = [];
    this.cotizacion.controls['selectModelo'].setValue('');
    this.cotizacion.controls['selectDescripcion'].setValue('');
    this.cotizacion.controls['selectSubDescripcion'].setValue('');
    this.autosService.getModelos(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.urlType).subscribe(
      resp => {
        this.modelos = resp;
      },
      (err: HttpErrorResponse) => {
        if (err.status === 401) {
          console.error(err.status);
          console.log('Sesion Caducada');
        }
      });
  }

  //funcion que hace la peticion al servicio para extraer el catalogo de colonias solo para afirma
  getColonias(): void {
    if(this.aseguradoraSelected === 'Afirme' && this.cotizacion.controls['cp'].value.length === 5){
      this.autosService.getColonias(this.cotizacion.controls['cp'].value).subscribe(
          data => {
          
            this.colonias = data;
            this.ciudad=this.colonias[0].ciudad;
            // this.getDescuentoAfirme(this.ciudad);
          },
          err => {
            console.error('ERROR: ', err);
          }
      );

    }

  }

  //se obtiene el descuento para la aseguradora de afirme
  getDescuentoAfirme(ciudad:string){
    this.autosService.getDescuentoAfirme(ciudad).subscribe(
        data => {
          this.descuentoAfirme = data;
          this.descuento = this.descuentoAfirme.descuento;
          //manda el valor del descuento al localStorage
          localStorage.setItem('descAfi', this.descuento.toString());
        },
        err => {
          console.error('ERROR: ', err);
        }
    );
  }

  get f() { 
    return this.cotizacion.controls; }

  onSubmit() {
    
    // this.getColonias();
    this.submitted = true;
    this.loaderGo = true;
    if (this.cotizacion.invalid) {
      this.loaderGo = false;
      return; //lo quite para no llenar el form y revisar el form
    }
    //envia el genero al localStorage
    localStorage.setItem('_t', (this.cotizacion.controls['genero'].value === 'MASCULINO') ? 'H' : 'M');
    localStorage.setItem('cpAfirme', this.cotizacion.controls.cp.value);
    // let _insurance = this.aseguradoraSelected.split('_').join('')===  "generalDeSeguros" ? 'GENERAL':this.aseguradoraSelected === 'la_latino' ? 'LATINO':this.aseguradoraSelected.split('_').join('');
    // obtiene el valor del descuento por aseguradora
    // this.descuento = this.descuento === 0 ? GENERALS.discount.find(el => el.insurance.toUpperCase() === _insurance.toUpperCase()).discount : this.descuento;
    let _typeQuote = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase()).disabled;
    //se genera el año de nacimiento
    if(localStorage.getItem('tokenAs') === 'GNP'){
      const fechaN = new Date().getFullYear() - this.cotizacion.controls['edad'].value;
      this.fechaNacimientoL = '01/01/' + fechaN;
    }else if(localStorage.getItem('tokenAs').toUpperCase() === 'AIG'){
      const fechaN = new Date().getFullYear() - this.cotizacion.controls['edad'].value;
      this.fechaNacimientoL = fechaN + '-01-01';
    }else if(localStorage.getItem('tokenAs') !== 'GNP'){
      const fechaN = new Date().getFullYear() - this.cotizacion.controls['edad'].value;
      this.fechaNacimientoL = '01-01-' + fechaN;
    }
    let _typePack = '';
    //Valida que tipo de url y asigana el tipo de paquete
    if (this.urlType === 'privado') {
      _typePack = 'ALL';
      // _typePack = 'AMPLIA';
    } else {
      _typePack = (_typeQuote) ? 'AMPLIA' : (this.aseguradoraSelected.toUpperCase() === 'QUALITAS' || this.aseguradoraSelected.toUpperCase() === 'GNP' || this.aseguradoraSelected.toUpperCase() === 'ZURICH' || this.aseguradoraSelected.toUpperCase() === 'AXA' || this.aseguradoraSelected.toUpperCase() === 'MIGO') ? 'ALL' : 'ALL'; // eldd change to mayus when back change
    }

    // Se genera el json de la cotizacion
    if(localStorage.getItem('tokenAs') !== 'Aba'){
      this.cotizacionRequest = {
        aseguradora:this.aseguradoraSelected.toUpperCase(),
        clave: this.selectedDetalles,
        cp: this.cotizacion.controls['cp'].value,
        descripcion: this.cotizacion.controls['selectDescripcion'].value,
        detalle: this.cotizacion.controls['selectSubDescripcion'].value,
        descuento: this.descuento,
        edad: this.cotizacion.controls['edad'].value.toString(),
        fechaNacimiento: this.fechaNacimientoL,
        genero: this.cotizacion.controls['genero'].value,
        marca: this.cotizacion.controls['selectMarca'].value,
        modelo: this.cotizacion.controls['selectModelo'].value,
        movimiento: 'cotizacion',
        paquete: _typePack,
        servicio: 'PARTICULAR',
      }
    }else{
      this.cotizacionRequest = {
        aseguradora: this.aseguradoraSelected.toUpperCase(),
        clave: this.selectedDetalles,
        cp: this.cotizacion.controls['cp'].value,
        descripcion: this.cotizacion.controls['selectDescripcion'].value,
        descuento: this.descuento,
        detalle: this.cotizacion.controls['selectSubDescripcion'].value,
        edad: this.cotizacion.controls['edad'].value.toString(),
        // fechaNacimiento: this.fechaNacimientoL,
        fechaNacimiento: this.fechaNacimientoL,
        genero: this.cotizacion.controls['genero'].value,
        marca: this.cotizacion.controls['selectMarca'].value,
        modelo: this.cotizacion.controls['selectModelo'].value,
        movimiento: 'cotizacion',
        paquete: _typePack,
        servicio: 'PARTICULAR',
      }
    }
    //
    const datosDelFormulario = JSON.stringify(this.cotizacionRequest);
    localStorage.setItem('datosFormularioCot',datosDelFormulario)
    
    this.cotizacionRequest.descuento = this.descuento;
    if (this.aseguradoraSelected.toUpperCase() === 'MIGO') {
      this.cotizacionRequest.submarca = this.cotizacion.controls['selectSubDescripcion'].value;
    }
    //se obtiene el token del localstorage
    let _token = localStorage.getItem('token') || '';
    this.tokenData = _token;
    //Se descodifica el token
    this.idAgente = jwt_decode(this.tokenData);
    //Se genera el json de dataSaveCot
    //TODO
    const dataSaveCot = {
      idAgente: +this.idAgente['sub'].split(':')[1],
      peticion: JSON.stringify(this.cotizacionRequest),
      respuesta: "[]"
    };
    if (!_typeQuote) {
      //convierte a string el json y lo manda al localStorage
      sessionStorage.setItem('baseQuotation', JSON.stringify(this.cotizacionRequest));
      if (this.urlType === 'particular') {
    
        //se ejecuta la funcion y se manda como argumento el json dataSaveCot
          this._quotations3Ways(dataSaveCot, true);
      }
      else {
    
        this.autosService.cotizar(this.cotizacionRequest, this.urlType).subscribe(
          resp => {
            console.log('resp--------->',resp);

            
           
            dataSaveCot.respuesta = JSON.stringify(resp)
            //TODO
            console.log('la resp de qatesion',resp);
            this.isLoading = false;
            this.loaderGo = false;

            if(resp.coberturas){
              if (resp.coberturas.length > 0) {
                  
                  localStorage.setItem('tmpQuotation', JSON.stringify(resp))
                  this._saveQuote(dataSaveCot);
                  this._validateResult(resp);
              } else {
                  this.loaderGo = false;
                  if (resp.CodigoError) {
                    Swal.fire({
                      title: resp.CodigoError,
                      allowOutsideClick: false,
                      allowEscapeKey: false,
                      showConfirmButton: true
                    });
                  }
              }
            }

            if(resp.Coberturas){
              if (resp.Coberturas.length > 0) {
                localStorage.setItem('tmpQuotation', JSON.stringify(resp))
                this._saveQuote(dataSaveCot);
                this._validateResult(resp);
              } else {
                this.loaderGo = false;
                if (resp.CodigoError) {
                  Swal.fire({
                    title: resp.CodigoError,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showConfirmButton: true
                  });
                }
              }
            }


          },
          error => {
            console.log('18.2.1 error');
            Swal.fire({
              title: 'Error',
              text: 'Hubo un error, favor de volver a intentar la cotización.',
              icon: 'error',
            });
            this.isLoading = false;
            this.loaderGo = false;
            console.error(`Error al cotizar ${error}`);
          }
        );
      }
    }
    else {
    
      this.autosService.cotizar(this.cotizacionRequest, this.urlType).subscribe(
        resp => {

          
          this.loaderGo = false;
          if (resp.coberturas.length > 0) {
            localStorage.setItem('tmpQuotation', JSON.stringify(resp))
            this._saveQuote(dataSaveCot);
            this._validateResult(resp);
          } else {
            this.loaderGo = false;
            if (resp.CodigoError) {
              Swal.fire({
                title: resp.CodigoError,
                allowOutsideClick: false,
                allowEscapeKey: false,
                showConfirmButton: true
              });
            }
          }
        },
        error => {
          console.log('18.2.1 error');
          Swal.fire({
            title: 'Error',
            text: 'Hubo un error, favor de volver a intentar la cotización.',
            icon: 'error',
          });
          this.isLoading = false;
          this.loaderGo = false;
          console.error(`Error al cotizar ${error}`);
        }
      );
    }

  }

  private _quotations3Ways(dataSaveCot: any, type: boolean): void {
  
    this.autosService.cotizar3Ways(this.cotizacionRequest).subscribe(
      succ => {
        dataSaveCot.respuesta = JSON.stringify(succ)
        
        // if (this.aseguradoraSelected.toUpperCase()=== 'AXA' || this.aseguradoraSelected.toUpperCase()=== 'ZURICH'){
        if (succ.Aseguradora){
         let cotizacionMayus = JSON.stringify(succ);
          succ = this.parseToLower(cotizacionMayus);
        }

        if(dataSaveCot.peticion){
          let peticion = JSON.parse(dataSaveCot.peticion);
         // if(peticion.aseguradora.toString().toUpperCase() === 'MIGO'){ succ.aseguradora = peticion.aseguradora.toString().toUpperCase(); }
        }
        if (succ.codigoError) {
          this.isLoading = false;
          this.loaderGo = false;
          //TODO
          Swal.fire({
            title: 'Error',
            text: 'Hubo un error, favor de volver a intentar la cotización.',
            icon: 'error',
          });
        } else {
          console.log('18.1');
          this.tmpQuotation.push(succ);
          if (type)
            localStorage.setItem('lastCotizacion', JSON.stringify(succ));
            this._makeQuotations3Types(this.cotizacionRequest, dataSaveCot, succ);
        }

      },
      err => {
        Swal.fire({
          title: 'Error',
          text: 'Hubo un error, favor de volver a intentar la cotización.',
          icon: 'error',
        });
        this.isLoading = false;
        this.loaderGo = false;
        console.error(`Error al cotizar ${err}`);
        console.log('ERROR 18.1.1');
      }
    );
  }
  // Funcion para convertir de mayusculas a minusculas las iniciales del json
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
          console.log("prop: ", prop);
        }
      }
    }
    return json;
  }

  nodoCotizacion:any;
  nodoCoberturas:any;

  private _makeQuotations3Types(request: any, dataSaveCot: any, _3ways): void {
    request.paquete = 'AMPLIA';
    let dataCotizacionAmplia:any = [];
    if(this.tmpQuotation[0]){
       dataCotizacionAmplia = this.tmpQuotation[0];
       if(dataCotizacionAmplia.cotizacion && dataCotizacionAmplia.coberturas){
        let aseg = localStorage.getItem("tokenAs").toUpperCase();
        if( aseg === "GNP" && dataCotizacionAmplia.cotizacion[0].especial || aseg === "AIG" ){
          this.nodoCotizacion = dataCotizacionAmplia.cotizacion[1].amplia[0].anual;
          this.nodoCoberturas = dataCotizacionAmplia.coberturas[1].amplia;
        }else{
          this.nodoCotizacion = dataCotizacionAmplia.cotizacion[0].amplia[0].anual;
          this.nodoCoberturas = dataCotizacionAmplia.coberturas[0].amplia;
        }

           dataCotizacionAmplia.cotizacion = this.nodoCotizacion;
           dataCotizacionAmplia.coberturas = this.nodoCoberturas;
           dataCotizacionAmplia.paquete = 'AMPLIA';
           this.tmpQuotation.push(JSON.parse(localStorage.getItem('lastCotizacion')));
           _3ways = JSON.parse(localStorage.getItem('lastCotizacion'));
       }
    }
    if(dataCotizacionAmplia.aseguradora){
      localStorage.setItem('tmpQuotation', JSON.stringify(dataCotizacionAmplia))

      this._saveQuote(dataSaveCot);
      this._validateResult(_3ways);
    }
  }

  private _validateResult(result: CotizacionResponseInterface): void {
    let _token = localStorage.getItem('token') || '';
    this.cotizo = true;
    /**
     * Funcion para validar que traiga precio
     */
    let _insurance = localStorage.getItem('tokenAs');
    this.cotizacionResponse = result;
    this.emisionRequest = result;
    this.typeSingleMulti = (GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase()).disabled) ? 'single' : (this.urlType === 'privado') ? 'single' : 'multi';
    localStorage.setItem('quotationType',this.typeSingleMulti);
    this.tokenData = _token;
    this.idAgente = jwt_decode(this.tokenData);
    this.loaderGo = false;

    if (this.cotizacionResponse && (this.cotizacionResponse.cotizacion || this.cotizacionResponse.Cotizacion)) {
      // && this.cotizacionResponse.Cotizacion.PrimaTotal && Number(this.cotizacionResponse.Cotizacion.PrimaTotal) > 500) {
      localStorage.setItem('lastCotizacion', JSON.stringify(result));
      this.messageError = '';
      this.isLoading = true; // Promise.resolve(true);
      if (this.typeSingleMulti === 'single') {
        // this._limpiarCoberturas();
        this.precioFin = Number((this.cotizacionResponse.cotizacion) ? this.cotizacionResponse.cotizacion.primaTotal : this.cotizacionResponse.Cotizacion.PrimaTotal);
        this.precioDescuento = (Number((this.cotizacionResponse.cotizacion) ? this.cotizacionResponse.cotizacion.primaTotal : this.cotizacionResponse.Cotizacion.PrimaTotal) * this.descuento) / 100;
        this.precioGral = this.precioFin * 1.30;//es estático para todas en lo que se desarrolla un servicio.
      }
    } else {
      if (this.cotizacionResponse.codigoError === undefined) {
        this.messageError = 'Error al cotizar';
      } else {
        this.messageError = this.cotizacionResponse.codigoError;
      }
    }
  }

  private _saveQuote(dataSaveCot: any): void {

    
    this.catalogosService.guardarCotizacion(dataSaveCot).subscribe(
      respuesta => {
        localStorage.setItem('cotizacionSaved', JSON.stringify(respuesta));
        this._saveNew(respuesta);
      },
      err => {
        console.error(err);
      }
    );
  }

  private _saveNew(respuesta: any): void {
    
    let _petition = this._makeJSONSave(respuesta.peticion);
    this.catalogosService.guardarNewFormat(respuesta.id, _petition).subscribe(
      respuesta => {
        localStorage.setItem('cotizacionSavedNew', JSON.stringify(respuesta));
      },
      err => {
        console.error(err);
      }
    );
  }

  private _limpiarCoberturas(): void {
    if (this.cotizacionResponse !== undefined) {
      if (this.cotizacionResponse.coberturas) {
        for (const c in this.cotizacionResponse.coberturas[0]) {
          if (this.cotizacionResponse.coberturas[0][c] === '-') {
            delete this.cotizacionResponse.coberturas[0][c];
          }
        }
      }
    }

    let _coverage = (localStorage.getItem('tokenAs').toUpperCase() === GENERALS.discount[0].insurance.toUpperCase()) ? this.cotizacionResponse.Coberturas[0] : this.cotizacionResponse.coberturas;
    this.cotizacionResponse.coberturas = Object.values(_coverage);
  }

  getDetalles(): void {
    this.detalles = this.subdescripciones;
    const detallesDisp = this.detalles.length - 1;
    for (let i = 0; i <= detallesDisp; i++) {
      if (this.cotizacion.controls['selectSubDescripcion'].value === this.detalles[i].text) {
        this.selectedDetalles = this.detalles[i].id;
      }
    }
  }

  getDescripciones() {
    this.subdescripciones = [];
    this.selectedDetalles = [];

    this.cotizacion.controls['selectDescripcion'].setValue('');
    this.cotizacion.controls['selectSubDescripcion'].setValue('');
    
    this.autosService.getDescripciones(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.cotizacion.controls['selectModelo'].value, this.urlType).subscribe(
      resp => {
        this.descripciones = resp;
      },
      err => {
        console.error(err);
      }
    );
  }

  getSubdescripciones() {

    
    this.cotizacion.controls['selectSubDescripcion'].setValue('');
    this.autosService.getSubdescripciones(this.aseguradoraSelected.toLowerCase() === 'primero'?'primero_seguros':this.aseguradoraSelected.toLowerCase(), this.cotizacion.controls['selectMarca'].value, this.cotizacion.controls['selectModelo'].value, this.cotizacion.controls['selectDescripcion'].value, this.urlType).subscribe(
      resp => {
        this.subdescripciones = resp;
      },
      err => {
        console.error(err);
      }
    );
  }

  recotizar(setValue: boolean): void {
    this.cotizo = setValue;
    this.tmpQuotation.pop();
    
    if(!this.cotizo){
      this.submitted = false;
      this.cotizacion = new FormGroup({
        selectMarca: new FormControl('', [Validators.required]),
        selectModelo: new FormControl('', [Validators.required]),
        selectDescripcion: new FormControl('', [Validators.required]),
        selectSubDescripcion: new FormControl('', [Validators.required]),
        //selectDetalle: new FormControl('', [Validators.required]),
        edad: new FormControl('', [Validators.required]),
        genero: new FormControl('', [Validators.required]),
        cp: new FormControl('', [Validators.required, Validators.maxLength(5), Validators.minLength(4)]),
      });
      console.log(this.cotizacion);
    };
    
  }

  private _makeJSONSave(petition: any): object {
    let _saved = JSON.parse(localStorage.getItem('tmpQuotation'));
    return _saved;
  }

  numberOnly(event: any): boolean {
    return this.onlyNumber.validateInput(event);
  }
}
