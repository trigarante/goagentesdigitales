import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoQuotesComponent } from './go-quotes.component';

describe('GoQuotesComponent', () => {
  let component: GoQuotesComponent;
  let fixture: ComponentFixture<GoQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
