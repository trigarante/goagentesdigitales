import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TerminalResonseInterface } from 'src/app/_interfaces';
import { CotizacionService } from 'src/app/_services';
import * as moment from 'moment';
import { __awaiter } from 'tslib';

@Component({
  selector: 'app-response-terminal',
  templateUrl: './response-terminal.component.html',
  styleUrls: ['./response-terminal.component.css']
})
export class ResponseTerminalComponent implements OnInit {
  cianne: string = '';
  message: string = '';
  polizaURL: string = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cotizacionServices: CotizacionService
  ) {
    this.cianne = this.activatedRoute.snapshot.params.responseTerminal;
    console.log('responseTerminal', this.cianne);
    this.activatedRoute.queryParams.subscribe((params: TerminalResonseInterface) => {
      console.log(params);

      if(localStorage.getItem('tokenAs').toUpperCase() === 'ABA') {
        // status 9
        this._sendTerminalResonse(params);
      }
      if (Number(params.Res) !== 2) {
        this._sendTerminalResonse(params);
      } else {
        this.message = 'No se realizo el cobro.';
      }
    });
  }

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      console.log(val instanceof NavigationEnd)
    });
  }

  private _sendTerminalResonse(params: TerminalResonseInterface): void {
    let _body = this._makeJSON(params);

    this.cotizacionServices.savePago(_body).subscribe(
      succ => {
        this.message = 'El cobro fue exitoso.';
        this._getFile(succ);
      },
      err => {
        console.error(err);
      }
    );
  }

  private _makeJSON(params: TerminalResonseInterface): any {
    let _tokenAs = localStorage.getItem('tokenAs');
    let _storage = JSON.parse(localStorage.getItem('lastEmision'));
    let _storage2 = JSON.parse(JSON.parse(localStorage.getItem('cotizacionSaved')).petition);
    let _quotation = JSON.parse(localStorage.getItem('lastEmision'));
    let _birthdate = moment(_quotation.Cliente.FechaNacimiento).format('YYYY-MM-DD');
    let _data = {
      "cp": _storage2.cp,
      "edad": _storage2.edad,
      "clave": _storage2.clave,
      "marca": _storage2.marca,
      "genero": _storage2.genero,
      "modelo": _storage2.modelo,
      "paquete": "AMPLIA",
      "servicio": _storage2.servicio,
      "descuento": _storage2.descuento,
      "movimiento": _storage2.movimiento,
      "descripcion": _storage2.descripcion,
      "numeroMotor": _storage.Vehiculo.NoMotor,
      "numeroSerie": _storage.Vehiculo.NoSerie,
      "numeroPlacas": _storage.Vehiculo.NoPlacas,
      "fechaNacimiento": moment(_storage.Cliente.FechaNacimiento).format('YYYY-MM-DD')
    };
    let _plusYear = moment().add(1, 'years').format('YYYY-MM-DD');
    return {
      "aseguradora": _tokenAs,
      "clienteModel": {
        "idPais": 1,
        "razonSocial": 1,
        "nombre": _quotation.Cliente.Nombre,
        "paterno": _quotation.Cliente.ApellidoPat,
        "materno": _quotation.Cliente.ApellidoMat,
        "cp": _quotation.Cliente.Direccion.CodPostal,
        "calle": _quotation.Cliente.Direccion.Calle,
        "numInt": _quotation.Cliente.Direccion.NoInt,
        "numExt": _quotation.Cliente.Direccion.NoExt,
        "idColonia": 0,
        "genero": (_quotation.Cliente.Genero === 'MASCULINO') ? 'M' : 'F',
        "telefonoFijo": "",
        "telefonoMovil": _quotation.Cliente.Telefono,
        "correo": _quotation.Cliente.Email,
        "fechaNacimiento": _birthdate,
        "curp": _quotation.Cliente.CURP,
        "rfc": _quotation.Cliente.RFC,
        "archivo": "",
        "archivoSubido": 0
      },
      "productoClienteModel": {
        "idSolictud": "1474386",
        "idSubRamo": 1,
        "datos": JSON.stringify(_data)
      },
      "registroModel": {
        "idEmpleado": 0,
        "idTipoPago": 1,
        "idEstadoPoliza": 5,
        "idProductoSocio": 0,
        "idFlujoPoliza": 10,
        "poliza": params.Pol,
        "fechaInicio": moment().format('YYYY-MM-DD'),
        "primaNeta": _storage.Emision.PrimaNeta,
        "archivo": "https://www.eot.gnp.com.mx//urlFilenetWeb/getDocument?id=pQwkgTHnlZafTMOdYwsAULisvtj0tSqse5expyu6gOcizSC7T/7ERAy4yj55irso/BF8l7cY+O2Mi/SVeRjnqg==",
        "fechaFin": _plusYear
      },
      "recibosModel": {
        "idEmpleado": 0,
        "idEstadoRecibos": 4,
        "fechaVigencia": _plusYear,
        "fechaLiquidacion": null,
        "fechaPromesaPago": null,
        "numero": 1,
        "activo": 1,
        "cantidad": _storage.Emision.PrimaNeta
      },
      "pagos": {
        "idFormaPago": 2,
        "idEstadoPago": 1,
        "idEmpleado": 0,
        "fechaPago": moment().format('YYYY-MM-DD'),
        "cantidad": _storage.Emision.PrimaNeta,
        "archivo": "https://www.eot.gnp.com.mx//urlFilenetWeb/getDocument?id=pQwkgTHnlZafTMOdYwsAULisvtj0tSqse5expyu6gOcizSC7T/7ERAy4yj55irso/BF8l7cY+O2Mi/SVeRjnqg==",
        "datos": "{\"MedioPago\":\"DEBITO\",\"NombreTarjeta\":\"\",\"Banco\":\"\",\"NoTarjeta\":\"\",\"MesExp\":\"\",\"AnioExp\":\"\",\"CodigoSeguridad\":\"\",\"NoClabe\":\"\",\"Carrier\":0,\"MSI\":\"\"}"
      },
      "accessToken": localStorage.getItem('token')
    }
  }

  private _getFile(succ: any): void {
    this.cotizacionServices.getPoliza(succ.poliza).subscribe(
      succ => {
        this.polizaURL = succ;
      },
      err => {
        console.error(err);
      }
    );
  }

  recotizar(type: boolean): void {
    this.router.navigate(['admin/cotiza-go/particular/']);
  }
}
