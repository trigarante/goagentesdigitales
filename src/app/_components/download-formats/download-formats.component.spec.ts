import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadFormatsComponent } from './download-formats.component';

describe('DownloadFormatsComponent', () => {
  let component: DownloadFormatsComponent;
  let fixture: ComponentFixture<DownloadFormatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadFormatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadFormatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
