import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';

import { DownloadFormatsComponent } from './download-formats.component';
import { GoComponent } from './go/go.component';
import { InsuranceCarrierComponent } from './insurance-carrier/insurance-carrier.component';

const routes: Routes = [
  { path: '', component: DownloadFormatsComponent, children: [
    { path: 'aseguradoras', component: InsuranceCarrierComponent, canActivate: [AuthGuardGuard] },
    { path: 'go', component: GoComponent, canActivate: [AuthGuardGuard] },

    { path: ':any', redirectTo: '/admin/aseguradoras', pathMatch: 'full' },
    { path: '', redirectTo: '/admin/aseguradoras', pathMatch: 'full' }
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DownloadFormatsRoutingModule { }
