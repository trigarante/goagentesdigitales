import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DownloadFormatsRoutingModule } from './download-formats-routing.module';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';

import { DownloadFormatsComponent } from './download-formats.component';
import { InsuranceCarrierComponent } from './insurance-carrier/insurance-carrier.component';
import { GoComponent } from './go/go.component';

@NgModule({
  declarations: [
    DownloadFormatsComponent,
    InsuranceCarrierComponent,
    GoComponent
  ],
  imports: [
    CommonModule,
    DownloadFormatsRoutingModule,

    CommonLocalModule
  ]
})
export class DownloadFormatsModule { }
