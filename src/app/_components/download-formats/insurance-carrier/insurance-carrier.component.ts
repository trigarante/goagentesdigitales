import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-insurance-carrier',
  templateUrl: './insurance-carrier.component.html',
  styleUrls: ['./insurance-carrier.component.css']
})
export class InsuranceCarrierComponent implements OnInit {
  datos = ['ABA', 'AFIRME', 'AIG','ANA', 'ATLAS', 'AXA','BANORTE', 'BXMAS', 'EL AGUILA','GENERAL DE SEGUROS', 'GNP', 'HDI','INBURSA', 'LA LATINO', 'MAPFRE','PRIMERO SEGUROS', 'QUALITAS', 'SURA','ZURICH'];
  //
  filtroArchivos=[];
  abaSelected: boolean = false;
  afirmeSelected: boolean = false;
  aigSelected: boolean = false;
  anaSelected: boolean = false;
  atlasSelected: boolean = false;
  axaSelected: boolean = false;
  banorteSelected: boolean = false;
  bxmasSelected: boolean = false;
  elaguilaSelected: boolean = false;
  gdsSelected: boolean = false;
  gnpSelected: boolean = false;
  hdiSelected: boolean = false;
  inbursaSelected: boolean = false;
  lalatinoSelected: boolean = false;
  mapfreSelected: boolean = false;
  primerosegurosSelected: boolean = false;
  qualitasSelected: boolean = false;
  suraSelected: boolean = false;
  zurichSelected: boolean = false;
  //
  aseguradora = new FormGroup({
    aseguradoraSelect: new FormControl('', [Validators.required]),
  });
    public formatos: any;


  constructor(private capacitacion:CapacitacionService) { }

  ngOnInit(): void {
      this.getFormatosyCondiciones();
  }
  getFormatosyCondiciones(){
      this.capacitacion.getFormatosYCondiciones().subscribe(resp=>{
         this.formatos= resp;
         console.log(resp);
      })
  }


  optionSelected() {
      this.filtroArchivos=[];
      let aseguradora=this.aseguradora.controls['aseguradoraSelect'].value.toLowerCase();
      this.formatos.forEach((el)=>{
          if( el.nombre.includes(aseguradora )===true){
              this.filtroArchivos.push(el);
          }

      });
    switch(this.aseguradora.controls['aseguradoraSelect'].value) {
      case 'ABA': 
                this.abaSelected = true;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'AFIRME': 
                this.abaSelected = false;
                this.afirmeSelected = true;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'AIG': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = true;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'ANA': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = true;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'ATLAS': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = true;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'AXA': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = true;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'BANORTE': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = true;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'BXMAS': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = true;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'EL AGUILA': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = true;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'GENERAL DE SEGUROS': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = true;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'GNP': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = true;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'HDI': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = true;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'INBURSA': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = true;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'LA LATINO': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = true;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'MAPFRE': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = true;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'PRIMERO SEGUROS': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = true;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'QUALITAS': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = true;
                this.suraSelected = false;
                this.zurichSelected = false;
                break;
      case 'SURA': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = true;
                this.zurichSelected = false;
                break;
      case 'ZURICH': 
                this.abaSelected = false;
                this.afirmeSelected = false;
                this.aigSelected = false;
                this.anaSelected = false;
                this.atlasSelected = false;
                this.axaSelected = false;
                this.banorteSelected = false;
                this.bxmasSelected = false;
                this.elaguilaSelected = false;
                this.gdsSelected = false;
                this.gnpSelected = false;
                this.hdiSelected = false;
                this.inbursaSelected = false;
                this.lalatinoSelected = false;
                this.mapfreSelected = false;
                this.primerosegurosSelected = false;
                this.qualitasSelected = false;
                this.suraSelected = false;
                this.zurichSelected = true;
                break;
    }
  }
}
