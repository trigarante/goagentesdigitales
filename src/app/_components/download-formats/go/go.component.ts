import { Component, OnInit } from '@angular/core';
import {CapacitacionService} from "../../../_services/capacitacion.service";

@Component({
  selector: 'app-go',
  templateUrl: './go.component.html',
  styleUrls: ['./go.component.css']
})
export class GoComponent implements OnInit {
    public imgMaterialRed: any;
    public datosAmi: any=[];
    public descuentosMes: any=[];
    public go: any=[];
    public usoPortal: any=[];

  constructor(private capacitacion:CapacitacionService) { }

  ngOnInit(): void {
    this.getMaterialRedes();
  }
getMaterialRedes(){
    this.capacitacion.getMaterialesRedes().subscribe(resp=>{
        this.imgMaterialRed = resp;
        //Filtra las imagenes por tipo de etiqueta
        for (let i = 0 ; i<this.imgMaterialRed.length; i++){
            if( this.imgMaterialRed[i].etiqueta.includes('datos-amis' )===true){
                this.datosAmi.push(this.imgMaterialRed[i]);
            }else if (this.imgMaterialRed[i].etiqueta.includes('descuentos-del-mes' )===true){
                this.descuentosMes.push(this.imgMaterialRed[i]);
            }else if (this.imgMaterialRed[i].etiqueta.includes('go' )===true){
                this.go.push(this.imgMaterialRed[i]);
            }else if (this.imgMaterialRed[i].etiqueta.includes('uso-del-portal' )===true){
                this.usoPortal.push(this.imgMaterialRed[i]);
            }
        }
    });

}

}
