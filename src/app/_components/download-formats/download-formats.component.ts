import { Component, OnInit } from '@angular/core';
import { MenuToggleService } from 'src/app/_services';

@Component({
  selector: 'app-download-formats',
  templateUrl: './download-formats.component.html',
  styleUrls: ['./download-formats.component.css']
})
export class DownloadFormatsComponent implements OnInit {
toggleMenu: boolean = false;
  constructor(  private menuToggleService: MenuToggleService
  ) { }

  ngOnInit(): void {
  this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {this.toggleMenu = item;});
  }
}
