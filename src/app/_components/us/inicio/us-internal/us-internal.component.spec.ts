import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsInternalComponent } from './us-internal.component';

describe('UsInternalComponent', () => {
  let component: UsInternalComponent;
  let fixture: ComponentFixture<UsInternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsInternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
