import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MisionInternalComponent } from './mision-internal.component';

describe('MisionInternalComponent', () => {
  let component: MisionInternalComponent;
  let fixture: ComponentFixture<MisionInternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MisionInternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MisionInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
