import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisionInternalComponent } from './vision-internal.component';

describe('VisionInternalComponent', () => {
  let component: VisionInternalComponent;
  let fixture: ComponentFixture<VisionInternalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisionInternalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisionInternalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
