import { Component, OnInit } from '@angular/core';
import { MenuToggleService } from 'src/app/_services';
@Component({
  selector: 'app-us',
  templateUrl: './us.component.html',
  styleUrls: ['./us.component.css']
})
export class UsComponent implements OnInit {
toggleMenu: boolean = false;
  constructor(  private menuToggleService: MenuToggleService
  ) { }

  ngOnInit(): void {
  this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {this.toggleMenu = item;});
  }

}
