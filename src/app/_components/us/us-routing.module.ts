import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';
import { InicioComponent } from './inicio/inicio.component';

import { UsComponent } from './us.component';

const routes: Routes = [
  {
    path: '', component: UsComponent, children: [
      { path: 'inicio', component: InicioComponent, canActivate: [AuthGuardGuard] },

      { path: ':any', redirectTo: '/admin/quienes-somos/inicio', pathMatch: 'full' },
      { path: '', redirectTo: '/admin/quienes-somos/inicio', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsRoutingModule { }
