import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsRoutingModule } from './us-routing.module';
import { UsComponent } from './us.component';
import { CommonLocalModule } from 'src/app/_modules/common-local.module';
import { InicioComponent } from './inicio/inicio.component';
import { UsInternalComponent } from './inicio/us-internal/us-internal.component';
import { MisionInternalComponent } from './inicio/mision-internal/mision-internal.component';
import { VisionInternalComponent } from './inicio/vision-internal/vision-internal.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthBarrierInterceptor } from 'src/app/_interceptors';

@NgModule({
  declarations: [
    UsComponent,
    InicioComponent,
    UsInternalComponent,
    MisionInternalComponent,
    VisionInternalComponent
  ],
  imports: [
    CommonModule,
    UsRoutingModule,

    CommonLocalModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthBarrierInterceptor,
      multi: true
    }
  ]
})
export class UsModule { }
