import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AsegGOComponent} from "./aseg-go/aseg-go.component";
import {AsegGazComponent} from "./aseg-gaz/aseg-gaz.component";
import {AseguradorasOnOffRoutingModule} from "./aseguradoras-on-off-routing.module";
import {CommonLocalModule} from "../../_modules/common-local.module";



@NgModule({
  declarations: [
    AsegGOComponent,
    AsegGazComponent
  ],
  imports: [
    CommonModule,
    AseguradorasOnOffRoutingModule,
    CommonLocalModule
  ]
})
export class AseguradorasOnOffModule { }
