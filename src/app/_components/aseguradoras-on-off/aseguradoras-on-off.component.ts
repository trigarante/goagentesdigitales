
import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {MenuToggleService} from "../../_services";

@Component({
  selector: 'app-aseguradoras-on-off',
  templateUrl: './aseguradoras-on-off.component.html',
  styleUrls: ['./aseguradoras-on-off.component.css']
})
export class AseguradorasOnOffComponent implements OnInit {
  // toggleMenu: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {

  }

  opcion(opcionSeleccionada){
    if(opcionSeleccionada === 1){
      this.router.navigate(['admin/desactivarAseg/desactivarGo']);
    }else if(opcionSeleccionada === 2){
      this.router.navigate(['admin/desactivarAseg/desactivarGaz']);
    }
  }

}
