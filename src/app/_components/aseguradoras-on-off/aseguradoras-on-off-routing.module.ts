import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {AseguradorasOnOffComponent} from "./aseguradoras-on-off.component";
import {AsegGazComponent} from "./aseg-gaz/aseg-gaz.component";
import {AsegGOComponent} from "./aseg-go/aseg-go.component";


const routes:Routes = [
  {
    path: '', component: AseguradorasOnOffComponent, children: [
      {path: 'desactivarGaz', component: AsegGazComponent},
      {path: 'desactivarGo', component: AsegGOComponent},
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AseguradorasOnOffRoutingModule { }
