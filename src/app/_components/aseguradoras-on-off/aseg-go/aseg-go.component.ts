import { Component, OnInit } from '@angular/core';
import {AseguradorasOnOffService} from "../../../_services/aseguradoras-on-off.service";
import Swal from "sweetalert2";
import {environment as ENVIROMENTS} from "../../../../environments/environment.generals";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-aseg-go',
  templateUrl: './aseg-go.component.html',
  styleUrls: ['./aseg-go.component.css']
})
export class AsegGOComponent implements OnInit {

  aseguradorasEstado = [];
  displayedColumns = ['logoAseguradora', 'aseguradora','descuentoActual','descripcion', 'onOff']
  dataSource;

  descuentoMax = [];
  descuentoMin = [];
  nuevoDescuentoMax;
  nuevoDescuentoMin;
  cambioDeDescuento;
  descuentoSelectGeneral = [];

  constructor(private servicesOnOff: AseguradorasOnOffService, private fb :FormBuilder) { }
  formDescuento = this.fb.group({
    selectAseguradora :['', Validators.required],
    selectDescuento :['', Validators.required],
  })

  ngOnInit(): void {
    this.abrirServicio()

  }

  abrirServicio(){
    this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
      this.dataSource = dataAseg;
      this.aseguradorasEstado = dataAseg
      dataAseg.forEach((dataDescuentosMaxMin) => {
        this.descuentoMin.push([(dataDescuentosMaxMin.minDiscount),(dataDescuentosMaxMin.insuranceCompanyID)]);
        this.descuentoMax.push([(dataDescuentosMaxMin.maxDiscount),(dataDescuentosMaxMin.insuranceCompanyID)])
      })
      dataAseg.forEach((aseguradora) => {
        if(aseguradora.webServiceEnabled === true){
          this.aseguradorasArreglo.push(aseguradora.insuranceCompanyID)
        }
      })
    })
  }
  prenderApagar(aseguradora, estado){
    if(estado === false){
      estado = true;
    }else if(estado === true){
      estado = false
    }

    this.servicesOnOff.cambioEstadoAsegTotal(aseguradora,estado).subscribe((DataInf) => {
      // console.log('OK', DataInf)
      if(DataInf.company === 'ELAGUILA'){
        DataInf.company = 'EL_AGUILA'
      }
      if(DataInf.company === 'LATINO'){
        DataInf.company = 'LA_LATINO'
      }
      if(DataInf.company === 'GENERALDESEGUROS'){
        DataInf.company = 'GENERAL_DE_SEGUROS'
      }
      if(DataInf.company === 'ELPOTOSI'){
        DataInf.company = 'EL_POTOSI'
      }
      if(DataInf.company === 'PRIMEROSEGUROS'){
        DataInf.company = 'PRIMERO'
      }
      for(let asegSelect = 0 ; asegSelect < ENVIROMENTS.particual_quote.length ; asegSelect++){
        if ((ENVIROMENTS.particual_quote[asegSelect].insurance_carrier).toUpperCase() === (DataInf.company)){
          // console.log(!DataInf.isEnabled, '!DataInf.isEnabled')
          ENVIROMENTS.particual_quote[asegSelect].disabled = !DataInf.isEnabled
        }
      }

    }, error => {
      this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
        this.dataSource = dataAseg;
        this.aseguradorasEstado = dataAseg
      })
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Comunicate con el administrador, no se pudo apagar/encender la aseguradora',
      })
    })
  }

  aseguradorasArreglo = []

  obtenerDescuento(aseguradora){
    if(aseguradora === ''){
      this.nuevoDescuentoMax = '';
      this.descuentoSelectGeneral = []
    }else{
      this.descuentoSelectGeneral = []
      this.descuentoMax.forEach((desMax) => {
        if(desMax[1] === aseguradora){
          this.nuevoDescuentoMax = desMax[0]
        }
      })
      this.descuentoMin.forEach((descMin) => {
        if(descMin[1] === aseguradora){
          this.nuevoDescuentoMin = descMin[0]
        }
      })
      for(let k = 0; k <= this.nuevoDescuentoMax; k++){
        this.descuentoSelectGeneral.push(k)
      }
    }

  }
  cambioDescuento(aseguradora){
    if(this.formDescuento.controls.selectDescuento.value === undefined || this.formDescuento.controls.selectDescuento.value === ''){
      this.cambioDeDescuento = ''
    }else if(this.formDescuento.controls.selectAseguradora.value === ''){
      this.cambioDeDescuento = ''
      this.descuentoSelectGeneral = []
    } else{
      this.cambioDeDescuento = this.formDescuento.controls.selectDescuento.value
      Swal.fire({
        title: 'CONTINUAR',
        text: "Esto cambiara el descuento, ¿Deseas continuar?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Cambiar descuento'
      }).then((result) => {
        if (result.isConfirmed) {
          if(((this.cambioDeDescuento >= (this.nuevoDescuentoMin)) && (this.cambioDeDescuento <= this.nuevoDescuentoMax)) &&  (this.cambioDeDescuento !== '')){
            this.servicesOnOff.cambioDescuentoAsegBaz(aseguradora, this.cambioDeDescuento, 2201574).subscribe(data => {
              if (data[0].discount === null) {
                Swal.fire('El descuento no se pudo actualizar')
              } else {
                Swal.fire(
                    'Descuento agregado',
                    'Se actualizara el descuento',
                    'success'
                )
                this.servicesOnOff.ViewEnableAseg(2201574).subscribe((dataAseg) => {
                  this.dataSource = dataAseg;
                  this.aseguradorasEstado = dataAseg
                })
                
                //se resetea el formulario para evitar el doble click al cambiar descuento en mas de una aseguradora
                this.formDescuento = this.fb.group({
                  selectAseguradora :['', Validators.required],
                  selectDescuento :['', Validators.required],
                });
              }
            }, error => {
              Swal.fire('El descuento no se pudo actualizar')
            })
          }else{
            Swal.fire('El descuento no es valido para la aseguradora')
          }
        }
      })
    }
  }

};
