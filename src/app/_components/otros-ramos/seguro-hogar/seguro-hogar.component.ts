import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services';
import { environment } from 'src/environments/environment.generals';

@Component({
  selector: 'app-seguro-hogar',
  templateUrl: './seguro-hogar.component.html',
  styleUrls: ['./seguro-hogar.component.css']
})
export class SeguroHogarComponent implements OnInit {
  loaderGo: boolean = false;
  edadGenerada = [];
  pisosGenerado = [];
  dateBirthHogar: any;
  bodyHogar: any;
  bodyHogarForm: any;


  cotizarHogar = this.fbHohar.group({
    cobIncencioHogar: ['', Validators.required],
    edadHogar: ['', Validators.required],
    generoHogar: ['', Validators.required],
    codigoPHogar:['', [Validators.required,Validators.pattern(environment.regex_cp)]],
    peligroRio: ['', Validators.required],
    coverturasAddHogar: ['', Validators.required],
    nombreAsegHogar:['', [Validators.required,Validators.pattern(environment.regex_name)]],
    emailAsegHogar: ['', [Validators.required, Validators.email, Validators.pattern(/[a-zA-Z0-9._-s]+@[a-zA-Z]+\.[a-zA-Z]/)]],
    telefonoHogar: ['', [Validators.required, Validators.pattern(environment.regex_phone_number2)]],
    numeroAsegHogar: ['', Validators.required]
  })

  constructor(private otrosRamosService: OtrosRamosServicesService,
              private fbHohar:FormBuilder,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.generarEdad();
    this.generarPisos();
  }

  generarEdad(){
    for(let i = 18; i < 100; i++){
      this.edadGenerada.push(i)
    }
  }

  generarPisos(){
    for(let i = 1; i < 50; i++){
      this.pisosGenerado.push(i)
    }
  }

  construyendoData(){
    const anioNac = new Date().getFullYear() - this.cotizarHogar.controls.edadHogar.value;
    this.dateBirthHogar = '01/01/' + anioNac;
    this.bodyHogarForm = {
      idRamo:86,
      coberturaContraIncendios : this.cotizarHogar.controls.cobIncencioHogar.value,
      coberturasAdicionales: this.cotizarHogar.controls.coverturasAddHogar.value,
      cercaDeRio : this.cotizarHogar.controls.peligroRio.value,
      telefono:  this.cotizarHogar.controls.telefonoHogar.value.trim(),
      numeroPisosInmbuebles: this.cotizarHogar.controls.numeroAsegHogar.value,
      codigoPostal:  this.cotizarHogar.controls.codigoPHogar.value.trim(),
      edad: this.cotizarHogar.controls.edadHogar.value,
      fechaNacimiento: this.dateBirthHogar,
      genero:  this.cotizarHogar.controls.generoHogar.value,
      email:this.cotizarHogar.controls.emailAsegHogar.value.trim(),
      nombreCompleto:this.cotizarHogar.controls.nombreAsegHogar.value.trim()
    }
    this.bodyHogar = {
      idAgente: localStorage.getItem('id'),
      idSubramo: 0,
      peticion: JSON.stringify(this.bodyHogarForm),
      respuesta: "[]",
    }
  }

  
  get erroresHogar () { return this.cotizarHogar.controls; }
  

  updateData(){
    this.construyendoData();
   // console.log('Aqui se actualiza el body', this.bodyHogar)
  }

  btnCotizar(){
    this.loaderGo = true;
    this.construyendoData();
    this.otrosRamosService.guardarDatosHogar(this.bodyHogar).subscribe(data => {
      this.loaderGo = false;
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Hemos recibido correctamente tu solicitud, en breve te haremos llegar las mejores propuestas del producto a tu Whatsapp y correo electronico',
        confirmButtonText: 'Ok',
        width: '50vw',
        // timer: 2500
      }).then(() => {
        this.router.navigateByUrl('/admin/otros-ramos');
      });
      // this.cotizarHogar.reset()
    }, error => {
      if(error.status === 401){    
        this.userService.destroySession();
        this.userService.signOut();
      }else{
        this.loaderGo = false;
        Swal.fire('Comunicate con el Administrador');
      }
    })
  }
}
