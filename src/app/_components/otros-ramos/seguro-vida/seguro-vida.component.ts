import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import { Router } from '@angular/router';
import { UserService } from 'src/app/_services';
import { environment } from 'src/environments/environment.generals';

@Component({
  selector: 'app-seguro-vida',
  templateUrl: './seguro-vida.component.html',
  styleUrls: ['./seguro-vida.component.css']
})
export class SeguroVidaComponent implements OnInit {

  edadGenerada = [];
  dateBirthVida: any;
  bodyVida: any;
  bodyVidaForm: any;
  loaderGo: boolean = false;

  cotizaVida = this.fbVida.group({
    edadVida: ['', Validators.required],
    generoVida: ['', Validators.required],
    codigoPVida:['', [Validators.required,Validators.pattern(environment.regex_cp)]],
    nivelHospitalarioVida: ['', Validators.required],
    coverturasAddVida: ['', Validators.required],
    nombreAsegVida:['', [Validators.required, Validators.pattern(environment.regex_name_moral)] ],
    emailAsegVida: ['', [Validators.required, Validators.email, Validators.pattern(/[a-zA-Z0-9._-s]+@[a-zA-Z]+\.[a-zA-Z]/)]],
    telefonoVida: ['', [Validators.required, Validators.pattern(environment.regex_phone_number2)]],
    numeroAsegVida: ['', Validators.required],
    fumasVida: [''],
  })

  constructor(private otrosRamosService: OtrosRamosServicesService,
              private fbVida:FormBuilder,
              private router:Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.generarEdad();
  }

  get erroresVida() {return this.cotizaVida.controls;}

  generarEdad(){
    for(let i = 1; i < 81; i++){
      this.edadGenerada.push(i)
    }
  }

  isMoral:any;
  changeNameValidators(){
    let genderValue = this.cotizaVida.get('generoVida').value;
    
    (genderValue != 'MORAL') ? this.isMoral = false : this.isMoral = true;

    let campo = this.cotizaVida.controls.nombreAsegVida;
    if(this.isMoral){
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name_moral)])
      campo.updateValueAndValidity();
    }else{
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name)])
      campo.updateValueAndValidity();
    }
  }

  construyendoData(){
    const anioNac = new Date().getFullYear() - this.cotizaVida.controls.edadVida.value;
    this.dateBirthVida = '01/01/' + anioNac;
    this.bodyVidaForm = {
      idRamo:85,
      coberturasAdicionales: this.cotizaVida.controls.coverturasAddVida.value,
      nivelHospitalario: this.cotizaVida.controls.nivelHospitalarioVida.value,
      telefono:  this.cotizaVida.controls.telefonoVida.value.trim(),
      numeroDeAsegurados: this.cotizaVida.controls.numeroAsegVida.value,
      codigoPostal:  this.cotizaVida.controls.codigoPVida.value.trim(),
      edad: this.cotizaVida.controls.edadVida.value,
      fechaNacimiento: this.dateBirthVida,
      genero:  this.cotizaVida.controls.generoVida.value,
      email:this.cotizaVida.controls.emailAsegVida.value.trim(),
      nombreCompleto:this.cotizaVida.controls.nombreAsegVida.value.trim(),
      fumas : this.cotizaVida.controls.fumasVida.value
    }
    this.bodyVida = {
      idAgente: localStorage.getItem('id'),
      idSubramo: 0,
      peticion: JSON.stringify(this.bodyVidaForm),
      respuesta: "[]",
    }
  }

  esMayor:boolean;
  displayFumasField(){
    this.esMayor = this.cotizaVida.controls.edadVida.value >= 18;
    let campo = this.cotizaVida.controls.fumasVida;

    if(this.esMayor){
      campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required])
      campo.updateValueAndValidity();
    }else{
      campo.setErrors(null);
      campo.clearValidators();
      campo.updateValueAndValidity();
    }
    
  }

  updateData(){
    // se crea este metodo para posterior actualizacion del body a un servicio
    this.construyendoData();
    console.log('Aqui se actualiza el body', this.bodyVida)
  }

  btnCotizar(){
    this.loaderGo = true;
    this.construyendoData();
    this.otrosRamosService.guardarDatosVida(this.bodyVida).subscribe(data => {
      this.loaderGo = false;
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Hemos recibido correctamente tu solicitud, en breve te haremos llegar las mejores propuestas del producto a tu Whatsapp y correo electronico',
        confirmButtonText: 'Ok',
        width: '50vw',
        // timer: 2500
      }).then(() => {
        this.router.navigateByUrl('/admin/otros-ramos');
      });
      // this.cotizaVida.reset()
    }, error => {
      if(error.status === 401){    
        this.userService.destroySession();
        this.userService.signOut();
      }else{
        this.loaderGo = false;
        Swal.fire('Comunicate con el Administrador');
      }
    })
  }
}
