import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import { environment } from 'src/environments/environment.generals';
import { UserService } from 'src/app/_services';

@Component({
  selector: 'app-gastos-medicos-mayor',
  templateUrl: './gastos-medicos-mayor.component.html',
  styleUrls: ['./gastos-medicos-mayor.component.css']
})
export class GastosMedicosMayorComponent implements OnInit {

  loaderGo: boolean = false;
  edadGenerada = []
  aseguradosGenera = []
  bodyGastosMedicos:any;
  bodyGastosMedicosForm:any;
  dateBirthGast: any;
  submitted: boolean = false;
  counter: number;
  valorChard: any;
  arregloValor = [];
  validarError = false;
  isMoral: boolean;



  cotizarGastos = this.fbGmm.group({
    edadMedicosGas: ['', Validators.required],
    generoMedicosGas: ['', Validators.required],
    codigoPMedicosGas:['', [Validators.required,Validators.pattern(environment.regex_cp)]],
    nivelHospitalarioMedicosGas: ['', Validators.required],
    coverturasAddMedicosGas: ['', Validators.required],
    emailAsegMedicosGas: ['', [Validators.required, Validators.email, Validators.pattern(/[a-zA-Z0-9._-s]+@[a-zA-Z]+\.[a-zA-Z]/)]],
    nombreAsegMedicosGas:['', [Validators.required,Validators.pattern(environment.regex_name)]],
    telefonoMedicosGas: ['', [Validators.required,Validators.pattern(environment.regex_phone_number2)]],
    numeroAsegMedicosGas: ['', Validators.required],
  })
  
  constructor(private otrosRamosService: OtrosRamosServicesService, 
    private fbGmm:FormBuilder,
              private router:Router,
              private userService: UserService) {}

  ngOnInit(): void {
    this.generarEdad();
  }

  get erroresDeFormGmm() {return this.cotizarGastos.controls;}

  // validarCp(){
  //   this.arregloValor = [];
  //   this.counter = 0;
  //   for(let i = 0; i < (this.cotizarGastos.controls.codigoPMedicosGas.value).length; i++ ){
  //     this.arregloValor.push((this.cotizarGastos.controls.codigoPMedicosGas.value).charCodeAt(i))
  //     this.valorChard = (this.cotizarGastos.controls.codigoPMedicosGas.value).charCodeAt(i)
  //     if(this.valorChard === (this.cotizarGastos.controls.codigoPMedicosGas.value).charCodeAt(i+1)){
  //       this.counter = this.counter + 1;
  //     }
  //   }
  //   this.validarError = (this.arregloValor.length === 5 && this.counter < 4) ? false : true
  // }

  generarEdad(){
    for(let i = 1; i < 81; i++){
      this.edadGenerada.push(i)
    }
    for(let j = 1; j<80; j++){
      this.aseguradosGenera.push(j)

    }
  }

  changeNameValidators(){
    let genderValue = this.cotizarGastos.get('generoMedicosGas').value;
    
    (genderValue != 'MORAL') ? this.isMoral = false : this.isMoral = true;

    let campo = this.cotizarGastos.controls.nombreAsegMedicosGas;
    if(this.isMoral){
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name_moral)])
      campo.updateValueAndValidity();
    }else{
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name)])
      campo.updateValueAndValidity();
    }
  }

  construyendoData(){
    const anioNac = new Date().getFullYear() - this.cotizarGastos.controls.edadMedicosGas.value;

    this.dateBirthGast = '01/01/' + anioNac;
    this.bodyGastosMedicosForm = {
      idRamo:84,
      coberturasAdicionales: this.cotizarGastos.controls.coverturasAddMedicosGas.value,
      nivelHospitalario: this.cotizarGastos.controls.nivelHospitalarioMedicosGas.value,
      telefono:  this.cotizarGastos.controls.telefonoMedicosGas.value.trim(),
      numeroDeAsegurados: this.cotizarGastos.controls.numeroAsegMedicosGas.value,
      codigoPostal:  this.cotizarGastos.controls.codigoPMedicosGas.value.trim(),
      edad: this.cotizarGastos.controls.edadMedicosGas.value,
      fechaNacimiento: this.dateBirthGast,
      genero:  this.cotizarGastos.controls.generoMedicosGas.value,
      email:this.cotizarGastos.controls.emailAsegMedicosGas.value.trim(),
      nombreCompleto: this.cotizarGastos.controls.nombreAsegMedicosGas.value.trim()
    }
    
    this.bodyGastosMedicos = {
      idAgente: localStorage.getItem('id'),
      idSubramo: 0,
      peticion: JSON.stringify(this.bodyGastosMedicosForm),
      respuesta: "[]",
    }
  }

  updateData(){
    this.construyendoData();
  }

  btnCotizar(){
    this.loaderGo = true;
    this.submitted = true;
    this.construyendoData();
    if(this.cotizarGastos.invalid ){
      this.loaderGo = false;
    }else{
      this.otrosRamosService.guardarDataGmm(this.bodyGastosMedicos).subscribe(data => {
        this.loaderGo = false;
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Hemos recibido correctamente tu solicitud, en breve te haremos llegar las mejores propuestas del producto a tu Whatsapp y correo electronico',
          confirmButtonText: 'Ok',
          width: '50vw',
          // timer: 2500
        }).then(() => {
          this.router.navigateByUrl('/admin/otros-ramos');
        });
        // this.cotizarGastos.reset()
      }, error => {
        if(error.status === 401){    
          this.userService.destroySession();
          this.userService.signOut();
        }else{
          this.loaderGo = false;
          Swal.fire('Comunicate con el Administrador');
        }
      })
    }
  }
}
