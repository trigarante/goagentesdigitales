import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GastosMedicosMayorComponent } from './gastos-medicos-mayor/gastos-medicos-mayor.component';
import {otrosRamosRoutingModule} from "./otros-ramos-routing.module";
import { SeguroGrupalComponent } from './seguro-grupal/seguro-grupal.component';
import { SeguroVidaComponent } from './seguro-vida/seguro-vida.component';
import { SeguroHogarComponent } from './seguro-hogar/seguro-hogar.component';
import { SeguroViajeComponent } from './seguro-viaje/seguro-viaje.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {CommonLocalModule} from "../../_modules/common-local.module";



@NgModule({
  declarations: [
    GastosMedicosMayorComponent,
    SeguroGrupalComponent,
    SeguroVidaComponent,
    SeguroHogarComponent,
    SeguroViajeComponent
  ],
    imports: [
        CommonModule,
        otrosRamosRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        CommonLocalModule
    ], 
    exports:[
  ]
})
export class OtrosRamosModule { }
