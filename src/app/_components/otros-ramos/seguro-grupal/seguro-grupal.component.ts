import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.generals';
import { UserService } from 'src/app/_services';

@Component({
  selector: 'app-seguro-grupal',
  templateUrl: './seguro-grupal.component.html',
  styleUrls: ['./seguro-grupal.component.css']
})
export class SeguroGrupalComponent implements OnInit {
  loaderGo: boolean = false;
  edadGenerada = []
  dateBirthGrup: any;
  bodySeguroGrupal: any;
  bodySeguroGrupalForm: any
  habilitarNivelHosp: boolean = false;

  cotizarGrupal = this.fbGrupal.group({
    idRamoGrupal: ['', Validators.required],
    edadGrupal: ['', Validators.required],
    generoGrupal: ['', Validators.required],
    codigoPGrupal:['', [Validators.required,Validators.pattern(environment.regex_cp)]],
    nivelHospitalarioGrupal: [''],
    coverturasAddGrupal: ['', Validators.required],
    nombreAsegGrupal:['', [Validators.required, Validators.pattern(environment.regex_name_moral)] ],
    emailAsegGrupal: ['', [Validators.required, Validators.email, Validators.pattern(/[a-zA-Z0-9._-s]+@[a-zA-Z]+\.[a-zA-Z]/)]],
    telefonoGrupal: ['', [Validators.required,Validators.pattern(environment.regex_phone_number2)]],
    numeroAsegGrupal: ['', Validators.required],
  })

  constructor(private otrosRamosService: OtrosRamosServicesService,
              private fbGrupal:FormBuilder,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.generarEdad();
  }
  
  generarEdad(){
    this.edadGenerada = []
    for(let i = 18; i < 100; i++){
      this.edadGenerada.push(i)
    }
  }

  generarEdadGmm(){
    this.edadGenerada = []
    for(let i = 1; i < 81; i++){
      this.edadGenerada.push(i)
    }
  }

  get erroresGrupal() {return this.cotizarGrupal.controls;}
  
  isMoral:any;
  changeNameValidators(){
    let genderValue = this.cotizarGrupal.get('generoGrupal').value;
    
    (genderValue != 'MORAL') ? this.isMoral = false : this.isMoral = true;

    let campo = this.cotizarGrupal.controls.nombreAsegGrupal;
    if(this.isMoral){
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name_moral)])
      campo.updateValueAndValidity();
    }else{
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name)])
      campo.updateValueAndValidity();
    }
  }

  construyendoData(){
    const anioNac = new Date().getFullYear() - this.cotizarGrupal.controls.edadGrupal.value;
    this.dateBirthGrup = '01/01/' + anioNac;
    this.bodySeguroGrupalForm = {
      idRamo:89,
      seleccionaRamo: this.cotizarGrupal.controls.idRamoGrupal.value,
      coberturasAdicionales: this.cotizarGrupal.controls.coverturasAddGrupal.value,
      nivelHospitalario: (this.habilitarNivelHosp === true)?this.cotizarGrupal.controls.nivelHospitalarioGrupal.value:'',
      telefono:  this.cotizarGrupal.controls.telefonoGrupal.value.trim(),
      numeroDeAsegurados: this.cotizarGrupal.controls.numeroAsegGrupal.value,
      codigoPostal:  this.cotizarGrupal.controls.codigoPGrupal.value.trim(),
      edad: this.cotizarGrupal.controls.edadGrupal.value,
      fechaNacimiento: this.dateBirthGrup,
      genero:  this.cotizarGrupal.controls.generoGrupal.value,
      email:this.cotizarGrupal.controls.emailAsegGrupal.value.trim(),
      nombreCompleto:this.cotizarGrupal.controls.nombreAsegGrupal.value.trim()
    }
    this.bodySeguroGrupal = {
      idAgente: localStorage.getItem('id'),
      idSubramo: 0,
      peticion: JSON.stringify(this.bodySeguroGrupalForm),
      respuesta: "[]",
    }
  }

  updateData(){
    this.construyendoData();
  }

  selectGastos(){
    let campo = this.cotizarGrupal.controls.nivelHospitalarioGrupal;
    if(this.cotizarGrupal.controls.idRamoGrupal.value === 'GastosMedicos'){
      this.habilitarNivelHosp = true;
      this.generarEdadGmm();
      this.cotizarGrupal.controls.edadGrupal.setValue('');
      campo.clearValidators();
      campo.setValidators([Validators.required])
      campo.updateValueAndValidity();
    }else if(this.cotizarGrupal.controls.idRamoGrupal.value !== 'GastosMedicos'){
      this.habilitarNivelHosp = false;
      this.generarEdad();
      this.cotizarGrupal.controls.edadGrupal.setValue('');
      campo.clearValidators();
      campo.updateValueAndValidity();
    }
  }

  btnCotizar(){
    this.loaderGo = true;
    this.construyendoData();
    this.otrosRamosService.guardarDatosGrupal(this.bodySeguroGrupal).subscribe(data => {
      this.loaderGo = false;
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Hemos recibido correctamente tu solicitud, en breve te haremos llegar las mejores propuestas del producto a tu Whatsapp y correo electronico',
        confirmButtonText: 'Ok',
        width: '50vw',
        // timer: 2500
        }).then(() => {
          this.router.navigateByUrl('/admin/otros-ramos');
        });
      // this.cotizarGrupal.reset()
    }, error => {
      if(error.status === 401){    
        this.userService.destroySession();
        this.userService.signOut();
      }else{
        this.loaderGo = false;
        Swal.fire('Comunicate con el Administrador');
      }
    })
  }

}
