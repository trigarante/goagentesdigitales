import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OtrosRamosComponent} from "./otros-ramos.component";
import {GastosMedicosMayorComponent} from "./gastos-medicos-mayor/gastos-medicos-mayor.component";
import {SeguroGrupalComponent} from "./seguro-grupal/seguro-grupal.component";
import {SeguroHogarComponent} from "./seguro-hogar/seguro-hogar.component";
import {SeguroVidaComponent} from "./seguro-vida/seguro-vida.component";
import {SeguroViajeComponent} from "./seguro-viaje/seguro-viaje.component";

const routes: Routes = [
  {
    path: '', children: [
      {path: '', component: OtrosRamosComponent},
      {path: 'gastos-medicos', component: GastosMedicosMayorComponent},
      {path: 'seguro-grupal', component: SeguroGrupalComponent},
      {path: 'seguro-vida', component: SeguroVidaComponent},
      {path: 'seguro-hogar', component: SeguroHogarComponent},
      {path: 'seguro-viaje', component: SeguroViajeComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class otrosRamosRoutingModule { }
