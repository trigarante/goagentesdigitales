import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.generals';
import { UserService } from 'src/app/_services';

@Component({
  selector: 'app-seguro-viaje',
  templateUrl: './seguro-viaje.component.html',
  styleUrls: ['./seguro-viaje.component.css']
})
export class SeguroViajeComponent implements OnInit {
  loaderGo: boolean = false;
  edadGenerada = [];
  viajaDias = [];
  dateBirthViaje: any;
  bodyViaje: any;
  bodyViajeForm: any;


  cotizarViaje = this.fbViaje.group({
    numDiasViaje: ['', Validators.required],
    edadViaje: ['', Validators.required],
    generoViaje: ['', Validators.required],
    codigoPViaje:['', [Validators.required,Validators.pattern(environment.regex_cp)]],
    destinoInternacional: ['', Validators.required],
    coverturasAddViaje: ['', Validators.required],
    nombreAsegViaje:['', [Validators.required, Validators.pattern(environment.regex_name)] ],
    emailAsegViaje: ['', [Validators.required, Validators.email, Validators.pattern(/[a-zA-Z0-9._-s]+@[a-zA-Z]+\.[a-zA-Z]/)]],
    telefonoViaje: ['', [Validators.required, Validators.pattern(environment.regex_phone_number2)]],
    sumaAssegGmm: ['', [Validators.required,Validators.pattern(environment.regex_suma_aseg)]],
    sumaAssegfallecimiento: new FormControl('', [Validators.required, Validators.pattern(environment.regex_suma_aseg)]),
  })

  constructor(private otrosRamosService: OtrosRamosServicesService,
              private fbViaje:FormBuilder,
              private router:Router,
              private userService: UserService) { }

  ngOnInit(): void {
    this.generarEdad();
  }

  generarEdad(){
    for(let i = 17; i < 100; i++){
      this.edadGenerada.push(i)
    }
    for(let j = 1; j <= 60; j++){
      this.viajaDias.push(j)
    }
  }

  get erroresViaje () { return this.cotizarViaje.controls; }

  isMoral:any;
  changeNameValidators(){
    let genderValue = this.cotizarViaje.get('generoViaje').value;
    
    (genderValue != 'MORAL') ? this.isMoral = false : this.isMoral = true;

    let campo = this.cotizarViaje.controls.nombreAsegViaje;
    if(this.isMoral){
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name_moral)])
      campo.updateValueAndValidity();
    }else{
      // campo.setErrors(null);
      campo.clearValidators();
      campo.setValidators([Validators.required,Validators.pattern(environment.regex_name)])
      campo.updateValueAndValidity();
    }
  }

  construyendoData(){
    const anioNac = new Date().getFullYear() - this.cotizarViaje.controls.edadViaje.value;
    this.dateBirthViaje = '01/01/' + anioNac;
    this.bodyViajeForm = {
      idRamo:87,
      coberturasAdicionales: this.cotizarViaje.controls.coverturasAddViaje.value,
      numeroDiasViaje: this.cotizarViaje.controls.numDiasViaje.value,
      telefono:  this.cotizarViaje.controls.telefonoViaje.value.trim(),
      codigoPostal:  this.cotizarViaje.controls.codigoPViaje.value.trim(),
      edad: this.cotizarViaje.controls.edadViaje.value,
      fechaNacimiento: this.dateBirthViaje,
      genero:  this.cotizarViaje.controls.generoViaje.value,
      email:this.cotizarViaje.controls.emailAsegViaje.value.trim(),
      nombreCompleto:this.cotizarViaje.controls.nombreAsegViaje.value.trim(),
      destinoInternacional: this.cotizarViaje.controls.destinoInternacional.value,
      sumaAssegGmm: this.cotizarViaje.controls.sumaAssegGmm.value.trim(),
      sumaAssegfallecimiento: this.cotizarViaje.controls.sumaAssegfallecimiento.value.trim(),
    }
    this.bodyViaje = {
      idAgente: localStorage.getItem('id'),
      peticion: JSON.stringify(this.bodyViajeForm),
      respuesta: "[]",
    }
  }

  updateData(){
    this.construyendoData();
    console.log('Aqui se actualiza el body', this.bodyViaje)
  }

  btnCotizar(){
    this.loaderGo = true;
    this.construyendoData();
    this.otrosRamosService.guardarDatosViaje(this.bodyViaje).subscribe(data => {
      this.loaderGo = false;
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Hemos recibido correctamente tu solicitud, en breve te haremos llegar las mejores propuestas del producto a tu Whatsapp y correo electronico',
        confirmButtonText: 'Ok',
        width: '50vw',
        // timer: 2500
      }).then(() => {
        this.router.navigateByUrl('/admin/otros-ramos');
      });
      // this.cotizarViaje.reset()
    }, error => {
      if(error.status === 401){    
        this.userService.destroySession();
        this.userService.signOut();
      }else{
        this.loaderGo = false;
        Swal.fire('Comunicate con el Administrador');
      }
    })
  }

}
