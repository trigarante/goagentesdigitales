import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-otros-ramos',
  templateUrl: './otros-ramos.component.html',
  styleUrls: ['./otros-ramos.component.css']
})
export class OtrosRamosComponent implements OnInit {

  rutasEntrada = [
      {insurance_carrier: 'gastos-medicos', nombreClave:'GASTOS MEDICOS MAYOR'},
      {insurance_carrier: 'seguro-grupal', nombreClave:'SEGURO GRUPAL (FLOTILLA)'},
      {insurance_carrier: 'seguro-vida', nombreClave:'SEGURO DE VIDA'},
      {insurance_carrier: 'seguro-hogar', nombreClave:'DAÑOS HOGAR'},
      {insurance_carrier: 'seguro-viaje', nombreClave:'SEGURO DE VIAJE'}]


  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  sendCotizacionAseguradora(id: any){
      switch (id){
            case 'gastos-medicos':
                this.router.navigateByUrl('/admin/otros-ramos/gastos-medicos')
            break;
          case 'seguro-grupal':
              this.router.navigateByUrl('/admin/otros-ramos/seguro-grupal')
              break;
          case 'seguro-vida':
              this.router.navigateByUrl('/admin/otros-ramos/seguro-vida')
              break;
          case 'seguro-hogar':
              this.router.navigateByUrl('/admin/otros-ramos/seguro-hogar')
              break;
          case 'seguro-viaje':
              this.router.navigateByUrl('/admin/otros-ramos/seguro-viaje')
              break;
      }
   }

}
