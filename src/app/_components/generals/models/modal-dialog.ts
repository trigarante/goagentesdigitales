export enum AlertType { INFO, WARNING, ERROR }

export class ModalDialog {
    title: string;
    content: string;
    // alertType: AlertType;
    closeButtonLabel: string;
    type: string;

    constructor(data?) {
        if (data) {
            this.title = data.title;
            this.content = data.content;
            // this.alertType = data.alertType;
            this.closeButtonLabel = data.closeButtonLabel;
            this.type = data.type;
        }
    }
}
