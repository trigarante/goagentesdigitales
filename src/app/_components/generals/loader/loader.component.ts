import { Component, OnInit } from '@angular/core';
import { EvetnsEmiterLocalService } from 'src/app/_services';

@Component({
  selector: 'loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  aseguradoraSelected: string = '';
  active: boolean = false;



  constructor(
    private evetnsEmiterLocalService: EvetnsEmiterLocalService
  ) {
    this.evetnsEmiterLocalService.getLoader()
      .subscribe((item: any) => {
        let _loader = JSON.parse(item);
        this.active = _loader.active;
        this.aseguradoraSelected = _loader.insurance;
      });
  }

  ngOnInit(): void {
  }

}
