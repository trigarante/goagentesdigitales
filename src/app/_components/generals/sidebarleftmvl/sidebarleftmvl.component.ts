import {Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserService, MenuToggleService } from 'src/app/_services';
import { HostListener } from "@angular/core";
import { environment as GENERALS } from 'src/environments/environment.generals';
import { MatSidenav } from '@angular/material/sidenav';
import {Router} from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'sidebarleftmvl',
  templateUrl: './sidebarleftmvl.component.html',
  styleUrls: ['./sidebarleftmvl.component.css']
})
export class SidebarleftmvlComponent implements OnInit {

  @ViewChild('sidenav') sidenav: MatSidenav;
  @ViewChild('menuMobile') menuMobile: ElementRef<HTMLElement>;
  toggle1: boolean = false;
  toggle2: boolean = false;
  toggle2_1: boolean = false;
  toggle2_2: boolean = true;
  toggle3: boolean = false;
  toggle4: boolean = false;
  toggle4_1: boolean = false;
  toggle5: boolean = false;
  toggle6: boolean = false;
  toggleMenu: boolean = false;
  scrHeight: any;
  scrWidth: any;
  reason = '';
  statesExitShowAlert: any = {};
  
  user: any;
  iName: any;
  iLastName: any;

  toggle: boolean = false;
 goId: string;



  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;

    if (this.scrWidth <= GENERALS.responsive.width) {
      setTimeout(() => {
        this.eventLocal(true);
      }, 200);
    }
  }

  constructor(
    private menuToggleService: MenuToggleService,
    public router: Router,

    private userService: UserService,


    ) {
    this.getScreenSize();
  }

  ngOnInit(): void {
    this.goId = localStorage.getItem('goId');
    this._getInfo();


    this.menuToggleService.getNavChangeEmitter().subscribe(
      (item: any) => { 
        this.toggleMenu = item; 
        if(this.scrWidth <= GENERALS.responsive.width && item) {
          // this.menuMobile.nativeElement.click();
        }
      }
    );
  }
  eventLocal(toggle: boolean): void {
    this.menuToggleService.emitNavChangeEvent(toggle);
  }




  routerRedirect(rout: string):void{
     let stateRedirect = false;
    if(localStorage.getItem("emissionStates")){
      this.statesExitShowAlert = JSON.parse(localStorage.getItem("emissionStates"));
    }

    if(localStorage.getItem("estadoProceso") && localStorage.getItem("emissionStates")){
        if(this.statesExitShowAlert.clientData.state && this.statesExitShowAlert.clientData.showAlert){

          Swal.fire({
            icon: 'success',
            title: 'Asegura  el precio de tu emisión ',
            text: ' terminandola ahora!',
            footer: 'Gracias!'
          });
          this.statesExitShowAlert.clientData.showAlert = false;
          localStorage.setItem('emissionStates',JSON.stringify(this.statesExitShowAlert));
          return;
        }else if(this.statesExitShowAlert.addressData.state && this.statesExitShowAlert.addressData.showAlert){


          Swal.fire({
            icon: 'success',
            title: 'Asegura  el precio de tu emisión ',
            text: ' terminandola ahora!',
            footer: 'Gracias!'
          });
          this.statesExitShowAlert.addressData.showAlert = false;
          localStorage.setItem('emissionStates',JSON.stringify(this.statesExitShowAlert));
          return;

        }else if(this.statesExitShowAlert.vehiculoData.state && this.statesExitShowAlert.vehiculoData.showAlert){


          Swal.fire({
            icon: 'success',
            title: 'Asegura  el precio de tu emisión ',
            text: ' terminandola ahora!',
            footer: 'Gracias!'
          });
          this.statesExitShowAlert.vehiculoData.showAlert = false;
          localStorage.setItem('emissionStates',JSON.stringify(this.statesExitShowAlert));
          return;

        }else if(this.statesExitShowAlert.payData.state && this.statesExitShowAlert.payData.showAlert){
          Swal.fire({
            icon: 'success',
            title: 'Asegura  el precio de tu emisión ',
            text: ' terminandola ahora!',
            footer: 'Gracias!'
          });
          this.statesExitShowAlert.payData.showAlert = false;
          localStorage.setItem('emissionStates',JSON.stringify(this.statesExitShowAlert));
          return;

        }else{
          stateRedirect = true;
        }

    }else{
      stateRedirect = true;
    }
    switch (rout){

      case 'promociones-estacionales':
        if (stateRedirect){
          this.router.navigateByUrl('/admin/inicio/promociones-estacionales');
        }else{
          console.log("show message");
        }
        break;
      case 'tabla-de-meses-sin-intereses':
        this.router.navigateByUrl('/admin/inicio/tabla-de-meses-sin-intereses');
        break;
      case 'cuaderno-de-incentivos':
        this.router.navigateByUrl('/admin/inicio/cuaderno-de-incentivos');
        break;
      case 'calendario':
        this.router.navigateByUrl('/admin/inicio/calendario');
        break;
      case 'directorio':
        this.router.navigateByUrl('/admin/inicio/directorio');
        break;
      case 'quienes-somos':
        this.router.navigateByUrl('/admin/quienes-somos');
        break;
      case 'particular':
        this.router.navigateByUrl('/admin/cotiza-go/particular');
        break;
      case 'multicotizador':
        this.router.navigateByUrl('/admin/cotiza-go/multicotizador');
        break;
      case 'valor-factura':
        this.router.navigateByUrl('/admin/cotiza-go/valor-factura');
        break;
      case 'privado':
        this.router.navigateByUrl('/admin/cotiza-go/privado');
        break;


      case 'mis-cotizaciones':
        this.router.navigateByUrl('/admin/oficina-virtual/mis-cotizaciones');
        break;
      case 'mis-polizas':
        this.router.navigateByUrl('/admin/oficina-virtual/mis-polizas');
        break;
      case 'mis-renovaciones':
        this.router.navigateByUrl('/admin/oficina-virtual/mis-renovaciones');
        break;
      case 'mi-cobranza':
        this.router.navigateByUrl('/admin/oficina-virtual/mi-cobranza');
        break;
      case 'mis-comisiones':
        this.router.navigateByUrl('/admin/oficina-virtual/mis-comisiones');
        break;
      case 'mis-clientes':
        this.router.navigateByUrl('/admin/oficina-virtual/mis-clientes');
        break;

      case 'autos':
        this.router.navigateByUrl('/admin/capacitacion/autos');
        break;


      case 'producto':
        this.router.navigateByUrl('/admin/capacitacion/producto');
        break;
      case 'portal-go':
        this.router.navigateByUrl('/admin/capacitacion/portal-go');
        break;
      case 'cobranza-go':
        this.router.navigateByUrl('/admin/capacitacion/cobranza-go');
        break;
      case 'metodo-de-pago':
        this.router.navigateByUrl('/admin/capacitacion/metodo-de-pago');
        break;
        case 'argumentos-ventas':
        this.router.navigateByUrl('/admin/capacitacion/argumentos-ventas');
        break;
        case 'prospectar':
        this.router.navigateByUrl('/admin/capacitacion/prospectar');
        break;


      case 'go':
        this.router.navigateByUrl('/admin/descargar-formatos/go');
        break;
      case 'aseguradoras':
        this.router.navigateByUrl('/admin/descargar-formatos/aseguradoras');
        break;
      
      case 'oficina-virtual':
        this.router.navigateByUrl('/admin/oficina-virtual/dasboard/promociones-estacionales');
        break;

      case 'emisiones-cobrar':
        this.router.navigateByUrl('/admin/oficina-virtual/dasboard/emisiones-por-cobrar');
        break;

      case 'subsecuente':
        this.router.navigateByUrl('/admin/oficina-virtual/dasboard/subsecuente');
        break;

      case 'virtual-card':
        this.router.navigate(['/virtual-card'], { queryParams: {goId:this.goId} } );
        break;
      
      case 'mis-leads':
        this.router.navigateByUrl('/admin/oficina-virtual/dasboard/mis-leads');
      break;
      case 'otrosRamos':
        // aqio va a ir la ruta de otros ramos
        this.router.navigateByUrl('/admin/otros-ramos');
        break;

      case 'desactivarAseg':
        // aqio va a ir la ruta de otros ramos
        this.router.navigateByUrl('/admin/desactivarAseg');
        break;
      case 'desactivarAseg/Baz':
        // aqio va a ir la ruta de otros ramos
        this.router.navigate(['admin/desactivarAseg/desactivarGaz']);
        break;
      case 'desactivarAseg/Go':
        // aqio va a ir la ruta de otros ramos
        this.router.navigate(['admin/desactivarAseg/desactivarGo']);
        break;
      case 'cotizacionesGenerales':
        this.router.navigate(['admin/tablaDeCotizaciones/cotizacionesGenerales']);
        break;

      default:
        console.log("DEFAULT");
        break;
    }


  }

  private _getInfo(): void {
    // Revisar que no truene
    // this.userService.getInfo().subscribe(
    //
    //   succ => {
    //     this.user = succ;
    //     this.iName = succ.nombre.substring(0,1);
    //     this.iLastName = succ.apellidoPaterno.substring(0,1);
    //     // console.log(this.user);
    //   },
    //   err => {
    //     console.error(err);
    //   }
    // );
  }

  /*ultimo*/
  
  linkLocal(redirect: string): void {
    this.toggle = false;
    if (redirect === '/logOut') {
      this.logOut();
    } else {
      this.router.navigateByUrl(redirect);
    }
  }
  

  logOut(): void {
    this.userService.destroySession();
    this.userService.signOut();
  }
 
}
