import { Component, Input, OnInit } from '@angular/core';
import { AlertInterface } from 'src/app/_interfaces';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  @Input() alert: AlertInterface = { type: '', message: '' };

  constructor() { }

  ngOnInit(): void {
    this._setClass();
  }

  private _setClass(): void {
    switch (this.alert.type) {
      case 'error':
      case 'danger':
        this.alert.class = 'danger';
        break;
    }
  }

  private _clearAlert(): void {
    setTimeout(() => {
      this.alert.type = 'none'; // = { type: '', message: '' };
    }, 900);
  }
}
