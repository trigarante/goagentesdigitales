import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-gracias-pago',
  templateUrl: './gracias-pago.component.html',
  styleUrls: ['./gracias-pago.component.css']
})
export class GraciasPagoComponent implements OnInit {

  aseguradoraSelected: string = '';
  datosAuto: any;
  tiempoEspera : boolean;
  constructor( ) {  }

  ngOnInit(): void {
    //obtinen el nombre de la aseguradora almacenada en el  localStorage
    this.aseguradoraSelected = (localStorage.getItem('tokenAs'))? localStorage.getItem('tokenAs') : (localStorage.getItem('tmpMultiSelected')).toLowerCase() ;
    this.datosAuto = localStorage.getItem('')
    // se agrega tiempo a la vista
    setTimeout(() => {
      this.tiempoEspera = true
    }, 5000);

    localStorage.removeItem("estadoProceso");
    localStorage.removeItem("emissionStates");
  }




}
