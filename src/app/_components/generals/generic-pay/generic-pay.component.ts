import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import RfcFacil from 'rfc-facil';
// import curp from 'curp';
import Swal from 'sweetalert2';
import {
  CodPostalInterface,
  EmisionRequestInterface,
  CardTypeInterface,
  CotizacionResponseInterface
} from 'src/app/_interfaces';
// @ts-ignore
import { GnpPago3DService, CatalogosService, CotizacionService, EvetnsEmiterLocalService, OnlyNumbersService, ToLowerCaseKeyService } from 'src/app/_services';
import jwt_decode from 'jwt-decode';
import curp from 'curp';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { MatStepper } from '@angular/material/stepper';
import { environment } from 'src/environments/environment';
import {error} from "@angular/compiler/src/util";
import {Subscription} from "rxjs";

declare var require: any
const FileSaver = require('file-saver');
const validateRfc = require('validate-rfc');

@Component({
  selector: 'app-generic-pay',
  templateUrl: './generic-pay.component.html',
  styleUrls: ['./generic-pay.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GenericPayComponent implements OnInit {
  cotizacionResponse: CotizacionResponseInterface;
  // pruebaEmision :CotizacionResponseInterface = {
  //   "pago":{
  //     "msi":"CL",
  //     "banco":"HSBC",
  //     "mesExp":"01",
  //     "anioExp":"2025",
  //     "carrier":1,
  //     "noClabe":"",
  //     "medioPago":"DEBITO",
  //     "noTarjeta":"4912824500454949",
  //     "nombreTarjeta":"LUIS ENRIQUE ",
  //     "codigoSeguridad":"693"
  //   },
  //   "cliente":{
  //     "rfc":"EALL430424",
  //     "curp":"",
  //     "edad":79,
  //     "email":"OMAREAGO@HOTMAIL.COM",
  //     "genero":"MASCULINO",
  //     "nombre":"LUIS ENRIQUE",
  //     "telefono":"5544778899",
  //     "direccion":{
  //       "pais":"",
  //       "calle":"OCHOA",
  //       "noExt":"3802",
  //       "noInt":"",
  //       "ciudad":"Chihuahua",
  //       "colonia":"Unidad Proletaria",
  //       "codPostal":"31054",
  //       "poblacion":"Chihuahua"
  //     },
  //     "ocupacion":"",
  //     "apellidoMat":"LEDEZMA",
  //     "apellidoPat":"ESCARCEGA ",
  //     "tipoPersona":"",
  //     "fechaNacimiento":"24/04/1943"
  //   },
  //   "emision":{
  //     "poliza":"7160241107",
  //     "derechos":"520",
  //     "impuesto":"1315.86",
  //     "recargos":"-157.23",
  //     "terminal":"",
  //     "documento":"http://qbcenter.qualitas.com.mx/poliza/c7160241107.pdf|http://qbcenter.qualitas.com.mx/poliza/r7160241107.pdf|http://qbcenter.qualitas.com.mx/poliza/p7160241107.pdf",
  //     "primaNeta":"7861.37",
  //     "resultado":true,
  //     "primaTotal":"9540",
  //     "primerPago":"",
  //     "idCotizacion":"",
  //     "pagosSubsecuentes":""
  //   },
  //   "paquete":"AMPLIA",
  //   "vehiculo":{
  //     "uso":"PARTICULAR",
  //     "clave":"2331",
  //     "marca":"NISSAN",
  //     "codUso":"",
  //     "modelo":2016,
  //     "noMotor":"QR25505797L",
  //     "noSerie":"JN8BT2776GW514314",
  //     "codMarca":"",
  //     "noPlacas":"EJT2299",
  //     "servicio":"PARTICULAR",
  //     "subMarca":"",
  //     "descripcion":"X-TRAIL",
  //     "codDescripcion":""
  //   },
  //   "descuento":"30",
  //   "coberturas":[
  //     {
  //       "RC":"-NRESPONSABILIDAD CIVIL -S3000000-D0",
  //       "RCBienes":"-",
  //       "RCPExtra":"-",
  //       "Cristales":"-",
  //       "RoboTotal":"-NROBO TOTAL -S220000-D 10",
  //       "RCFamiliar":"-",
  //       "RCPersonas":"-",
  //       "RCExtension":"-",
  //       "RCExtranjero":"-",
  //       "DanosMateriales":"-NDANOS MATERIALES -S220000-D5",
  //       "DefensaJuridica":"-NGASTOS LEGALES -S3000000-D0",
  //       "MuerteAccidental":"-NMUERTE DEL CONDUCTOR POR ACCIDENTE -S100000-D0",
  //       "AsitenciaCompleta":"-NASISTENCIA VIAL -S20000-D0",
  //       "DanosMaterialesPP":"-",
  //       "GastosMedicosEvento":"-NGASTOS MEDICOS -S200000-D0",
  //       "GastosMedicosOcupantes":"-NACCIDENTES OCUPANTES -S100000-D0"
  //     }
  //   ],
  //   "cotizacion":{
  //     "cotID":"",
  //     "verID":"",
  //     "cotIncID":"",
  //     "derechos":"520",
  //     "impuesto":"1315.86",
  //     "recargos":"-157.23",
  //     "verIncID":"",
  //     "primaNeta":"7861.37",
  //     "resultado":"true",
  //     "primaTotal":"9540",
  //     "primerPago":"",
  //     "idCotizacion":"0538481529",
  //     "pagosSubsecuentes":""
  //   },
  //   "aseguradora":"QUALITAS",
  //   "codigoError":"Pago con respuesta correcta",
  //   "urlRedireccion":"",
  //   "periodicidadDePago":"0"
  // }


  commonRequest: object = {};
  regexPhone: any = GENERALS.regex_phone_number;
  aseguradoraSelected: string = '';
  submitted: boolean = false;
  stepFix: any;
  idAgente: any;
  idCot: any;
  cotizacionSaved: any; // object;
  emisionRequest: EmisionRequestInterface = {};
  emisionRequestResp: any;
  emisionRequestGnp: any;
  emisionResponse: EmisionRequestInterface = {};
  coloniasAfirme:any;
  cAfirme:any;
  nombreResumen: string = '';
  strMask: string = '';
  apellidoPResumen: string = '';
  apellidoMResumen: string = '';
  rfc: string = '';
  curp: any;
  datosPersonales: FormGroup = new FormGroup({
    nombre: new FormControl('', [Validators.required]),
    aPaterno: new FormControl('', [Validators.required]),
    aMaterno: new FormControl('', [Validators.required]),
    correo: new FormControl('', [Validators.required, Validators.email, Validators.pattern(GENERALS.regex_mail)]),
    telefono: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    nacimientoEstado: new FormControl('', [Validators.required]),
    fechaNacimiento: new FormControl('', [Validators.required]),
    rfc: new FormControl('', [Validators.required]),
    curp: new FormControl('', [Validators.required])
  });
  datosDireccion: FormGroup = new FormGroup({
    estadoMun: new FormControl('', [Validators.required]),
    colonia: new FormControl('', [Validators.required]),
    calleNum: new FormControl('', [Validators.required]),
    numero: new FormControl('', [Validators.required]),
    numInterior: new FormControl('')
  });
  datosPago: FormGroup = new FormGroup({
    tipoTarjeta: new FormControl('', [Validators.required]),
    nombreTarjeta: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
    banco: new FormControl('', [Validators.required]),
    metodoPago: new FormControl('', [Validators.required]),
    formaPago: new FormControl('', [Validators.required]),
    numTarjeta: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(18)]),
    mesVencimiento: new FormControl('', [Validators.required]),
    anioVencimiento: new FormControl('', [Validators.required]),
    cvv: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(5), Validators.pattern("^[0-9]*$")])
  });
  datosVehiculo: FormGroup = new FormGroup({
    niv: new FormControl('', [Validators.required, Validators.minLength(15), Validators.maxLength(20)]),
    motor: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]),
    placa: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(8)])
  });
  colonias: CodPostalInterface[] = [];
  cp: string = '';
  estado: string = '';
  delMun: string = '';
  codigoPostal: string='';
  emisionWithoutPaid: boolean = false;
  emisionWithoutPaidGNP: boolean = false;
  emisionWithoutPaidAFIRME: boolean = false;
  terminalExterna: string = '';
  documentos: string = '';
  poliza: string = '';
  _token: string = '';
  _form: any;
  @ViewChild('stepper') stepper: MatStepper;
  optPay: CardTypeInterface[] = [];
  lastQuotation;
  dateBirthDay: string = '';
  typeLocal: number = 0;
  loaderGo: boolean = false;
  useTerminalVirtual: boolean = false;
  externalTerminalURL: string = '';
  loadExternalTerminal: boolean = false;
  fromMulty: boolean = false;
  urlType: string = '';
  actionEmiterDataVehiculo = 'false';
  public subscriber: Subscription;
  bancos: any;
  tarjetasGnp: any;
  buttonEmisionInteraccion: boolean = false;
  statesExitShowAlert: any = {'clientData':{'state':true, 'showAlert':true},
                              'addressData':{'state':false, 'showAlert':true},
                              'vehiculoData':{'state':false, 'showAlert':true},
                              'paymentMethods':{'state':false, 'showAlert':true},
                              'payData':{'state':false, 'showAlert':true}};
 delegacionAfirme: string;
 coloniaAfirme: string;
 asegAfirme: string;
 datosFormularioCot:any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private catalogosService: CatalogosService,
    private gnpPago3DService: GnpPago3DService,
    private cotizacionService: CotizacionService,
    private onlyNumber: OnlyNumbersService,
    private parseToLower: ToLowerCaseKeyService,
    private evetnsEmiterLocalService: EvetnsEmiterLocalService
  ) {
    this.commonRequest = {
      personal: this.datos_personales,
      address: this.datos_direccion,
      car: this.datos_vehiculo,
      card: this.datos_card
    };
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    })


  }

  ngOnInit(): void {
    this.calcularDiaActual()
    if(localStorage.getItem('tmpMultiSelected')){ localStorage.setItem("coverageSelected","AMPLIA"); localStorage.setItem("payFormSelected","ANUAL");}
    if(!localStorage.getItem("estadoProceso")){ localStorage.setItem("estadoProceso","enProcesoEmision");}
    if(!localStorage.getItem("emissionStates")){ localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));}
    let _cotizacionSaved;
    if(localStorage.getItem('cotizacionSaved')){ _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';}

    this.typeLocal = (this.emisionRequest.Aseguradora) ? 1 : 2;
    this._setInit();
    this._validateMultyQuotation();
    if(!this.emisionRequest.pago){
      this.emisionRequest.pago = {"medioPago":"","nombreTarjeta":"","banco":"","noTarjeta":"","mesExp":"","anioExp":"","codigoSeguridad":"","noClabe":"","carrier":0,"msi":""};
    }

    this.emisionRequestResp = this.emisionRequest;
    if(localStorage.getItem('tmpMultiSelected')){
      this.emisionRequestResp = JSON.parse(localStorage.getItem('lastCotizacion'));
      this.emisionRequest = this.emisionRequestResp;
    }


    this.cotizacionService.saveFake(_cotizacionSaved.id, this.emisionRequest).subscribe(
        succ => { localStorage.setItem('savedFake', JSON.stringify(succ));
          this.updateData(2)},
        err => { console.error(err); });
    let _url = window.location.href.split('/');
    this.useTerminalVirtual = (_url[_url.length - 1] === 'virtual') ? true : false;


  }

  ngOnDestroy():void{
    localStorage.removeItem("estadoProceso");
    localStorage.removeItem("emissionStates");
  }


  disableBack():void{

    let timesPressed:number = 0;

    window.history.pushState(null, "", window.location.href);        
    window.onpopstate = function() {
      timesPressed += 1;
      if(timesPressed < 3){
        window.history.pushState(null, "", window.location.href);
        Swal.fire({
          icon: 'success',
          title: 'Asegura  el precio de tu emisión ',
          text: ' terminandola ahora!',
          footer: 'Gracias!'
        });
      }else{
        if(timesPressed === 3){
          window.history.back();
        }
      }
    };

  }

  private _setInit(): void {

    this.disableBack();

    if (!localStorage.getItem('_t')) {
      this.router.navigateByUrl('admin/cotiza-go/particular');
    }
    let _multi = localStorage.getItem('tmpMultiSelected') || null;

    this._token = localStorage.getItem('token') || '';
    this._form = localStorage.getItem('_t');
    let _tmpmulti = JSON.parse(localStorage.getItem('tmpMultAskQuotation')) || null;
    this.fromMulty = (localStorage.getItem('_tmpMultyPush')) ? true : false;
    this._eventLocal(false);

    if (!_multi) {
      let _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';
      this.emisionRequest = JSON.parse(_cotizacionSaved.respuesta);
      console.log("request", this.emisionRequest);
      this.aseguradoraSelected = localStorage.getItem('tokenAs');
      this.getColonias(JSON.parse(_cotizacionSaved.peticion).cp);
    } else {
      this.aseguradoraSelected = localStorage.getItem('tmpMultiSelected');
      this.getColonias(_tmpmulti[0].cp)
    }
    // if(this.emisionRequest.Cliente){
    //   this.getColoniasMigo(this.emisionRequest.Cliente.Direccion.CodPostal)
    // }else
    if(this.emisionRequest.cliente){
      // this.getColoniasMigo(this.emisionRequest.cliente.direccion.codPostal)
    }

    if(this.aseguradoraSelected.toUpperCase() ==='AFIRME'){
      this.catalogosService.getColoniasAfirme(localStorage.getItem('cpAfirme')? localStorage.getItem('cpAfirme'): '09410').subscribe(data =>{
        this.coloniasAfirme = data;
        this.cAfirme=this.coloniasAfirme.direcciones.municipios[0].colonias;
        this.delegacionAfirme = this.coloniasAfirme.direcciones.municipios[0].nombreMunicipio+'-'+this.coloniasAfirme.direcciones.municipios[0].idMunicipio;
        this.coloniaAfirme = this.cAfirme[0].nombreColonia +'-'+this.cAfirme[0].idColonia;

      })
    }
    this.datosPago.controls['formaPago'].disable();
    // this.bancos = this.getBancosGNP();
    // this.tarjetasGnp = this.getTerjetaValidaGNP();

  }

  private _eventLocal(active: boolean, insurance?: string): void {
    this.evetnsEmiterLocalService.emitLoader(JSON.stringify({ active: active, insurance: insurance }));
  }

  private _validateMultyQuotation(): void {

    let _multyInsurance = localStorage.getItem('tmpMultiSelected') || null;
    if (_multyInsurance) {
      this.aseguradoraSelected = _multyInsurance;
      let _tmpQuotation = JSON.parse(localStorage.getItem('multiQuote'));
      let _quotation = _tmpQuotation.find(el => el.aseguradora.toLowerCase() === _multyInsurance.toLowerCase()).cotizacion;
      localStorage.setItem('lastCotizacion', JSON.stringify(_quotation));
    }
  }

  get datos_personales() { return this.datosPersonales.controls; }
  get datos_direccion() { return this.datosDireccion.controls; }
  get datos_vehiculo() { return this.datosVehiculo.controls; }
  get datos_card() { return this.datosPago.controls; }

  calcularRfc(): void {
    this.testCurp();
    if (this.datos_personales.nombre.value && this.datos_personales.aPaterno.value && this.datos_personales.aMaterno.value && this.datos_personales.nacimientoEstado.value && this.datos_personales.fechaNacimiento.value) {
      //this.fechaCompleta = moment(this.fechaNacimiento).format('DD/MM/YYYY');
      const rfc: string = RfcFacil.forNaturalPerson({
        name: this.datosPersonales.controls['nombre'].value,
        firstLastName: this.datosPersonales.controls['aPaterno'].value,
        secondLastName: this.datosPersonales.controls['aMaterno'].value,
        day: +moment(this.datosPersonales.controls['fechaNacimiento'].value).format('D'),
        month: +moment(this.datosPersonales.controls['fechaNacimiento'].value).format('M'),
        year: moment(this.datosPersonales.controls['fechaNacimiento'].value).get('year')
      });
      let _rfc = rfc;
      this.datosPersonales.controls['rfc'].setValue(_rfc);
      if(!validateRfc(_rfc).isValid){
        Swal.fire({
          icon: 'warning',
          title: 'Detalles RFC!',
          text: 'El RFC debe contener 13 caracteres y debe ser un dato veridico.',
          footer: 'Favor de incluir la homoclave.'
        });
      }
    }
  }

  testCurp(): void {
    if (this.datos_personales.nombre.value && this.datos_personales.aPaterno.value && this.datos_personales.aMaterno.value && this.datos_personales.nacimientoEstado.value && this.datos_personales.fechaNacimiento.value) {
      const persona = curp.getPersona();
      persona.nombre = this.datos_personales.nombre.value;
      persona.apellidoPaterno = this.datos_personales.aPaterno.value;
      persona.apellidoMaterno = this.datos_personales.aMaterno.value;
      persona.genero = (this._form.length == 1) ? this._form : (this._form.toUpperCase() === 'MASCULINO') ? 'H' : 'F';
      let day = moment(this.datos_personales.fechaNacimiento.value).format('D');
      let month = moment(this.datos_personales.fechaNacimiento.value).format('M');
      let year = moment(this.datos_personales.fechaNacimiento.value).get('year');
      persona.fechaNacimiento = day + '-' + month + '-' + year;
      persona.estado = this.datos_personales.nacimientoEstado.value;
      this.datosPersonales.controls['curp'].setValue(curp.generar(persona));
    }
  }

  pago(value: any, responseEmision: any): void {
    this.emisionRequest = responseEmision
    this.idAgente = jwt_decode(this._token);
    const idAgente = +this.idAgente['sub'].split(':')[1];
    this.catalogosService.pagar(this.emisionRequest).subscribe(resp => {
      this.emisionResponse = resp;
      localStorage.setItem('lastEmision', JSON.stringify(resp));

      if(resp.Emision){
        if(resp.Emision.Resultado == true && resp.Emision.Poliza !== '' && resp.Emision.Documento !== ''){
          Swal.fire({
            icon: 'success',
            title: 'El pago se ha realizado exitosamente!',
            text: 'Registro almacenado en nuestro sistema!',
            footer: ''
          });
        }
      }else{
        if(resp.emision.resultado == true && resp.emision.poliza !== '' && resp.emision.documento !== ''){
          Swal.fire({
            icon: 'success',
            title: 'El pago se ha realizado exitosamente!',
            text: 'Registro almacenado en nuestro sistema!',
            footer: ''
          });
        }
      }

      if(resp.Emision){
        if(resp.Emision.Documento !== '' && resp.Emision.Poliza !== '' && resp.Emision.Resultado == true){
          this.catalogosService.guardarPagoQualitas(this.emisionResponse, idAgente.toString(), value.idRegistro).subscribe(
              respuesta => {
                localStorage.setItem('lastPaidSaved', JSON.stringify(respuesta));
                this.datosFormularioCot = JSON.parse(localStorage.getItem('datosFormularioCot'));
                this.emisionResponse.vehiculo.subMarca = this.datosFormularioCot.descripcion

                let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
                if (this.emisionResponse.aseguradora === 'GNP'){
                  let card = localStorage.getItem('card');
                  this.emisionResponse.pago.noTarjeta = card;
                }
                this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 7).subscribe(
                    succ => {
                      console.table(_SavedFake);
                    },
                    err => {
                      console.error(err);
                    }
                );

              },
              err => {
                console.error(err);
              });
        }else{

          let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
          this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 9).subscribe(
              succ => {
                console.table(_SavedFake);
              },
              err => {
                console.error(err);
              }
          );

        }
      }else if (resp.emision){//aqui se agrego validacion 08/02/22
        if(resp.emision.documento !== '' && resp.emision.poliza !== '' && resp.emision.resultado == true){
          this.catalogosService.guardarPagoQualitas(this.emisionResponse, idAgente.toString(), value.idRegistro).subscribe(
              respuesta => {
                localStorage.setItem('lastPaidSaved', JSON.stringify(respuesta));

                let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
                if (this.emisionResponse.aseguradora === 'GNP'){
                  let card = localStorage.getItem('card');
                  this.emisionResponse.pago.noTarjeta = card;
                }
                this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 7).subscribe(
                    succ => {
                      console.table(_SavedFake);
                    },
                    err => {
                      console.error(err);
                    }
                );

              },
              err => {
                console.error(err);
              });
        }else{

          let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
          this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 9).subscribe(
              succ => {
                console.table(_SavedFake);
              },
              err => {
                console.error(err);
              }
          );

        }
      }
      else{

        /*PROCESO PARA GUARDAR EL PAGO EXITOSO*/
        if(resp.emision.documento !== '' && resp.emision.poliza !== '' && resp.emision.resultado == true){
          let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
          this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 7).subscribe(
              succ => {
                console.log(_SavedFake);
              },
              err => {
                console.error(err);
              }
          );
          this.catalogosService.guardarPagoQualitas(this.emisionResponse, idAgente.toString(), value.idRegistro).subscribe(
              respuesta => {
                localStorage.setItem('lastPaidSaved', JSON.stringify(respuesta));
              },
              err => {
                console.error(err);
              });
        }else{

          let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
          this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse, 9).subscribe(
              succ => {
                console.log(_SavedFake);
              },
              err => {
                console.error(err);
              }
          );
        }
      }
    },
        err => {
          console.error(err);
        });
  }

  private _camelCase(insurance: string): string {
    return insurance.charAt(0).toUpperCase() + insurance.slice(1);
  }

  private _valueCity(type: number, value: string): string {
    // console.log('valor ciudad',type,value)
    return (type === 1) ? value.split('|')[1].trim() : value.split('|')[0].trim();
  }

  private _setParam(): void {
    let _cotizacionSaved, _cotizacionSaved2, _insurance, _cp, _clave, _description, _marca, _model, _coberturasTMP, _cotizacionTMP, _emision, _pP;
    let _tokenAs = localStorage.getItem('tokenAs') || localStorage.getItem('tmpMultiSelected');
    let _mm = localStorage.getItem('tmpMultiSelected');
    let _descuento = JSON.parse(localStorage.getItem('lastCotizacion'));
    this.datosFormularioCot = JSON.parse(localStorage.getItem('datosFormularioCot'));

    
    if (!_mm) {
      this.lastQuotation = this.parseToLower.parseToLower(JSON.parse(JSON.parse(localStorage.getItem('cotizacionSaved')).respuesta));
      let _cotizacion = JSON.parse(localStorage.getItem('tmpQuotation'));
      _cotizacionSaved = _cotizacion; // JSON.parse(_cotizacion.respuesta);
      _coberturasTMP = _cotizacionSaved.Coberturas;
      _cotizacionTMP = _cotizacionSaved.Cotizacion;
      let _date = (this.datos_personales.fechaNacimiento.value) ? this.datos_personales.fechaNacimiento.value : (this.urlType === 'privado') ? _cotizacionSaved.cliente.fechaNacimiento : _cotizacionSaved.Cliente.FechaNacimiento;
      let _birthdate = moment(_date).format('DD/MM/YYYY');
      _cotizacionSaved2 = _cotizacionSaved = {
        edad: (this.urlType === 'privado') ? _cotizacion.cliente.edad : _cotizacion.Cliente.Edad,
        fechaNacimiento: _birthdate,
        genero: (this.urlType === 'privado') ? _cotizacion.cliente.genero : _cotizacion.Cliente.Genero,
        Paquete: (this.urlType === 'privado') ? _cotizacion.paquete : _cotizacion.Paquete,
      }
      _insurance = (this.urlType === 'privado') ? _cotizacion.aseguradora.toUpperCase() : _cotizacion.Aseguradora.toUpperCase(); // this.lastQuotation.aseguradora.toUpperCase();
      _cp = (this.urlType === 'privado') ? _cotizacion.cliente.direccion.codPostal : _cotizacion.Cliente.Direccion.CodPostal; // this.lastQuotation.cliente.direccion.codPostal;
      _clave = (this.urlType === 'privado') ? _cotizacion.vehiculo.clave : _cotizacion.Vehiculo.Clave; // this.lastQuotation.vehiculo.clave;
      _description = (this.urlType === 'privado') ? _cotizacion.vehiculo.descripcion : _cotizacion.Vehiculo.Descripcion; // this.lastQuotation.vehiculo.descripcion;
      _marca = (this.urlType === 'privado') ? _cotizacion.vehiculo.marca : _cotizacion.Vehiculo.Marca; // this.lastQuotation.vehiculo.marca;
      _model = (this.urlType === 'privado') ? _cotizacion.vehiculo.modelo : _cotizacion.Vehiculo.Modelo; // this.lastQuotation.vehiculo.modelo;
      _emision = (this.urlType === 'privado') ? _cotizacion.emision : _cotizacion.Emision; // this.lastQuotation.emision;
      _pP = (this.urlType === 'privado') ? _cotizacion.periodicidadDePago : _cotizacion.PeriodicidadDePago; // this.lastQuotation.periodicidadDePago;
    }
    else {
      let _lQ = JSON.parse(localStorage.getItem('lastCotizacion'));
      _coberturasTMP = _lQ.Coberturas;
      _cotizacionTMP = _lQ.Cotizacion;
      _emision = _lQ.Emision;
      _pP = _lQ.PeriodicidadDePago
      this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
      let _tmp = JSON.parse(localStorage.getItem('tmpMultAskQuotation'));
      let _tmpQ = _tmp.find(el => el.aseguradora.toLowerCase() === _mm.toLowerCase());
      _cotizacionSaved2 = _cotizacionSaved = {
        edad: _tmpQ.edad,
        fechaNacimiento: moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY'),
        genero: _tmpQ.genero,
        Paquete: _tmpQ.paquete,
      }
      _insurance = _mm.toUpperCase();
      let _jT = JSON.parse(this.lastQuotation.peticion);
      _cp = _jT.cp;
      _clave = _jT.clave;
      _description = _jT.descripcion;
      _marca = _jT.marca;
      _model = _jT.modelo;
    }

    let _insuranceToken = (_tokenAs.split('_').join('').toUpperCase() === 'LALATINO') ? 'LATINO' : (_tokenAs.split('_').join('').toUpperCase() === 'GENERALDESEGUROS') ? 'GENERAL' : _tokenAs.split('_').join('').toUpperCase();

    this.emisionRequest = {
      Aseguradora: _insurance,
      Cliente: {
        Nombre: this.datos_personales.nombre.value,
        ApellidoPat: this.datos_personales.aPaterno.value,
        ApellidoMat: this.datos_personales.aMaterno.value,
        CURP: this.datos_personales.curp.value,
        Edad: Number(_cotizacionSaved2.edad),
        Email: this.datos_personales.correo.value,
        FechaNacimiento: (this.datos_personales.fechaNacimiento.value) ? moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY') : '', // _cotizacionSaved2.fechaNacimiento,
        Genero: _cotizacionSaved2.genero, //  === "MASCULINO" ? "M" : "F",
        Ocupacion: '', // *
        RFC: this.datos_personales.rfc.value,
        Telefono: this.datos_personales.telefono.value,
        TipoPersona: 'F', // *
        Direccion: {
          Calle: this.datos_direccion.calleNum.value,
          Ciudad: (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '', // (_cotizacionSaved.Paquete === "Amplia") ? this.datos_direccion.estadoMun.value : this._valueCity(this.datos_direccion.estadoMun.value),
          CodPostal: _cp,
          Colonia: this.datos_direccion.colonia.value,
          NoExt: this.datos_direccion.numero.value,
          NoInt: this.datos_direccion.numInterior.value,
          Pais: '',
          Poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
        }
      },
      Coberturas: _coberturasTMP,
      CodigoError: '',
      // Descuento: GENERALS.discount.find(el => el.insurance.toUpperCase() === _insuranceToken).discount.toString(),
      Descuento:_descuento.descuento,
      Cotizacion: _cotizacionTMP,
      Emision: _emision,
      Paquete: _cotizacionSaved.Paquete.toUpperCase(), // this.lastQuotation.Paquete,
      PeriodicidadDePago: _pP,
      UrlRedireccion: '',
      Pago: {
        AnioExp: (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '',
        Banco: (this.datos_card.banco.value) ? this.datos_card.banco.value : '',
        Carrier: 0,
        CodigoSeguridad: (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '',
        MSI: (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '',
        MedioPago: (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '',
        MesExp: (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '',
        NoClabe: "",
        NoTarjeta: (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '',
        NombreTarjeta: (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '',
      },
      Vehiculo: {
        Clave: _clave,
        CodDescripcion: "",
        CodMarca: "",
        CodUso: "",
        Descripcion: _description,
        Marca: _marca,
        Modelo: _model,
        NoMotor: this.datos_vehiculo.motor.value,
        NoPlacas: this.datos_vehiculo.placa.value,
        NoSerie: this.datos_vehiculo.niv.value,
        Servicio: "PARTICULAR",
        SubMarca: this.datosFormularioCot.descripcion,
        Uso: "PARTICULAR"
      }
    };
  }

  private _setParamRequestCliente(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    if(!this.emisionRequestResp.cliente){this.emisionRequestResp.cliente = _cotizacion.cliente;}
    if(this.emisionRequestResp.cliente){
      this.emisionRequestResp.cliente = _cotizacion.cliente;
      this.emisionRequestResp.cliente.nombre          = this.datos_personales.nombre.value;
      this.emisionRequestResp.cliente.apellidoPat     = this.datos_personales.aPaterno.value;
      this.emisionRequestResp.cliente.apellidoMat     = this.datos_personales.aMaterno.value;
      this.emisionRequestResp.cliente.curp            = this.datos_personales.curp.value;
      this.emisionRequestResp.cliente.email           = this.datos_personales.correo.value;
      this.emisionRequestResp.cliente.fechaNacimiento = this.emisionRequestResp.aseguradora === 'GENERAL_DE_SEGUROS'? this.datos_personales.fechaNacimiento.value: moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY') ;
      this.emisionRequestResp.cliente.rfc             = this.datos_personales.rfc.value;
      this.emisionRequestResp.cliente.telefono        = this.datos_personales.telefono.value;
      this.emisionRequestResp.cliente.beneficiarioPreferente = this.datos_personales.aPaterno.value + ' ' + this.datos_personales.aMaterno.value + ' ' + this.datos_personales.nombre.value;
      this.emisionRequestResp.cliente.tipoPersona = 'FISICA';
    }
  }

  private _setParamRequestDireccion(): void {

    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    // console.log('_cotizacion',_cotizacion);
    if(!this.emisionRequestResp.cliente){this.emisionRequestResp.cliente.direccion = _cotizacion.cliente.direccion;}
    if(this.emisionRequestResp.cliente){
      this.emisionRequestResp.cliente.direccion = _cotizacion.cliente.direccion;
      this.emisionRequestResp.cliente.direccion.calle  = this.datos_direccion.calleNum.value;
      this.emisionRequestResp.cliente.direccion.ciudad     = (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '';
      this.emisionRequestResp.cliente.direccion.codPostal    = _cotizacion.cliente.direccion.codPostal;
      this.emisionRequestResp.cliente.direccion.colonia   =  this.datos_direccion.colonia.value;
      this.emisionRequestResp.cliente.direccion.noExt    = this.datos_direccion.numero.value;
      this.emisionRequestResp.cliente.direccion.noInt    = this.datos_direccion.numInterior.value;
      this.emisionRequestResp.cliente.direccion.pais     = 'Mexico';
      this.emisionRequestResp.cliente.direccion.ciudad   = _cotizacion.aseguradora==='AFIRME'? this.delegacionAfirme :this.emisionRequest.aseguradora === 'generaldeseguros'? this.remove_accents(this._valueCity(2, this.datos_direccion.estadoMun.value) ): this._valueCity(2, this.datos_direccion.estadoMun.value);
      }
  }

  private _setParamRequestVehiculo(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    if(!this.emisionRequestResp.vehiculo){this.emisionRequestResp.vehiculo = _cotizacion.vehiculo;}
    if(this.emisionRequestResp.vehiculo){
      this.emisionRequestResp.vehiculo = _cotizacion.vehiculo;
      this.emisionRequestResp.vehiculo.noMotor   = this.datos_vehiculo.motor.value;
      this.emisionRequestResp.vehiculo.noPlacas  = this.datos_vehiculo.placa.value;
      this.emisionRequestResp.vehiculo.noSerie   = this.datos_vehiculo.niv.value;
      this.emisionRequestResp.vehiculo.servicio  = 'PARTICULAR';
      this.emisionRequestResp.vehiculo.uso       = 'PARTICULAR';
      //this.emisionRequestResp = this.emisionRequest;
    }

  }

  private _setParamRequestPago(): void {
    if(this.emisionRequestResp.pago){
      if(!this.emisionRequestResp.pago){
        this.emisionRequestResp.pago = {
          pago: {
            anioExp: '',
            banco: '',
            carrier: '',
            codigoSeguridad: '',
            msi: '',
            medioPago: '',
            mesExp: '',
            noClabe: '',
            noTarjeta: '',
            nombreTarjeta: '',
          },
        }
      }
      this.emisionRequestResp.pago.anioExp          = (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '';
      this.emisionRequestResp.pago.banco            = (this.datos_card.banco.value) ? this.datos_card.banco.value : '';
      this.emisionRequestResp.pago.carrier          = 0;
      this.emisionRequestResp.pago.codigoSeguridad  = (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '';
      this.emisionRequestResp.pago.msi              = (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '';
      this.emisionRequestResp.pago.medioPago        = (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '';
      this.emisionRequestResp.pago.mesExp           = (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '';
      this.emisionRequestResp.pago.noClabe          = '';
      this.emisionRequestResp.pago.nombreTarjeta    = (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '';
      this.emisionRequestResp.pago.noTarjeta        = (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '';
      if(this.emisionRequestResp.aseguradora === 'GNP' && this.emisionRequestResp.pago.noTarjeta != ''){
        localStorage.setItem('card',this.emisionRequestResp.pago.noTarjeta)
      }
      delete this.emisionRequestResp.pago.pago;
    }



  }

  private _setParamRequestResto(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    this.emisionRequest.periodicidadDePago = localStorage.getItem('payFormSelected');
    let _tokenAs = localStorage.getItem('tokenAs') || localStorage.getItem('tmpMultiSelected');
    let _insuranceToken = (_tokenAs.split('_').join('').toUpperCase() === 'LALATINO') ? 'LATINO' : (_tokenAs.split('_').join('').toUpperCase() === 'GENERALDESEGUROS') ? 'GENERAL' : (_tokenAs.split('_').join('').toUpperCase() === 'PRIMEROSEGUROS') ? 'PRIMERO':_tokenAs.split('_').join('').toUpperCase();
    // this.emisionRequestResp.descuento = GENERALS.discount.find(el => el.insurance.toUpperCase() === _insuranceToken).discount.toString();
    this.emisionRequestResp.descuento = _cotizacion.descuento;
    this.emisionRequestResp.aseguradora = _cotizacion.aseguradora ? '':_cotizacion.aseguradora;
    this.emisionRequestResp.paquete = localStorage.getItem('coverageSelected') ? localStorage.getItem('coverageSelected'):'AMPLIA' ;
    this.emisionRequestResp.emision = typeof _cotizacion.emision !== 'undefined' ? _cotizacion.emision : {};
  }

  //Dehabilitar boton
  actionMethod($event:MouseEvent){
    ($event.target as HTMLButtonElement).disabled=true;
    this.buttonEmisionInteraccion = true;
  }
  datosAtomar:any
  bodyPago:any
  selectCober:any
  recorreCoberturas:any
  recorerCotizacion:any
  formaPago:any;
  idTarjetaCarrier =0;
  laC:any;
  emitir(pay: number): void {
    if(pay == 1 && this.datosPago.status === 'VALID'){
      if(this.datosPago.controls['banco'].value === ''
          || this.datosPago.controls['anioVencimiento'].value === ''
          || this.datosPago.controls['cvv'].value === ''
          || this.datosPago.controls['metodoPago'].value === ''
          || this.datosPago.controls['mesVencimiento'].value === ''
          || this.datosPago.controls['numTarjeta'].value === ''
          || this.datosPago.controls['numTarjeta'].value === ''
          || this.datosPago.controls['tipoTarjeta'].value === ''
          || this.datosPago.controls['formaPago'].value === ''){

        this.buttonEmisionInteraccion = false;

        Swal.fire({
          icon: 'warning',
          title: 'Datos de Pago requeridos!',
          text: ' Para poder realizar el proceso de emisión mediante esta opción es necesario ingresar los datos de pago.',
          footer: 'Gracias!'
        });
        return;
      }
        Swal.fire({
            icon: 'success',
            title: 'Estamos realizando tu transacción, ',
            text: ' te confirmaremos el estatus en el e-mail que tenemos registrado',
            footer: 'Gracias!'
        });
    }else if(pay == 2){
      //TODO
      
      if(this.aseguradoraSelected === "migo"){
      
        this.loaderGo = true;
        
        this.datosAtomar = JSON.parse(localStorage.getItem('lastCotizacion'))
        this.formaPago = localStorage.getItem('payFormSelected')
        this.selectCober = localStorage.getItem('coverageSelected')
        this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
        this.laC = JSON.parse(localStorage.getItem('laC'));

        let _jT = JSON.parse(this.lastQuotation.peticion);
    
    
        if((this.selectCober).toLowerCase() == 'amplia'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][0]['amplia']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][0]['amplia'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'limitada'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][1]['limitada']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][1]['limitada'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'rc'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][2]['rc']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][2]['rc'][0][`${(this.formaPago).toLowerCase()}`]
        }
        //delete this.datosAtomar['emision'].fechaEmision
         console.log('original',this.datosAtomar['emision']);
         console.log('this.datos_direccion------>',this.emisionResponse);
         this.datosAtomar['emision']['fechaEmision'] = '';
         console.log('agregado',this.datosAtomar['emision']);
        this.bodyPago  = {
          aseguradora: this.datosAtomar['aseguradora'],
          cliente: {
            nombre: this.datos_personales.nombre.value,
            apellidoPat: this.datos_personales.aPaterno.value,
            apellidoMat: this.datos_personales.aMaterno.value,
            //beneficiarioPreferente: this.datos_personales.nombre.value + this.datos_personales.aPaterno.value + this.datos_personales.aMaterno.value,
            curp: this.datos_personales.curp.value,
            edad: this.datosAtomar['cliente'].edad,
            email: this.datos_personales.correo.value,
            fechaNacimiento: (this.datos_personales.fechaNacimiento.value) ? moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY') : '',
            genero: this.datosAtomar['cliente'].genero,
            occupacion: '',
            rfc: this.datos_personales.rfc.value,
            telefono: this.datos_personales.telefono.value,
            tipoPersona: 'F',
            direccion: {
              calle: this.datos_direccion.calleNum.value,
              ciudad: (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '',
              codPostal: _jT.cp,
              colonia: this.datos_direccion.colonia.value,
              noExt: this.datos_direccion.numero.value,
              noInt: this.datos_direccion.numInterior.value,
              pais: this.laC.cliente.direccion.pais,
              poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
            }
          },
          coberturas: this.recorreCoberturas,
          codigoError: '',
          descuento:(this.datosAtomar.descuento).toString(),
          cotizacion: this.recorerCotizacion,
          emision: this.datosAtomar['emision'],//TODO
          paquete: localStorage.getItem('coverageSelected').toUpperCase(),
          periodicidadDePago: this.formaPago,
          urlRedireccion: '',
          pago: {
            anioExp: (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '',
            banco: (this.datos_card.banco.value) ? this.datos_card.banco.value : 'Seleccionar un banco',
            carrier: this.idTarjetaCarrier,
            codigoSeguridad: (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '',
            msi: (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '',
            medioPago: (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '',
            mesExp: (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '',
            noClabe: "",
            noTarjeta: (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '',
            nombreTarjeta: (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '',
            referencia: "",
            resultado : false,
          },
          vehiculo: {
            clave: _jT.clave,
            codDescripcion: '',
            codMarca: '',
            codUso: "",
            descripcion: _jT.descripcion,
            marca: _jT.marca,
            modelo: _jT.modelo,
            noMotor: this.datos_vehiculo.motor.value,
            noPlacas: this.datos_vehiculo.placa.value,
            noSerie: this.datos_vehiculo.niv.value,
            servicio: "PARTICULAR",
            subMarca: this.datosAtomar.vehiculo.subMarca,
            uso: "PARTICULAR"
          }
        };


    
        this.catalogosService.postEmisionMigo(this.bodyPago).subscribe(dataEmision => {
          
          console.log('loque regresa',dataEmision);
        }, (errorEmision) => {
          this.loaderGo = false;
          Swal.fire({
            icon: 'error',
            title: 'Oops comunicate con el administrador emision invalida',
          })

        }, 
        () => {
          this.loaderGo = false;
          console.log('todo salio bien');
          Swal.fire({
            icon: 'success',
            title: 'Solicitud exitosa, ',
            text: 'te haremos llegar la póliza a tu e mail registrado con nosotros',
            footer: 'Gracias!'
          }); 
        }
        );
        // console.log('----------si pase');

        //TODO
      }

      if(this.aseguradoraSelected === "la_latino"){
      
        this.loaderGo = true;
        
        this.datosAtomar = JSON.parse(localStorage.getItem('lastCotizacion'))
        this.formaPago = localStorage.getItem('payFormSelected')
        this.selectCober = localStorage.getItem('coverageSelected')
        this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
        this.datosFormularioCot = JSON.parse(localStorage.getItem('datosFormularioCot'));
        this.laC = JSON.parse(localStorage.getItem('laC'));
        let _jT = JSON.parse(this.lastQuotation.peticion);
    
    
        if((this.selectCober).toLowerCase() == 'amplia'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][0]['amplia']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][0]['amplia'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'limitada'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][1]['limitada']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][1]['limitada'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'rc'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][2]['rc']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][2]['rc'][0][`${(this.formaPago).toLowerCase()}`]
        }


        const parts = this.datos_personales.fechaNacimiento.value;
        console.log('parts----------------->',parts);



        //delete this.datosAtomar['emision'].fechaEmision
         console.log('original',this.datosAtomar['emision']);
         this.datosAtomar['emision']['fechaEmision'] = '';
         console.log('agregado',this.datosAtomar['emision']);
        this.bodyPago  = {
          aseguradora: "LaLatino",
          cliente: {
            nombre: this.datos_personales.nombre.value,
            apellidoPat: this.datos_personales.aPaterno.value,
            apellidoMat: this.datos_personales.aMaterno.value,
            //beneficiarioPreferente: this.datos_personales.nombre.value + this.datos_personales.aPaterno.value + this.datos_personales.aMaterno.value,
            curp: this.datos_personales.curp.value,
            edad: this.datosAtomar['cliente'].edad,
            email: this.datos_personales.correo.value,
            fechaNacimiento: this.datos_personales.fechaNacimiento.value,
            genero: this.datosAtomar['cliente'].genero,
            ocupacion: '',
            rfc: this.datos_personales.rfc.value,
            telefono: this.datos_personales.telefono.value,
            tipoPersona: 'F',
            direccion: {
              calle: this.datos_direccion.calleNum.value,
              ciudad: (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '',
              codPostal: _jT.cp,
              colonia: this.colonias[0]['idAsentaCpcons'],
              noExt: this.datos_direccion.numero.value,
              noInt: this.datos_direccion.numInterior.value,
              pais: this.laC.cliente.direccion.pais,
              poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
            }
          },
          coberturas: this.recorreCoberturas,
          codigoError: '',
          descuento:(this.datosAtomar.descuento).toString(),
          cotizacion: this.recorerCotizacion,
          emision: this.datosAtomar['emision'],//TODO
          paquete: localStorage.getItem('coverageSelected').toUpperCase(),
          periodicidadDePago: this.formaPago,
          urlRedireccion: '',
          pago: {
            anioExp: (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '',
            banco: '',//(this.datos_card.banco.value) ? this.datos_card.banco.value : 'Seleccionar un banco',
            carrier: this.idTarjetaCarrier,
            codigoSeguridad: (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '',
            msi: (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '',
            medioPago: (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '',
            mesExp: (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '',
            noClabe: "",
            noTarjeta: (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '',
            nombreTarjeta: (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '',
            referencia: "",
            resultado : false,
          },
          vehiculo: {
            clave: _jT.clave,
            codDescripcion: '',
            codMarca: '',
            codUso: "",
            descripcion: _jT.descripcion,
            marca: _jT.marca,
            modelo: _jT.modelo,
            noMotor: this.datos_vehiculo.motor.value,
            noPlacas: this.datos_vehiculo.placa.value,
            noSerie: this.datos_vehiculo.niv.value,
            servicio: "PARTICULAR",
            subMarca: this.datosFormularioCot.descripcion,
            uso: "PARTICULAR"
          }
        };

        debugger
        console.log('this.bodyPago',this.bodyPago);
        this.catalogosService.postEmisionLatino(this.bodyPago).subscribe(dataEmision => {
          //TODO
          console.log('loque regresa',dataEmision);
        }, (errorEmision) => {
          this.loaderGo = false;
          Swal.fire({
            icon: 'error',
            title: 'Oops comunicate con el administrador emision invalida',
          })

        }, 
        () => {
          this.loaderGo = false;
          console.log('todo salio bien');
          Swal.fire({
            icon: 'success',
            title: 'Solicitud exitosa, ',
            text: 'te haremos llegar la póliza a tu e mail registrado con nosotros',
            footer: 'Gracias!'
        }); 
        }
        );
        // console.log('----------si pase');

        //TODO
      }
      if(this.aseguradoraSelected === "aig"){
      
        this.loaderGo = true;
        
        this.datosAtomar = JSON.parse(localStorage.getItem('lastCotizacion'))
        this.formaPago = localStorage.getItem('payFormSelected')
        this.selectCober = localStorage.getItem('coverageSelected')
        this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
        this.datosFormularioCot = JSON.parse(localStorage.getItem('datosFormularioCot'));
        this.laC = JSON.parse(localStorage.getItem('laC'));
        let _jT = JSON.parse(this.lastQuotation.peticion);
    
        console.log('this.selectCober',this.datosAtomar);
        if((this.selectCober).toLowerCase() == 'amplia'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][1]['amplia']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][1]['amplia'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'limitada'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][2]['limitada']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][2]['limitada'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'rc'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][3]['rc']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][3]['rc'][0][`${(this.formaPago).toLowerCase()}`]
        }else if((this.selectCober).toLowerCase() == 'gold'){
          this.recorreCoberturas = this.datosAtomar['coberturas'][0]['rc']
          this.recorerCotizacion = this.datosAtomar['cotizacion'][0]['rc'][0][`${(this.formaPago).toLowerCase()}`]
        }


        const parts = this.datos_personales.fechaNacimiento.value;
        console.log('parts----------------->',parts);



        //delete this.datosAtomar['emision'].fechaEmision
         console.log('original',this.datosAtomar['emision']);
         this.datosAtomar['emision']['fechaEmision'] = '';
         console.log('agregado',this.datosAtomar['emision']);
        this.bodyPago  = {
          aseguradora: "aig",
          cliente: {
            nombre: this.datos_personales.nombre.value,
            apellidoPat: this.datos_personales.aPaterno.value,
            apellidoMat: this.datos_personales.aMaterno.value,
            //beneficiarioPreferente: this.datos_personales.nombre.value + this.datos_personales.aPaterno.value + this.datos_personales.aMaterno.value,
            curp: this.datos_personales.curp.value,
            edad: this.datosAtomar['cliente'].edad,
            email: this.datos_personales.correo.value,
            fechaNacimiento: this.datos_personales.fechaNacimiento.value,
            genero: this.datosAtomar['cliente'].genero,
            ocupacion: '',
            rfc: this.datos_personales.rfc.value,
            telefono: this.datos_personales.telefono.value,
            tipoPersona: 'F',
            direccion: {
              calle: this.datos_direccion.calleNum.value,
              ciudad: (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '',
              codPostal: _jT.cp,
              colonia: this.colonias[0]['idAsentaCpcons'],
              noExt: this.datos_direccion.numero.value,
              noInt: this.datos_direccion.numInterior.value,
              pais: this.laC.cliente.direccion.pais,
              poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
            }
          },
          coberturas: this.recorreCoberturas,
          codigoError: '',
          descuento:(this.datosAtomar.descuento).toString(),
          cotizacion: this.recorerCotizacion,
          emision: this.datosAtomar['emision'],//TODO
          paquete: localStorage.getItem('coverageSelected').toUpperCase(),
          periodicidadDePago: this.formaPago,
          urlRedireccion: '',
          pago: {
            anioExp: (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '',
            banco: '',//(this.datos_card.banco.value) ? this.datos_card.banco.value : 'Seleccionar un banco',
            carrier: this.idTarjetaCarrier,
            codigoSeguridad: (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '',
            msi: (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '',
            medioPago: (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '',
            mesExp: (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '',
            noClabe: "",
            noTarjeta: (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '',
            nombreTarjeta: (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '',
            referencia: "",
            resultado : false,
          },
          vehiculo: {
            clave: _jT.clave,
            codDescripcion: '',
            codMarca: '',
            codUso: "",
            descripcion: _jT.descripcion,
            marca: _jT.marca,
            modelo: _jT.modelo,
            noMotor: this.datos_vehiculo.motor.value,
            noPlacas: this.datos_vehiculo.placa.value,
            noSerie: this.datos_vehiculo.niv.value,
            servicio: "PARTICULAR",
            subMarca: this.datosFormularioCot.descripcion,
            uso: "PARTICULAR"
          }
        };
       
 
        console.log('this.bodyPago',this.bodyPago);
        this.catalogosService.postEmisionAig(this.bodyPago).subscribe(dataEmision => {
          //TODO
          console.log('loque regresa',dataEmision);
        }, (errorEmision) => {
          this.loaderGo = false;
          Swal.fire({
            icon: 'error',
            title: 'Oops comunicate con el administrador emision invalida',
          })

        }, 
        () => {
          this.loaderGo = false;
          console.log('todo salio bien');
          Swal.fire({
            icon: 'success',
            title: 'Solicitud exitosa, ',
            text: 'te haremos llegar la póliza a tu e mail registrado con nosotros',
            footer: 'Gracias!'
        }); 
        }
        );
        // console.log('----------si pase');

        //TODO
      }

      
      console.log('--------this.aseguradoraSelected',this.aseguradoraSelected);
        if (this.aseguradoraSelected === "GNP") {this.validateCard(pay);}
      if(this.aseguradoraSelected !== "la_latino" && this.aseguradoraSelected !== "migo" &&this.aseguradoraSelected !== "aig"){
        Swal.fire({
            icon: 'success',
            title: 'Solicitud exitosa, ',
            text: 'te haremos llegar la póliza a tu e mail registrado con nosotros',
            footer: 'Gracias!'
        }); 
      }
      
      
      }
    if (this.datosPago.status === 'INVALID'){return;}
    if( this.aseguradoraSelected === "GNP" ){ if(pay == 1){this.continueEmisionGnp(pay);} return; }
    if(this.datosPago.status !== 'VALID'){ return; }
    this.idAgente = jwt_decode(this._token);
    const dataSaveCot = +this.idAgente['sub'].split(':')[1];
    let configAseguradora = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase());
    if(!localStorage.getItem("tmpMultiSelected") && !configAseguradora.allLowerCase){ this.setPaso1to3(); this._setParam(); }
    let _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';
    this.loaderGo = true;


    if(localStorage.getItem('tmpMultiSelected')){ configAseguradora = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === localStorage.getItem('tmpMultiSelected').toUpperCase()); }
    // SE VALIDA QUE SEA AXA CON LA QUE SE COTIZA YA QUE RESPONDE MAL Y SOLO SE GUARDA EN LA BASE DE DATOS , ESTE IF
    // SE DEBE DE QUITAR CUANDO WS CORRIJA EL SERVICIO DE AXA

      if(!localStorage.getItem("tmpMultiSelected") && this.emisionRequest.cotizacion && this.emisionRequest.coberturas){
        this.emisionRequest.cotizacion = this.replaceJsonEmision(this.emisionRequest);
        this.emisionRequest.coberturas = this.replaceJsonCoverage(this.emisionRequest);
      }




      if(this.emisionRequest.periodicidadDePago){
      this.emisionRequest.periodicidadDePago = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'): 'ANUAL';
    }else if(typeof this.emisionRequest.periodicidadDePago === 'undefined'){
      this.emisionRequest.periodicidadDePago = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'): 'ANUAL';
    }

    if(!localStorage.getItem("tmpMultiSelected") && !configAseguradora.allLowerCase) {
      if (configAseguradora.useLowerCase) {
        this.emisionRequest = this.toCamel(this.emisionRequest);
        this.deleteJsonUperCase();
        if (localStorage.getItem('tmpMultiSelected')) {
          this.emisionRequest.cotizacion = this.toCamel(this.emisionRequest.cotizacion)
        }
      }
    }

    if(!localStorage.getItem("tmpMultiSelected") && !configAseguradora.allLowerCase) {
        if (!this.emisionRequest.Cotizacion) {
          this.emisionRequestResp.cotizacion = this.emisionRequest.cotizacion;
          this.emisionRequestResp.coberturas = this.emisionRequest.coberturas;
          this.emisionRequestResp.periodicidadDePago = this.emisionRequest.periodicidadDePago;
        }
        // else {
        //   this.emisionRequestResp.cotizacion = this.emisionRequest.Cotizacion;
        //   this.emisionRequestResp.coberturas = this.emisionRequest.Coberturas;
        //   this.emisionRequestResp.periodicidadDePago = this.emisionRequest.PeriodicidadDePago;
        // }
    }

    this.emisionRequest = this.emisionRequestResp;
    this.emisionRequest = this.configurationByInsurer(this.emisionRequest);
    localStorage.setItem('tmpQuotation',JSON.stringify(this.emisionRequest));

        this.emisionRequest.cliente.direccion.poblacion = this.emisionRequest.cliente.direccion.poblacion.toString().replace('é','e').replace('í','i').replace('ú','u').replace('ó','o').replace('á','a');

        if(this.emisionRequest.aseguradora === ''){ if(localStorage.getItem('tokenAs')){ this.emisionRequest.aseguradora = localStorage.getItem('tokenAs').toUpperCase();}
          else if(localStorage.getItem('tmpMultiSelected')){ this.emisionRequest.aseguradora = localStorage.getItem('tmpMultiSelected').toUpperCase(); }
        }

      if(typeof this.emisionRequest.emision.iDCotizacion !== 'undefined'){ delete this.emisionRequest.emision.iDCotizacion;  this.emisionRequest.emision.idCotizacion = '';}
      if(this.emisionRequest.Pago){ this.emisionRequest.pago = this.toCamel(this.emisionRequest.Pago); delete this.emisionRequest.Pago;}

      this.catalogosService.emitir(this.emisionRequest, _cotizacionSaved).subscribe(
        resp => {
          // resp = this.pruebaEmision;
          let responseEmision = resp;
          this.loaderGo = false;
          // console.log('responseEmision',responseEmision);

          if(configAseguradora.external_terminal){

            /* Se guarda la emision*/
            let payForm = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'):'ANUAL';
            responseEmision.cliente.fechaNacimiento = this.changeFormartBirthdate(responseEmision.cliente.fechaNacimiento);
            this.catalogosService.emitir_Save(responseEmision, dataSaveCot.toString(), _cotizacionSaved.id, payForm).subscribe(
                respuesta => { localStorage.setItem('lastEmisionSaved', JSON.stringify(respuesta)); },
                err => { console.error(err); });

            if(responseEmision.emision){
              if(responseEmision.emision.terminal) {
                this.loadExternalTerminal = true;
                localStorage.setItem('lastEmision', JSON.stringify(responseEmision));
                let _url = new URL(responseEmision.emision.terminal);
                let _cianne = (this.aseguradoraSelected.toUpperCase() === 'GNP') ? _url.searchParams.get("IdT") : '0';
                this.externalTerminalURL = responseEmision.emision.terminal + environment.base_url + 'admin/cotiza-go/terminal/' + _cianne; // + GENERALS.terminal_resp;


                /*MIGO NO DEBE REALIZAR LA REDIRECCION A LA REMINAL VIRTUAL*/
                if(responseEmision.aseguradora.toUpperCase() !== 'MIGO'){
                  if (responseEmision.emision.documento)
                    FileSaver.saveAs(responseEmision.emision.documento);
                  window.open(this.externalTerminalURL, "_blank");
                  this.router.navigateByUrl('admin/dashboard');
                }else if(responseEmision.aseguradora.toUpperCase() === 'MIGO'){
                  Swal.fire({
                    icon: 'success',
                    title: 'La emisión se ha realizado exitosamente, No olvides realizar el cobro de la misma.!',
                    text: resp.CodigoError,
                    footer: ''
                  });
                }


              }


            }


          }
          else {

            if(pay == 0){
                  if (typeof responseEmision.CodigoError !== 'undefined') {
                    Swal.fire({
                      icon: 'success',
                      title: 'La emisión se ha realizado exitosamente, No olvides realizar el cobro de la misma.!',
                      text: resp.CodigoError,
                      footer: ''
                    });
                  }else{

                    Swal.fire({
                      icon: 'success',
                      title: 'La emisión se ha realizado exitosamente, No olvides realizar el cobro de la misma .!',
                      text: resp.codigoError,
                      footer: ''
                    });

                    /*Si la aseguradora emite y cobra en un solo proceso*/
                    let payForm = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'):'ANUAL';
                    this.emisionResponse = responseEmision;
                    responseEmision.cliente.fechaNacimiento = this.changeFormartBirthdate(responseEmision.cliente.fechaNacimiento);
                    this.catalogosService.emitir_Save(this.emisionResponse, dataSaveCot.toString(), _cotizacionSaved.id, payForm).subscribe(
                        respuesta => {
                          localStorage.setItem('lastEmisionSaved', JSON.stringify(respuesta));
                        },
                        err => {
                          console.error(err);
                        });
                  }

            }
            else{


              console.log("top-end => Emisión - OK")
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Emisión - OK!',
                showConfirmButton: false,
                timer: 1500
              });

              this.emisionResponse = responseEmision;
              localStorage.setItem('lastEmision', JSON.stringify(responseEmision));
              let payForm = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'):'ANUAL';


              let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
              this.cotizacionService.updateFake(_SavedFake.id, this.emisionResponse,9).subscribe(
                  succ => { console.log("OK");},
                  err => {
                    console.error(err);
                  }
              );



              // if(responseEmision.Emision){
              //   if(responseEmision.Emision.Poliza !== ''){
              //     this.emisionWithoutPaid = true;
              //     this.poliza = responseEmision.Emision.Poliza;
              //     if(responseEmision.Emision.Documento !== ''){ this.documentos = responseEmision.Emision.Documento.split('|');}
              //   }
              // }else
                if(responseEmision.emision){
                if(responseEmision.emision.poliza !== ''){
                  this.emisionWithoutPaid = true;
                  if(responseEmision.aseguradora==='GENERAL_DE_SEGUROS'){
                    let polizaGS = responseEmision.emision.poliza.split(' ',1);
                    let polizaSplit = polizaGS[0].split('-');
                    this.poliza=polizaSplit[1];
                    responseEmision.emision.poliza=this.poliza;
                    console.log('this.poliza', responseEmision.emision.poliza)
                  }else{
                    this.poliza = responseEmision.emision.poliza;
                  }
                  if(responseEmision.emision.documento !== ''){ this.documentos = responseEmision.emision.documento.split('|');}
                  if(responseEmision.emision.terminal !== ''){
                    if(responseEmision.aseguradora==='ABA'){
                      this.terminalExterna = responseEmision.emision.terminal.replace('http','https');
                    }
                    this.terminalExterna = responseEmision.emision.terminal.split('|');}
                }
              }




              /*ESTA ES LA PARTE PARA EMISION Y PAGO POR SEPARADO - RAZON POR LA CUAL SE DETONA EL SERVICIO DE PAGO DESDE EL FRONT*/
              responseEmision.cliente.fechaNacimiento = this.changeFormartBirthdate(responseEmision.cliente.fechaNacimiento);
              this.catalogosService.emitir_Save(this.emisionResponse, dataSaveCot.toString(), _cotizacionSaved.id, payForm).subscribe(
                  respuesta => {
                    localStorage.setItem('lastEmisionSaved', JSON.stringify(respuesta));
                    if (pay == 1)
                      if(configAseguradora.paymentTwoSteps){
                        this.pago(respuesta, this.emisionResponse);
                      }else{

                        /*PARTE PARA EMITIR Y GUARDAR EN UN SOLO PASO*/
                        console.log("EMISION Y PAGO EN UN SOLO SERVICIO")
                      }
                  },
                  err => {
                    console.error(err);
                  });
            }
          }
        },
        err => {
                  this.loaderGo = false;
                  let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
                  if(err.status === 400 || err.status === '400'){
                    this.emisionRequest = err.error;
                    this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequest,8).subscribe(
                        succ => { console.log("OK");},
                        err => { console.error(err); }
                    );
                  }

                  Swal.fire({
                    icon: 'warning',
                    title: 'Error en el proceso de emision, favor de validar los datos.',
                    // text: JSON.stringify(err),
                    footer: ''
                  });
                });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  getColonias(cp: any): void {

    this.catalogosService.getColonias(cp).subscribe(
      data => {
        console.log('DATA', data);
        this.colonias = data;
        console.log('Data colonia',data)
        this.estado = data[0].estado;
        this.delMun = data[0].delMun;
        this.datosDireccion.controls['estadoMun'].setValue(this.estado + ' | ' + this.delMun);
      },
      err => {
        console.error(err);
      }
    );
  }

  // getColoniasMigo(cp: any): void {
  //   this.catalogosService.getColoniasMigo(cp).subscribe(
  //       data => {
  //         this.colonias = data;
  //         this.delMun = data[0].descripcion;
  //         this.codigoPostal = data[0].codigoPostal;
  //       },
  //       err => {
  //         console.error(err);
  //       }
  //   );
  // }

  getBancosGNP():void{
    this.gnpPago3DService.getBancos().subscribe(
        data =>{
          if(typeof data.CATALOGO !== 'undefined'){
            this.bancos = data.CATALOGO;
          }else {
            this.bancos = [];
          }
    },
      err => {
          console.log("error al traer el catalogo de los bancos")

    }
    );
  }

  getTerjetaValidaGNP():void{
    this.gnpPago3DService.getTipoTargetaValida().subscribe(
        resp =>{
          if(typeof resp.CATALOGO !== 'undefined'){
            this.tarjetasGnp = resp.CATALOGO;
          }else {
            this.tarjetasGnp = resp;
          }
        },
        err => {
          console.log("error al traer el catalogo de tarjetas")

        }
    );
  }

  setPaso1to3() {
    let day = moment(this.datosPersonales.controls['fechaNacimiento'].value).format('D');
    let month = moment(this.datosPersonales.controls['fechaNacimiento'].value).format('M');
    let year = moment(this.datosPersonales.controls['fechaNacimiento'].value).get('year');
    // this.emisionRequest = {
    //   Cliente: {
    //     Nombre: this.datosPersonales.controls['nombre'].value,
    //     Telefono: this.datosPersonales.controls['telefono'].value,
    //     ApellidoPat: this.datosPersonales.controls['aPaterno'].value,
    //     ApellidoMat: this.datosPersonales.controls['aMaterno'].value,
    //     Email: this.datosPersonales.controls['correo'].value,
    //     RFC: this.datosPersonales.controls['rfc'].value,
    //     CURP: this.datosPersonales.controls['curp'].value,
    //     FechaNacimiento: day + '/' + month + '/' + year,
    //     Direccion: {
    //       Calle: this.datosDireccion.controls['calleNum'].value,
    //       Ciudad: this.delMun,
    //       Colonia: this.datosDireccion.controls['colonia'].value,
    //       NoExt: this.datosDireccion.controls['numero'].value,
    //       NoInt: this.datosDireccion.controls['numInterior'].value,
    //       Pais: 'México',
    //       Poblacion: this.estado.toString().replace('é','e').replace('í','i').replace('ú','u').replace('ó','o').replace('á','a'),
    //     }
    //   },
    //   Vehiculo: {
    //     NoPlacas: this.datosVehiculo.controls['motor'].value,
    //     NoMotor: this.datosVehiculo.controls['motor'].value,
    //     NoSerie: this.datosVehiculo.controls['niv'].value,
    //   }
    // };
  }

  // setPaso4() {
    // this.emisionRequest = {
    //   Pago: {
    //     Banco: this.datosPago.controls['banco'].value,
    //     AnioExp: this.datosPago.controls['anioVencimiento'].value,
    //     CodigoSeguridad: this.datosPago.controls['cvv'].value,
    //     MedioPago: this.datosPago.controls['metodoPago'].value,
    //     MesExp: this.datosPago.controls['mesVencimiento'].value,
    //     NoTarjeta: this.datosPago.controls['numTarjeta'].value,
    //     NombreTarjeta: this.datosPago.controls['nombreTarjeta'].value
    //   }
    // };
  // }

  setOptPay(event: any): void {
    this.optPay = [];
    if(this.aseguradoraSelected.toUpperCase() !== 'GNP'){
      if (event.value === 'DEBITO') {
        this.optPay.push({ label: 'Contado', value: '1' });
      } else {
        this.optPay.push({ label: 'Contado', value: '1' });
        this.optPay.push({ label: '3 Meses', value: '3MSI' });
        this.optPay.push({ label: '6 Meses', value: '6MSI' });
        this.optPay.push({ label: '12 Meses', value: '12MSI' });
      }
    }else{
      this.optPay.push({ label: 'Contado', value: '1' });
    }

    this.datosPago.controls['formaPago'].enable();
    if(this.aseguradoraSelected.toUpperCase() === 'GNP'){ this.getPlanPay();}
  }

  getPlanPay():void{
    if(this.aseguradoraSelected !== 'GNP'){ return;}
    this.optPay = [];
    this.optPay.push({ label: 'Contado', value: '1' });
    if(localStorage.getItem("payFormSelected")) {
      if(localStorage.getItem("payFormSelected") === 'ANUAL') {
          this.gnpPago3DService.getPayPans(this.datosPago.controls['numTarjeta'].value.toString().substring(0, 6).toString()).subscribe(resp => {
              if (typeof resp.ELEMENTO !== 'undefined') {
                for (let i = 0; i < resp.ELEMENTO.length; i++) {
                  this.optPay.push({label: resp.ELEMENTO[i].VALOR, value: resp.ELEMENTO[i].CLAVE});
                }
              } else {
                for (let i = 0; i < resp.length; i++) {
                  this.optPay.push({label: resp[i].VALOR, value: resp[i].CLAVE});
                }
              }
          },
          err => {

            console.log(err);
          });
      }
    }
  }

  fechaActual: any[] = [];
  mesBanco   : any[] = [];
  calcularDiaActual() {
    for (let j = 1; j <= 12; j++) {
      this.mesBanco.push(j);
    }
  }

  calcularAnioActual(mes){
    this.fechaActual = []
    if(mes <= (new Date().getMonth() +  1)){
      console.log('aqui entro')
      // se genera el año actual mas 10 años
      for(let i=new Date().getFullYear() + 1; i <= new Date().getFullYear() + 11 ; i++){
        this.fechaActual.push(i);
      }
    }else{
      console.log('aqui entro2')
      // se genera el año actual mas 10 años
      for(let i=new Date().getFullYear(); i <= new Date().getFullYear() + 11 ; i++){
        this.fechaActual.push(i);
      }
    }
  }

  validateCard(pay: number):void{
    if(this.aseguradoraSelected !=="GNP"){ return; }
    //if(!localStorage.getItem("tmpMultiSelected")){ this.setPaso1to3(); this._setParam(); }
    let emisionRequestBack = this.emisionRequest;
    if(!localStorage.getItem("tmpMultiSelected")){
      // if(emisionRequestBack.Aseguradora){
      //   emisionRequestBack.Cotizacion = this.replaceJsonEmision(emisionRequestBack);
      //   emisionRequestBack.Coberturas = this.replaceJsonCoverage(emisionRequestBack);
      // }else{
        emisionRequestBack.cotizacion = this.replaceJsonEmision(emisionRequestBack);
        emisionRequestBack.coberturas = this.replaceJsonCoverage(emisionRequestBack);
      // }

    }
    // if(typeof emisionRequestBack.Aseguradora !== 'undefined'){ if(emisionRequestBack.Aseguradora ===''){ emisionRequestBack.Aseguradora = this.aseguradoraSelected.toUpperCase(); }}
    // else { emisionRequestBack.aseguradora = this.aseguradoraSelected.toUpperCase(); }
    emisionRequestBack.aseguradora = this.aseguradoraSelected.toUpperCase();
    let configAseguradora = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === this.aseguradoraSelected.toUpperCase());
    if(localStorage.getItem('tmpMultiSelected')){ configAseguradora = GENERALS.particual_quote.find(el => el.insurance_carrier.toUpperCase() === localStorage.getItem('tmpMultiSelected').toUpperCase()); }
    emisionRequestBack.PeriodicidadDePago = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'): 'ANUAL';

    if(!localStorage.getItem("tmpMultiSelected")){
      if (configAseguradora.useLowerCase){
        //emisionRequestBack = this.toCamel(emisionRequestBack);
        //this.deleteJsonUperCase();
        if(localStorage.getItem('tmpMultiSelected')){
          //emisionRequestBack.cotizacion = this.toCamel(emisionRequestBack.cotizacion)
        }
      }
    }


    emisionRequestBack = this.configurationByInsurer(emisionRequestBack);
    if(typeof emisionRequestBack.emision.iDCotizacion !== 'undefined'){ delete emisionRequestBack.emision.iDCotizacion;}
    emisionRequestBack.emision.idCotizacion = '';
    this.emisionRequestGnp = emisionRequestBack;

    this.gnpPago3DService.validateCard(emisionRequestBack).subscribe(resp => { emisionRequestBack=resp; this.emisionRequestGnp = resp },
        err =>{ this.emisionRequestGnp = emisionRequestBack;  console.log(err); });

    if(pay == 2){this.continueEmisionGnp(pay)};
  }

  setCardType(event: any): void {

    this.datosPago.controls['tipoTarjeta'].setValue(event.target.value);
  }

  updateData(idEstadoEmision: number): void {
    this._setParamRequestCliente();
    this._setParamRequestDireccion();
    this._setParamRequestVehiculo();
    this._setParamRequestPago();
    this._setParamRequestResto();
    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    if(localStorage.getItem("tokenAs")){
      this.emisionRequestResp.aseguradora = localStorage.getItem("tokenAs").toUpperCase();
    }else if(localStorage.getItem("tmpMultiSelected")){
      this.emisionRequestResp.aseguradora = localStorage.getItem("tmpMultiSelected").toUpperCase();
    }

    /*Logica para que cambia el estatus a 5 si y solo si todos los datos del pago estan registrados*/
    if(idEstadoEmision == 5 ){
      if(this.datosPago.controls['banco'].value === ''
          || this.datosPago.controls['anioVencimiento'].value === ''
          || this.datosPago.controls['cvv'].value === ''
          || this.datosPago.controls['metodoPago'].value === ''
          || this.datosPago.controls['mesVencimiento'].value === ''
          || this.datosPago.controls['numTarjeta'].value === ''
          || this.datosPago.controls['numTarjeta'].value === ''
          || this.datosPago.controls['tipoTarjeta'].value === ''
          || this.datosPago.controls['formaPago'].value === ''){

        idEstadoEmision = 4;

      }else {
        idEstadoEmision = 5;
      }

    }
    if (this.emisionRequestResp.aseguradora === 'GNP'){
      let card = localStorage.getItem('card');
      this.emisionRequestResp.pago.noTarjeta = card;
    }
    this.datosFormularioCot = JSON.parse(localStorage.getItem('datosFormularioCot'));
    this.emisionRequestResp.vehiculo.subMarca = this.datosFormularioCot.descripcion
    this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequestResp, idEstadoEmision).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        }
      );

  }

  validDataVehiculo():void{
    if(this.datosVehiculo.status === 'VALID'){
      this.actionEmiterDataVehiculo = 'true';
    }
  }

  showMessage():void{
    Swal.fire({
      icon: 'success',
      title: 'Solicitud  exitosa, ',
      text: ' confirma los datos para el cobro.',
      footer: 'Gracias!'
    });
  }

  numberOnly(event: any): boolean {
    return this.onlyNumber.validateInput(event);
  }

  back(): void {
    this.router.navigateByUrl('admin/cotiza-go/multicotizador/1');
  }

  replaceJsonEmision(emisionRequest: any):any {

    let periodicity = (localStorage.getItem('coverageSelected'))?localStorage.getItem('coverageSelected').toLowerCase():'amplia';
    let frecuencyPay = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected').toLowerCase():'anual';
    let coverages = (localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):[];
    if(localStorage.getItem('tmpMultiSelected')){ return (emisionRequest.Cotizacion)?emisionRequest.Cotizacion:emisionRequest.cotizacion ; }
    if (typeof coverages === "string") { coverages = JSON.parse(coverages).cotizacion;}
    let coverage: any[] = [];
    let _return: any[] = [];
    switch (periodicity) {
      default:
      case 'amplia':

        if(frecuencyPay === 'anual') {
          if (coverages[0].amplia) {
            if (coverages[0].amplia[0].anual) {
              coverage = coverages[0].amplia[0].anual;
            } else if (coverages[0].amplia[1].anual) {
              coverage = coverages[0].amplia[1].anual;
            } else if (coverages[0].amplia[2].anual) {
              coverage = coverages[0].amplia[1].anual;
            }
          } else if (coverages[1].amplia) {
            if (coverages[1].amplia[0].anual) {
              coverage = coverages[1].amplia[0].anual;
            } else if (coverages[1].amplia[1].anual) {
              coverage = coverages[1].amplia[1].anual;
            } else if (coverages[1].amplia[2].anual) {
              coverage = coverages[1].amplia[1].anual;
            }
          } else {
            if (coverages[2].amplia[0].anual) {
              coverage = coverages[2].amplia[0].anual;
            } else if (coverages[2].amplia[1].anual) {
              coverage = coverages[2].amplia[1].anual;
            } else if (coverages[2].amplia[2].anual) {
              coverage = coverages[2].amplia[1].anual;
            }
          }
        }


        if(frecuencyPay === 'semestral') {
          if (coverages[0].amplia) {
            if (coverages[0].amplia[0].semestral) {
              coverage = coverages[0].amplia[0].semestral;
            } else if (coverages[0].amplia[1].semestral) {
              coverage = coverages[0].amplia[1].semestral;
            } else if (coverages[0].amplia[2].semestral) {
              coverage = coverages[0].amplia[1].semestral;
            }
          } else if (coverages[1].amplia) {
            if (coverages[1].amplia[0].semestral) {
              coverage = coverages[1].amplia[0].semestral;
            } else if (coverages[1].amplia[1].semestral) {
              coverage = coverages[1].amplia[1].semestral;
            } else if (coverages[1].amplia[2].semestral) {
              coverage = coverages[1].amplia[1].semestral;
            }
          } else {
            if (coverages[2].amplia[0].semestral) {
              coverage = coverages[2].amplia[0].semestral;
            } else if (coverages[2].amplia[1].semestral) {
              coverage = coverages[2].amplia[1].semestral;
            } else if (coverages[2].amplia[2].semestral) {
              coverage = coverages[2].amplia[1].semestral;
            }
          }
        }


        if(frecuencyPay === 'trimestral') {
          if (coverages[0].amplia) {
            if (coverages[0].amplia[0].trimestral) {
              coverage = coverages[0].amplia[0].trimestral;
            } else if (coverages[0].amplia[1].trimestral) {
              coverage = coverages[0].amplia[1].trimestral;
            } else if (coverages[0].amplia[2].trimestral) {
              coverage = coverages[0].amplia[1].trimestral;
            }
          } else if (coverages[1].amplia) {
            if (coverages[1].amplia[0].trimestral) {
              coverage = coverages[1].amplia[0].trimestral;
            } else if (coverages[1].amplia[1].trimestral) {
              coverage = coverages[1].amplia[1].trimestral;
            } else if (coverages[1].amplia[2].trimestral) {
              coverage = coverages[1].amplia[1].trimestral;
            }
          } else {
            if (coverages[2].amplia[0].trimestral) {
              coverage = coverages[2].amplia[0].trimestral;
            } else if (coverages[2].amplia[1].trimestral) {
              coverage = coverages[2].amplia[1].trimestral;
            } else if (coverages[2].amplia[2].trimestral) {
              coverage = coverages[2].amplia[1].trimestral;
            }
          }

        }


        if(frecuencyPay === 'mensual') {
          if (coverages[0].amplia) {
            if (coverages[0].amplia[0].mensual) {
              coverage = coverages[0].amplia[0].mensual;
            } else if (coverages[0].amplia[1].mensual) {
              coverage = coverages[0].amplia[1].mensual;
            } else if (coverages[0].amplia[2].mensual) {
              coverage = coverages[0].amplia[1].mensual;
            }
          } else if (coverages[1].amplia) {
            if (coverages[1].amplia[0].mensual) {
              coverage = coverages[1].amplia[0].mensual;
            } else if (coverages[1].amplia[1].mensual) {
              coverage = coverages[1].amplia[1].mensual;
            } else if (coverages[1].amplia[2].mensual) {
              coverage = coverages[1].amplia[1].mensual;
            }
          } else {
            if (coverages[2].amplia[0].mensual) {
              coverage = coverages[2].amplia[0].mensual;
            } else if (coverages[2].amplia[1].mensual) {
              coverage = coverages[2].amplia[1].mensual;
            } else if (coverages[2].amplia[2].mensual) {
              coverage = coverages[2].amplia[1].mensual;
            }
          }
        }

        break;
      case 'limitada':

        if(frecuencyPay === 'anual') {
          if (coverages[0].limitada) {
            if (coverages[0].limitada[0].anual) {
              coverage = coverages[0].limitada[0].anual;
            } else if (coverages[0].limitada[1].anual) {
              coverage = coverages[0].limitada[1].anual;
            } else if (coverages[0].limitada[2].anual) {
              coverage = coverages[0].limitada[1].anual;
            }
          } else if (coverages[1].limitada) {
            if (coverages[1].limitada[0].anual) {
              coverage = coverages[1].limitada[0].anual;
            } else if (coverages[1].limitada[1].anual) {
              coverage = coverages[1].limitada[1].anual;
            } else if (coverages[1].limitada[2].anual) {
              coverage = coverages[1].limitada[1].anual;
            }
          } else {
            if (coverages[2].limitada[0].anual) {
              coverage = coverages[2].limitada[0].anual;
            } else if (coverages[2].limitada[1].anual) {
              coverage = coverages[2].limitada[1].anual;
            } else if (coverages[2].limitada[2].anual) {
              coverage = coverages[2].limitada[1].anual;
            }
          }
        }

        if(frecuencyPay === 'semestral') {
          if (coverages[0].limitada) {
            if (coverages[0].limitada[0].semestral) {
              coverage = coverages[0].limitada[0].semestral;
            } else if (coverages[0].limitada[1].semestral) {
              coverage = coverages[0].limitada[1].semestral;
            } else if (coverages[0].limitada[2].semestral) {
              coverage = coverages[0].limitada[1].semestral;
            }
          } else if (coverages[1].limitada) {
            if (coverages[1].limitada[0].semestral) {
              coverage = coverages[1].limitada[0].semestral;
            } else if (coverages[1].limitada[1].semestral) {
              coverage = coverages[1].limitada[1].semestral;
            } else if (coverages[1].limitada[2].semestral) {
              coverage = coverages[1].limitada[1].semestral;
            }
          } else {
            if (coverages[2].limitada[0].semestral) {
              coverage = coverages[2].limitada[0].semestral;
            } else if (coverages[2].limitada[1].semestral) {
              coverage = coverages[2].limitada[1].semestral;
            } else if (coverages[2].limitada[2].semestral) {
              coverage = coverages[2].limitada[1].semestral;
            }
          }
        }

        if(frecuencyPay === 'trimestral') {
          if (coverages[0].limitada) {
            if (coverages[0].limitada[0].trimestral) {
              coverage = coverages[0].limitada[0].trimestral;
            } else if (coverages[0].limitada[1].trimestral) {
              coverage = coverages[0].limitada[1].trimestral;
            } else if (coverages[0].limitada[2].trimestral) {
              coverage = coverages[0].limitada[1].trimestral;
            }
          } else if (coverages[1].limitada) {
            if (coverages[1].limitada[0].trimestral) {
              coverage = coverages[1].limitada[0].trimestral;
            } else if (coverages[1].limitada[1].trimestral) {
              coverage = coverages[1].limitada[1].trimestral;
            } else if (coverages[1].limitada[2].trimestral) {
              coverage = coverages[1].limitada[1].trimestral;
            }
          } else {
            if (coverages[2].limitada[0].trimestral) {
              coverage = coverages[2].limitada[0].trimestral;
            } else if (coverages[2].limitada[1].trimestral) {
              coverage = coverages[2].limitada[1].trimestral;
            } else if (coverages[2].limitada[2].trimestral) {
              coverage = coverages[2].limitada[1].trimestral;
            }
          }
        }

        if(frecuencyPay === 'mensual') {
          if (coverages[0].limitada) {
            if (coverages[0].limitada[0].mensual) {
              coverage = coverages[0].limitada[0].mensual;
            } else if (coverages[0].limitada[1].mensual) {
              coverage = coverages[0].limitada[1].mensual;
            } else if (coverages[0].limitada[2].mensual) {
              coverage = coverages[0].limitada[1].mensual;
            }
          } else if (coverages[1].limitada) {
            if (coverages[1].limitada[0].mensual) {
              coverage = coverages[1].limitada[0].mensual;
            } else if (coverages[1].limitada[1].mensual) {
              coverage = coverages[1].limitada[1].mensual;
            } else if (coverages[1].limitada[2].mensual) {
              coverage = coverages[1].limitada[1].mensual;
            }
          } else {
            if (coverages[2].limitada[0].mensual) {
              coverage = coverages[2].limitada[0].mensual;
            } else if (coverages[2].limitada[1].mensual) {
              coverage = coverages[2].limitada[1].mensual;
            } else if (coverages[2].limitada[2].mensual) {
              coverage = coverages[2].limitada[1].mensual;
            }
          }
        }


        break;
      case 'rc':

        if(frecuencyPay === 'anual') {
          if (coverages[0].rc) {
            if (coverages[0].rc[0].anual) {
              coverage = coverage = coverages[0].rc[0].anual;
            } else if (coverages[0].rc[1].anual) {
              coverage = coverages[0].rc[1].anual;
            } else if (coverages[0].rc[2].anual) {
              coverage = coverages[0].rc[1].anual;
            }
          } else if (coverages[1].rc) {
            if (coverages[1].rc[0].anual) {
              coverage = coverages[1].rc[0].anual;
            } else if (coverages[1].rc[1].anual) {
              coverage = coverages[1].rc[1].anual;
            } else if (coverages[1].rc[2].anual) {
              coverage = coverages[1].rc[1].anual;
            }
          } else {
            if (coverages[2].rc[0].anual) {
              coverage = coverages[2].rc[0].anual;
            } else if (coverages[2].rc[1].anual) {
              coverage = coverages[2].rc[1].anual;
            } else if (coverages[2].rc[2].anual) {
              coverage = coverages[2].rc[1].anual;
            }
          }
        }


        if(frecuencyPay === 'semestral') {
          if (coverages[0].rc) {
            if (coverages[0].rc[0].semestral) {
              coverage = coverages[0].rc[0].semestral;
            } else if (coverages[0].rc[1].semestral) {
              coverage = coverages[0].rc[1].semestral;
            } else if (coverages[0].rc[2].semestral) {
              coverage = coverages[0].rc[1].semestral;
            }
          } else if (coverages[1].rc) {
            if (coverages[1].rc[0].semestral) {
              coverage = coverages[1].rc[0].semestral;
            } else if (coverages[1].rc[1].semestral) {
              coverage = coverages[1].rc[1].semestral;
            } else if (coverages[1].rc[2].semestral) {
              coverage = coverages[1].rc[1].semestral;
            }
          } else {
            if (coverages[2].rc[0].semestral) {
              coverage = coverages[2].rc[0].semestral;
            } else if (coverages[2].rc[1].semestral) {
              coverage = coverages[2].rc[1].semestral;
            } else if (coverages[2].rc[2].semestral) {
              coverage = coverages[2].rc[1].semestral;
            }
          }
        }


        if(frecuencyPay === 'trimestral') {
          if (coverages[0].rc) {
            if (coverages[0].rc[0].trimestral) {
              coverage = coverages[0].rc[0].trimestral;
            } else if (coverages[0].rc[1].trimestral) {
              coverage = coverages[0].rc[1].trimestral;
            } else if (coverages[0].rc[2].trimestral) {
              coverage = coverages[0].rc[1].trimestral;
            }
          } else if (coverages[1].rc) {
            if (coverages[1].rc[0].trimestral) {
              coverage = coverages[1].rc[0].trimestral;
            } else if (coverages[1].rc[1].trimestral) {
              coverage = coverages[1].rc[1].trimestral;
            } else if (coverages[1].rc[2].trimestral) {
              coverage = coverages[1].rc[1].trimestral;
            }
          } else {
            if (coverages[2].rc[0].trimestral) {
              coverage = coverages[2].rc[0].trimestral;
            } else if (coverages[2].rc[1].trimestral) {
              coverage = coverages[2].rc[1].trimestral;
            } else if (coverages[2].rc[2].trimestral) {
              coverage = coverages[2].rc[1].trimestral;
            }
          }
        }



        if(frecuencyPay === 'mensual') {
          if (coverages[0].rc) {
            if (coverages[0].rc[0].mensual) {
              coverage = coverages[0].rc[0].mensual;
            } else if (coverages[0].rc[1].mensual) {
              coverage = coverages[0].rc[1].mensual;
            } else if (coverages[0].rc[2].mensual) {
              coverage = coverages[0].rc[1].mensual;
            }
          } else if (coverages[1].rc) {
            if (coverages[1].rc[0].mensual) {
              coverage = coverages[1].rc[0].mensual;
            } else if (coverages[1].rc[1].mensual) {
              coverage = coverages[1].rc[1].mensual;
            } else if (coverages[1].rc[2].mensual) {
              coverage = coverages[1].rc[1].mensual;
            }
          } else {
            if (coverages[2].rc[0].mensual) {
              coverage = coverages[2].rc[0].mensual;
            } else if (coverages[2].rc[1].mensual) {
              coverage = coverages[2].rc[1].mensual;
            } else if (coverages[2].rc[2].mensual) {
              coverage = coverages[2].rc[1].mensual;
            }
          }
        }



        break;
    }

    return coverage;

  }

  replaceJsonCoverage(emisionRequest: any): any {
    // SE VALIDA QUE SEA AXA CON LA QUE SE COTIZA YA QUE RESPONDE MAL Y SOLO SE GUARDA EN LA BASE DE DATOS , ESTE IF
    // SE DEBE DE QUITAR CUANDO WS CORRIJA EL SERVICIO DE AXA
    if (this.aseguradoraSelected.toUpperCase()==="AXA"){
      this.loaderGo = false;
    }else {
      let periodicity = (localStorage.getItem('coverageSelected')) ? localStorage.getItem('coverageSelected').toLowerCase() : 'amplia';
      let coverages = (localStorage.getItem('lastCotizacion')) ? localStorage.getItem('lastCotizacion') : [];
      if (localStorage.getItem('tmpMultiSelected')) {
        return (emisionRequest.Coberturas) ? emisionRequest.Coberturas : emisionRequest.coberturas;
      }
      if (typeof coverages === "string") {
        coverages = JSON.parse(coverages).coberturas;
      }
      let coverage: any[] = [];

      switch (periodicity) {
        default:
        case 'amplia':
          if (coverages[0].amplia) {
            if (coverages[0].amplia) {
              coverage = coverages[0].amplia;
            }
          } else if (coverages[1].amplia) {
            if (coverages[1].amplia[0]) {
              coverage = coverages[1].amplia;
            }
          } else {
            if (coverages[2].amplia[0]) {
              coverage = coverages[2].amplia;
            }
          }
          break;
        case 'limitada':
          if (coverages[0].limitada) {
            if (coverages[0].limitada) {
              coverage = coverages[0].limitada;
            }
          } else if (coverages[1].limitada) {
            if (coverages[1].limitada[0]) {
              coverage = coverages[1].limitada;
            }
          } else {
            if (coverages[2].limitada[0]) {
              coverage = coverages[2].limitada;
            }
          }
          break;
        case 'rc':
          if (coverages[0].rc) {
            if (coverages[0].rc) {
              coverage = coverages[0].rc;
            }
          } else if (coverages[1].rc) {
            if (coverages[1].rc[0]) {
              coverage = coverages[1].rc;
            }
          } else {
            if (coverages[2].rc[0]) {
              coverage = coverages[2].rc;
            }
          }
          break;
      }

      return coverage;
    }
  }

  toCamelSubNivel(jsonData: any):any{
    if (typeof jsonData === 'object'){
      for (var key in jsonData){
        var newKey = '';
        if(key.length>0){ newKey =  this.camelize(key);}
        jsonData[newKey] = jsonData[key];
        delete jsonData[key];
        if(typeof jsonData[newKey] !== 'undefined'){
          if (Object.keys(jsonData[newKey]).length>1){
            this.toCamelSubNivel(jsonData[newKey]);
          }
        }
      }
      return jsonData;
    }else { return jsonData; }
  }

  camelize(indexData: any):any{
    if(indexData == 'MSI'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'RFC'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'CURP'){ indexData = indexData.toString().toLowerCase();
    }else { indexData = indexData.charAt(0).toLowerCase() + indexData.substring(1,indexData.length); }
    return  indexData;
  }

  toCamel(dataJson: any): any{
    if (typeof dataJson === 'object'){
      if(Object.keys(dataJson).length){
        for (var indexData in dataJson){
          var newIndexData = '';
          if(indexData.length>0){ newIndexData = this.camelize(indexData); }
          dataJson[newIndexData] =  dataJson[indexData];
          delete dataJson[indexData];
          if(typeof dataJson[newIndexData] !== 'undefined') {
            if (Object.keys(dataJson[newIndexData]).length > 0 && newIndexData !== 'coberturas' && newIndexData !== 'Coberturas' && newIndexData !== 'cotizacion') {
              dataJson[newIndexData] = this.toCamelSubNivel(dataJson[newIndexData]);
            }
          }
        }
        return dataJson;
      }
    }else { return dataJson; }
  }

  deleteJsonUperCase(): void{
    if(this.emisionRequest.Aseguradora){delete this.emisionRequest.Aseguradora;}
    if(this.emisionRequest.Cliente){delete this.emisionRequest.Cliente;}
    if(this.emisionRequest.Coberturas){delete this.emisionRequest.Coberturas;}
    if(this.emisionRequest.CodigoError){delete this.emisionRequest.CodigoError;}
    if(this.emisionRequest.Descuento){delete this.emisionRequest.Descuento;}
    if(this.emisionRequest.Cotizacion){delete this.emisionRequest.Cotizacion;}
    if(this.emisionRequest.Emision){delete this.emisionRequest.Emision;}
    if(this.emisionRequest.Paquete){delete this.emisionRequest.Paquete;}
    if(this.emisionRequest.PeriodicidadDePago){delete this.emisionRequest.PeriodicidadDePago;}
    if(this.emisionRequest.UrlRedireccion){delete this.emisionRequest.UrlRedireccion;}
    if(this.emisionRequest.Pago){delete this.emisionRequest.Pago;}
    if(this.emisionRequest.Vehiculo){delete this.emisionRequest.Vehiculo;}
  }

  configurationByInsurer(emisionRequest:any):any{
    let aseguradora = '';
    if(emisionRequest.Aseguradora){ aseguradora = emisionRequest.Aseguradora.toString().toLowerCase();}
    else if(emisionRequest.aseguradora){ aseguradora = emisionRequest.aseguradora.toString().toLowerCase();}
    else if( localStorage.getItem('tokenAs')){ aseguradora = localStorage.getItem('tokenAs').toLowerCase();   }
     else{ aseguradora = emisionRequest.aseguradora.toString().toLowerCase();  }

     switch (aseguradora){
       case 'migo':
         // if(emisionRequest.Aseguradora){
         //   emisionRequest.Cliente.TipoPersona = 'F';
         //   this.emisionRequest.Cliente.BeneficiarioPreferente = emisionRequest.Cliente.ApellidoPat +' '+ emisionRequest.Cliente.ApellidoMat +' '+emisionRequest.Cliente.Nombre;
         //   this.emisionRequest.Cliente.FechaNacimiento = emisionRequest.Cliente.FechaNacimiento.split('/')[2] +'-'+ emisionRequest.Cliente.FechaNacimiento.split('/')[1] +'-'+ emisionRequest.Cliente.FechaNacimiento.split('/')[0];
         //   this.emisionRequest.Cliente.Direccion.CodPostal = this.codigoPostal;
         //   if(!validateRfc(this.emisionRequest.cliente.rfc.toString()).isValid){
         //     this.loaderGo = false;
         //     Swal.fire({
         //       icon: 'warning',
         //       title: 'Detalles RFC!',
         //       text: 'El RFC debe contener 13 caracteres y debe ser un dato veridico.',
         //       footer: 'Favor de incluir la homoclave.'
         //     });
         //     return this.emisionRequest;
         //   }
         // }else{
           emisionRequest.cliente.tipoPersona = 'F';
           this.emisionRequest.cliente.beneficiarioPreferente = emisionRequest.cliente.apellidoPat +' '+ emisionRequest.cliente.apellidoMat +' '+emisionRequest.cliente.nombre;
           this.emisionRequest.cliente.fechaNacimiento = emisionRequest.cliente.fechaNacimiento.split('/')[2] +'-'+ emisionRequest.cliente.fechaNacimiento.split('/')[1] +'-'+ emisionRequest.cliente.fechaNacimiento.split('/')[0];
           this.emisionRequest.cliente.direccion.codPostal = this.codigoPostal;
           if(!validateRfc(this.emisionRequest.cliente.rfc.toString()).isValid){
             this.loaderGo = false;
             Swal.fire({
               icon: 'warning',
               title: 'Detalles RFC!',
               text: 'El RFC debe contener 13 caracteres y debe ser un dato veridico.',
               footer: 'Favor de incluir la homoclave.'
             });
             return this.emisionRequest;
           }
         // }

         break;
       case 'mapfre':
          // if(this.emisionRequest.Cliente){
          //   this.emisionRequest.Cliente.FechaNacimiento = emisionRequest.Cliente.FechaNacimiento.split('/')[2] +'-'+ emisionRequest.Cliente.FechaNacimiento.split('/')[1] +'-'+ emisionRequest.Cliente.FechaNacimiento.split('/')[0];
          // }else{
            this.emisionRequest.cliente.fechaNacimiento = emisionRequest.cliente.fechaNacimiento.split('/')[2] +'-'+ emisionRequest.cliente.fechaNacimiento.split('/')[1] +'-'+ emisionRequest.cliente.fechaNacimiento.split('/')[0];
          // }

         break;
       default:
           // if(emisionRequest.Aseguradora){ emisionRequest.Cliente.TipoPersona = 'F'; }
           // else
             if(emisionRequest.aseguradora){ emisionRequest.cliente.tipoPersona = 'F'; }
           else{ emisionRequest.cliente.tipoPersona = 'F'; }
         break;

     }

    return emisionRequest;
  }

  changeStatusEmisionNext(state: string):void{
    if(localStorage.getItem("emissionStates")){ this.statesExitShowAlert = JSON.parse(localStorage.getItem("emissionStates")); }

    switch (state){
      case 'clientData':
        console.log("dataClient");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.clientData.state = false;
          this.statesExitShowAlert.addressData.state=true;
        }
        break;
      case 'addressData':
        console.log("addressData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.addressData.state = false;
          this.statesExitShowAlert.vehiculoData.state=true;
        }
        break;
      case 'vehiculoData':
        console.log("vehiculoData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.vehiculoData.state = false;
          this.statesExitShowAlert.paymentMethods.state=true;
        }
        break;
        case 'paymentMethods':
        console.log("paymentMethods");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.paymentMethods.state=false;
          this.statesExitShowAlert.payData.state=true;
        }
        break;
      case 'payData':
        console.log("payData");
        break;
      default:
        console.log("default");
        break;

    }

    localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));

  }

  changeStatusEmisionBack(state: string):void{

    if(localStorage.getItem("emissionStates")){ this.statesExitShowAlert = JSON.parse(localStorage.getItem("emissionStates")); }
    switch (state){
      case 'clientData':
        console.log("dataClient");
        break;
      case 'addressData':
        console.log("addressData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.addressData.state = false;
          this.statesExitShowAlert.clientData.state=true;
        }
        break;
      case 'vehiculoData':
        console.log("vehiculoData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.vehiculoData.state = false;
          this.statesExitShowAlert.addressData.state=true;
        }
        break;
      case 'payData':
        console.log("payData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.payData.state = false;
          this.statesExitShowAlert.vehiculoData.state=true;
        }
        break;
      default:
        console.log("default");
        break;

    }
    localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));

  }

  continueEmisionGnp(pay: number):void{

    if(this.datosPago.controls['banco'].value === ''
        || this.datosPago.controls['anioVencimiento'].value === ''
        || this.datosPago.controls['cvv'].value === ''
        || this.datosPago.controls['metodoPago'].value === ''
        || this.datosPago.controls['mesVencimiento'].value === ''
        || this.datosPago.controls['numTarjeta'].value === ''
        || this.datosPago.controls['numTarjeta'].value === ''
        || this.datosPago.controls['tipoTarjeta'].value === ''
        || this.datosPago.controls['formaPago'].value === ''){

        /*COMO NO HAY DATOS DEL PAGO NO PERMITE EMITIR*/
        this.updateFakeEmision(4);
        return;
    }

    this.idAgente = jwt_decode(this._token);
    this.emisionRequestGnp.pago.anioExp = (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '';
    this.emisionRequestGnp.pago.codigoSeguridad = (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '';
    this.emisionRequestGnp.pago.mesExp = (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '';
    this.emisionRequestGnp.pago.medioPago = this.getDataMetodoPago(this.emisionRequestGnp.pago.medioPago);
    // if(this.emisionRequestGnp.Cliente){
    //   this.emisionRequestGnp.Cliente.Direccion.Poblacion =  this.emisionRequestGnp.Cliente.Direccion.Poblacion.toString().replace('é','e').replace('í','i').replace('ú','u').replace('ó','o').replace('á','a');
    // }else{
      this.emisionRequestGnp.cliente.direccion.poblacion = this.emisionRequestGnp.cliente.direccion.poblacion.toString().replace('é','e').replace('í','i').replace('ú','u').replace('ó','o').replace('á','a');
    // }

    this.emisionRequestGnp.periodicidadDePago = localStorage.getItem('payFormSelected');
    this.emisionRequestGnp.paquete = localStorage.getItem("coverageSelected")?localStorage.getItem("coverageSelected"):'AMPLIA';
    this.emisionRequestGnp.pago.msi = this.datos_card.formaPago.value;
    /*Se limpia el codigo de error de la tarjeta no valida (DEBITO)*/
    this.emisionRequestGnp.codigoError = '';
    /*SE OBTIENEN LOS DATOS DEL NODO EMISION*/
    this.gnpPago3DService.emision_uat(this.emisionRequestGnp).subscribe(resp => {

      this.emisionRequestGnp = resp;
      if(this.emisionRequestGnp.emision.primaTotal !== '' && this.emisionRequestGnp.emision.idCotizacion !== '' && this.emisionRequestGnp.codigoError === ''){
        this.updateFakeEmision(9);

        if(pay == 1 && this.datosPago.status === 'VALID') {
          /*Se manda a llamar la siguiente funcion que se encarga de realizar el pago* usando los datos de la tarjeta*/
          this.crear_id_pago();
        }

      }else {
        this.updateFakeEmision(8);
      }

    }, error => {
      if(error.status === 400 || error.status === '400'){
        this.emisionRequestGnp = error.error;
        this.updateFakeEmision(8);
      }
    });

  }
  remove_accents(word):void{

    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

  }
  crear_id_pago():void{

    /*SE OBTIENEN LOS DATOS DEL NODO EMISION*/
    this.emisionRequestGnp.pago.msi === 'CL'? '1':this.emisionRequestGnp.pago.msi;
    this.gnpPago3DService.crear_id_pago(this.emisionRequestGnp).subscribe(resp => {
      this.emisionRequestGnp = resp;
      if(this.emisionRequestGnp.emision.primaTotal !== '' && this.emisionRequestGnp.emision.idCotizacion !== '' && this.emisionRequestGnp.codigoError === ''){
        this.updateFakeEmision(9);
        this.obtener_emision();
      }else {
        this.updateFakeEmision(8);
      }

    }, error => {
      if(error.status === 400 || error.status === '400'){
        this.emisionRequestGnp = error.error;
        this.updateFakeEmision(8);
      }
    });

  }

  obtener_emision():void{
    this.idAgente = jwt_decode(this._token);
    const dataSaveCot = +this.idAgente['sub'].split(':')[1];
    let _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';
    let payForm = (localStorage.getItem('payFormSelected'))?localStorage.getItem('payFormSelected'):'ANUAL';
    const idAgente = +this.idAgente['sub'].split(':')[1];

    this.gnpPago3DService.obtener_emision(this.emisionRequestGnp).subscribe(resp => {
      this.emisionRequestGnp = resp;

      if(this.emisionRequestGnp.emision.primaTotal !== '' && this.emisionRequestGnp.emision.idCotizacion !== '' && this.emisionRequestGnp.codigoError === ''){
        this.updateFakeEmision(9);
        this.emisionWithoutPaidGNP = true;
        if(resp.emision.documento !== '' && resp.emision.poliza !== '' && resp.emision.resultado == true) {
          /*GUARDAR LA EMISION */ // => ES NECESARIO EL NUMERO DE POLIZA
          this.catalogosService.emitir_Save(this.emisionRequestGnp, dataSaveCot.toString(), _cotizacionSaved.id, payForm).subscribe(
              respuesta => {
                localStorage.setItem('lastEmisionSaved', JSON.stringify(respuesta));
                this.emisionWithoutPaidGNP = true;
                /*GUARDAR PAGO*/ // Se ejecuta hasta este punto dado el numero de poliza es necesario para el prodeco de guardar emision y pago (guardarPagoQualitas depende de emitir_Save)
                this.catalogosService.guardarPagoQualitas(this.emisionRequestGnp, idAgente.toString(), respuesta.idRegistro).subscribe(
                    respuesta => {
                      localStorage.setItem('lastPaidSaved', JSON.stringify(respuesta));
                      this.emisionWithoutPaidGNP = true;
                      let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
                      this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequestGnp, 7).subscribe(
                          succ => {
                            console.table(_SavedFake);
                          },
                          err => {
                            console.error(err);
                          }
                      );

                    },
                    err => {
                      console.error(err);
                    });


              },
              err => {
                console.error(err);
              });
        }

      }else {
        this.updateFakeEmision(8);
      }
    }, error => {
      console.log(error);

    });


  }

  getDataMetodoPago(data: string):string{
    let medioPago = "";
    switch (data) {
      case 'CLABE':
        medioPago = 'C';
        break;
      case 'Tarj. de dèbito':
        medioPago = 'D';
        break;
      case 'Tarj. de crÃ©dito':
        medioPago = 'R';
        break;
      case 'Tarj. de crÃ\u0083Â©dito':
        medioPago = 'R';
        break;
      case 'Tarj. de crÃÂ©dito':
        medioPago = 'R';
        break;
      case 'Tarj. de credito':
        medioPago = 'R';
        break;
      case 'Convenio CIE':
        medioPago = 'E';
        break;
      case 'Monedero':
        medioPago = 'M';
        break;
      case 'Cuenta No Estándar':
        medioPago = 'N';
        break;
      case 'Tarj. de crèdito':
        medioPago = 'R';
        break;
      default:
        medioPago = 'D';
        break;
    }

    return medioPago;
  }

  changeFormartBirthdate(birthdate:string):string{
    let birthdateClient;
    if(birthdate.split('-').length > 1){
        birthdateClient = birthdate.split('-')[2]
            + '/' +birthdate.split('-')[1]
            + '/' +birthdate.split('-')[0];
    }else if(birthdate.split('/').length<3){
        birthdateClient = birthdate.split('-')[2]
            + '/' +birthdate.split('-')[1]
            + '/' +birthdate.split('-')[0];
    }else{
      birthdateClient = birthdate;
    }

    return birthdateClient;
  }

  updateFakeEmision(estado: number):void{
    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    if (this.emisionRequestGnp.aseguradora === 'GNP'){
      let card = localStorage.getItem('card');
      this.emisionRequestGnp.pago.noTarjeta = card;
    }
    this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequestGnp,estado).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        }
    );

  }

}
