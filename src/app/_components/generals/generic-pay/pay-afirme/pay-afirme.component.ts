import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import RfcFacil from 'rfc-facil';
import Swal from 'sweetalert2';
import {
  CodPostalInterface,
  EmisionRequestInterface,
  CardTypeInterface,
  CotizacionResponseInterface
} from 'src/app/_interfaces';

import {
  GnpPago3DService,
  CatalogosService,
  CotizacionService,
  EvetnsEmiterLocalService,
  OnlyNumbersService,
  ToLowerCaseKeyService,
  LoginService
} from 'src/app/_services';
import curp from 'curp';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { MatStepper } from '@angular/material/stepper';
import {Subscription} from "rxjs";
import {DomSanitizer,SafeResourceUrl} from "@angular/platform-browser";
declare var require: any
const validateRfc = require('validate-rfc');

@Component({
  selector: 'app-pay-afirme',
  templateUrl: './pay-afirme.component.html',
  styleUrls: ['./pay-afirme.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class PayAfirmeComponent implements OnInit {
  cotizacionResponse: CotizacionResponseInterface;
  commonRequest: object = {};
  aseguradoraSelected: string = '';
  submitted: boolean = false;
  idAgente: any;
  cotizacionSaved: any;
  emisionRequest: EmisionRequestInterface = {};
  emisionRequestResp: any;
  emisionRequestGnp: any;
  emisionResponse: EmisionRequestInterface = {};
  coloniasAfirme:any;
  cAfirme:any;
  rfc: string = '';
  curp: any;

  datosPersonales: FormGroup = new FormGroup({
    nombre: new FormControl('', [Validators.required]),
    aPaterno: new FormControl('', [Validators.required]),
    aMaterno: new FormControl('', [Validators.required]),
    correo: new FormControl('', [Validators.required, Validators.email, Validators.pattern(GENERALS.regex_mail)]),
    telefono: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),
    nacimientoEstado: new FormControl('', [Validators.required]),
    fechaNacimiento: new FormControl('', [Validators.required]),
    rfc: new FormControl('', [Validators.required]),
    curp: new FormControl('', [Validators.required])
  });
  datosDireccion: FormGroup = new FormGroup({
    estadoMun: new FormControl('', [Validators.required]),
    colonia: new FormControl('', [Validators.required]),
    calleNum: new FormControl('', [Validators.required]),
    numero: new FormControl('', [Validators.required]),
    numInterior: new FormControl('')
  });
  datosPago: FormGroup = new FormGroup({
    tipoTarjeta: new FormControl('', [Validators.required]),
    nombreTarjeta: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
    banco: new FormControl('', [Validators.required]),
    metodoPago: new FormControl('', [Validators.required]),
    formaPago: new FormControl('', [Validators.required]),
    numTarjeta: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(18)]),
    mesVencimiento: new FormControl('', [Validators.required]),
    anioVencimiento: new FormControl('', [Validators.required]),
    cvv: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(5), Validators.pattern("^[0-9]*$")])
  });
  datosVehiculo: FormGroup = new FormGroup({
    niv: new FormControl('', [Validators.required, Validators.minLength(15), Validators.maxLength(20)]),
    motor: new FormControl('', [Validators.minLength(6), Validators.maxLength(20)]),
    placa: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(8)])
  });
  colonias: CodPostalInterface[] = [];
  cp: string = '';
  estado: string = '';
  delMun: string = '';
  codigoPostal: string='';
  emisionWithoutPaid: boolean = false;
  emisionWithoutPaidGNP: boolean = false;
  emisionWithoutPaidAFIRME: boolean = false;
  terminalExterna: string = '';
  documentos: string = '';
  poliza: string = '';
  _token: string = '';
  _form: any;
  @ViewChild('stepper') stepper: MatStepper;
  optPay: CardTypeInterface[] = [];
  lastQuotation;
  typeLocal: number = 0;
  loaderGo: boolean = false;
  useTerminalVirtual: boolean = false;
  externalTerminalURL: string = '';
  loadExternalTerminal: boolean = false;
  fromMulty: boolean = false;
  urlType: string = '';
  actionEmiterDataVehiculo = 'false';
  public subscriber: Subscription;
  bancos: any;
  tarjetasGnp: any;
  buttonEmisionInteraccion: boolean = false;
  statesExitShowAlert: any = {'clientData':{'state':true, 'showAlert':true},
    'addressData':{'state':false, 'showAlert':true},
    'vehiculoData':{'state':false, 'showAlert':true},
    'paymentMethods':{'state':false, 'showAlert':true},
    'payData':{'state':false, 'showAlert':true}};
  delegacionAfirme: string;
  coloniaAfirme: string;
  asegAfirme: string;
  numeroTargetaOculto: any;
  numeroTargetaVisible: any;
  ojoOculto: boolean = true;
  fechaActual: any[] = [];
  mesBanco   : any[] = [];
  bodyPago: any
  datosAtomar: any;
  formaPago: any;
  selectCober: any
  recorreCoberturas: any;
  recorerCotizacion: any;
  numeroValido: boolean = true;
  numeroSerie: any
  numeroMotor: any
  urlLinkDePago: SafeResourceUrl;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private catalogosService: CatalogosService,
      private gnpPago3DService: GnpPago3DService,
      private cotizacionService: CotizacionService,
      private onlyNumber: OnlyNumbersService,
      private parseToLower: ToLowerCaseKeyService,
      private evetnsEmiterLocalService: EvetnsEmiterLocalService,
      private sanitizer: DomSanitizer,
      private loginService: LoginService,
) {
    this.commonRequest = {
      personal: this.datos_personales,
      address: this.datos_direccion,
      car: this.datos_vehiculo,
      card: this.datos_card
    };
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    })
  }
  ngOnInit(): void {
    // this.calcularAnioActual();
    if(localStorage.getItem('tmpMultiSelected')){ localStorage.setItem("coverageSelected","AMPLIA"); localStorage.setItem("payFormSelected","ANUAL");}
    if(!localStorage.getItem("estadoProceso")){ localStorage.setItem("estadoProceso","enProcesoEmision");}
    if(!localStorage.getItem("emissionStates")){ localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));}
    let _cotizacionSaved;
    if(localStorage.getItem('cotizacionSaved')){ _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';}

    this.typeLocal = (this.emisionRequest.Aseguradora) ? 1 : 2;
    this._setInit();
    this._validateMultyQuotation();
    if(!this.emisionRequest.pago){
      this.emisionRequest.pago = {"medioPago":"","nombreTarjeta":"","banco":"","noTarjeta":"","mesExp":"","anioExp":"","codigoSeguridad":"","noClabe":"","carrier":0,"msi":""};
    }

    this.emisionRequestResp = this.emisionRequest;
    if(localStorage.getItem('tmpMultiSelected')){
      this.emisionRequestResp = JSON.parse(localStorage.getItem('lastCotizacion'));
      this.emisionRequest = this.emisionRequestResp;
    }


    this.cotizacionService.saveFake(_cotizacionSaved.id, this.emisionRequest).subscribe(
        succ => { localStorage.setItem('savedFake', JSON.stringify(succ));
          this.updateData(2)},
        err => { console.error(err); });
    let _url = window.location.href.split('/');
    this.useTerminalVirtual = (_url[_url.length - 1] === 'virtual') ? true : false;


  }

  ngOnDestroy():void{
    localStorage.removeItem("estadoProceso");
    localStorage.removeItem("emissionStates");
  }

  disableBack():void{

    let timesPressed:number = 0;

    window.history.pushState(null, "", window.location.href);        
    window.onpopstate = function() {
      timesPressed += 1;
      if(timesPressed < 3){
        window.history.pushState(null, "", window.location.href);
        Swal.fire({
          icon: 'success',
          title: 'Asegura  el precio de tu emisión ',
          text: ' terminandola ahora!',
          footer: 'Gracias!'
        });
      }else{
        if(timesPressed === 3){
          window.history.back();
        }
      }
    };

  }

  private _setInit(): void {

    this.disableBack();

    if (!localStorage.getItem('_t')) {
      this.router.navigateByUrl('admin/cotiza-go/particular');
    }
    let _multi = localStorage.getItem('tmpMultiSelected') || null;

    this._token = localStorage.getItem('token') || '';
    this._form = localStorage.getItem('_t');
    let _tmpmulti = JSON.parse(localStorage.getItem('tmpMultAskQuotation')) || null;
    this.fromMulty = (localStorage.getItem('_tmpMultyPush')) ? true : false;
    this._eventLocal(false);

    if (!_multi) {
      let _cotizacionSaved = JSON.parse(localStorage.getItem('cotizacionSaved')) || '';
      this.emisionRequest = JSON.parse(_cotizacionSaved.respuesta);
      this.aseguradoraSelected = localStorage.getItem('tokenAs');
      this.getColonias(JSON.parse(_cotizacionSaved.peticion).cp);
    } else {
      this.aseguradoraSelected = localStorage.getItem('tmpMultiSelected');
      this.getColonias(_tmpmulti[0].cp)
    }
    if(this.emisionRequest.cliente){
    }

    if(this.aseguradoraSelected.toUpperCase() ==='AFIRME'){
      this.catalogosService.getColoniasAfirme(localStorage.getItem('cpAfirme')? localStorage.getItem('cpAfirme'): '09410').subscribe(data =>{
        this.coloniasAfirme = data;
        this.cAfirme=this.coloniasAfirme.municipios[0].colonias;
        this.delegacionAfirme = this.coloniasAfirme.municipios[0].nombreMunicipio+'-'+this.coloniasAfirme.municipios[0].idMunicipio;
        this.coloniaAfirme = this.cAfirme[0].nombreColonia +'-'+this.cAfirme[0].idColonia;

      })
    }
    this.datosPago.controls['formaPago'].disable();
    this.bancos = this.obtenerBancosMap();
  }
  private _eventLocal(active: boolean, insurance?: string): void {
    this.evetnsEmiterLocalService.emitLoader(JSON.stringify({ active: active, insurance: insurance }));
  }
  private _validateMultyQuotation(): void {

    let _multyInsurance = localStorage.getItem('tmpMultiSelected') || null;
    if (_multyInsurance) {
      this.aseguradoraSelected = _multyInsurance;
      let _tmpQuotation = JSON.parse(localStorage.getItem('multiQuote'));
      let _quotation = _tmpQuotation.find(el => el.aseguradora.toLowerCase() === _multyInsurance.toLowerCase()).cotizacion;
      localStorage.setItem('lastCotizacion', JSON.stringify(_quotation));
      console.log('lastCotizacion', _quotation)
    }
  }
  get datos_personales() { return this.datosPersonales.controls; }
  get datos_direccion() { return this.datosDireccion.controls; }
  get datos_vehiculo() { return this.datosVehiculo.controls; }
  get datos_card() { return this.datosPago.controls; }
  calcularRfc(): void {
    this.testCurp();

    if (this.datos_personales.nombre.value && this.datos_personales.aPaterno.value && this.datos_personales.aMaterno.value && this.datos_personales.nacimientoEstado.value && this.datos_personales.fechaNacimiento.value) {
      const rfc: string = RfcFacil.forNaturalPerson({
        name: this.datosPersonales.controls['nombre'].value,
        firstLastName: this.datosPersonales.controls['aPaterno'].value,
        secondLastName: this.datosPersonales.controls['aMaterno'].value,
        day: +moment(this.datosPersonales.controls['fechaNacimiento'].value).format('D'),
        month: +moment(this.datosPersonales.controls['fechaNacimiento'].value).format('M'),
        year: moment(this.datosPersonales.controls['fechaNacimiento'].value).get('year')
      });
      let _rfc = rfc;
      this.datosPersonales.controls['rfc'].setValue(_rfc);
      if(!validateRfc(_rfc).isValid){
        Swal.fire({
          icon: 'warning',
          title: 'Detalles RFC!',
          text: 'El RFC debe contener 13 caracteres y debe ser un dato veridico.',
          footer: 'Favor de incluir la homoclave.'
        });
      }
    }
  }
  testCurp(): void {
    if (this.datos_personales.nombre.value && this.datos_personales.aPaterno.value && this.datos_personales.aMaterno.value && this.datos_personales.nacimientoEstado.value && this.datos_personales.fechaNacimiento.value) {
      const persona = curp.getPersona();
      persona.nombre = this.datos_personales.nombre.value;
      persona.apellidoPaterno = this.datos_personales.aPaterno.value;
      persona.apellidoMaterno = this.datos_personales.aMaterno.value;
      persona.genero = (this._form.length == 1) ? this._form : (this._form.toUpperCase() === 'MASCULINO') ? 'H' : 'F';
      let day = moment(this.datos_personales.fechaNacimiento.value).format('D');
      let month = moment(this.datos_personales.fechaNacimiento.value).format('M');
      let year = moment(this.datos_personales.fechaNacimiento.value).get('year');
      persona.fechaNacimiento = day + '-' + month + '-' + year;
      persona.estado = this.datos_personales.nacimientoEstado.value;
      this.datosPersonales.controls['curp'].setValue(curp.generar(persona));
    }
  }
  private _valueCity(type: number, value: string): string {
    return (type === 1) ? value.split('|')[1].trim() : value.split('|')[0].trim();
  }
  private _setParamRequestCliente(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    if(!this.emisionRequestResp.cliente){this.emisionRequestResp.cliente = _cotizacion.cliente;}
    if(this.emisionRequestResp.cliente){
      this.emisionRequestResp.cliente = _cotizacion.cliente;
      this.emisionRequestResp.cliente.nombre          = this.datos_personales.nombre.value;
      this.emisionRequestResp.cliente.apellidoPat     = this.datos_personales.aPaterno.value;
      this.emisionRequestResp.cliente.apellidoMat     = this.datos_personales.aMaterno.value;
      this.emisionRequestResp.cliente.curp            = this.datos_personales.curp.value;
      this.emisionRequestResp.cliente.email           = this.datos_personales.correo.value;
      this.emisionRequestResp.cliente.fechaNacimiento = this.emisionRequestResp.aseguradora === 'GENERAL_DE_SEGUROS'? this.datos_personales.fechaNacimiento.value: moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY') ;
      this.emisionRequestResp.cliente.rfc             = this.datos_personales.rfc.value;
      this.emisionRequestResp.cliente.telefono        = this.datos_personales.telefono.value;
      this.emisionRequestResp.cliente.beneficiarioPreferente = this.datos_personales.aPaterno.value + ' ' + this.datos_personales.aMaterno.value + ' ' + this.datos_personales.nombre.value;
      this.emisionRequestResp.cliente.tipoPersona = 'FISICA';
    }
  }

  private _setParamRequestDireccion(): void {

    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    if(!this.emisionRequestResp.cliente){this.emisionRequestResp.cliente.direccion = _cotizacion.cliente.direccion;}
    if(this.emisionRequestResp.cliente){
      this.emisionRequestResp.cliente.direccion = _cotizacion.cliente.direccion;
      this.emisionRequestResp.cliente.direccion.calle  = this.datos_direccion.calleNum.value;
      this.emisionRequestResp.cliente.direccion.ciudad     = (this.datos_direccion.estadoMun.value) ? this._valueCity(1, this.datos_direccion.estadoMun.value) : '';
      this.emisionRequestResp.cliente.direccion.codPostal    = _cotizacion.cliente.direccion.codPostal;
      this.emisionRequestResp.cliente.direccion.colonia   =  this.datos_direccion.colonia.value;
      this.emisionRequestResp.cliente.direccion.noExt    = this.datos_direccion.numero.value;
      this.emisionRequestResp.cliente.direccion.noInt    = this.datos_direccion.numInterior.value;
      this.emisionRequestResp.cliente.direccion.pais     = 'Mexico';
      this.emisionRequestResp.cliente.direccion.ciudad   = _cotizacion.aseguradora==='AFIRME'? this.delegacionAfirme :this.emisionRequest.aseguradora === 'generaldeseguros'? this.remove_accents(this._valueCity(2, this.datos_direccion.estadoMun.value) ): this._valueCity(2, this.datos_direccion.estadoMun.value);
    }
  }

  private _setParamRequestVehiculo(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    if(!this.emisionRequestResp.vehiculo){this.emisionRequestResp.vehiculo = _cotizacion.vehiculo;}
    if(this.emisionRequestResp.vehiculo){
      this.emisionRequestResp.vehiculo = _cotizacion.vehiculo;
      this.emisionRequestResp.vehiculo.noMotor   = this.datos_vehiculo.motor.value;
      this.emisionRequestResp.vehiculo.noPlacas  = this.datos_vehiculo.placa.value;
      this.emisionRequestResp.vehiculo.noSerie   = this.datos_vehiculo.niv.value;
      this.emisionRequestResp.vehiculo.servicio  = 'PARTICULAR';
      this.emisionRequestResp.vehiculo.uso       = 'PARTICULAR';
    }

  }

  private _setParamRequestPago(): void {
    if(this.emisionRequestResp.pago){
      if(!this.emisionRequestResp.pago){
        this.emisionRequestResp.pago = {
          pago: {
            anioExp: '',
            banco: '',
            carrier: '',
            codigoSeguridad: '',
            msi: '',
            medioPago: '',
            mesExp: '',
            noClabe: '',
            noTarjeta: '',
            nombreTarjeta: '',
          },
        }
      }
      this.emisionRequestResp.pago.anioExp          = (this.datos_card.anioVencimiento.value) ? this.datos_card.anioVencimiento.value : '';
      this.emisionRequestResp.pago.banco            = (this.datos_card.banco.value) ? this.datos_card.banco.value : '';
      this.emisionRequestResp.pago.carrier          = this.idTarjetaCarrier;
      this.emisionRequestResp.pago.codigoSeguridad  = (this.datos_card.cvv.value) ? this.datos_card.cvv.value : '';
      this.emisionRequestResp.pago.msi              = (this.datos_card.formaPago.value) ? this.datos_card.formaPago.value : '';
      this.emisionRequestResp.pago.medioPago        = (this.datos_card.metodoPago.value) ? this.datos_card.metodoPago.value : '';
      this.emisionRequestResp.pago.mesExp           = (this.datos_card.mesVencimiento.value) ? this.datos_card.mesVencimiento.value : '';
      this.emisionRequestResp.pago.noClabe          = '';
      this.emisionRequestResp.pago.nombreTarjeta    = (this.datos_card.nombreTarjeta.value) ? this.datos_card.nombreTarjeta.value : '';
      this.emisionRequestResp.pago.noTarjeta        = (this.datos_card.numTarjeta.value) ? this.datos_card.numTarjeta.value : '';
      delete this.emisionRequestResp.pago.pago;
    }



  }

  private _setParamRequestResto(): void {
    let _cotizacion = JSON.parse((localStorage.getItem('lastCotizacion'))?localStorage.getItem('lastCotizacion'):localStorage.getItem('tmpQuotation'));
    this.emisionRequest.periodicidadDePago = localStorage.getItem('payFormSelected');
    let _tokenAs = localStorage.getItem('tokenAs') || localStorage.getItem('tmpMultiSelected');
    this.emisionRequestResp.descuento = _cotizacion.descuento;
    this.emisionRequestResp.aseguradora = _cotizacion.aseguradora ? '':_cotizacion.aseguradora;
    this.emisionRequestResp.paquete = localStorage.getItem('coverageSelected') ? localStorage.getItem('coverageSelected'):'AMPLIA' ;
    this.emisionRequestResp.emision = typeof _cotizacion.emision !== 'undefined' ? _cotizacion.emision : {};
  }

  /* Dehabilitar boton */
  actionMethod($event:MouseEvent){
    ($event.target as HTMLButtonElement).disabled=true;
    this.buttonEmisionInteraccion = true;
  }

  emitirSinPago(): void {

    this.loaderGo = true;

    this.datosAtomar = JSON.parse(localStorage.getItem('lastCotizacion'))
    this.formaPago = localStorage.getItem('payFormSelected')
    this.selectCober = localStorage.getItem('coverageSelected')
    this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
    let _jT = JSON.parse(this.lastQuotation.peticion);

    if (localStorage.getItem('tmpMultiSelected') !== 'AFIRME') {
      if ((this.selectCober).toLowerCase() == 'amplia') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][0]['amplia']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][0]['amplia'][0][`${(this.formaPago).toLowerCase()}`]
      } else if ((this.selectCober).toLowerCase() == 'limitada') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][1]['limitada']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][1]['limitada'][0][`${(this.formaPago).toLowerCase()}`]
      } else if ((this.selectCober).toLowerCase() == 'rc') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][2]['rc']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][2]['rc'][0][`${(this.formaPago).toLowerCase()}`]
      }
      delete this.datosAtomar['emision'].fechaEmision
      delete this.datosAtomar['emision'].inicioVigencia
    }else{
      this.recorreCoberturas = this.datosAtomar['coberturas']
      this.recorerCotizacion = this.datosAtomar['cotizacion']
    }

    this.bodyPago = {
      aseguradora: this.datosAtomar['aseguradora'],
      urlRedireccion: '',
      cliente: {
        nombre: this.datos_personales.nombre.value,
        apellidoPat: this.datos_personales.aPaterno.value,
        apellidoMat: this.datos_personales.aMaterno.value,
        curp: this.datos_personales.curp.value,
        edad: this.datosAtomar['cliente'].edad,
        email: this.datos_personales.correo.value,
        fechaNacimiento: (this.datos_personales.fechaNacimiento.value) ? moment(this.datos_personales.fechaNacimiento.value).format('YYYY-MM-DD') : '',
        genero: this.datosAtomar['cliente'].genero,
        ocupacion: '',
        rfc: this.datos_personales.rfc.value,
        telefono: this.datos_personales.telefono.value,
        tipoPersona: 'F',
        direccion: {
          calle: this.datos_direccion.calleNum.value,
          ciudad: (this.IdCpValidarEnBody),
          codPostal: _jT.cp,
          colonia: (this.datos_direccion.colonia.value),
          noExt: this.datos_direccion.numero.value,
          noInt: this.datos_direccion.numInterior.value,
          pais: '',
          poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
        }
      },
      coberturas: this.recorreCoberturas,
      codigoError: '',
      descuento: (this.datosAtomar.descuento).toString(),
      cotizacion: this.recorerCotizacion,
      emision: this.datosAtomar['emision'],
      paquete: localStorage.getItem('coverageSelected').toUpperCase(),
      periodicidadDePago: this.formaPago,
      // urlRedireccion: '',
      pago: {
        anioExp: '',
        banco: '',
        carrier: 0,
        codigoSeguridad:  '',
        msi: '',
        medioPago: '',
        mesExp: '',
        noClabe: '',
        noTarjeta: '',
        nombreTarjeta: '',
      },
      vehiculo: {
        clave: _jT.clave,
        codDescripcion: '',
        codMarca: '',
        codUso: "",
        descripcion: _jT.descripcion,
        marca: _jT.marca,
        modelo: _jT.modelo,
        noMotor: this.datos_vehiculo.motor.value || this.numeroMotor,
        noPlacas: this.datos_vehiculo.placa.value,
        noSerie: this.datos_vehiculo.niv.value,
        servicio: "PARTICULAR",
        subMarca: this.datosAtomar.vehiculo.subMarca,
        uso: "PARTICULAR"
      }
    };
    this.IdCpValidarEnBody = this.bodyPago
    
    

    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    this.catalogosService.postEmisionAfirme(this.bodyPago).subscribe((dataAfirme) => {
      
      if(dataAfirme.codigoError){
        
        let iniciojson = dataAfirme.codigoError.indexOf("{");
        let finjson = dataAfirme.codigoError.indexOf("}");

        let errJson = dataAfirme.codigoError.substring(iniciojson,(finjson+1));
        let dataError = JSON.parse(errJson);
        
        this.loaderGo = false;

        Swal.fire({
          icon: 'error',
          title: 'Emision Fallida, ',
          text: dataError.message,
        });

        this.cotizacionService.updateFake(_SavedFake.id, dataAfirme, 8).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        });

      }else{

        this.loaderGo = false;

        Swal.fire({
          icon: 'success',
          title: 'Solicitud exitosa, ',
          text: 'te haremos llegar la póliza a tu Email registrado con nosotros',
          footer: 'Gracias!'
        });
  
        this.cotizacionService.updateFake(_SavedFake.id, dataAfirme, 9).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        });
      }
    }, (errorAfirme) => {
      
    this.cotizacionService.updateFake(_SavedFake.id, this.bodyPago, 8).subscribe(
    succ => { console.log("OK");},
    err => {
      console.error(err);
    });

    this.loaderGo = false;

    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Favor de comunicar al administrador de la pagina',
    });
    })

  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
IdCpValidarEnBody: any;
  getColonias(cp: any): void {
    this.catalogosService.getColonias(cp).subscribe(
        data => {
          this.colonias = data;
          this.estado = data[0].estado;
          this.delMun = data[0].delMun;
          this.datosDireccion.controls['estadoMun'].setValue(this.estado + ' | ' + this.delMun);
          this.IdCpValidarEnBody = data[0].idCp;
        },
        err => {
          console.error(err);
        }
    );
  }
  setOptPay(event: any): void {
    this.optPay = [];
    if(this.aseguradoraSelected.toUpperCase() !== 'GNP'){
      if (event.value === 'DEBITO') {
        this.optPay.push({ label: 'Contado', value: '1' });
      } else {
        this.optPay.push({ label: 'Contado', value: '1' });
        this.optPay.push({ label: '3 Meses', value: '3MSI' });
        this.optPay.push({ label: '6 Meses', value: '6MSI' });
        this.optPay.push({ label: '12 Meses', value: '12MSI' });
      }
    }else{
      this.optPay.push({ label: 'Contado', value: '1' });
    }

    this.datosPago.controls['formaPago'].enable();
  }
  setCardType(event: any): void {
    this.datosPago.controls['tipoTarjeta'].setValue(this.idTarjetaCarrier)
  }
  updateData(idEstadoEmision: number): void {
    /* descomentar codigo para que no guarde en fake*/
    // if(idEstadoEmision !== 0){
    //   idEstadoEmision = null;
    // }
    /* descomentar codigo para que no guarde en fake*/
    this._setParamRequestCliente();
    this._setParamRequestDireccion();
    this._setParamRequestVehiculo();
    this._setParamRequestPago();
    this._setParamRequestResto();
    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    if(localStorage.getItem("tokenAs")){
      this.emisionRequestResp.aseguradora = localStorage.getItem("tokenAs").toUpperCase();
    }else if(localStorage.getItem("tmpMultiSelected")){
      this.emisionRequestResp.aseguradora = localStorage.getItem("tmpMultiSelected").toUpperCase();
    }

    /*Logica para que cambia el estatus a 5 si y solo si todos los datos del pago estan registrados*/
    if(idEstadoEmision == 4 ){
      if(this.datosPago.controls['banco'].value === ''
          || this.datosPago.controls['anioVencimiento'].value === ''
          || this.datosPago.controls['cvv'].value === ''
          || this.datosPago.controls['metodoPago'].value === ''
          || this.datosPago.controls['mesVencimiento'].value === ''
          || this.datosPago.controls['numTarjeta'].value === ''
          || this.datosPago.controls['formaPago'].value === ''){
        idEstadoEmision = 4;
      }else {
        idEstadoEmision = 5;
      }

    }
    this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequestResp, idEstadoEmision).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        }
    );

  }
  contadorPeticion = 0
  validDataVehiculo():void{
    this.loaderGo = true;
    if(this.datosVehiculo.status === 'VALID'){
      this.actionEmiterDataVehiculo = 'true';
    }

    if( (this.datos_vehiculo.placa.value !== '') && (this.datos_vehiculo.niv.value === '') ){
      if((this.contadorPeticion >= 0) && this.contadorPeticion <= 3) {
        this.catalogosService.getPlacasVheiculo(this.datos_vehiculo.placa.value).subscribe((data) => {
          this.contadorPeticion = this.contadorPeticion + 1;
          this.loaderGo = false;
          // if (data != null) {
          //   Swal.fire('El numero de placa es correcto')
          // }

          this.numeroValido = true;
          this.numeroSerie = data.infoVehicular.niv;
          this.numeroMotor = (data.infoVehicular.niv).substr(-6);
          this.datos_vehiculo.motor.setValue(this.numeroMotor)
          this.datos_vehiculo.niv.setValue(this.numeroSerie)

        }, (error) => {
          if(error.status === 401){
            localStorage.removeItem('plToken')
            this.obtenerTokenPlacas();
            setTimeout(() => {
              this.validDataVehiculo()
              console.log("Delayed for 1 second.");
            }, 2000);
          }else{
            this.contadorPeticion = this.contadorPeticion + 1;
            this.loaderGo = false;
            this.numeroValido = false;
            if (this.datosVehiculo.status === 'VALID') {
              this.actionEmiterDataVehiculo = 'true';
            }
            if (this.datos_vehiculo.niv.value === '') {
              Swal.fire(
                  'La placa ingresada no se encuentra',
                  'favor de ingresar numero de serie',
                  'question'
              )
            }
          }
        })
      }else{
        this.loaderGo = false;
      }
    }else{

      this.numeroMotor = (this.datos_vehiculo.niv.value).substr(-6);
      this.loaderGo = false;
    }
  }
  tokenDePlacas: any;
  obtenerTokenPlacas(){
    this.loginService.getTokenPlacas().subscribe((tokenRepuve) =>{
      this.loginService.disparadorDePlacas.emit({data:this.tokenDePlacas })
      this.tokenDePlacas = tokenRepuve.accessToken
      localStorage.setItem('plToken', this.tokenDePlacas)
    })
  }

  numberOnly(event: any): boolean {
    return this.onlyNumber.validateInput(event);
  }
  back(): void {
    this.router.navigateByUrl('admin/cotiza-go/multicotizador/1');
  }
  toCamelSubNivel(jsonData: any):any{
    if (typeof jsonData === 'object'){
      for (var key in jsonData){
        var newKey = '';
        if(key.length>0){ newKey =  this.camelize(key);}
        jsonData[newKey] = jsonData[key];
        delete jsonData[key];
        if(typeof jsonData[newKey] !== 'undefined'){
          if (Object.keys(jsonData[newKey]).length>1){
            this.toCamelSubNivel(jsonData[newKey]);
          }
        }
      }
      return jsonData;
    }else { return jsonData; }
  }
  camelize(indexData: any):any{
    if(indexData == 'MSI'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'RFC'){ indexData = indexData.toString().toLowerCase();
    }else if(indexData == 'CURP'){ indexData = indexData.toString().toLowerCase();
    }else { indexData = indexData.charAt(0).toLowerCase() + indexData.substring(1,indexData.length); }
    return  indexData;
  }
  toCamel(dataJson: any): any{
    if (typeof dataJson === 'object'){
      if(Object.keys(dataJson).length){
        for (var indexData in dataJson){
          var newIndexData = '';
          if(indexData.length>0){ newIndexData = this.camelize(indexData); }
          dataJson[newIndexData] =  dataJson[indexData];
          delete dataJson[indexData];
          if(typeof dataJson[newIndexData] !== 'undefined') {
            if (Object.keys(dataJson[newIndexData]).length > 0 && newIndexData !== 'coberturas' && newIndexData !== 'Coberturas' && newIndexData !== 'cotizacion') {
              dataJson[newIndexData] = this.toCamelSubNivel(dataJson[newIndexData]);
            }
          }
        }
        return dataJson;
      }
    }else { return dataJson; }
  }
  changeStatusEmisionNext(state: string):void{
    if(localStorage.getItem("emissionStates")){ this.statesExitShowAlert = JSON.parse(localStorage.getItem("emissionStates")); }

    switch (state){
      case 'clientData':
       // console.log("dataClient");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.clientData.state = false;
          this.statesExitShowAlert.addressData.state=true;
        }
        break;
      case 'addressData':
        // console.log("addressData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.addressData.state = false;
          this.statesExitShowAlert.vehiculoData.state=true;
        }
        break;
      case 'vehiculoData':
        // console.log("vehiculoData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.vehiculoData.state = false;
          this.statesExitShowAlert.paymentMethods.state=true;
        }
        break;
      case 'paymentMethods':
        // console.log("paymentMethods");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.paymentMethods.state=false;
          this.statesExitShowAlert.payData.state=true;
        }
        break;
      case 'payData':
        // console.log("payData");
        break;
      default:
        // console.log("default");
        break;

    }

    localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));

  }
  changeStatusEmisionBack(state: string):void{

    if(localStorage.getItem("emissionStates")){ this.statesExitShowAlert = JSON.parse(localStorage.getItem("emissionStates")); }
    switch (state){
      case 'clientData':
        // console.log("dataClient");
        break;
      case 'addressData':
        // console.log("addressData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.addressData.state = false;
          this.statesExitShowAlert.clientData.state=true;
        }
        break;
      case 'vehiculoData':
        // console.log("vehiculoData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.vehiculoData.state = false;
          this.statesExitShowAlert.addressData.state=true;
        }
        break;
      case 'payData':
        // console.log("payData");
        if(localStorage.getItem("emissionStates")){
          this.statesExitShowAlert.payData.state = false;
          this.statesExitShowAlert.vehiculoData.state=true;
        }
        break;
      default:
        console.log("default");
        break;

    }
    localStorage.setItem("emissionStates", JSON.stringify(this.statesExitShowAlert));

  }
  remove_accents(word):void{
    return word.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }
  updateFakeEmision(estado: number):void{
    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    if (this.emisionRequestGnp.aseguradora === 'GNP'){
      let card = localStorage.getItem('card');
      this.emisionRequestGnp.pago.noTarjeta = card;
    }
    this.cotizacionService.updateFake(_SavedFake.id, this.emisionRequestGnp,estado).subscribe(
        succ => { console.log("OK");},
        err => {
          console.error(err);
        }
    );

  }
  bancosCompletos : any;
  tipoTarjeta : any;
  idTarjetaCarrier: any;
  TipoDeTargetaPorID: any;
  obtenerBancosMap(){
    this.catalogosService.getBancosAfirme().subscribe((data) => {
      this.bancosCompletos = data
    })
  }
  seleccionarImagen : any;
  numeroDetargetaInicial() {
    this.TipoDeTargetaPorID = this.datos_card.numTarjeta.value
    if (this.TipoDeTargetaPorID.startsWith(3)){
      this.seleccionarImagen = 'american'
      this.tipoTarjeta = 'AMERICA EXPRESS'
      this.idTarjetaCarrier = '3'
    }else if(this.TipoDeTargetaPorID.startsWith(4)){
      this.seleccionarImagen = 'visa'
      this.tipoTarjeta = 'VISA'
      this.idTarjetaCarrier = '1'
    }else if(this.TipoDeTargetaPorID.startsWith(5)){
      this.seleccionarImagen = 'mastercard'
      this.tipoTarjeta = 'MASTER CARD'
      this.idTarjetaCarrier = '2'
    }else {
      this.seleccionarImagen = ''
      this.tipoTarjeta = 'NUMERO DE TARGETA INVALIDO'
    }
    // })

  }
agregarValidPAgo
  idPolicyData;
  idReceipData
  pagoAfirme() {
    this.loaderGo = true;

    this.datosAtomar = JSON.parse(localStorage.getItem('lastCotizacion'))
    this.formaPago = localStorage.getItem('payFormSelected')
    this.selectCober = localStorage.getItem('coverageSelected')
    this.lastQuotation = JSON.parse(localStorage.getItem('cotizacionSaved'));
    let _jT = JSON.parse(this.lastQuotation.peticion);

    if (localStorage.getItem('tmpMultiSelected') !== 'AFIRME') {
      if ((this.selectCober).toLowerCase() == 'amplia') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][0]['amplia']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][0]['amplia'][0][`${(this.formaPago).toLowerCase()}`]
      } else if ((this.selectCober).toLowerCase() == 'limitada') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][1]['limitada']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][1]['limitada'][0][`${(this.formaPago).toLowerCase()}`]
      } else if ((this.selectCober).toLowerCase() == 'rc') {
        this.recorreCoberturas = this.datosAtomar['coberturas'][2]['rc']
        this.recorerCotizacion = this.datosAtomar['cotizacion'][2]['rc'][0][`${(this.formaPago).toLowerCase()}`]
      }
      delete this.datosAtomar['emision'].fechaEmision
      delete this.datosAtomar['emision'].inicioVigencia
    }else{
      this.recorreCoberturas = this.datosAtomar['coberturas']
      this.recorerCotizacion = this.datosAtomar['cotizacion']
    }

    this.bodyPago = {
      aseguradora: this.datosAtomar['aseguradora'],
      urlRedireccion: '',
      cliente: {
        nombre: this.datos_personales.nombre.value,
        apellidoPat: this.datos_personales.aPaterno.value,
        apellidoMat: this.datos_personales.aMaterno.value,
        curp: this.datos_personales.curp.value,
        edad: this.datosAtomar['cliente'].edad,
        email: this.datos_personales.correo.value,
        // fechaNacimiento: (this.datos_personales.fechaNacimiento.value) ? moment(this.datos_personales.fechaNacimiento.value).format('DD/MM/YYYY') : '',
        fechaNacimiento: (this.datos_personales.fechaNacimiento.value) ? moment(this.datos_personales.fechaNacimiento.value).format('YYYY-MM-DD') : '',
        genero: this.datosAtomar['cliente'].genero,
        ocupacion: '',
        rfc: this.datos_personales.rfc.value,
        telefono: this.datos_personales.telefono.value,
        tipoPersona: 'F',
        direccion: {
          calle: this.datos_direccion.calleNum.value,
          ciudad: (this.IdCpValidarEnBody),
          codPostal: _jT.cp,
          colonia: (this.datos_direccion.colonia.value),
          noExt: this.datos_direccion.numero.value,
          noInt: this.datos_direccion.numInterior.value,
          pais: '',
          poblacion: (this.datos_direccion.estadoMun.value) ? this._valueCity(2, this.datos_direccion.estadoMun.value) : ''
        }
      },
      coberturas: this.recorreCoberturas,
      codigoError: '',
      descuento: (this.datosAtomar.descuento).toString(),
      cotizacion: this.recorerCotizacion,
      emision: this.datosAtomar['emision'],
      paquete: localStorage.getItem('coverageSelected').toUpperCase(),
      periodicidadDePago: '',
      // urlRedireccion: '',
      pago: {
        anioExp: '',
        banco: '',
        carrier: 0,
        codigoSeguridad:  '',
        msi: '',
        medioPago: '',
        mesExp: '',
        noClabe: '',
        noTarjeta: '',
        nombreTarjeta: '',
      },
      vehiculo: {
        clave: _jT.clave,
        codDescripcion: '',
        codMarca: '',
        codUso: "",
        descripcion: _jT.descripcion,
        marca: _jT.marca,
        modelo: _jT.modelo,
        noMotor: this.datos_vehiculo.motor.value || this.numeroMotor,
        noPlacas: this.datos_vehiculo.placa.value,
        noSerie: this.datos_vehiculo.niv.value,
        servicio: "PARTICULAR",
        subMarca: this.datosAtomar.vehiculo.subMarca,
        uso: "PARTICULAR"
      }
    };
    this.IdCpValidarEnBody = this.bodyPago
    this.bodyPago.pago.noTarjeta = 'tdd'
    console.log('----------',this.bodyPago.pago.noTarjeta);
    let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';
    console.log(this.bodyPago);

     this.catalogosService.postEmisionAfirme(this.bodyPago).subscribe((dataAfirme) => {
       if(dataAfirme.emision.terminal === ""){
          Swal.fire({
            icon: 'warning',
            title: 'Lo sentimos intente más tarde!',
            text: 'Se produjo un problema al cargar el formulario de pago.',
          }).then(() => {
            this.router.navigateByUrl('admin/cotiza-go/particular/cotizador');
          });
       }
       this.agregarValidPAgo = dataAfirme;
       this.cotizacionService.updateFake(_SavedFake.id, dataAfirme,9).subscribe(
         succ => { console.log("OK");},
         err => {
           console.error(err);
          }
          );
       this.urlLinkDePago = this.sanitizer.bypassSecurityTrustResourceUrl(dataAfirme.emision.terminal);
       this.loaderGo = false;
    }, (errorAfirme) => {
       this.cotizacionService.updateFake(_SavedFake.id, this.bodyPago,9).subscribe(
           succ => { console.log("OK");},
           err => {
             console.error(err);
           }
       );
       this.loaderGo = false;
       Swal.fire({
         icon: 'error',
         title: 'Oops...',
         text: 'Favor de comunicar al administrador de la pagina',
       })
     })

  }
  continuarGracias(){
    // this.agregarValidPAgo = this.bodyPago
    Swal.fire({
      title: '¿Deseas continar?',
      text: "Si aun no concluyes el pago favor de no salir de la pagina",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar'
    }).then((result) => {
      if (result.isConfirmed) {
        let _SavedFake = JSON.parse(localStorage.getItem('savedFake')) || '';

        this.idPolicyData = (this.agregarValidPAgo.emision.idCotizacion !== undefined) ? this.agregarValidPAgo.emision.idCotizacion : '';
        this.idReceipData = (this.agregarValidPAgo.emision.poliza !== undefined) ? this.agregarValidPAgo.emision.poliza : '';
        this.catalogosService.validDataPago(this.idPolicyData, this.idReceipData).subscribe((dataValidPago) => {
          if(dataValidPago.data.applyTransact === true){
            this.cotizacionService.updateFake(_SavedFake.id, this.bodyPago,11).subscribe(
                succ => { console.log("OK");},
                err => {
                  console.error(err);
                }
            );
          }else{
            this.cotizacionService.updateFake(_SavedFake.id, this.bodyPago,12).subscribe(
                succ => { console.log("OK");},
                err => {
                  console.error(err);
                }
            );
          }
          console.log(dataValidPago)
        })
        this.router.navigateByUrl('admin/cotiza-go/particular/gracias');
      }
    })
  }

  estadosNacimiento = [
    {value: 'AS', text: 'AGUASCALIENTES'},
    {value: 'BC', text: 'BAJA CALIFORNIA'},
    {value: 'BS', text: 'BAJA CALIFORNIA SUR'},
    {value: 'CC', text: 'CAMPECHE'},
    {value: 'CL', text: 'COAHUILA DE ZARAGOZA'},
    {value: 'CM', text: 'COLIMA'},
    {value: 'CS', text: 'CHIAPAS'},
    {value: 'CH', text: 'CHIHUAHUA'},
    {value: 'DF', text: 'CIUDAD DE MEXICO'},
    {value: 'DG', text: 'DURANGO'},
    {value: 'GT', text: 'GUANAJUATO'},
    {value: 'GR', text: 'GUERRERO'},
    {value: 'HG', text: 'HIDALGO'},
    {value: 'JC', text: 'JALISCO'},
    {value: 'MC', text: 'MEXICO'},
    {value: 'MN', text: 'MICHOACAN DE OCAMPO'},
    {value: 'MS', text: 'MORELOS'},
    {value: 'NT', text: 'NAYARIT'},
    {value: 'NL', text: 'NUEVO LEON'},
    {value: 'OC', text: 'OAXACA'},
    {value: 'PL', text: 'PUEBLA'},
    {value: 'QT', text: 'QUERETARO DE ARTEAGA'},
    {value: 'QR', text: 'QUINTANA ROO'},
    {value: 'SP', text: 'SAN LUIS POTOSI'},
    {value: 'SL', text: 'SINALOA'},
    {value: 'SR', text: 'SONORA'},
    {value: 'TC', text: 'TABASCO'},
    {value: 'TS', text: 'TAMAULIPAS'},
    {value: 'TL', text: 'TLAXCALA'},
    {value: 'VZ', text: 'VERACRUZ'},
    {value: 'YN', text: 'YUCATAN'},
    {value: 'ZS', text: 'ZACATECAS'},
    {value: 'NE', text: 'NACIDO EN EL EXTRANJERO'}];


}
function jwt_decode(_token: string): any {
  throw new Error('Function not implemented.');
}

