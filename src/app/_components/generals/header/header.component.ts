import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { UserService, MenuToggleService } from 'src/app/_services';
import { ResponsiveInterface } from 'src/app/_interfaces';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  user: any;
  iName: any;
  iLastName: any;
  toggle: boolean = false;
  toggleMenu: boolean = false;
  elem: any;
  displayMessages: boolean = false;
  scrHeight: any;
  scrWidth: any;
  responsive: ResponsiveInterface;
  GENERALSE = GENERALS;
  isFullScreen: boolean = false;
  @HostListener('document:fullscreenchange', ['$event'])
  @HostListener('document:webkitfullscreenchange', ['$event'])
  @HostListener('document:mozfullscreenchange', ['$event'])
  @HostListener('document:MSFullscreenChange', ['$event'])

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;

    if (this.scrWidth <= GENERALS.responsive.width) {
      setTimeout(() => {
        this.eventLocal(true);
      }, 500);
    }
  }

  constructor(
    private router: Router,
    private userService: UserService,
    private menuToggleService: MenuToggleService,
    @Inject(DOCUMENT) private document: any
  ) {
    this.getScreenSize();
  }

  ngOnInit(): void {
    this._getInfo();
    this.elem = document.documentElement;
  }

  fullscreenmodes(event) {
    this.chkScreenMode();
  }
  
  chkScreenMode() {
    if (document.fullscreenElement) {
      //fullscreen
      this.isFullScreen = true;
    } else {
      //not in full screen
      this.isFullScreen = false;
    }
  }

  fullScreen(): void {
    if (!this.isFullScreen) {
      if (this.elem.requestFullscreen) {
        this.elem.requestFullscreen();
      } else if (this.elem.mozRequestFullScreen) {
        /* Firefox */
        this.elem.mozRequestFullScreen();
      } else if (this.elem.webkitRequestFullscreen) {
        /* Chrome, Safari and Opera */
        this.elem.webkitRequestFullscreen();
      } else if (this.elem.msRequestFullscreen) {
        /* IE/Edge */
        this.elem.msRequestFullscreen();
      }
    } else {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        /* Firefox */
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        /* Chrome, Safari and Opera */
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        /* IE/Edge */
        this.document.msExitFullscreen();
      }
    }
  }

  private _getInfo(): void {
    this.userService.getInfo().subscribe(

      succ => {
        this.user = succ;
        this.iName = succ.nombre.substring(0,1);
        this.iLastName = succ.apellidoPaterno.substring(0,1);
      },
      err => {
        console.error(err);
      }
    );
  }

  eventLocal(toggle: boolean): void {
    this.menuToggleService.emitNavChangeEvent(toggle);
  }

  linkLocal(redirect: string): void {
    this.toggle = false;
    if (redirect === '/logOut') {
      this.logOut();
    } else {
      this.router.navigateByUrl(redirect);
    }
  }

  logOut(): void {
    this.userService.destroySession();
    this.userService.signOut();
  }
}
