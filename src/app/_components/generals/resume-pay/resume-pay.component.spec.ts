import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumePayComponent } from './resume-pay.component';

describe('ResumePayComponent', () => {
  let component: ResumePayComponent;
  let fixture: ComponentFixture<ResumePayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResumePayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumePayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
