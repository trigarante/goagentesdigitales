import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HomologationParseResultService, ToLowerCaseKeyService } from 'src/app/_services';
import { EvetnsEmiterLocalService } from 'src/app/_services/evetns-emiter-local.service';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Component({
  selector: 'resume-pay',
  templateUrl: './resume-pay.component.html',
  styleUrls: ['./resume-pay.component.css']
})
export class ResumePayComponent implements OnInit, AfterViewInit {
  @Input() aseguradoraSelected: string;
  @Input() commonRequest;
  @Input() valorPrecio;

  lastQuotation;
  totalDisplay: number = 0;
  totalvalorPrecio : any;
  aseguradoraDisplay: string = '';

  datosPersonalesResume: boolean = true;
  direccionResume: boolean = false;
  vehiculoResume: boolean = false;
  pagoResume: boolean = false;
  urlType: string = '';
  generlsDiscount = GENERALS.discount
  bancsMSI;

  constructor(
      private router: Router,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private evetnsEmiterLocalService: EvetnsEmiterLocalService,
    private toLowerService: ToLowerCaseKeyService,
    private homologationParseResultService: HomologationParseResultService
  ) {
    this.route.params.subscribe(params => {
      this.urlType = params.typeQuote;
    });
  }

  ngOnInit(): void {
    if(this.aseguradoraSelected === 'general_De_Seguros'||this.aseguradoraSelected === 'GS'){
      this.aseguradoraSelected = 'GENERAL';
    }else if(this.aseguradoraSelected=== 'la_latino'){
      this.aseguradoraSelected = 'LATINO'
    }else if(this.aseguradoraSelected === 'el_potosi'){
      this.aseguradoraSelected = 'ELPOTOSI'
    }
    this.bancsMSI = this.generlsDiscount.find(el => el.insurance.toUpperCase() === this.aseguradoraSelected.toUpperCase() ).bancs;
    this.commonRequest;
    this.aseguradoraSelected;
    // this.aseguradoraSelected = localStorage.getItem('tokenAs') || localStorage.getItem('tmpMultiSelected') || null;

    this.lastQuotation = JSON.parse(localStorage.getItem('lastCotizacion'));
    this.totalvalorPrecio = localStorage.getItem('valorPrecio')
    // localStorage.setItem('totalP', this.lastQuotation.cotizacion.primaTotal);
    if (this.aseguradoraSelected.toUpperCase() !== GENERALS.particual_quote[0].insurance_carrier.toUpperCase() && this.aseguradoraSelected.toUpperCase() !== GENERALS.particual_quote[1].insurance_carrier.toUpperCase()) {
      this.lastQuotation = this.toLowerService.parseToLower(this.lastQuotation);
    }

    if (this.urlType === 'privado') {
      this.totalDisplay = this.lastQuotation.cotizacion.primaTotal;
      // this.totalDisplay = this.totalvalorPrecio;
    } else if (this.urlType === 'particular') {
      if(this.lastQuotation.cotizacion.primaTotal){
        this.totalDisplay = this.lastQuotation.cotizacion.primaTotal;
        // this.totalDisplay = this.totalvalorPrecio;
      }else {
        this.totalDisplay = Number(localStorage.getItem('totalP'));
      }

      //OBTIENE EL VALOR DIRECTO DEL JSON DE LA COTIZACION
      // this.totalDisplay = this.lastQuotation.cotizacion.primaTotal;
    }

    this.evetnsEmiterLocalService.getResumeChangeEmitter()
      .subscribe((item: any) => {
        switch (item) {
          case 2:
            if (this.commonRequest.address.estadoMun.value && this.commonRequest.address.calleNum.value) {
              this.datosPersonalesResume = false;
              this.direccionResume = true;
              this.vehiculoResume = false;
              this.pagoResume = false;
            }
            break;
          case 3:
            if (this.commonRequest.car.niv.value) {
              this.datosPersonalesResume = false;
              this.direccionResume = false;
              this.vehiculoResume = true;
              this.pagoResume = false;
            }
            this.cdRef.detectChanges();
            break;
          case 4:
            if (this.commonRequest.card.tipoTarjeta.value) {
              this.datosPersonalesResume = false;
              this.direccionResume = false;
              this.vehiculoResume = false;
              this.pagoResume = true;
            }
            this.cdRef.detectChanges();
            break;
          default:
            this.datosPersonalesResume = true;
            this.direccionResume = false;
            this.vehiculoResume = false;
            this.pagoResume = false;
            break;
        }
      });
  }

  ngAfterViewInit() {
    this.cdRef.detectChanges();
  }

  setDisplay(step: number): boolean {
    // this.evetnsEmiterLocalService.emitResumeChangeEvent(step);
    return true;
  }
}
