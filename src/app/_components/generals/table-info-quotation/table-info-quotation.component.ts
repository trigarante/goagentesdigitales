import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'table-info-quotation',
  templateUrl: './table-info-quotation.component.html',
  styleUrls: ['./table-info-quotation.component.css']
})
export class TableInfoQuotationComponent implements OnInit {
  @Input() coverage: any;
  @Input() aseg: any;

  constructor() { }

  ngOnInit(): void {
    this.coverage=this.parseToLower(JSON.stringify(this.coverage));
  }
  // Funcion para convertir de mayusculas a minusculas las iniciales del json
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            //console.log('newprop');
            //console.log(newprop);
            //console.log('json[newprop]');
            //console.log(json[newprop]);
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
        }
      }
    }
    return json;
  }

  convertObject(value: any) {
    let _return = [];
    let _llll = value.forEach(element => {
      for (let _key in element) {
        if (element[_key].length > 2) {
          _return.push(element[_key]);
        }
      }
    });

    return _return;
  }
}
