import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableInfoQuotationComponent } from './table-info-quotation.component';

describe('TableInfoQuotationComponent', () => {
  let component: TableInfoQuotationComponent;
  let fixture: ComponentFixture<TableInfoQuotationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableInfoQuotationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableInfoQuotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
