import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MessagesComponent implements OnInit {
  option: any;
  promo: any = GENERALS.month_promo;

  constructor(
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.option = params.option;
    });
  }

  ngOnInit(): void {
    let _month = GENERALS.month_promo.title + moment().lang("es").format('MMMM')
    this.promo.title = _month.charAt(0).toUpperCase() + _month.slice(1);
  }

}
