import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubirArchivoRegistroComponentComponent } from './subir-archivo-registro-component.component';

describe('SubirArchivoRegistroComponentComponent', () => {
  let component: SubirArchivoRegistroComponentComponent;
  let fixture: ComponentFixture<SubirArchivoRegistroComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubirArchivoRegistroComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubirArchivoRegistroComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
