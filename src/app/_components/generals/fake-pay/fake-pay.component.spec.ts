import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FakePayComponent } from './fake-pay.component';

describe('FakePayComponent', () => {
  let component: FakePayComponent;
  let fixture: ComponentFixture<FakePayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FakePayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakePayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
