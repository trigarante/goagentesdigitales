import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalDialog } from '../../models';

export enum AlertType { INFO, WARNING, ERROR }

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  dataLocal: any;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: ModalDialog
  ) { 
    this.dataLocal = data['data'];
  }

  ngOnInit(): void {
  }

  getAlertIcon(alertType?: string): string {
    switch (alertType) {
      default:
      case 'info': return 'info';
      case 'warning': return 'warning';
      case 'error': return 'error';
    }
  }
}
