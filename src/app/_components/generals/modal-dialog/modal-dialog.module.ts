import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './_services';
import { MaterialModule } from 'src/app/_modules/material.module';
import { CommonLocalModule } from 'src/app/_modules/common-local.module';


@NgModule({
    declarations: [
        ModalComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        CommonLocalModule
    ],
    providers: [
        ModalService
    ]
})
export class ModalDialogModule { }
