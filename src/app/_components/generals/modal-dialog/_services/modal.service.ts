import { Injectable } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../';

export enum AlertType { INFO, WARNING, ERROR }

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
    public dialog: MatDialog
  ) { }

  openInfoQuotationModal(title: string, option: any) { 
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '95%',
      data: new ModalComponent({
        title: title,
        content: option,
        closeButtonLabel: 'Cerrar',
        type: 'tableContent'
      })
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('After Close Modal', result);
    });
  }

  // openConfirmModal(message: string, callBackFunction: Function) {
  //   const dialogRef = this.dialog.open(ConfirmComponent, {
  //     width: '300px',
  //     data: new ModalConfirmData({
  //       title: 'CONFIRM',
  //       content: message,
  //       confirmButtonLabel: 'Confirm',
  //       closeButtonLabel: 'Close'
  //     })
  //   });

  //   dialogRef.afterClosed().subscribe(result => callBackFunction(result));
  // }
}
