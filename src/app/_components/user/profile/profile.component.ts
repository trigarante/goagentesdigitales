import { Component, OnInit } from '@angular/core';
import { DatosServiceService } from 'src/app/_services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  mostrarCambioPass: boolean = false;
  cambiarImagen: boolean = false;
  user: any;

  constructor(
    private datosService: DatosServiceService
  ) { }

  ngOnInit(): void {
    this._getData();
  }

  private _getData(): void {
    this.datosService.get().subscribe(
      data => {
        this.user = data;
      }, 
      err => {
        console.error(err);
      }
    );
  }
}
