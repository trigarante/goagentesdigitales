import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';
import { ProfileComponent } from './profile/profile.component';
import { DatosServiceService } from 'src/app/_services';

@NgModule({
  declarations: [
    UserComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,

    CommonLocalModule
  ],
  providers: [
    DatosServiceService
  ]
})
export class UserModule { }
