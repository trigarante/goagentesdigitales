import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';
import { ProfileComponent } from './profile/profile.component';

import { UserComponent } from './user.component';

const routes: Routes = [
  { path: '', component: UserComponent, children: [
    { path: 'perfil', component: ProfileComponent, canActivate: [AuthGuardGuard] },

    { path: ':any', redirectTo: '/admin/perfil', pathMatch: 'full' },
    { path: '', redirectTo: '/admin/perfil', pathMatch: 'full' }
    ] 
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
