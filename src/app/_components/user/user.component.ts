import { Component, OnInit } from '@angular/core';
import { MenuToggleService } from 'src/app/_services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
toggleMenu: boolean = false;
  constructor(  private menuToggleService: MenuToggleService
  ) { }

  ngOnInit(): void {
  this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {this.toggleMenu = item;});
  }

}
