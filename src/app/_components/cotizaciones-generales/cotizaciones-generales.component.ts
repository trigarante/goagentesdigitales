import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-cotizaciones-generales',
  templateUrl: './cotizaciones-generales.component.html',
  styleUrls: ['./cotizaciones-generales.component.css']
})
export class CotizacionesGeneralesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  opcion(id){
    if(id === 1){
      this.router.navigate(['admin/cotizaciones/cotizacionesAutos'])
    }else if(id === 2) {
      this.router.navigate(['admin/cotizaciones/cotizacionesRamos',84])
    }else if(id === 3) {
      this.router.navigate(['admin/cotizaciones/cotizacionesRamos',85])
    }else if(id === 4) {
      this.router.navigate(['admin/cotizaciones/cotizacionesRamos',89])
    }else if(id === 5) {
      this.router.navigate(['admin/cotizaciones/cotizacionesRamos',86])
    }else if(id === 6) {
      this.router.navigate(['admin/cotizaciones/cotizacionesRamos',87])
    }
  }

}
