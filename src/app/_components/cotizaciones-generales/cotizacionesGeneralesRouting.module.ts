import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CotizacionesGeneralesComponent} from "./cotizaciones-generales.component";
import {CotizacionesPortalComponent} from "./cotizaciones-portal/cotizaciones-portal.component";
import {CotizacionRamosComponent} from "./cotizacion-ramos/cotizacion-ramos.component";
const routes:Routes = [
    {
        path: '', children: [
            {path: '', component: CotizacionesGeneralesComponent},
            {path: 'cotizacionesAutos', component: CotizacionesPortalComponent},
            {path: 'cotizacionesRamos/:id', component: CotizacionRamosComponent},
        ]
    }
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CotizacionesGeneralesRoutingModule { }
