import {Component, Input, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {CotizacionResponseInterface} from "../../../../_interfaces";
import {CatalogosService, EvetnsEmiterLocalService} from "../../../../_services";
import {Router} from "@angular/router";
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-tabla-cotiza',
  templateUrl: './tabla-cotiza.component.html',
  styleUrls: ['./tabla-cotiza.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TablaCotizaComponent implements OnInit {
  dataSource : any;
  @Input() coverage;
  @Input() aseg;
  displayedColumns = ['marca', 'modelo', 'movimiento',  'emitir',];
  expandedElement: CotizacionResponseInterface | null;
  selectedID;
  loaderGoEmision : boolean;
  bodymultyQoute;

  constructor(    private evetnsEmiterLocalService: EvetnsEmiterLocalService,
                  private catalogosService: CatalogosService,
                  public router: Router, ) { }

  ngOnInit(): void {
    let elimina = (this.coverage.respuesta).filter((item) => ((item.cotizacion.primaTotal !== '') && item.cotizacion.primaTotal !== '0.0'))
    this.dataSource = elimina;
    this.dataSource = this.dataSource.sort(function(a, b) {
      let datoA=0;
      let datoB=0
      if(a['cotizacion']){
        if(a['cotizacion']['primaTotal'] === '' || a['cotizacion']['primaTotal'] === '0.0'){
          datoA = 100000
        }else{
          datoA = a['cotizacion']['primaTotal'];
        }
      }
      if(b['cotizacion']){
        if(b['cotizacion']['primaTotal'] === '' || b['cotizacion']['primaTotal'] === '0.0'){
          datoB = 1000000
        }else{
          datoB = b['cotizacion']['primaTotal'];
        }
      }
      return Number(datoA) - Number(datoB);
    });
  }
  nuevaDataSinVacio;
  highlight(row, detailRow?) {
    localStorage.setItem('row', JSON.stringify(row))
    this.bodymultyQoute = [{
      aseguradora : row.aseguradora,
      cotizacion : row,
      version : []
    }]
    localStorage.setItem('multiQuote', JSON.stringify(this.bodymultyQoute));
    localStorage.setItem('lastCotizacion', JSON.stringify(row))
    this.selectedID = (this.selectedID !== row.aseguradora) ? row.aseguradora : null;
  }
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
        }
      }
    }
    return json;
  }

  jumpWizard(insurance: string): void {
    let nuevoQ = localStorage.getItem('tmpMultAskQuotation');
    if(localStorage.getItem('tmpMultAskQuotation')){
      nuevoQ = JSON.parse(nuevoQ);
      nuevoQ[0]['aseguradora'] = insurance;
      localStorage.setItem('tmpMultAskQuotation', JSON.stringify(nuevoQ));
    }
    this.loaderGoEmision = true
    localStorage.setItem('tmpMultiSelected', insurance);
    this._eventLocal(true, insurance);
    this._setCotizacionSaved(insurance);
  }
  private _eventLocal(active: boolean, insurance: string): void {
    this.evetnsEmiterLocalService.emitLoader(JSON.stringify({ active: active, insurance: insurance }));
  }
  private _setCotizacionSaved(insurance: string): void {
    let _tmp = JSON.parse(localStorage.getItem('tmpMultAskQuotation'));
    let nuevaPet = JSON.parse(localStorage.getItem('row'))
    let objetPet = [];
    objetPet.push(nuevaPet)
    let _quotation = _tmp.filter(el => el.aseguradora.toLowerCase() === insurance.toLowerCase())[0];
    localStorage.removeItem('cotizacionSaved');
    let _cotizacionRequest = _tmp.find(el => el.aseguradora.toLowerCase() === insurance.toLowerCase());
    let _token = localStorage.getItem('token') || '';
    let tokenData = _token;
    let idAgente = jwt_decode(tokenData);
    const dataSaveCot = {
      idAgente: +idAgente['sub'].split(':')[1],
      peticion: JSON.stringify(_cotizacionRequest),
      // respuesta: "[]",
      respuesta: nuevaPet ? JSON.stringify(objetPet) : ''
    };
    this.catalogosService.guardarCotizacion(dataSaveCot).subscribe(
        respuesta => {
          this.loaderGoEmision = false;
          localStorage.setItem('cotizacionSaved', JSON.stringify(respuesta));
          if(localStorage.getItem('tmpMultiSelected') === 'MAPFRE'){
            this.router.navigate(['admin/cotiza-go/particular/emision/mapfre']);
          }else if(localStorage.getItem('tmpMultiSelected') === 'GNP'){
            this.router.navigate(['admin/cotiza-go/particular/emision/gnp']);
          }else if(localStorage.getItem('tmpMultiSelected') === 'QUALITAS'){
            this.router.navigate(['admin/cotiza-go/particular/emision/qualitas']);
          }else if(localStorage.getItem('tmpMultiSelected') === 'BANORTE'){
            this.router.navigate(['admin/cotiza-go/particular/emision/banorte']);
          }else if(localStorage.getItem('tmpMultiSelected') === 'ABA'){
            this.router.navigate(['admin/cotiza-go/particular/emision/aba']);
          }else if(localStorage.getItem('tmpMultiSelected') === 'AFIRME'){
            this.router.navigate(['admin/cotiza-go/particular/emision/afirme']);
          }else {
            this.router.navigate(['admin/cotiza-go/particular/emision/']);
          }
        },
        err => {
          this.loaderGoEmision = false;
        }
    );
    localStorage.setItem('cotizacionSaved', JSON.stringify(_quotation));
  }

}
