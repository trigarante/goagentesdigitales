import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-tabla-cot-cob',
  templateUrl: './tabla-cot-cob.component.html',
  styleUrls: ['./tabla-cot-cob.component.css']
})
export class TablaCotCobComponent implements OnInit {
  @Input() coverage2
  @Input() aseg2
  constructor() { }
  ngOnInit(): void {
    this.coverage2 = this.parseToLower(JSON.stringify(this.coverage2));
  }
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
        }
      }
    }
    return json;
  }
  convertObject(value: any) {
    let _return = [];
    let _llll = value.forEach(element => {
      for (let _key in element) {
        if (element[_key].length > 2) {
          _return.push(element[_key]);
        }
      }
    });
    return _return;
  }
}
