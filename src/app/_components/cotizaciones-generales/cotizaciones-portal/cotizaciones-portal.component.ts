import { Component, OnInit } from '@angular/core';
import {CotizacionResponseInterface} from "../../../_interfaces";
import {AseguradorasOnOffService} from "../../../_services/aseguradoras-on-off.service";
import Swal from "sweetalert2";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-cotizaciones-portal',
  templateUrl: './cotizaciones-portal.component.html',
  styleUrls: ['./cotizaciones-portal.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CotizacionesPortalComponent implements OnInit {
  dataSource: any;
  displayedColumns = ['marca', 'modelo', 'movimiento',  'emitir',];
  expandedElement: CotizacionResponseInterface | null;
  selectedID;
  aumentar: number = 0;
  cotizacionesAgreg = []
  desabilitaBtn: boolean;
  nuevaEstructura = [];
  loaderGo: boolean = false;

  constructor(private tablaCotizaciones: AseguradorasOnOffService) { }
  ngOnInit(): void {
    this.cotizacionesPrincipal(this.aumentar);
  }
  cotizacionesPrincipal(valor : number){
    this.loaderGo = true;
    this.aumentar += valor
    this.tablaCotizaciones.tablaDeCotizacionesService(localStorage.getItem('goId'), this.aumentar, 10).subscribe((dataTable) => {
      this.nuevaEstructura = []
      if(dataTable.length === 0){
        Swal.fire('No hay datos');
        this.desabilitaBtn = true;
      }else{
        this.desabilitaBtn = false;
        for(let j = 0; j < dataTable.length; j++){
          for(let k = 0; k < dataTable[j].respuesta.length; k++) {
            if (dataTable[j].respuesta[k].body) {
              dataTable[j].respuesta[k] = dataTable[j].respuesta[k].body
            }
          }
        }
        this.dataSource = dataTable
      }
      this.loaderGo = false;
    })
  }
  highlight(row, detailRow?) {
    this.cotizacionesAgreg = [];
    for(let i = 0; i < this.dataSource.length; i++){
      if (this.dataSource[i] === row){
        this.cotizacionesAgreg.push(this.dataSource[i].peticion);
        localStorage.setItem('cpAfirme', this.dataSource[i].peticion.cp);
        localStorage.setItem('_tmpMultyPush', JSON.stringify(this.dataSource[i]));
        localStorage.setItem('_t', this.dataSource[i].peticion.genero);
        localStorage.setItem('cotizador', '1')
      }
    }
    localStorage.setItem('tmpMultAskQuotation', JSON.stringify(this.cotizacionesAgreg));
  }

}
