import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CotizacionesGeneralesRoutingModule} from "./cotizacionesGeneralesRouting.module";
import {CommonLocalModule} from "../../_modules/common-local.module";
import {MaterialModule} from "../../_modules/material.module";
import {CotizacionesPortalComponent} from "./cotizaciones-portal/cotizaciones-portal.component";
import {TablaCotizaComponent} from "./cotizaciones-portal/tabla-cotiza/tabla-cotiza.component";
import {TablaCotCobComponent} from "./cotizaciones-portal/tabla-cotiza/tabla-cot-cob/tabla-cot-cob.component";
import {CotizacionRamosComponent} from "./cotizacion-ramos/cotizacion-ramos.component";



@NgModule({
  declarations: [
    CotizacionesPortalComponent,
    TablaCotizaComponent,
    TablaCotCobComponent,
    CotizacionRamosComponent
  ],
  imports: [
    CommonModule,
    CotizacionesGeneralesRoutingModule,
    CommonLocalModule,
    MaterialModule
  ],
  exports:[

  ]
})
export class CotizacionesGeneralesModule { }
