import { Component, OnInit } from '@angular/core';
import {OtrosRamosServicesService} from "../../../_services/otros-ramos-services.service";
import Swal from "sweetalert2";
import {FormBuilder, Validators} from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cotizacion-ramos',
  templateUrl: './cotizacion-ramos.component.html',
  styleUrls: ['./cotizacion-ramos.component.css']
})
export class CotizacionRamosComponent implements OnInit {

  displayedColumns: string[] = ['idAgente', 'idSubramo', 'datosPeticion'];
  dataSource: any;
  loaderGo : boolean = false;
  aumentar: number = 0;
  desabilitaBtn: boolean;
  vistaDeTabla: boolean = false;
  idRamoSelected
  urlOption: string = '';
  private readParams: any;
  constructor(private otrosRamosService : OtrosRamosServicesService,
              private fb :FormBuilder,
              private aRoute: ActivatedRoute,
              private router: Router) { this.loaderGo = false }

  formSelectRamo = this.fb.group({
    SelectRamo:['', Validators.required],
  })

  ngOnInit(): void {
    
     this.readParams= this.aRoute.params.subscribe(params => {
      this.idRamoSelected = +params['id'];
    });

    this.urlOption = '/admin/cotizaciones/cotizacionesRamos/'+ this.idRamoSelected;

    this.selectTable(this.idRamoSelected)
  }

  selectTable(idRamo: number){
    this.loaderGo = true;
    this.aumentar = 0
    this.desabilitaBtn = false;
    this.dataSource = []
    this.vistaDeTabla = true
    this.otrosRamosService.verOtrosRamos(localStorage.getItem('goId'), 0,10,idRamo).subscribe((dataView) =>{
      this.dataSource = dataView
      this.loaderGo = false;
      if(this.dataSource.length === 0){
        // Swal.fire('No hay datos');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'No hay datos',
      }).then((_) => {
        this.router.navigateByUrl('/admin/cotizaciones');
      })
      }
      // console.log(dataView, "Es la data de selectTable");
    })
  }

  cotizacionesPrincipal(valor : number){
    this.aumentar += valor
    this.otrosRamosService.verOtrosRamos(localStorage.getItem('goId'), this.aumentar,10,this.idRamoSelected).subscribe((dataView) =>{
      let size:any = [];
      size = dataView;
      if(size.length === 0){
        Swal.fire('No hay datos');
        this.aumentar -= 1;
        return;
      }else{
        this.desabilitaBtn = false;
        this.dataSource = dataView
      }
    })

  }

  jumpWizard(){
    Swal.fire('Aun en desarrollo')
  }

}
