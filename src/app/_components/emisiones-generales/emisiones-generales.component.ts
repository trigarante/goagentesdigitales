import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-emisiones-generales',
  templateUrl: './emisiones-generales.component.html',
  styleUrls: ['./emisiones-generales.component.css']
})
export class EmisionesGeneralesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  emisionesSelect(){
    this.router.navigateByUrl('/admin/emisionesGenerales/dowloadsPoliza')
    console.log('emison dowloadsPoliza')
  }

}
