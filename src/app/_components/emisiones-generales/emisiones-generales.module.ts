import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmisionesPolizaComponent } from './emisiones-poliza/emisiones-poliza.component';
import {EmisionesGeneralesRoutingModule} from "./emisiones-generales-routing.module";
import {MaterialModule} from "../../_modules/material.module";
import {CommonLocalModule} from "../../_modules/common-local.module";



@NgModule({
  declarations: [
    EmisionesPolizaComponent
  ],
    imports: [
        CommonModule,
        EmisionesGeneralesRoutingModule,
        MaterialModule,
        CommonLocalModule
    ]
})
export class EmisionesGeneralesModule { }
