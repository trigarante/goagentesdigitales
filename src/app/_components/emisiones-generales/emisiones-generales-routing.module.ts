import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {EmisionesPolizaComponent} from "./emisiones-poliza/emisiones-poliza.component";
import {EmisionesGeneralesComponent} from "./emisiones-generales.component";

const routes:Routes = [
  {
    path: '', children: [
      {path: '', component: EmisionesGeneralesComponent },
      {path: 'dowloadsPoliza', component: EmisionesPolizaComponent },
    ]
  }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, CommonModule]
})
export class EmisionesGeneralesRoutingModule { }
