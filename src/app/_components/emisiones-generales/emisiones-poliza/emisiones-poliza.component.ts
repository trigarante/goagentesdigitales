import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {EmisionesGeneralesService} from "../../../_services/emisiones-generales.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-emisiones-poliza',
  templateUrl: './emisiones-poliza.component.html',
  styleUrls: ['./emisiones-poliza.component.css']
})
export class EmisionesPolizaComponent implements OnInit {

  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'acciones'];
  displayedColumns: string[] = ['idCotizacion', 'aseguradora', 'cliente', 'email','genero' ,'rfc','primaTotal','periodicidadDePago','paquete', 'detalle','acciones'];
  // dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource : MatTableDataSource<any>;
  buscarGeneral = true;
  loaderGo : boolean;

  nombreCliente: string;
  // documento = '';
  desabilitaBtn

  constructor(private emisionesGeneralesService : EmisionesGeneralesService) { }

  ngOnInit(): void {
    this.emisionesGeneralesService.vistaTablaEmisiones(localStorage.getItem('goId'), 0,10).subscribe(data => {
      this.dataSource = new MatTableDataSource(data)
      
      this.dataSource.filterPredicate = function(data, filter: string): boolean {
        return data.registroEmision.nombreCliente.toLowerCase().includes(filter) ||
                data.registroEmision.correo.toLowerCase().includes(filter) ||
                data.registroEmision.poliza.toLowerCase().includes(filter);
      };

    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      console.log(data,filter);
      return data.registroEmision.nombreCliente.toLowerCase().includes(filter) ||
              data.registroEmision.correo.toLowerCase().includes(filter) ||
              data.registroEmision.poliza.toLowerCase().includes(filter);
    };
    this.dataSource.filter = filterValue;
  }
  
  aumentar: number = 0;
  cotizacionesPrincipal(valor : number){
    this.loaderGo = true;
    this.aumentar += valor;
    this.emisionesGeneralesService.vistaTablaEmisiones(localStorage.getItem('goId'), this.aumentar,10).subscribe(data => {
      this.loaderGo = false;
      this.dataSource = new MatTableDataSource(data);
      if(data.length === 0){
        Swal.fire('No hay datos');
        this.desabilitaBtn = true;
      }else{
        this.desabilitaBtn = false;
      }
    })
  }

  eventoDescargar(elemento){
    if(typeof elemento === 'object'){
      window.open(elemento[0].poliza)
    }else{
      window.open(elemento)
    }
  }

}
