import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthsWithoutInterestComponent } from './months-without-interest.component';

describe('MonthsWithoutInterestComponent', () => {
  let component: MonthsWithoutInterestComponent;
  let fixture: ComponentFixture<MonthsWithoutInterestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthsWithoutInterestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthsWithoutInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
