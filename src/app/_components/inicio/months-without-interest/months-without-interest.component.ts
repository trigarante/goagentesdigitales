import { Component, OnInit } from '@angular/core';
import { T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import {ImagenesService} from '../../../_services/imagenes.service';
@Component({
  selector: 'app-months-without-interest',
  templateUrl: './months-without-interest.component.html',
  styleUrls: ['./months-without-interest.component.css']
})
export class MonthsWithoutInterestComponent implements OnInit {
  tableMSI: string;
  imgMSI=[];
  constructor(
    private t2020Service: T2020Service,
    private imagenesService : ImagenesService
  ) { }

  ngOnInit(): void {
    this._getPhoneBook();
    this._getServiceimagenesMSI();
  }

  private _getPhoneBook(): void {
    this.t2020Service.getFilesByArea(23).subscribe(
      succ => {
        console.log('succ',succ);
        this.tableMSI = environment.endpoint_t2020_drive + succ[0].url;
      }, 
      err => {
        console.error(err.messageError);
      }
    );
  }
  _getServiceimagenesMSI():void{
  this.imagenesService.getMesesSinInt().subscribe((resp:[])=>{
      this.imgMSI = resp;
       //console.log('Se esta consumiendo con exito imagenesService', this.imgMSI);
    })
  }

}
