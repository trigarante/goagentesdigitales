import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncentiveNotebookComponent } from './incentive-notebook.component';

describe('IncentiveNotebookComponent', () => {
  let component: IncentiveNotebookComponent;
  let fixture: ComponentFixture<IncentiveNotebookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncentiveNotebookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncentiveNotebookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
