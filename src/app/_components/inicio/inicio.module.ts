import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InicioRoutingModule } from './inicio-routing.module';
import { InicioComponent } from './inicio.component';

import { CommonLocalModule } from 'src/app/_modules/common-local.module';
import { SpecialPromotionsComponent } from './special-promotions/special-promotions.component';
import { MonthsWithoutInterestComponent } from './months-without-interest/months-without-interest.component';
import { IncentiveNotebookComponent } from './incentive-notebook/incentive-notebook.component';
import { CalendarComponent } from './calendar/calendar.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthBarrierInterceptor } from 'src/app/_interceptors';
@NgModule({
    declarations: [
        InicioComponent,
        SpecialPromotionsComponent,
        MonthsWithoutInterestComponent,
        IncentiveNotebookComponent,
        CalendarComponent,
    ],
    imports: [
        CommonModule,
        InicioRoutingModule,

        CommonLocalModule
    ],
    exports: [

    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthBarrierInterceptor,
            multi: true
        }
    ]
})
export class InicioModule { }
