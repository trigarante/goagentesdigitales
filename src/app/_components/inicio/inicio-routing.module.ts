import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';
import { PhoneBookComponent } from '../dashboard';
import { CalendarComponent } from './calendar/calendar.component';
import { IncentiveNotebookComponent } from './incentive-notebook/incentive-notebook.component';

import { InicioComponent } from './inicio.component';
import { MonthsWithoutInterestComponent } from './months-without-interest/months-without-interest.component';
import { SpecialPromotionsComponent } from './special-promotions/special-promotions.component';

const routes: Routes = [
  { path: '', component: InicioComponent, children: [
    { path: 'promociones-estacionales', component: SpecialPromotionsComponent, canActivate: [AuthGuardGuard] },
    { path: 'tabla-de-meses-sin-intereses', component: MonthsWithoutInterestComponent, canActivate: [AuthGuardGuard] },
    { path: 'cuaderno-de-incentivos', component: IncentiveNotebookComponent, canActivate: [AuthGuardGuard] },
    { path: 'calendario', component: CalendarComponent, canActivate: [AuthGuardGuard] },
    { path: 'directorio', component: PhoneBookComponent, canActivate: [AuthGuardGuard] },

    { path: ':any', redirectTo: '/admin/promociones-estacionales', pathMatch: 'full' },
    { path: '', redirectTo: '/admin/promociones-estacionales', pathMatch: 'full' }
    ] 
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InicioRoutingModule { }
