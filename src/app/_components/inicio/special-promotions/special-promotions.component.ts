import { Component, OnInit } from '@angular/core';
import { PromotionsService, T2020Service } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import { environment as GENERALS } from 'src/environments/environment.generals';
import {ImagenesService} from "../../../_services/imagenes.service";

@Component({
  selector: 'app-special-promotions',
  templateUrl: './special-promotions.component.html',
  styleUrls: ['./special-promotions.component.css']
})
export class SpecialPromotionsComponent implements OnInit {
  nombreMes: string = 'AGOSTO';
  abrevMes: string = 'AGO';
  files = [];
  promociones= [];
  imagenesMulti= [];

  constructor(
    private promotionService: PromotionsService,
    private imagenesService : ImagenesService
  ) { }

  ngOnInit(): void {
    this._getServiceBannerMulti();
    this._getServicePromo();
  }

  // SERVICIO: TRAE LAS PROMOCIONES DE LA TABLA
   _getServicePromo(): void{
    this.imagenesService.getPromociones().subscribe((resp:[])=>{
      this.promociones = resp;
       console.log('Ses esta consumiendo con exito', this.promociones);
    })
  }
  _getServiceBannerMulti():void{
    this.imagenesService.getImgBannerMulti().subscribe((resp:[])=>{
        this.imagenesMulti = resp;
         console.log('Se esta consumiendo con exito', this.imagenesMulti);
    })
  }
}
