import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialPromotionsComponent } from './special-promotions.component';

describe('SpecialPromotionsComponent', () => {
  let component: SpecialPromotionsComponent;
  let fixture: ComponentFixture<SpecialPromotionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecialPromotionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialPromotionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
