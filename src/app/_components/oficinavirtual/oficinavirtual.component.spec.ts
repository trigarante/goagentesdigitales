import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OficinavirtualComponent } from './oficinavirtual.component';

describe('OficinavirtualComponent', () => {
  let component: OficinavirtualComponent;
  let fixture: ComponentFixture<OficinavirtualComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OficinavirtualComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OficinavirtualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
