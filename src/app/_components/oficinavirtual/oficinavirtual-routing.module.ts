import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardGuard } from 'src/app/_guards/auth-guard.guard';
import { OficinavirtualComponent } from './oficinavirtual.component';
import { VirtualComponent } from './virtual/virtual.component';
import {EmissionsReceivableComponent} from "./emissions-receivable/emissions-receivable.component";
import {SubsequentComponent} from "./subsequent/subsequent.component";
// import {BusinessCardsComponent} from "./business-cards/business-cards.component";
import { MisLeadsComponent } from './mis-leads/mis-leads.component';

const routes: Routes = [
  { path: '', component:OficinavirtualComponent , children:[
    { path: 'promociones-estacionales', component:VirtualComponent , canActivate: [AuthGuardGuard] },
    // { path: 'tarjeta-presentacion', component:BusinessCardsComponent , canActivate: [AuthGuardGuard] },
    { path: 'emisiones-por-cobrar', component:EmissionsReceivableComponent , canActivate: [AuthGuardGuard] },
    { path: 'subsecuente', component:SubsequentComponent, canActivate: [AuthGuardGuard] },
    { path: 'mis-leads', component:MisLeadsComponent , canActivate: [AuthGuardGuard] },
  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OficinavirtualRoutingModule { }
