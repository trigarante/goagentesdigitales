import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetalleLeadsComponent } from './modal-detalle-leads.component';

describe('ModalDetalleLeadsComponent', () => {
  let component: ModalDetalleLeadsComponent;
  let fixture: ComponentFixture<ModalDetalleLeadsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDetalleLeadsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetalleLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
