import {Component, Input, OnInit} from '@angular/core';
import {OficinaVirtualService} from "../../../../_services/oficina-virtual.service";
import {LeadsDetalles} from "../../../../_interfaces/leadsDetalles";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";


@Component({
    selector: 'app-modal-detalle-leads',
    templateUrl: './modal-detalle-leads.component.html',
    styleUrls: ['./modal-detalle-leads.component.css']
})
export class ModalDetalleLeadsComponent implements OnInit {
    @Input() idEmision: any;
    @Input() idCotizacion: any;
    detalles;
    detallesCotizacion;
    mensaje: string;

    constructor(private modalService: NgbModal, private oficinaVirtualService: OficinaVirtualService) {
    }

    ngOnInit(): void {
        if (this.idEmision !== null) {
            this.getDetails(this.idEmision);
        }else {
            this.getDetailsCot(this.idCotizacion);
        }
    }

    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'xl'}).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getDetails(idEmision) {
        this.oficinaVirtualService.getLeadsDetalles(idEmision).subscribe(
            (resp: any) => {
                this.detalles = resp;
            });
    }

    getDetailsCot(idCotizacion) {
        this.oficinaVirtualService.getLeadsDetallesCotizacion(idCotizacion).subscribe(
            (resp: any) => {
                console.log('-----> resp',resp);
                this.detallesCotizacion = resp[0];
                this.detallesCotizacion.aseguradoras= this.detallesCotizacion.aseguradoras.filter(aseg => Object.entries(aseg).length !== 0);

            });
    }

}
