import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OficinaVirtualService} from "../../../../_services/oficina-virtual.service";
import {Pipe, PipeTransform} from '@angular/core';
import Swal from "sweetalert2";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-modal-poliza',
    templateUrl: './modal-poliza.component.html',
    styleUrls: ['./modal-poliza.component.css']
})
export class ModalPolizaComponent implements OnInit {
    @Input() id: number;
    @Input() url: string;
    @Input() icono: string;
    @Input() titulo: string;
    recibos: any;
    idAgente: any;
    idRecibo:string;
    suscripcion:Subscription;
    constructor(private modalService: NgbModal, private oficinaVirtual: OficinaVirtualService,) {

    }

    ngOnInit(): void {
        this.idAgente = Number(localStorage.getItem('id'));
        if (this.url === undefined || this.url === null || this.url === '') {
            this.url = 'sin valor'
        }
        if (this.titulo === 'COMPROBANTE DE PAGO' || this.titulo === 'RECIBO DE PAGO' && this.id !== undefined) {
            this.getComprobantePago();
            this.getReciboPago();
        }
        this.suscripcion = this.oficinaVirtual.refresh$.subscribe(()=>{
            if (this.titulo === 'COMPROBANTE DE PAGO' || this.titulo === 'RECIBO DE PAGO' && this.id !== undefined) {
                this.getComprobantePago();
                this.getReciboPago();
            }
        })
    }


    getComprobantePago() {
        this.oficinaVirtual.getComprobantePago(this.id).subscribe((resp) => {
            resp.forEach((el) => {
                this.idRecibo=el.idrecibo
                this.url = el.archivo;
            });
        });
    }

    getReciboPago() {
        this.oficinaVirtual.getRecibosPago(this.id).subscribe((resp) => {
            this.recibos = resp;
        })
    }

    cargarRecibo(file, idRecibo: string, numero: number) {
        console.log(file, numero, idRecibo);
        let reciboId = parseInt(idRecibo);
        const formData = new FormData();
        formData.append('file', file['target']['files'][0], reciboId + '-' + 'recibo');
        this.oficinaVirtual.recibo(reciboId, numero, formData).subscribe(respuesta => {
            let archivo = respuesta.Location;
            this.updateRecibo(reciboId, archivo, 'recibo');
        }, err => {
            // this.loaderGo=false;
            Swal.fire({
                title: 'Error',
                text: 'Error a subir archivo',
                icon: 'error',
            })
            console.error(err, 'no se genero el link');
        })
    }

    updateRecibo(reciboId, archivo, tipoArchivo) {
      const data = {'idFolder':  archivo};
        this.oficinaVirtual.updateRecibo(reciboId, data).subscribe(resp=>{
          console.log(resp);
          this.guardarLog(reciboId,tipoArchivo);
          // this.loaderGo=true;
        },err => {
          console.error(err, 'no subio');
        })

    }
  private guardarLog(idRegistro,tipoArchivo): void {
    const dataLog = {
      "agente": this.idAgente,
      "idRegistro": idRegistro,
      "tipo": tipoArchivo
    }
    this.oficinaVirtual.guardarLog(dataLog).subscribe(
        respuesta => {
          console.log(respuesta);},
        err => {
          console.error(err);
        }
    );
  }


    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'xl'}).result.then((result) => {
            // this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
             // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

}
