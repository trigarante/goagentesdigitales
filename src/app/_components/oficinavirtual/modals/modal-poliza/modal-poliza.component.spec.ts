import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPolizaComponent } from './modal-poliza.component';

describe('ModalPolizaComponent', () => {
  let component: ModalPolizaComponent;
  let fixture: ComponentFixture<ModalPolizaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalPolizaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPolizaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
