import {AfterContentInit, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {OficinaVirtualService} from "../../../_services/oficina-virtual.service";
import {Leads} from "../../../_interfaces/leads";
import {Subscription} from "rxjs";
import Swal from "sweetalert2";
import {FormBuilder, FormGroup} from "@angular/forms";
import { delay } from 'rxjs/operators';

@Component({
    selector: 'app-mis-leads',
    templateUrl: './mis-leads.component.html',
    styleUrls: ['./mis-leads.component.css']
})
export class MisLeadsComponent implements AfterViewInit {
    public search: string = '';
    public page: number = 0;
    public page2: number = 0;
    contador: number = 1;
    contador2: number = 1;
    tam: number = 0;
    tam2: number = 0;
    loaderGo: boolean = false;
    goId: string;
    selectValue=[
        {
            "id": 1,
            "name": "VENTA"
        },
        {
            "id": 2,
            "name": "EMISIÓN"
        },
        {
            "id": 3,
            "name": "SIN VENTA"
        },
        {
            "id": 4,
            "name": "NO CONTACTABLE"
        }
    ];
    suscripcion:Subscription;
    valor
    verSeleccion: string = '';
    sortData: any;
    sortDataVacia: any;
    mostrarTabla : boolean = true;
    hasEstadoValue: boolean = false;

    misLedsVacios = this.fbleads.group(
        {
            estado : '',
        }
    )


    // displayedColumns: string[] = ['Nombre', 'Email', 'Teléfono', 'Precio', 'Fecha', 'Hora', 'Estatus', 'Detalles'];
    // dataSource : MatTableDataSource<any>;
    // @ViewChild(MatPaginator) paginator: MatPaginator;
    // @ViewChild(MatSort) sort: MatSort;

    constructor(private oficinaVirtual: OficinaVirtualService, private fbleads: FormBuilder) {
    }
    cambioDeTabla(){
        (this.misLedsVacios.controls.estado.value != "") ? this.hasEstadoValue = true : this.hasEstadoValue = false;

        if(this.misLedsVacios.controls.estado.value === 'landing'){
            this.mostrarTabla = true
        }else if(this.misLedsVacios.controls.estado.value === 'portal'){
            this.mostrarTabla = false
        }
    }

    ngAfterViewInit() {
        // this.goId = Number(localStorage.getItem('goId'));
        this.goId = localStorage.getItem('goId');
        this.getLeads();
        this.suscripcion = this.oficinaVirtual.refresh$.subscribe(()=>{
            this.getLeads();
        })
    }
    datosVacios = [];
    getLeads() {
        this.loaderGo = true;
        this.oficinaVirtual.getLeads(this.goId)
        .pipe(delay(1000))
        .subscribe(
            (resp: any) => {

                console.log('resp2',resp)

                if(resp.length === 0){
                    this.loaderGo = false;
                    Swal.fire({
                        title: 'No hay datos',
                        icon: 'error'
                    })
                    return ;
                }
                for(let i = 0; i < resp.length; i++){
                    if(resp[i].idOrigen === 2){ //TODO
                     
                        this.datosVacios.push(resp[i])
                        delete resp[i]
                    }
                }
                //console.log('this.datosVacios',this.datosVacios);

                let leadsVacio = this.datosVacios
                let leads = resp;
                leads = leads.filter(Boolean);
                this.sortData = leads.sort((a,b)=>  new Date(b.fechaCotizaciones).getTime() - new Date(a.fechaCotizaciones).getTime() );

                //console.log(this.sortData, "Leads ---------------------------->");
                this.sortDataVacia = leadsVacio.sort((a,b)=>  new Date(b.fechaCotizaciones).getTime() - new Date(a.fechaCotizaciones).getTime() );
                //console.log('this.sortDataVacia',this.sortDataVacia);
                this.tam = Math.round( this.sortData.length/ 5);
                this.tam2 = Math.round( this.sortDataVacia.length/ 5);
                this.loaderGo = false;
            });

    }

    nextPage() {
        this.page += 5;
        this.contador += 1;
    }

    prevPage() {
        if (this.page > 0)
            this.page -= 5;
        this.contador -= 1;
    }

    ///////////////////////////////////////////

    nextPage2() {
        this.page2 += 5;
        this.contador2 += 1;
    }

    prevPage2() {
        if (this.page2 > 0)
            this.page2 -= 5;
        this.contador2 -= 1;
    }

    estatusVenta(e,idEmision,goId){
        console.log(goId)
        let estadoVenta=parseInt(e.target.value);
        let data = {
            idEmision: parseInt(idEmision),
            goid: goId,
            idEstadoVenta: estadoVenta
        }
        this.oficinaVirtual.estadoVenta(idEmision,data).subscribe(resp=>{
            console.log('ok')
        })

    }
}
