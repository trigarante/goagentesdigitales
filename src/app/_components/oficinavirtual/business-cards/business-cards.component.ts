import { Component, OnInit } from '@angular/core';
import html2canvas from "html2canvas";
import {jsPDF} from "jspdf";
import {image} from "html2canvas/dist/types/css/types/image";
import {isAuto} from "html2canvas/dist/types/render/background";
import {CapacitacionService} from "../../../_services/capacitacion.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ImagenesService} from "../../../_services/imagenes.service";

@Component({
    selector: 'app-business-cards',
    templateUrl: './business-cards.component.html',
    styleUrls: ['./business-cards.component.css']
})
export class BusinessCardsComponent implements OnInit {
    goId: any;
    card: any;
    nomCompleto: string;
    whats:string;
    imagenUrl:any;
    urlAfiliado:any;

    constructor(private capacitacionServece:CapacitacionService,
                private router:ActivatedRoute,
                private imageBanner:ImagenesService) { }

    ngOnInit(): void {
        this.router.queryParams.subscribe(params=>{
            this.goId = params.goId;
        })
        this.getVirtualCard();

    }
    getVirtualCard(){
        this.capacitacionServece.getVirtualCard(this.goId).subscribe(resp =>{
            this.card= resp;
            this.urlAfiliado = resp.url;
            console.log('es resp',this.urlAfiliado)
            this.nomCompleto = this.card.nombre +' '+ this.card.apellidoPaterno+' ' + this.card.apellidoMaterno;
            this.whats = "https://api.whatsapp.com/send/?phone=%2B521" + this.card.telefono+ "&text&app_absent=0";
        })
        this.imageBanner.getImgBannerMulti().subscribe((data) =>{
            this.imagenUrl = data[0].url
        })
    }



}
