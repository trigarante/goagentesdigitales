import { Component, OnInit } from '@angular/core';
import {OficinaVirtualService} from "../../../_services/oficina-virtual.service";
import Swal from "sweetalert2";
import {element} from "protractor";

@Component({
  selector: 'app-virtual',
  templateUrl: './virtual.component.html',
  styleUrls: ['./virtual.component.css']
})
export class VirtualComponent implements OnInit {

  public polizaTotales: [];
  idAgente:number;
  fechaInicio:any;
  fechaFin:any;
  porFecha:any;
  public compensacionFiltro: [];
  public totalPolizaEmitida: any;
  public totalPolizaCobradas: any;
  public totalPrimaEmitida: any;
  public totalPrimaCobrada: any;
  public totalcompensacion: any;


  constructor(private oficinaVirtual:OficinaVirtualService) { }

  ngOnInit(): void {
    this.idAgente=Number(localStorage.getItem('id'));
    this._getPolizaTotales();

  }

  _getPolizaTotales(){
    this.fechaInicio = '';
    this.fechaFin = '';
    this.polizaTotales=[];
    this.totalPolizaEmitida ='';
    this.oficinaVirtual.getTotalesPrima(this.idAgente).subscribe((resp: any) => {
      this.polizaTotales=resp;
      this.totalPolizaEmitida= Math.round(resp.reduce((sum, value) => (typeof value.numeroPolizasEmitidas === "string" ? sum + parseInt(value.numeroPolizasEmitidas) : sum), 0));
      this.totalPolizaCobradas= Math.round(resp.reduce((sum, value) => (typeof value.numeroPolizasCobradas === "string" ? sum + parseInt(value.numeroPolizasCobradas) : sum), 0));
      this.totalPrimaEmitida= Math.round(resp.reduce((sum, value) => (typeof value.primaNetaEmitida === "number" ? sum + value.primaNetaEmitida : sum), 0));
      this.totalPrimaCobrada= Math.round(resp.reduce((sum, value) => (typeof value.primaNetaCobrada  === "number" ? sum + value.primaNetaCobrada : sum), 0));
    });
    this.filtroIdConsecionarias();
  }

  _filtroFechas(){
    if (this.fechaFin ==='' && this.fechaInicio ===''){
      Swal.fire('INGRESE LA FECHA')
    }else {
      console.log(this.fechaInicio)
      this.polizaTotales =[];
      this.totalPolizaEmitida=0;
      this.totalPolizaCobradas=0;
      this.totalPrimaEmitida=0;
      this.totalPrimaCobrada=0;
      if (this.fechaInicio && this.fechaFin) {
        this.oficinaVirtual.getMisResulTablaFiltro(this.idAgente,this.fechaInicio,this.fechaFin).subscribe((resp)=>{
          this.polizaTotales=resp;
          this.totalPolizaEmitida= Math.round(resp.reduce((sum, value) => (typeof value.numeroPolizasEmitidas === "string" ? sum + parseInt(value.numeroPolizasEmitidas) : sum), 0));
          this.totalPolizaCobradas= Math.round(resp.reduce((sum, value) => (typeof value.numeroPolizasCobradas === "string" ? sum + parseInt(value.numeroPolizasCobradas) : sum), 0));
          this.totalPrimaEmitida= Math.round(resp.reduce((sum, value) => (typeof value.primaNetaEmitida === "number" ? sum + value.primaNetaEmitida : sum), 0));
          this.totalPrimaCobrada= Math.round(resp.reduce((sum, value) => (typeof value.primaNetaCobrada  === "number" ? sum + value.primaNetaCobrada : sum), 0));

        });
        this._filtroFechasConsecionarias();
      }
    }

  }
  _filtroFechasConsecionarias(){
    this.totalcompensacion=0;
    this.oficinaVirtual.getCompensacionFecha(this.idAgente,this.fechaInicio, this.fechaFin).subscribe((resp)=>{
      this.compensacionFiltro = resp;
      this.totalcompensacion= Math.round(resp.reduce((sum, value) => (typeof value.compensacion === "number" ? sum + value.compensacion : sum), 0));
      console.log(Math.round(this.totalcompensacion));

      console.log(resp, 'compensacion');
    });

  }
  filtroIdConsecionarias(){
    this.totalcompensacion=0;
    this.oficinaVirtual.getCompensacionId(this.idAgente).subscribe((resp)=>{
      this.totalcompensacion= Math.round(resp.reduce((sum, value) => (typeof value.compensacion === "number" ? sum + value.compensacion : sum), 0));
    });

  }

}
