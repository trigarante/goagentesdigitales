import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsequentComponent } from './subsequent.component';

describe('SubsequentComponent', () => {
  let component: SubsequentComponent;
  let fixture: ComponentFixture<SubsequentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsequentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsequentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
