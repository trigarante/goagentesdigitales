import {AfterContentInit, AfterViewInit, Component, OnInit, ViewChild,DoCheck} from '@angular/core';
import {OficinaVirtualService} from "../../../_services/oficina-virtual.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";

import {element} from "protractor";

@Component({
  selector: 'app-subsequent',
  templateUrl: './subsequent.component.html',
  styleUrls: ['./subsequent.component.css']
})
export class SubsequentComponent implements AfterViewInit{
  mostrarTabla=false;
  idAgente:number;
  subsecuente=[];
  displayedColumns: string[] = ['poliza', 'ramo', 'compañia', 'primaTotal','primaNeta','marca','submarca','modelo','inicioVigencia','fechaLimite','numRecibo'];
  dataSource : MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private oficinaVirtual:OficinaVirtualService) {}

  ngAfterViewInit() {
    this.idAgente=Number(localStorage.getItem('id'));
    this.getCobranza();

  }



getCobranza() {
    this.oficinaVirtual.getSubsecuente(this.idAgente).subscribe(
        (resp: any) => {
          // resp = this.parseToLower(JSON.stringify(resp));
          this.subsecuente = resp;
          let arr = [];
          arr = this.subsecuente;
          for (let i = 0; i < arr.length; i++) {
            this.getRecibos(Number(arr[i]['id']));
            arr[i]['numRecibo']='0';
            arr[i]['primaTotal']='0';
            var datosProducto = arr[i]['datosProductoCliente'] === null ? '' : this.parseToLower(JSON.stringify(arr[i]['datosProductoCliente']));
            arr[i]['datosProductoCliente'] = datosProducto;
          }
          this.subsecuente = arr;
        });
  setTimeout(() => {
    this.dataSource = new MatTableDataSource(this.subsecuente);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }, 500);






  }
   getRecibos(id:number){
    this.oficinaVirtual.getIdRegistro(id).subscribe(
        (resp: any) => {
         const indice = this.subsecuente.findIndex(i=>
            i.id==id);
         this.subsecuente[indice].numRecibo = resp.numero
         this.subsecuente[indice].primaTotal = resp.cantidad
        });


  }
  // Funcion para convertir de mayusculas a minusculas las iniciales del json
  parseToLower(jsonText) {
    let json ;
    if (typeof jsonText == "string") {
      json = JSON.parse(jsonText);
    }
    if (typeof json == "object") {
      for (let prop in json) {
        if (
            typeof json[prop] === "string" ||
            typeof json[prop] === "boolean" ||
            typeof json[prop] === "number"
        ) {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            //prop inicia con minuscula
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            if (newprop == "rFC") {
              newprop = "rfc";
            }
            if (newprop == "cURP") {
              newprop = "curp";
            }
            if (newprop == "mSI") {
              newprop = "msi";
            }
            json[newprop] = json[prop];
            delete json[prop];
          }
        } else if (typeof json[prop] === "object") {
          if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
            let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
            json[newprop] = json[prop];
            //console.log('newprop');
            //console.log(newprop);
            //console.log('json[newprop]');
            //console.log(json[newprop]);
            delete json[prop];
            //Excepcion para no modificar el contenido de Coberturas
            if (newprop != "coberturas") {
              json[newprop] = this.parseToLower(
                  JSON.stringify(json[newprop])
              );
            }
          } else {
            //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
            //Excepcion para no modificar el contenido de Coberturas
            if (prop != "coberturas") {
              json[prop] = this.parseToLower(JSON.stringify(json[prop]));
            }
          }
        } else {
          console.log(
              "Tipo de dato diferente al esperado: ",
              typeof json[prop]
          );
          console.log("prop: ", prop);
        }
      }
    }
    return json;
  }
  //Filtrado de datos en tabla
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
