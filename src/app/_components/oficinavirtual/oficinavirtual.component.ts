import { Component, OnInit } from '@angular/core';
import { MenuToggleService } from 'src/app/_services';

@Component({
  selector: 'app-oficinavirtual',
  templateUrl: './oficinavirtual.component.html',
  styleUrls: ['./oficinavirtual.component.css']
})
export class OficinavirtualComponent implements OnInit {
  toggleMenu: boolean = false;
  constructor(  private menuToggleService: MenuToggleService
  ) { }

  ngOnInit(): void {
  this.menuToggleService.getNavChangeEmitter()
      .subscribe((item: any) => {this.toggleMenu = item;});
  }
}
