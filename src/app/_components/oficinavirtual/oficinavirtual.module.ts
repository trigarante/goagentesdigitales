import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OficinavirtualRoutingModule} from './oficinavirtual-routing.module';
import {VirtualComponent} from './virtual/virtual.component';
import {OficinavirtualComponent} from './oficinavirtual.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthBarrierInterceptor} from 'src/app/_interceptors';
import {CommonLocalModule} from 'src/app/_modules/common-local.module';
import {EmissionsReceivableComponent} from './emissions-receivable/emissions-receivable.component';
import {BrowserModule} from '@angular/platform-browser';
import {SubsequentComponent} from './subsequent/subsequent.component';
import {BusinessCardsComponent} from './business-cards/business-cards.component';
import {QRCodeModule} from "angularx-qrcode";
import {ModalPolizaComponent} from './modals/modal-poliza/modal-poliza.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import {MisLeadsComponent} from './mis-leads/mis-leads.component';
import {ModalDetalleLeadsComponent} from './modals/modal-detalle-leads/modal-detalle-leads.component';
import {PaginationPipe} from "../../_pipes/pagination.pipe";
import {NgbButtonsModule} from "@ng-bootstrap/ng-bootstrap";




@NgModule({
    declarations: [
        VirtualComponent,
        OficinavirtualComponent,
        EmissionsReceivableComponent,
        SubsequentComponent,
        ModalPolizaComponent,
        MisLeadsComponent,
        ModalDetalleLeadsComponent,
        PaginationPipe,

        // BusinessCardsComponent
    ],
    imports: [
        CommonModule,
        OficinavirtualRoutingModule,
        CommonLocalModule,
        QRCodeModule,
        PdfViewerModule,
        NgbButtonsModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthBarrierInterceptor,
            multi: true
        }
    ]
})
export class OficinavirtualModule {
}
