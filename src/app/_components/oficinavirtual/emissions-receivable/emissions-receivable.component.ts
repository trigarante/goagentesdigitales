import {AfterContentInit, AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OficinaVirtualService} from "../../../_services/oficina-virtual.service";
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {CotizacionesViewInterface} from "../../../_interfaces";
import {Subscription} from "rxjs";
import Swal from "sweetalert2";


@Component({
  selector: 'emissions-receivable',
  templateUrl: './emissions-receivable.component.html',
  styleUrls: ['./emissions-receivable.component.css']
})

export class EmissionsReceivableComponent implements OnInit,OnDestroy  {

    public search: string = '';
    public page: number = 0;
    contador: number= 1;
    tam: number=0;
    idAgente:number;
    cobranza=[];
    loaderGo:boolean=false;
    suscripcion:Subscription;

    constructor(private oficinaVirtual:OficinaVirtualService) {}

    ngOnInit() {
        this.idAgente=Number(localStorage.getItem('id'));
        this.getCobranza();
        this.suscripcion = this.oficinaVirtual.refresh$.subscribe(()=>{
            this.getCobranza();
        })

    }
    getCobranza() {
    this.oficinaVirtual.getCobranza(this.idAgente).subscribe(
        (resp: any) => {
            this.cobranza = resp;
            let arr = [];
            arr = this.cobranza;
            for (let i = 0; i < arr.length; i++) {
                var datosProducto = arr[i]['datosProductoCliente'] === null ? '' : this.parseToLower(JSON.stringify(arr[i]['datosProductoCliente']));
                arr[i]['datosProductoCliente'] = datosProducto;
            }
            this.cobranza = arr;
            this.tam = Math.round( this.cobranza.length/ 5);
            console.log('la data ',this.cobranza);
        });
}
    // Funcion para convertir de mayusculas a minusculas las iniciales del json
    parseToLower(jsonText) {
        let json ;
        if (typeof jsonText == "string") {
            json = JSON.parse(jsonText);
        }
        if (typeof json == "object") {
            for (let prop in json) {
                if (
                    typeof json[prop] === "string" ||
                    typeof json[prop] === "boolean" ||
                    typeof json[prop] === "number"
                ) {
                    if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
                        //prop inicia con minuscula
                        let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
                        if (newprop == "rFC") {
                            newprop = "rfc";
                        }
                        if (newprop == "cURP") {
                            newprop = "curp";
                        }
                        if (newprop == "mSI") {
                            newprop = "msi";
                        }
                        json[newprop] = json[prop];
                        delete json[prop];
                    }
                } else if (typeof json[prop] === "object") {
                    if (prop.charAt(0) != prop.charAt(0).toLowerCase()) {
                        let newprop = prop.charAt(0).toLowerCase() + prop.slice(1);
                        json[newprop] = json[prop];
                        //console.log('newprop');
                        //console.log(newprop);
                        //console.log('json[newprop]');
                        //console.log(json[newprop]);
                        delete json[prop];
                        //Excepcion para no modificar el contenido de Coberturas
                        if (newprop != "coberturas") {
                            json[newprop] = this.parseToLower(
                                JSON.stringify(json[newprop])
                            );
                        }
                    } else {
                        //Entra cuando atributo si inicia con mayuscula, y aplica la funcionalidad a su valor
                        //Excepcion para no modificar el contenido de Coberturas
                        if (prop != "coberturas") {
                            json[prop] = this.parseToLower(JSON.stringify(json[prop]));
                        }
                    }
                } else {
                    console.log(
                        "Tipo de dato diferente al esperado: ",
                        typeof json[prop]
                    );
                    console.log("prop: ", prop);
                }
            }
        }
        return json;
    }

    //Cargar el archivo
    cargarArchivo(files, idRegistro):any{
        let registro = parseInt(idRegistro);
        const formData = new FormData();
        formData.append('file', files['target']['files'][0], registro + '-' + 'poliza');
            this.oficinaVirtual.poliza(registro,formData).subscribe(respuesta=>{
                let archivo=respuesta.Location;
                this.updateFile(registro,archivo,'poliza');
            },err => {
                // this.loaderGo=false;
                Swal.fire({
                    title: 'Error',
                    text: 'Error a subir archivo',
                    icon: 'error',
                })
                console.error(err, 'no se genero el link');
            })

    }
    cargarCliente(files, idCliente):any{
        // this.loaderGo=true;
        let cliente = parseInt(idCliente);
        const formData = new FormData();
        formData.append('file', files['target']['files'][0], cliente + '-' + 'cliente');
            this.oficinaVirtual.art492(cliente,formData).subscribe(respuesta=>{
                let archivo=respuesta.Location;
                this.updateFile(idCliente,archivo,'cliente');
            },err => {
                // this.loaderGo=false;
                Swal.fire({
                    title: 'Error',
                    text: 'Error a subir archivo',
                    icon: 'error',
                })
                console.error(err, 'no se genero el link');
            })

    }
    private updateFile(id , archivo,tipoArchivo){
        const data = {'idFolder':  archivo};
        if (tipoArchivo==='poliza'){
            this.oficinaVirtual.updateFile(id, data).subscribe(resp=>{
                console.log(resp);
                this.guardarLog(id,tipoArchivo);
                // this.loaderGo=true;

            },err => {
                console.error(err, 'no subio');
            })
        }else if(tipoArchivo==='cliente'){
            this.oficinaVirtual.updateCliente(id, data).subscribe(resp=>{
                console.log(resp);
                this.guardarLog(id,tipoArchivo);
                // this.loaderGo=true;

            },err => {
                console.error(err, 'no subio');
            })
        }

    }
    private guardarLog(idRegistro,tipoArchivo): void {
        const dataLog = {
            "agente": this.idAgente,
            "idRegistro": idRegistro,
            "tipo": tipoArchivo
        }
        this.oficinaVirtual.guardarLog(dataLog).subscribe(
            respuesta => {
                console.log(respuesta);},
            err => {
                console.error(err);
            }
        );
    }
    nextPage() {
        this.page += 5;

        this.contador +=1;


    }
    prevPage() {
        if ( this.page > 0 )

            this.page -= 5;

        this.contador -=1;
    }
    onSearch( search: string ) {
        this.page = 0;
        this.search = search;
    }

    ngOnDestroy(): void {
        this.oficinaVirtual.refresh$.unsubscribe();
    }


}
