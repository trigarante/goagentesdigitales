import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmissionsReceivableComponent } from './emissions-receivable.component';

describe('EmissionsReceivableComponent', () => {
  let component: EmissionsReceivableComponent;
  let fixture: ComponentFixture<EmissionsReceivableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmissionsReceivableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmissionsReceivableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
