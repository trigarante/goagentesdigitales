import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertInterface } from 'src/app/_interfaces';
import { UserService } from 'src/app/_services';
import { AesEDecodeService, LoginService } from 'src/app/_services';
import jwt_decode from 'jwt-decode';
import Swal from 'sweetalert2';
import {tokenGo} from "../../../_interceptors";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  tokenDeGo: any;
  tokenDePlacas: any;
  disparadorDeGoId: any
  user: any;
  form = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  alert: AlertInterface = { type: '', message: '' };
  acept: boolean = false;
  rememberCheck: boolean = false;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private aesEDecodeService: AesEDecodeService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this._validateRemember();
  }
  ngOnDestroy():void{
    localStorage.removeItem("estadoProceso");
    localStorage.removeItem("emissionStates");
  }

  private _validateRemember(): void {
    let _remember = localStorage.getItem('remember') || null;
    if (_remember !== null) {
      let _values = JSON.parse(this.aesEDecodeService.decrypt(_remember));
      this.form.controls.username.setValue(_values.username);
      this.form.controls.password.setValue(_values.password);
      this.rememberCheck = true;
    }
  }

  get f() { return this.form.controls; }

  login(): void {
    this.form.value
    
    if (!this.form.valid) {
      this.alert.type = 'error';
      this.alert.message = '¡Error! Por favor, verifica tu usuario y contraseña.';
    }

    let tokenBranding = localStorage.getItem('tokenBranding') || null;
    if (!tokenBranding) {
      this.loginService.getTokenBranding().subscribe(
        succ => {
          localStorage.setItem('tokenBranding', succ.accessToken)
        }
      );
    }

    // this.loginService.getTokenAseguradoras().subscribe((dataToken) => {
    //   localStorage.setItem('tokenAseg', dataToken.accessToken)
    // })

    this.rememberMe();
    this.loginService.login(this.form.value).subscribe(
      succ => {
        localStorage.setItem('token', succ.token);
        let _id: any;
        _id = jwt_decode(succ.token);
        let _id2 = _id.sub.split(':')[1];
        localStorage.setItem('id', _id2,);
        this.userService.getInfo().subscribe(
          dataUser => {
              this.user = dataUser;
              localStorage.setItem('goId', this.user.goId);
              if(dataUser.idStatusAgente !== 1){
                Swal.fire({
                  icon: 'warning',
                  title: 'Agente Inactivo!',
                  text: 'Este agente fue inhabilitado tempotalmente.',
                  footer: ''
                });
                localStorage.clear();
                sessionStorage.clear();
              }else if(dataUser.idStatusAgente == 1){
                this.router.navigateByUrl('admin/dashboard');
              }
            },
            err => {
              console.error(err);
            }
        );

        //this.router.navigateByUrl('admin/dashboard');
        // this.router.navigate(['/dashboard/principal']);
      },
      err => {
        console.error('ERROR: ', err);
        this.alert.type = 'error';
        this.alert.message = '¡Error! Por favor, verifica tu usuario y contraseña.';
        setTimeout(() => {
          this.alert.type = '';
        }, 1500);
      }
    );
  }

  rememberMe(): void {
    if (this.acept) {
      let _code = { username: this.f.username.value, password: this.f.password.value };
      let _name = this.aesEDecodeService.encrypt(JSON.stringify(_code));
      localStorage.setItem('remember', _name);
    }
  }

  obtenerTokenGo(){
    this.loginService.getTokenGo(this.form.value).subscribe((dataTokenGo) => {
      this.loginService.disparadorDeTokenGo.emit({data: this.tokenDeGo})
      this.tokenDeGo = dataTokenGo.token
     // console.log('tokenGo', this.tokenDeGo)
     // console.log('tokenGo', dataTokenGo)
      // localStorage.setItem('ProvisionalToken',this.tokenDeGo )
    })
  }

  obtenerTokenPlacas(){
    this.loginService.getTokenPlacas().subscribe((tokenRepuve) =>{
      this.loginService.disparadorDePlacas.emit({data:this.tokenDePlacas })
      this.tokenDePlacas = tokenRepuve.accessToken
      localStorage.setItem('plToken', this.tokenDePlacas)
    })
  }


}
