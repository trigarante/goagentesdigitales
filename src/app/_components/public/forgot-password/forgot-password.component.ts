import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AesEDecodeService, LoginService} from "../../../_services";
import jwt_decode from "jwt-decode";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  formForgot = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    passwordconfirm: new FormControl('', [Validators.required]),
  });

  constructor(
      private router: Router,
      private loginService: LoginService,
      private aesEDecodeService: AesEDecodeService
  ) { }

  ngOnInit(): void {
    this._validateRemember();
  }

  private _validateRemember(): void {

  }

  get f() { return this.formForgot.controls; }


  forgot(): void {
    if(!this.formForgot.valid){
      Swal.fire(
          'Favor de llenar todos los campos!',
          'He intentar nuevamente...',
          'warning'
      )
    }else{

      if(this.formForgot.value.password === this.formForgot.value.passwordconfirm){
        this.loginService.loginforgot(this.formForgot.value).subscribe(
            succ => {
              if(succ){
                Swal.fire(
                    'Listo, la contraseña se ha actualizado',
                    'Favor de iniciar sesion nuevamente',
                    'success'
                )
                this.router.navigateByUrl('login');
              }else {
                Swal.fire(
                    'Usuario no encontrado',
                    'Favor de validar.',
                    'warning'
                )
              }

            },
            err => {
              Swal.fire(
                  'Usuario no encontrado',
                  'Favor de validar.',
                  'warning'
              )
            }
        );
      }else{
        Swal.fire(
            'Las contraseñas no son iguales.',
            'Favor de validar la información.',
            'error'
        )
      }

    }
  }

}
