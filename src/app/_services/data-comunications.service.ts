import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AseguradoraInterface } from '../_interfaces';
// import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class DataComunicationsService {
  datosObs = new Subject();
  // formDatos: FormGroup;
  datos: AseguradoraInterface = {};

  constructor() { }

  sendDatos(objeto: AseguradoraInterface): any {
    this.datos = objeto;
    this.datosObs.next(this.datos);
  }

  getDatos(): AseguradoraInterface {
    return this.datos;
  }
}
