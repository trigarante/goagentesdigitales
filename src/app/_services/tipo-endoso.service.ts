import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class TipoEndosoService {
    url: any;

    constructor(
        private http: HttpClient
    ) {
        this.url = environment.oficinaServices;
    }

    getTipoEndoso(): Observable<any[]> {
        return this.http.get<any[]>(this.url + '/endosos/tipo-endoso');
    }
}
