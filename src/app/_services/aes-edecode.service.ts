import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Injectable({
  providedIn: 'root'
})
export class AesEDecodeService {

  constructor() { }

  encrypt(value: any): string {
    return CryptoJS.AES.encrypt(value, GENERALS.secretKey.trim()).toString();
  }

  decrypt(textToDecrypt: any) {
    return CryptoJS.AES.decrypt(textToDecrypt, GENERALS.secretKey.trim()).toString(CryptoJS.enc.Utf8);
  }
}
