import { Injectable } from '@angular/core';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { ToLowerCaseKeyService } from './to-lower-case-key.service';

@Injectable({
  providedIn: 'root'
})
export class HomologationParseResultService {

  constructor(
    private toLowerCaseKeyService: ToLowerCaseKeyService
  ) { }

  getIDQuotation(event: any, insurance: string, quotationResponse: any, type: string, frequency: string) {
    let _quotation = this.toLowerCaseKeyService.parseToLower(quotationResponse);

    // _quotation = this._toObject(_quotation);
    let _return;

    if (!event) {
      if (_quotation.cotizacion[0].amplia) {
        if(_quotation.aseguradora.toUpperCase() === 'ABA'){
          _return = _quotation.cotizacion[0].amplia[0].anual.cotID;
        }else{
          _return = _quotation.cotizacion[0].amplia[0].anual.idCotizacion;
        }

      } else if (_quotation.cotizacion[1].amplia) {
        _return = _quotation.cotizacion[1].amplia[0].anual.idCotizacion
      } else {
        _return = _quotation.cotizacion[2].amplia[0].anual.idCotizacion
      }
    } else {
      switch (event.index) {
        case 0:
          if (_quotation.cotizacion[0].rc) {
            _return = _quotation.cotizacion[0].rc[0].anual.idCotizacion
          } else if (_quotation.cotizacion[1].rc) {
            _return = _quotation.cotizacion[1].rc[0].anual.idCotizacion
          } else if (_quotation.cotizacion[2].rc) {
            _return = _quotation.cotizacion[2].rc[0].anual.idCotizacion
          } else if (_quotation.cotizacion[3].rc) {
            _return = _quotation.cotizacion[3].rc[0].anual.idCotizacion
          }
        break;
        case 1:
          if (_quotation.cotizacion[0].limitada) {
            _return = _quotation.cotizacion[0].limitada[0].anual.idCotizacion
          } else if (_quotation.cotizacion[1].limitada) {
            _return = _quotation.cotizacion[1].limitada[0].anual.idCotizacion
          } else {
            _return = _quotation.cotizacion[2].limitada[0].anual.idCotizacion
          }
          break;
        case 2:
          if (_quotation.cotizacion[0].amplia) {
            _return = _quotation.cotizacion[0].amplia[0].anual.idCotizacion
          } else if (_quotation.cotizacion[1].amplia) {
            _return = _quotation.cotizacion[1].amplia[0].anual.idCotizacion
          } else {
            _return = _quotation.cotizacion[2].amplia[0].anual.idCotizacion
          }
        break;
        case 3:
          if (_quotation.cotizacion[0].especial) {
            _return = _quotation.cotizacion[0].especial[0].anual.idCotizacion
            
          } else if (_quotation.cotizacion[1].especial) {
            _return = _quotation.cotizacion[1].especial[0].anual.idCotizacion

          } else if (_quotation.cotizacion[2].especial) {
            _return = _quotation.cotizacion[2].especial[0].anual.idCotizacion
          }
        break;
        case 4:
          if (_quotation.cotizacion[0].gold) {
            _return = _quotation.cotizacion[0].gold[0].anual.idCotizacion
            
          } else if (_quotation.cotizacion[1].gold) {
            _return = _quotation.cotizacion[1].gold[0].anual.idCotizacion

          } else if (_quotation.cotizacion[2].gold) {
            _return = _quotation.cotizacion[2].gold[0].anual.idCotizacion
          }
        break;
      }
    }

    return _return;
  }

  getTotal(event: any, insurance: string, quotationResponse: any, type: string, frequency: string): any {
    let _quotation = quotationResponse; // this.toLowerCaseKeyService.parseToLower(quotationResponse);
    let _return: any = 0;

    if (!event) {
      if (_quotation.cotizacion[0].amplia) {
        _quotation.cotizacion[0].amplia[0].anual.primaTotal
      } else if (_quotation.cotizacion[1].amplia) {
        _quotation.cotizacion[1].amplia[0].anual.primaTotal
      } else if (_quotation.cotizacion[2].amplia) {
        _quotation.cotizacion[2].amplia[0].anual.primaTotal
      } else {
        _quotation.cotizacion.primaTotal
      }
    } else {
      switch (event.index) {
        default:
          switch (type) {
            case 'rc':
              if (frequency === 'anual') {
                if (_quotation.cotizacion[0].rc) {
                  _return = _quotation.cotizacion[0].rc[0].anual.primaTotal
                } else if (_quotation.cotizacion[1].rc) {
                  _return = _quotation.cotizacion[1].rc[0].anual.primaTotal
                } else if (_quotation.cotizacion[2].rc){
                  _return = _quotation.cotizacion[2].rc[0].anual.primaTotal
                } else if (_quotation.cotizacion[3].rc){
                  _return = _quotation.cotizacion[3].rc[0].anual.primaTotal
                }
              } else if (frequency === 'semestral') {
                if (_quotation.cotizacion[0].rc) {
                  _return = _quotation.cotizacion[0].rc[0].semestral.primaTotal

                } else if (_quotation.cotizacion[1].rc) {
                  _return = _quotation.cotizacion[1].rc[0].semestral.primaTotal

                } else if (_quotation.cotizacion[2].rc){
                  _return = _quotation.cotizacion[2].rc[0].semestral.primaTotal

                } else if (_quotation.cotizacion[3].rc){
                  _return = _quotation.cotizacion[3].rc[0].semestral.primaTotal
                }
              } else if (frequency === 'trimestral') {
                if (_quotation.cotizacion[0].rc) {
                  _return = _quotation.cotizacion[0].rc[0].trimestral.primaTotal

                } else if (_quotation.cotizacion[1].rc) {
                  _return = _quotation.cotizacion[1].rc[0].trimestral.primaTotal

                } else if (_quotation.cotizacion[2].rc){
                  _return = _quotation.cotizacion[2].rc[0].trimestral.primaTotal
                
                } else if (_quotation.cotizacion[3].rc){
                  _return = _quotation.cotizacion[3].rc[0].trimestral.primaTotal
                }
              } else if (frequency === 'mensual') {
                if (_quotation.cotizacion[0].rc) {
                  _return = _quotation.cotizacion[0].rc[0].mensual.primaTotal

                } else if (_quotation.cotizacion[1].rc) {
                  _return = _quotation.cotizacion[1].rc[0].mensual.primaTotal

                } else if (_quotation.cotizacion[2].rc){
                  _return = _quotation.cotizacion[2].rc[0].mensual.primaTotal
                  
                } else if (_quotation.cotizacion[3].rc){
                  _return = _quotation.cotizacion[3].rc[0].mensual.primaTotal
                }
              }
              break;
            case 'limitada':
              if (frequency === 'anual') {
                if (_quotation.cotizacion[0].limitada) {
                  _return = _quotation.cotizacion[0].limitada[0].anual.primaTotal
                } else if (_quotation.cotizacion[1].limitada) {
                  _return = _quotation.cotizacion[1].limitada[0].anual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].limitada[0].anual.primaTotal
                }
              } else if (frequency === 'semestral') {
                if (_quotation.cotizacion[0].limitada) {
                  _return = _quotation.cotizacion[0].limitada[0].semestral.primaTotal
                } else if (_quotation.cotizacion[1].limitada) {
                  _return = _quotation.cotizacion[1].limitada[0].semestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].limitada[0].semestral.primaTotal
                }
              } else if (frequency === 'trimestral') {
                if (_quotation.cotizacion[0].limitada) {
                  _return = _quotation.cotizacion[0].limitada[0].trimestral.primaTotal
                } else if (_quotation.cotizacion[1].limitada) {
                  _return = _quotation.cotizacion[1].limitada[0].trimestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].limitada[0].trimestral.primaTotal
                }
              } else if (frequency === 'mensual') {
                if (_quotation.cotizacion[0].limitada) {
                  _return = _quotation.cotizacion[0].limitada[0].mensual.primaTotal
                } else if (_quotation.cotizacion[1].limitada) {
                  _return = _quotation.cotizacion[1].limitada[0].mensual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].limitada[0].mensual.primaTotal
                }
              }
              break;
            case 'amplia':
              if (frequency === 'anual') {
                if (_quotation.cotizacion[0].amplia) {
                  _return = _quotation.cotizacion[0].amplia[0].anual.primaTotal
                } else if (_quotation.cotizacion[1].amplia) {
                  _return = _quotation.cotizacion[1].amplia[0].anual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].amplia[0].anual.primaTotal
                }
              } else if (frequency === 'semestral') {
                if (_quotation.cotizacion[0].amplia) {
                  _return = _quotation.cotizacion[0].amplia[0].semestral.primaTotal
                } else if (_quotation.cotizacion[1].amplia) {
                  _return = _quotation.cotizacion[1].amplia[0].semestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].amplia[0].semestral.primaTotal
                }
              } else if (frequency === 'trimestral') {
                if (_quotation.cotizacion[0].amplia) {
                  _return = _quotation.cotizacion[0].amplia[0].trimestral.primaTotal
                } else if (_quotation.cotizacion[1].amplia) {
                  _return = _quotation.cotizacion[1].amplia[0].trimestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].amplia[0].trimestral.primaTotal
                }
              } else if (frequency === 'mensual') {
                if (_quotation.cotizacion[0].amplia) {
                  _return = _quotation.cotizacion[0].amplia[0].mensual.primaTotal
                } else if (_quotation.cotizacion[1].amplia) {
                  _return = _quotation.cotizacion[1].amplia[0].mensual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].amplia[0].mensual.primaTotal
                }
              }
              break;
            case 'especial':
              if (frequency === 'anual') {
                if (_quotation.cotizacion[0].especial) {
                  _return = _quotation.cotizacion[0].especial[0].anual.primaTotal
                } else if (_quotation.cotizacion[1].especial) {
                  _return = _quotation.cotizacion[1].especial[0].anual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].especial[0].anual.primaTotal
                }
              } else if (frequency === 'semestral') {
                if (_quotation.cotizacion[0].especial) {
                  _return = _quotation.cotizacion[0].especial[0].semestral.primaTotal
                } else if (_quotation.cotizacion[1].especial) {
                  _return = _quotation.cotizacion[1].especial[0].semestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].especial[0].semestral.primaTotal
                }
              } else if (frequency === 'trimestral') {
                if (_quotation.cotizacion[0].especial) {
                  _return = _quotation.cotizacion[0].especial[0].trimestral.primaTotal
                } else if (_quotation.cotizacion[1].especial) {
                  _return = _quotation.cotizacion[1].especial[0].trimestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].especial[0].trimestral.primaTotal
                }
              } else if (frequency === 'mensual') {
                if (_quotation.cotizacion[0].especial) {
                  _return = _quotation.cotizacion[0].especial[0].mensual.primaTotal
                } else if (_quotation.cotizacion[1].especial) {
                  _return = _quotation.cotizacion[1].especial[0].mensual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].especial[0].mensual.primaTotal
                }
              }
            break;
            case 'gold':
              if (frequency === 'anual') {
                if (_quotation.cotizacion[0].gold) {
                  _return = _quotation.cotizacion[0].gold[0].anual.primaTotal
                } else if (_quotation.cotizacion[1].gold) {
                  _return = _quotation.cotizacion[1].gold[0].anual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].gold[0].anual.primaTotal
                }
              } else if (frequency === 'semestral') {
                if (_quotation.cotizacion[0].gold) {
                  _return = _quotation.cotizacion[0].gold[0].semestral.primaTotal
                } else if (_quotation.cotizacion[1].gold) {
                  _return = _quotation.cotizacion[1].gold[0].semestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].gold[0].semestral.primaTotal
                }
              } else if (frequency === 'trimestral') {
                if (_quotation.cotizacion[0].gold) {
                  _return = _quotation.cotizacion[0].gold[0].trimestral.primaTotal
                } else if (_quotation.cotizacion[1].gold) {
                  _return = _quotation.cotizacion[1].gold[0].trimestral.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].gold[0].trimestral.primaTotal
                }
              } else if (frequency === 'mensual') {
                if (_quotation.cotizacion[0].gold) {
                  _return = _quotation.cotizacion[0].gold[0].mensual.primaTotal
                } else if (_quotation.cotizacion[1].gold) {
                  _return = _quotation.cotizacion[1].gold[0].mensual.primaTotal
                } else {
                  _return = _quotation.cotizacion[2].gold[0].mensual.primaTotal
                }
              }
            break;
          }
          break;
        case 0:
          if (_quotation.cotizacion[0].rc) {
            _return = _quotation.cotizacion[0].rc[0].anual.primaTotal
          } else if (_quotation.cotizacion[1].rc) {
            _return = _quotation.cotizacion[1].rc[0].anual.primaTotal
          } else if (_quotation.cotizacion[2].rc){
            _return = _quotation.cotizacion[2].rc[0].anual.primaTotal
          } else if (_quotation.cotizacion[3].rc){
            _return = _quotation.cotizacion[3].rc[0].anual.primaTotal
          }
        break;
        case 1:
          if (_quotation.cotizacion[0].limitada) {
            _return = _quotation.cotizacion[0].limitada[0].anual.primaTotal
          } else if (_quotation.cotizacion[1].limitada) {
            _return = _quotation.cotizacion[1].limitada[0].anual.primaTotal
          } else {
            _return = _quotation.cotizacion[2].limitada[0].anual.primaTotal
          }
        break;
        case 2:
          if (_quotation.cotizacion[0].amplia) {
            _return = _quotation.cotizacion[0].amplia[0].anual.primaTotal
          } else if (_quotation.cotizacion[1].amplia) {
            _return = _quotation.cotizacion[1].amplia[0].anual.primaTotal
          } else {
            _return = _quotation.cotizacion[2].amplia[0].anual.primaTotal
          }
        break;
        case 3:
          if (_quotation.cotizacion[0].especial) {
            _return = _quotation.cotizacion[0].especial[0].anual.primaTotal
          } else if (_quotation.cotizacion[1].especial) {
            _return = _quotation.cotizacion[1].especial[0].anual.primaTotal
          } else {
            _return = _quotation.cotizacion[2].especial[0].anual.primaTotal
          }
        break;
        case 4:
          if (_quotation.cotizacion[0].gold) {
            _return = _quotation.cotizacion[0].gold[0].anual.primaTotal
          } else if (_quotation.cotizacion[1].gold) {
            _return = _quotation.cotizacion[1].gold[0].anual.primaTotal
          } else {
            _return = _quotation.cotizacion[2].gold[0].anual.primaTotal
          }
        break;
      }
    }

    return _return;
  }

  getOpt(event: any): string {
    return (!event) ? 'amplia' : (event.index === 0) ? 'rc' : (event.index === 1) ? 'limitada' : (event.index === 3) ? 'especial' : (event.index === 4) ? 'gold' : 'amplia';
  }

  getPays(periodicity: string, insurance: string, coverages: any, frequency: string): any {
    let _return: any[] = [];
    // coverages = this.toLowerCaseKeyServ ice.parseToLower(coverages);
    switch (periodicity) {
      default:
      case 'gold':
        if (coverages[0].gold) {
          _return.push({ first: coverages[0].gold[0][frequency].primerPago, subsecuent: coverages[0].gold[0][frequency].pagosSubsecuentes });
        } else if (coverages[1].gold) {
          _return.push({ first: coverages[1].gold[0][frequency].primerPago, subsecuent: coverages[1].gold[0][frequency].pagosSubsecuentes });
        } else if(coverages[2].gold) {
          _return.push({ first: coverages[2].gold[0][frequency].primerPago, subsecuent: coverages[2].gold[0][frequency].pagosSubsecuentes });
        }
        break;
      case 'especial':
        if (coverages[0].especial) {
          _return.push({ first: coverages[0].especial[0][frequency].primerPago, subsecuent: coverages[0].especial[0][frequency].pagosSubsecuentes });
        } else if (coverages[1].especial) {
          _return.push({ first: coverages[1].especial[0][frequency].primerPago, subsecuent: coverages[1].especial[0][frequency].pagosSubsecuentes });
        } else if(coverages[2].especial) {
          _return.push({ first: coverages[2].especial[0][frequency].primerPago, subsecuent: coverages[2].especial[0][frequency].pagosSubsecuentes });
        }
        break;
      case 'amplia':
        if (coverages[0].amplia) {
          _return.push({ first: coverages[0].amplia[0][frequency].primerPago, subsecuent: coverages[0].amplia[0][frequency].pagosSubsecuentes });
        } else if (coverages[1].amplia) {
          _return.push({ first: coverages[1].amplia[0][frequency].primerPago, subsecuent: coverages[1].amplia[0][frequency].pagosSubsecuentes });
        } else {
          _return.push({ first: coverages[2].amplia[0][frequency].primerPago, subsecuent: coverages[2].amplia[0][frequency].pagosSubsecuentes });
        }
        break;
      case 'limitada':
        if (coverages[0].limitada) {
          _return.push({ first: coverages[0].limitada[0][frequency].primerPago, subsecuent: coverages[0].limitada[0][frequency].pagosSubsecuentes });
        } else if (coverages[1].limitada) {
          _return.push({ first: coverages[1].limitada[0][frequency].primerPago, subsecuent: coverages[1].limitada[0][frequency].pagosSubsecuentes });
        } else {
          _return.push({ first: coverages[2].limitada[0][frequency].primerPago, subsecuent: coverages[2].limitada[0][frequency].pagosSubsecuentes });
        }
        break;
      case 'rc':
        if (coverages[0].rc) {
          _return.push({ first: coverages[0].rc[0][frequency].primerPago, subsecuent: coverages[0].rc[0][frequency].pagosSubsecuentes });
        } else if (coverages[1].rc) {
          _return.push({ first: coverages[1].rc[0][frequency].primerPago, subsecuent: coverages[1].rc[0][frequency].pagosSubsecuentes });
        } else if (coverages[2].rc) {
          _return.push({ first: coverages[2].rc[0][frequency].primerPago, subsecuent: coverages[2].rc[0][frequency].pagosSubsecuentes });
        } else if (coverages[3].rc) {
          _return.push({ first: coverages[3].rc[0][frequency].primerPago, subsecuent: coverages[3].rc[0][frequency].pagosSubsecuentes });
        }
        break;
    }

    return _return;
  }

  getCoverages(periodicity: string, insurance: string, coverages: any): any {

    console.log(insurance);
    if(insurance != 'AFIRME'){
    let _return: any[] = [];
    switch (periodicity) {
      default:
      case 'especial':
        if (coverages[0].especial) {
          if (coverages[0].especial[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].especial[0].anual, checked: true });
          } else if (coverages[0].especial[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].especial[1].anual, checked: true });
          } else if (coverages[0].especial[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].especial[1].anual, checked: true });
          }
        } else if (coverages[1].especial) {
          if (coverages[1].especial[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].especial[0].anual, checked: true });
          } else if (coverages[1].especial[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].especial[1].anual, checked: true });
          } else if (coverages[1].especial[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].especial[1].anual, checked: true });
          }
        } else if (coverages[2].especial) {
          if (coverages[2].especial[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].especial[0].anual, checked: true });
          } else if (coverages[2].especial[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].especial[1].anual, checked: true });
          } else if (coverages[2].especial[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].especial[1].anual, checked: true });
          }
        }

        if (coverages[0].especial) {
          if (coverages[0].especial[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].especial[0].semestral, checked: true });
          } else if (coverages[0].especial[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].especial[1].semestral, checked: true });
          } else if (coverages[0].especial[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].especial[1].semestral, checked: true });
          }
        } else if (coverages[1].especial) {
          if (coverages[1].especial[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].especial[0].semestral, checked: true });
          } else if (coverages[1].especial[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].especial[1].semestral, checked: true });
          } else if (coverages[1].especial[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].especial[1].semestral, checked: true });
          }
        } else if (coverages[2].especial){
          if (coverages[2].especial[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].especial[0].semestral, checked: true });
          } else if (coverages[2].especial[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].especial[1].semestral, checked: true });
          } else if (coverages[2].especial[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].especial[1].semestral, checked: true });
          }
        }

        if (coverages[0].especial) {
          if (coverages[0].especial[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].especial[0].trimestral, checked: true });
          } else if (coverages[0].especial[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].especial[1].trimestral, checked: true });
          } else if (coverages[0].especial[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].especial[1].trimestral, checked: true });
          }
        } else if (coverages[1].especial) {
          if (coverages[1].especial[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].especial[0].trimestral, checked: true });
          } else if (coverages[1].especial[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].especial[1].trimestral, checked: true });
          } else if (coverages[1].especial[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].especial[1].trimestral, checked: true });
          }
        } else if(coverages[2].especial){
          if (coverages[2].especial[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].especial[0].trimestral, checked: true });
          } else if (coverages[2].especial[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].especial[1].trimestral, checked: true });
          } else if (coverages[2].especial[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].especial[1].trimestral, checked: true });
          }
        }


        if (coverages[0].especial) {
          if (coverages[0].especial[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].especial[0].mensual, checked: true });
          } else if (coverages[0].especial[1]) {
            if(coverages[0].especial[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[0].especial[1].mensual, checked: true });
            }
          } else if (coverages[0].especial[2]) {
            if (coverages[0].especial[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].especial[1].mensual, checked: true });
            }
          }
        } 
        else if (coverages[1].especial) {
          if (coverages[1].especial[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].especial[0].mensual, checked: true });
          } else if (coverages[1].especial[1]) {
            if(coverages[1].especial[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[1].especial[1].mensual, checked: true });
            }
          } else if (coverages[1].especial[2]) {
            if (coverages[1].especial[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].especial[2].mensual, checked: true });
            }
          }
        } 
        else if (coverages[2].especial) {
          if (coverages[2].especial[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].especial[0].mensual, checked: true });
          } else if (coverages[2].especial[1]) {
            if(coverages[2].especial[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[2].especial[1].mensual, checked: true });
            }
          } else if (coverages[2].especial[2]) {
            if (coverages[2].especial[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].especial[2].mensual, checked: true });
            }
          }
        } 
        break;
      case 'gold':
        if (coverages[0].gold) {
          if (coverages[0].gold[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].gold[0].anual, checked: true });
          } else if (coverages[0].gold[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].gold[1].anual, checked: true });
          } else if (coverages[0].gold[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].gold[1].anual, checked: true });
          }
        } else if (coverages[1].gold) {
          if (coverages[1].gold[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].gold[0].anual, checked: true });
          } else if (coverages[1].gold[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].gold[1].anual, checked: true });
          } else if (coverages[1].gold[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].gold[1].anual, checked: true });
          }
        } else if(coverages[2].gold) {
          if (coverages[2].gold[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].gold[0].anual, checked: true });
          } else if (coverages[2].gold[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].gold[1].anual, checked: true });
          } else if (coverages[2].gold[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].gold[1].anual, checked: true });
          }
        }

        if (coverages[0].gold) {
          if (coverages[0].gold[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].gold[0].semestral, checked: true });
          } else if (coverages[0].gold[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].gold[1].semestral, checked: true });
          } else if (coverages[0].gold[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].gold[1].semestral, checked: true });
          }
        } else if (coverages[1].gold) {
          if (coverages[1].gold[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].gold[0].semestral, checked: true });
          } else if (coverages[1].gold[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].gold[1].semestral, checked: true });
          } else if (coverages[1].gold[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].gold[1].semestral, checked: true });
          }
        } else if(coverages[2].gold){
          if (coverages[2].gold[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].gold[0].semestral, checked: true });
          } else if (coverages[2].gold[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].gold[1].semestral, checked: true });
          } else if (coverages[2].gold[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].gold[1].semestral, checked: true });
          }
        }

        if (coverages[0].gold) {
          if (coverages[0].gold[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].gold[0].trimestral, checked: true });
          } else if (coverages[0].gold[1]) {
            if(coverages[0].gold[1].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[0].gold[1].trimestral, checked: true });
            }
          } else if (coverages[0].gold[2]) {
            if(coverages[0].gold[2].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[0].gold[2].trimestral, checked: true });
            }
          }
        } else if (coverages[1].gold) {
          if (coverages[1].gold[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].gold[0].trimestral, checked: true });
          } else if (coverages[1].gold[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].gold[1].trimestral, checked: true });
          } else if (coverages[1].gold[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].gold[1].trimestral, checked: true });
          }
        } else if(coverages[2].gold){
          if (coverages[2].gold[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].gold[0].trimestral, checked: true });
          } else if (coverages[2].gold[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].gold[1].trimestral, checked: true });
          } else if (coverages[2].gold[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].gold[1].trimestral, checked: true });
          }
        }


        if (coverages[0].gold) {
          if (coverages[0].gold[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].gold[0].mensual, checked: true });
          } else if (coverages[0].gold[1]) {
            if(coverages[0].gold[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[0].gold[1].mensual, checked: true });
            }
          } else if (coverages[0].gold[2]) {
            if (coverages[0].gold[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].gold[1].mensual, checked: true });
            }
          }
        } 
        else if (coverages[1].gold) {
          if (coverages[1].gold[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].gold[0].mensual, checked: true });
          } else if (coverages[1].gold[1]) {
            if(coverages[1].gold[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[1].gold[1].mensual, checked: true });
            }
          } else if (coverages[1].gold[2]) {
            if (coverages[1].gold[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].gold[2].mensual, checked: true });
            }
          }
        } 
        else if (coverages[2].gold) {
          if (coverages[2].gold[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].gold[0].mensual, checked: true });
          } else if (coverages[2].gold[1]) {
            if(coverages[2].gold[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[2].gold[1].mensual, checked: true });
            }
          } else if (coverages[2].gold[2]) {
            if (coverages[2].gold[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].gold[2].mensual, checked: true });
            }
          }
        } 
        break;
      case 'amplia':
        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[0].anual, checked: true });
          } else if (coverages[0].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          } else if (coverages[0].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[0].anual, checked: true });
          } else if (coverages[1].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          } else if (coverages[1].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[0].anual, checked: true });
          } else if (coverages[2].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          } else if (coverages[2].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[0].semestral, checked: true });
          } else if (coverages[0].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          } else if (coverages[0].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[0].semestral, checked: true });
          } else if (coverages[1].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          } else if (coverages[1].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[0].semestral, checked: true });
          } else if (coverages[2].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          } else if (coverages[2].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[0].trimestral, checked: true });
          } else if (coverages[0].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          } else if (coverages[0].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[0].trimestral, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[1].trimestral, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if(coverages[1].amplia[2].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[2].trimestral, checked: true });
            }
          } else {
            _return;
          }
        } else if (coverages[2].amplia){
          if (coverages[2].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[0].trimestral, checked: true });
          } else if (coverages[2].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          } else if (coverages[2].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          }
        }


        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].amplia[0].mensual, checked: true });
          } else if (coverages[0].amplia[1]) {
            if(coverages[0].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          } else if (coverages[0].amplia[2]) {
            if (coverages[0].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          }
        } 
        else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].amplia[0].mensual, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[1].mensual, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if (coverages[1].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[2].mensual, checked: true });
            }
          }
        } 
        else if (coverages[2].amplia) {
          if (coverages[2].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].amplia[0].mensual, checked: true });
          } else if (coverages[2].amplia[1]) {
            if(coverages[2].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[1].mensual, checked: true });
            }
          } else if (coverages[2].amplia[2]) {
            if (coverages[2].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[2].mensual, checked: true });
            }
          }
        } 
        break;
      case 'limitada':
        if (coverages[0].limitada) {
          if (coverages[0].limitada[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].limitada[0].anual, checked: true });
          } else if (coverages[0].limitada[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].limitada[1].anual, checked: true });
          } else if (coverages[0].limitada[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].limitada[1].anual, checked: true });
          }
        } else if (coverages[1].limitada) {
          if (coverages[1].limitada[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].limitada[0].anual, checked: true });
          } else if (coverages[1].limitada[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].limitada[1].anual, checked: true });
          } else if (coverages[1].limitada[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].limitada[1].anual, checked: true });
          }
        } else {
          if (coverages[2].limitada[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].limitada[0].anual, checked: true });
          } else if (coverages[2].limitada[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].limitada[1].anual, checked: true });
          } else if (coverages[2].limitada[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].limitada[1].anual, checked: true });
          }
        }

        if (coverages[0].limitada) {
          if (coverages[0].limitada[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].limitada[0].semestral, checked: true });
          } else if (coverages[0].limitada[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].limitada[1].semestral, checked: true });
          } else if (coverages[0].limitada[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].limitada[1].semestral, checked: true });
          }
        } else if (coverages[1].limitada) {
          if (coverages[1].limitada[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].limitada[0].semestral, checked: true });
          } else if (coverages[1].limitada[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].limitada[1].semestral, checked: true });
          } else if (coverages[1].limitada[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].limitada[1].semestral, checked: true });
          }
        } else {
          if (coverages[2].limitada[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].limitada[0].semestral, checked: true });
          } else if (coverages[2].limitada[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].limitada[1].semestral, checked: true });
          } else if (coverages[2].limitada[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].limitada[1].semestral, checked: true });
          }
        }

        if (coverages[0].limitada) {
          if (coverages[0].limitada[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].limitada[0].trimestral, checked: true });
          } else if (coverages[0].limitada[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].limitada[1].trimestral, checked: true });
          } else if (coverages[0].limitada[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].limitada[1].trimestral, checked: true });
          }
        } else if (coverages[1].limitada) {
          if (coverages[1].limitada[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].limitada[0].trimestral, checked: true });
          } else if (coverages[1].limitada[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].limitada[1].trimestral, checked: true });
          } else if (coverages[1].limitada[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].limitada[1].trimestral, checked: true });
          }
        } else {
          if (coverages[2].limitada[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].limitada[0].trimestral, checked: true });
          } else if (coverages[2].limitada[1]) {
            if (coverages[2].limitada[1].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[2].limitada[1].trimestral, checked: true });
            }
          } else if (coverages[2].limitada[2]) {
            if (coverages[2].limitada[2].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[2].limitada[2].trimestral, checked: true });
            }
          }
        }

        if (coverages[0].limitada) {
          if (coverages[0].limitada[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].limitada[0].mensual, checked: true });
          } else if (coverages[0].limitada[1]) {
            if (coverages[0].limitada[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].limitada[1].mensual, checked: true });
            }
          } else if (coverages[0].limitada[2]) {
            if (coverages[0].limitada[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].limitada[1].mensual, checked: true });
            }
          }
        } else if (coverages[1].limitada) {
          if (coverages[1].limitada[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].limitada[0].mensual, checked: true });
          } else if (coverages[1].limitada[1]) {
            if (coverages[1].limitada[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].limitada[1].mensual, checked: true });
            }
          } else if (coverages[1].limitada[2]) {
            if (coverages[1].limitada[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].limitada[1].mensual, checked: true });
            }
          }
        } else {
          if (coverages[2].limitada[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].limitada[0].mensual, checked: true });
          } else if (coverages[2].limitada[1]) {
            if (coverages[2].limitada[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].limitada[1].mensual, checked: true });
            }
          } else if (coverages[2].limitada[2]) {
            if (coverages[2].limitada[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].limitada[1].mensual, checked: true });
            }
          }
        }

        break;
      case 'rc':
        if (coverages[0].rc) {
          if (coverages[0].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[0].anual, checked: true });
          } else if (coverages[0].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[1].anual, checked: true });
          } else if (coverages[0].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[1].anual, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[0].anual, checked: true });
          } else if (coverages[1].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[1].anual, checked: true });
          } else if (coverages[1].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[1].anual, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[0].anual, checked: true });
          } else if (coverages[2].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[1].anual, checked: true });
          } else if (coverages[2].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[1].anual, checked: true });
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[0].anual, checked: true });
          } else if (coverages[3].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[1].anual, checked: true });
          } else if (coverages[3].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[1].anual, checked: true });
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[0].semestral, checked: true });
          } else if (coverages[0].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[1].semestral, checked: true });
          } else if (coverages[0].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[1].semestral, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[0].semestral, checked: true });
          } else if (coverages[1].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[1].semestral, checked: true });
          } else if (coverages[1].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[2].semestral, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[0].semestral, checked: true });
          } else if (coverages[2].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[1].semestral, checked: true });
          } else if (coverages[2].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[2].semestral, checked: true });
          }
        } else if (coverages[3].rc) {
          
          if (coverages[3].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[0].semestral, checked: true });
          } else if (coverages[3].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[1].semestral, checked: true });
          } else if (coverages[3].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[2].semestral, checked: true });
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[0].trimestral, checked: true });
          } else if (coverages[0].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[1].trimestral, checked: true });
          } else if (coverages[0].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[2].trimestral, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[0].trimestral, checked: true });
          } else if (coverages[1].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[1].trimestral, checked: true });
          } else if (coverages[1].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[2].trimestral, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[0].trimestral, checked: true });
          } else if (coverages[2].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[1].trimestral, checked: true });
          } else if (coverages[2].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[2].trimestral, checked: true });
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[3].rc[0].trimestral, checked: true });
          } else if (coverages[3].rc[1]) {
            if (coverages[3].rc[1].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[3].rc[1].trimestral, checked: true });
            }
          } else if (coverages[3].rc[2]) {
            if (coverages[3].rc[2].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[3].rc[2].trimestral, checked: true });
            }
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].rc[0].mensual, checked: true });
          } else if (coverages[0].rc[1]) {
            if (coverages[0].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].rc[1].mensual, checked: true });
            }
          } else if (coverages[0].rc[2]) {
            if (coverages[0].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].rc[0].mensual, checked: true });
          } else if (coverages[1].rc[1]) {
            if (coverages[1].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].rc[1].mensual, checked: true });
            }
          } else if (coverages[1].rc[2]) {
            if (coverages[1].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].rc[0].mensual, checked: true });
          } else if (coverages[2].rc[1]) {
            if (coverages[2].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].rc[1].mensual, checked: true });
            }
          } else if (coverages[2].rc[2]) {
            if (coverages[2].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[3].rc[0].mensual, checked: true });
          } else if (coverages[3].rc[1]) {
            if (coverages[3].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[3].rc[1].mensual, checked: true });
            }
          } else if (coverages[3].rc[2]) {
            if (coverages[3].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[3].rc[2].mensual, checked: true });
            }
          }
        }

        break;
    }
    return _return;
  }else{
    let _return: any[] = [];
    switch (periodicity) {
      default:

      
      
        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[0].anual, checked: true });
          } else if (coverages[0].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          } else if (coverages[0].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[0].anual, checked: true });
          } else if (coverages[1].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          } else if (coverages[1].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[0].anual, checked: true });
          } else if (coverages[2].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          } else if (coverages[2].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[0].semestral, checked: true });
          } else if (coverages[0].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          } else if (coverages[0].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[0].semestral, checked: true });
          } else if (coverages[1].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          } else if (coverages[1].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[0].semestral, checked: true });
          } else if (coverages[2].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          } else if (coverages[2].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[0].trimestral, checked: true });
          } else if (coverages[0].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          } else if (coverages[0].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[0].trimestral, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[1].trimestral, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if(coverages[1].amplia[2].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[2].trimestral, checked: true });
            }
          } else {
            _return;
          }
        } else if (coverages[2].amplia){
          if (coverages[2].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[0].trimestral, checked: true });
          } else if (coverages[2].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          } else if (coverages[2].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          }
        }


        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].amplia[0].mensual, checked: true });
          } else if (coverages[0].amplia[1]) {
            if(coverages[0].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          } else if (coverages[0].amplia[2]) {
            if (coverages[0].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          }
        } 
        else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].amplia[0].mensual, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[1].mensual, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if (coverages[1].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[2].mensual, checked: true });
            }
          }
        } 
        else if (coverages[2].amplia) {
          if (coverages[2].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].amplia[0].mensual, checked: true });
          } else if (coverages[2].amplia[1]) {
            if(coverages[2].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[1].mensual, checked: true });
            }
          } else if (coverages[2].amplia[2]) {
            if (coverages[2].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[2].mensual, checked: true });
            }
          }
        } 
        break;
      case 'amplia':
        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[0].anual, checked: true });
          } else if (coverages[0].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          } else if (coverages[0].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].amplia[1].anual, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[0].anual, checked: true });
          } else if (coverages[1].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          } else if (coverages[1].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].amplia[1].anual, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[0].anual, checked: true });
          } else if (coverages[2].amplia[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          } else if (coverages[2].amplia[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].amplia[1].anual, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[0].semestral, checked: true });
          } else if (coverages[0].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          } else if (coverages[0].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].amplia[1].semestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[0].semestral, checked: true });
          } else if (coverages[1].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          } else if (coverages[1].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].amplia[1].semestral, checked: true });
          }
        } else {
          if (coverages[2].amplia[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[0].semestral, checked: true });
          } else if (coverages[2].amplia[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          } else if (coverages[2].amplia[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].amplia[1].semestral, checked: true });
          }
        }

        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[0].trimestral, checked: true });
          } else if (coverages[0].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          } else if (coverages[0].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].amplia[1].trimestral, checked: true });
          }
        } else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[0].trimestral, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[1].trimestral, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if(coverages[1].amplia[2].trimestral){
              _return.push({ periodicity: 'trimestral', values: coverages[1].amplia[2].trimestral, checked: true });
            }
          } else {
            _return;
          }
        } else if (coverages[2].amplia){
          if (coverages[2].amplia[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[0].trimestral, checked: true });
          } else if (coverages[2].amplia[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          } else if (coverages[2].amplia[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].amplia[1].trimestral, checked: true });
          }
        }


        if (coverages[0].amplia) {
          if (coverages[0].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].amplia[0].mensual, checked: true });
          } else if (coverages[0].amplia[1]) {
            if(coverages[0].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          } else if (coverages[0].amplia[2]) {
            if (coverages[0].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].amplia[1].mensual, checked: true });
            }
          }
        } 
        else if (coverages[1].amplia) {
          if (coverages[1].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].amplia[0].mensual, checked: true });
          } else if (coverages[1].amplia[1]) {
            if(coverages[1].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[1].mensual, checked: true });
            }
          } else if (coverages[1].amplia[2]) {
            if (coverages[1].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].amplia[2].mensual, checked: true });
            }
          }
        } 
        else if (coverages[2].amplia) {
          if (coverages[2].amplia[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].amplia[0].mensual, checked: true });
          } else if (coverages[2].amplia[1]) {
            if(coverages[2].amplia[1].mensual){
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[1].mensual, checked: true });
            }
          } else if (coverages[2].amplia[2]) {
            if (coverages[2].amplia[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].amplia[2].mensual, checked: true });
            }
          }
        } 
        break;
    
    
        if (coverages[0].rc) {
          if (coverages[0].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[0].anual, checked: true });
          } else if (coverages[0].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[1].anual, checked: true });
          } else if (coverages[0].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[0].rc[1].anual, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[0].anual, checked: true });
          } else if (coverages[1].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[1].anual, checked: true });
          } else if (coverages[1].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[1].rc[1].anual, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[0].anual, checked: true });
          } else if (coverages[2].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[1].anual, checked: true });
          } else if (coverages[2].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[2].rc[1].anual, checked: true });
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[0].anual, checked: true });
          } else if (coverages[3].rc[1].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[1].anual, checked: true });
          } else if (coverages[3].rc[2].anual) {
            _return.push({ periodicity: 'anual', values: coverages[3].rc[1].anual, checked: true });
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[0].semestral, checked: true });
          } else if (coverages[0].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[1].semestral, checked: true });
          } else if (coverages[0].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[0].rc[1].semestral, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[0].semestral, checked: true });
          } else if (coverages[1].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[1].semestral, checked: true });
          } else if (coverages[1].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[1].rc[2].semestral, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[0].semestral, checked: true });
          } else if (coverages[2].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[1].semestral, checked: true });
          } else if (coverages[2].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[2].rc[2].semestral, checked: true });
          }
        } else if (coverages[3].rc) {
          
          if (coverages[3].rc[0].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[0].semestral, checked: true });
          } else if (coverages[3].rc[1].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[1].semestral, checked: true });
          } else if (coverages[3].rc[2].semestral) {
            _return.push({ periodicity: 'semestral', values: coverages[3].rc[2].semestral, checked: true });
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[0].trimestral, checked: true });
          } else if (coverages[0].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[1].trimestral, checked: true });
          } else if (coverages[0].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[0].rc[2].trimestral, checked: true });
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[0].trimestral, checked: true });
          } else if (coverages[1].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[1].trimestral, checked: true });
          } else if (coverages[1].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[1].rc[2].trimestral, checked: true });
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[0].trimestral, checked: true });
          } else if (coverages[2].rc[1].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[1].trimestral, checked: true });
          } else if (coverages[2].rc[2].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[2].rc[2].trimestral, checked: true });
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].trimestral) {
            _return.push({ periodicity: 'trimestral', values: coverages[3].rc[0].trimestral, checked: true });
          } else if (coverages[3].rc[1]) {
            if (coverages[3].rc[1].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[3].rc[1].trimestral, checked: true });
            }
          } else if (coverages[3].rc[2]) {
            if (coverages[3].rc[2].trimestral) {
              _return.push({ periodicity: 'trimestral', values: coverages[3].rc[2].trimestral, checked: true });
            }
          }
        }

        if (coverages[0].rc) {
          if (coverages[0].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[0].rc[0].mensual, checked: true });
          } else if (coverages[0].rc[1]) {
            if (coverages[0].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].rc[1].mensual, checked: true });
            }
          } else if (coverages[0].rc[2]) {
            if (coverages[0].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[0].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[1].rc) {
          if (coverages[1].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[1].rc[0].mensual, checked: true });
          } else if (coverages[1].rc[1]) {
            if (coverages[1].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].rc[1].mensual, checked: true });
            }
          } else if (coverages[1].rc[2]) {
            if (coverages[1].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[1].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[2].rc) {
          if (coverages[2].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[2].rc[0].mensual, checked: true });
          } else if (coverages[2].rc[1]) {
            if (coverages[2].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].rc[1].mensual, checked: true });
            }
          } else if (coverages[2].rc[2]) {
            if (coverages[2].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[2].rc[1].mensual, checked: true });
            }
          }
        } else if (coverages[3].rc) {
          if (coverages[3].rc[0].mensual) {
            _return.push({ periodicity: 'mensual', values: coverages[3].rc[0].mensual, checked: true });
          } else if (coverages[3].rc[1]) {
            if (coverages[3].rc[1].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[3].rc[1].mensual, checked: true });
            }
          } else if (coverages[3].rc[2]) {
            if (coverages[3].rc[2].mensual) {
              _return.push({ periodicity: 'mensual', values: coverages[3].rc[2].mensual, checked: true });
            }
          }
        }

        break;
    }
    return _return;
  }
  }

  getCoveragesList(coverages: any, type: string): any {


    console.log(coverages); 
    console.log(type);
    if(coverages.aseguradora != 'AFIRME' && coverages.aseguradora != 'afirme'){

      let _gold;
      if (coverages.coberturas[0].gold) {
        _gold = coverages.coberturas[0].gold;
      } else if (coverages.coberturas[1].gold) {
        _gold = coverages.coberturas[1].gold;
      } else if (coverages.coberturas[2].gold) {
        _gold = coverages.coberturas[2].gold;
      }
  
      let _especial;
      if (coverages.coberturas[0].especial) {
        _especial = coverages.coberturas[0].especial;
      } else if (coverages.coberturas[1].especial) {
        _especial = coverages.coberturas[1].especial;
      } else if (coverages.coberturas[2].especial) {
        _especial = coverages.coberturas[2].especial;
      }
  
      let _amplia;
      if (coverages.coberturas[0].amplia) {
        _amplia = coverages.coberturas[0].amplia;
      } else if (coverages.coberturas[1].amplia) {
        _amplia = coverages.coberturas[1].amplia;
      } else if (coverages.coberturas[2].amplia) {
        _amplia = coverages.coberturas[2].amplia;
      }else if (coverages.coberturas) {
        _amplia = coverages.coberturas;
      }
  
      let _limitada;
      if (coverages.coberturas[0].limitada) {
        _limitada = coverages.coberturas[0].limitada;
      } else if (coverages.coberturas[1].limitada) {
        _limitada = coverages.coberturas[1].limitada;
      } else if (coverages.coberturas[2].limitada) {
        _limitada = coverages.coberturas[2].limitada;
      }
  
      let _rc;
      if (coverages.coberturas[0].rc) {
        _rc = coverages.coberturas[0].rc;
      } else if (coverages.coberturas[1].rc) {
        _rc = coverages.coberturas[1].rc;
      } else if (coverages.coberturas[2].rc) {
        _rc =  coverages.coberturas[2].rc;
      } else if (coverages.coberturas[3].rc) {
        _rc =  coverages.coberturas[3].rc;
      }
        
      
  
      let _coverage = {
        gold: _gold,
        especial: _especial,
        amplia: _amplia,
        limitada: _limitada,
        rc: _rc
      };
  
      return _coverage;
    }else{
      console.log('estoy afuera');
      let _amplia;
      if (coverages.coberturas[0].amplia) {
        _amplia = coverages.coberturas[0].amplia;
      } else if (coverages.coberturas[1].amplia) {
        _amplia = coverages.coberturas[1].amplia;
      } else if (coverages.coberturas[2].amplia) {
        _amplia = coverages.coberturas[2].amplia;
      }else if (coverages.coberturas) {
        _amplia = coverages.coberturas;
      }

      let _coverage = {
        amplia: _amplia,
      };
  
      return _coverage;
    }
  }

  getQuotationList(coverages: any, type: string): any {

    console.log(coverages);
    if(coverages.aseguradora != 'AFIRME' && coverages.aseguradora != 'afirme'){

      let _gold;
      if (coverages.cotizacion[0].gold) {
        _gold = coverages.cotizacion[0].gold;
      } else if (coverages.cotizacion[1].gold) {
        _gold = coverages.cotizacion[1].gold;
      } else if (coverages.cotizacion[2].gold) {
        _gold = coverages.cotizacion[2].gold;
      }
  
      let _especial;
      if (coverages.cotizacion[0].especial) {
        _especial = coverages.cotizacion[0].especial;
      } else if (coverages.cotizacion[1].especial) {
        _especial = coverages.cotizacion[1].especial;
      } else if (coverages.cotizacion[2].especial) {
        _especial = coverages.cotizacion[2].especial;
      }
  
      let _amplia;
      if (coverages.cotizacion[0].amplia) {
        _amplia = coverages.cotizacion[0].amplia;
      } else if (coverages.cotizacion[1].amplia) {
        _amplia = coverages.cotizacion[1].amplia;
      } else if (coverages.cotizacion[2].amplia) {
        _amplia = coverages.cotizacion[2].amplia;
      }
  
      let _limitada;
      if (coverages.cotizacion[0].limitada) {
        _limitada = coverages.cotizacion[0].limitada;
      } else if (coverages.cotizacion[1].limitada) {
        _limitada = coverages.cotizacion[1].limitada;
      } else if (coverages.cotizacion[2].limitada) {
        _limitada = coverages.cotizacion[2].limitada;
      }
  
      let _rc;
      if (coverages.cotizacion[0].rc) {
        _rc = coverages.cotizacion[0].rc;
      } else if (coverages.cotizacion[1].rc) {
        _rc = coverages.cotizacion[1].rc;
      } else if (coverages.cotizacion[2].rc) {
            _rc =  coverages.cotizacion[2].rc;
      } else if (coverages.cotizacion[3].rc) {
            _rc =  coverages.cotizacion[3].rc;
      }
  
      let _coverage = {
        gold: _gold,
        especial: _especial,
        amplia: _amplia,
        limitada: _limitada,
        rc: _rc
      };
  
      return _coverage;
    } else{
  
  
      let _amplia;
      if (coverages.cotizacion[0].amplia) {
        _amplia = coverages.cotizacion[0].amplia;
      } else if (coverages.cotizacion[1].amplia) {
        _amplia = coverages.cotizacion[1].amplia;
      } else if (coverages.cotizacion[2].amplia) {
        _amplia = coverages.cotizacion[2].amplia;
      }

  
      let _coverage = {
        amplia: _amplia,
      };
  
      return _coverage;
    }
  }

  private _toObject(arr): any {
    var output = {};
    arr.forEach(function (item, index) {
      if (!item) return;
      if (Array.isArray(item)) {
        output[index] = this._toObject(item);
      } else {
        output[index] = item;
      }
    });
    return output;
  }
}
