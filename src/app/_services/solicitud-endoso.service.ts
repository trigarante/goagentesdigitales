import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class SolicitudEndosoService {

    url: any;

    constructor(
        private http: HttpClient
    ) {
        this.url = environment.oficinaServices;
    }

    post(data): Observable<any> {
        return this.http.post<any>(this.url + '/endosos/solicitud-endoso', data)
    }
}
