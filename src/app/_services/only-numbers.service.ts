import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OnlyNumbersService {

  constructor() { }

  validateInput(value: any): boolean {
    const charCode = (value.which) ? value.which : value.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
