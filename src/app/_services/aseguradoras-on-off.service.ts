import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AseguradorasOnOffService {

  constructor(private http:HttpClient) { }

  ViewEnableAseg(GoID): Observable<any>{
    return this.http.get(`${environment.ENDCOTIZACION}/agente/view-enabled`,{params: {GoID:GoID}})
  }
  cambioEstadoAseg(company,enabled,GoID): Observable<any> {
    return this.http.get(`${environment.ENDCOTIZACION}/agente/enable-disable-user-company`, {params: {company: company, enabled: enabled, GoID:GoID}})
  }

  cambioEstadoAsegTotal(company,enabled): Observable<any> {
    return this.http.get(`${environment.ENDCOTIZACION}/agente/disable-company`, {params: {company: company, enabled: enabled}})
  }

  cambioDescuentoAsegBaz(aseguradora, descuento, goId): Observable<any>{
    return this.http.put<any>(`${environment.ENDCOTIZACION}/agente/modify-discount?company=${aseguradora}&discount=${descuento}&goId=${goId}`, {params: {company: aseguradora, discount: descuento, goId: goId}})
  }


  tablaDeCotizacionesService(goId, PaginaActual, resultado): Observable<any>{
    return this.http.get<any>(`${environment.ENDCOTIZACION}/agente/view-quotes`, {params: {goId: goId, paginaActual: PaginaActual, resultadosPorPaginas : resultado}})
  }

}
