import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {CatalogosInterface} from "../_interfaces";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ImagenesService {
  urlImg             = environment.endpoint_t2022 + '/imagenes/home';
  urlImgMulti        = environment.endpoint_t2022 + '/imagenes/cotizador';
  urlDirectorioGo    = environment.endpoint_t2022 + '/directorio';
  urlMesesSinInt     = environment.endpoint_t2022 + '/msi';
  urlPromocionesVig  = environment.endpoint_t2022 + '/promociones-vigentes';
  constructor(private http: HttpClient) { }
    getImgBanner(): Observable<any> {return this.http.get<any>(this.urlImg) };
    getImgBannerMulti(): Observable<any> {return this.http.get<any>(this.urlImgMulti) };
    getDirectorioGo(): Observable<any> {return this.http.get<any>(this.urlDirectorioGo) };
    getMesesSinInt(): Observable<any> {return this.http.get<any>(this.urlMesesSinInt) };
    getPromociones(): Observable<any> {return this.http.get<any>(this.urlPromocionesVig) };

  }


