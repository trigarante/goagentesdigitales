import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpContext} from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { LoginResponseInterface } from '../_interfaces';
import {sinToken} from "../_interceptors";



@Injectable({
  providedIn: 'root'
})
export class LoginService {

  @Output() disparadorDeTokenGo: EventEmitter<any> = new EventEmitter()
  @Output() disparadorDePlacas: EventEmitter<any> = new EventEmitter()

  constructor(
    private http: HttpClient
  ) { }

  login(form: any): Observable<LoginResponseInterface> {
    // localStorage.clear();
    let _body = { username: form.username, password: form.password };
    return this.http.post<LoginResponseInterface>(environment.ENDPOINTBASE + 'authenticate', _body);
  }

  loginforgot(form: any): Observable<LoginResponseInterface> {
    // localStorage.clear();
    let _body = { username: form.username, password: form.password };
    return this.http.post<LoginResponseInterface>(environment.ENDPOINTBASE + 'reset', _body);
  }

  getTokenBranding(): Observable<any> {
    // localStorage.clear();
    let _body = { tokenData: 'MxQkZ8IhziDRxAl1PG4zvhQkYsx5XKa/mHxuu2nOXfM=' };
    return this.http.post<any>(environment.endpoint_privat_driver + 'v1/authenticate', _body)
  }

  getTokenGo(form: any): Observable<any>{
    // localStorage.clear();
    let _body = { username: form.username, password: form.password };
    return this.http.post(environment.ENDPOINTBASE + 'authenticate', _body)
  }

  getTokenPlacas(): Observable<any>{
    // localStorage.clear();
    let _body = {tokenData:'mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g='};
    return this.http.post(environment.ENDPOINTPERSISTANCE + 'v1/authenticate', _body,{context: new HttpContext().set(sinToken, true)})
  }
}
