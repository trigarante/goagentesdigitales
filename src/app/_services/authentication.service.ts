import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  helper: any;

  constructor(
    public jwtHelper: JwtHelperService, 
    public router: Router
  ) {
      this.helper = new JwtHelperService();
  }


  public isLoggedIn(): boolean {
      return new Date().getTime() <= this.helper.getTokenExpirationDate(localStorage.getItem('token')).getTime();
  }

  signOut(): any {
      localStorage.clear();
      sessionStorage.clear();
      this.router.navigate(['/']);
  }
}
