import { Injectable, EventEmitter } from '@angular/core';
// import {EventEmitter} from 'angular2/core';

@Injectable({
  providedIn: 'root'
})
export class MenuToggleService {
  navchange: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  emitNavChangeEvent(toggle: boolean): void {
    this.navchange.emit(toggle);
  }

  getNavChangeEmitter() {
    return this.navchange;
  }
}
