import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RemoveStorageQuotationService {

  constructor(
    private activatedRoute: ActivatedRoute
  ) { }

  removeStorage(type?: number): void {
    if(localStorage.getItem('cotizador') === '1'){
      localStorage.setItem('cotizador', '2')
    }else{
      if (!this.activatedRoute.snapshot.queryParams.Res) {
        localStorage.removeItem('lastEmision');
        localStorage.removeItem('cpAfirme');
        localStorage.removeItem('cotizador');
        localStorage.removeItem('lastEmisionSaved');
        localStorage.removeItem('tmpQuotation');
        localStorage.removeItem('cotizacionSaved');
        localStorage.removeItem('_t');
        localStorage.removeItem('lastCotizacion');
        localStorage.removeItem('savedFake');
        localStorage.removeItem('_tmpMultyPush');
        localStorage.removeItem('tmpQuotation2');
        localStorage.removeItem('quotationType');
        localStorage.removeItem('servicio');
        sessionStorage.removeItem('customerQuotation');
        sessionStorage.removeItem('tmpCustomQuotation');
        sessionStorage.removeItem('customQuotation');
        sessionStorage.removeItem('baseQuotation');
        sessionStorage.removeItem('coverageSelected');
        sessionStorage.removeItem('payFormSelected');
        sessionStorage.removeItem('cotizacionSavedNew');
        sessionStorage.removeItem('totalP');
        localStorage.removeItem('datosFormularioCot');
        localStorage.removeItem('laC');


        // localStorage.removeItem('tokenAs');

        if (type == 1) {
          localStorage.removeItem('cotizador');
          localStorage.removeItem('lastPaidSaved');
          localStorage.removeItem('multiQuote');
          localStorage.removeItem('tmpMultiSelected');
          localStorage.removeItem('tmpMultAskQuotation');
          localStorage.removeItem('coverageSelected');
          localStorage.removeItem('payFormSelected');
          localStorage.removeItem('cotizacionSavedNew');
          localStorage.removeItem('card');
          sessionStorage.removeItem('totalP');
          // localStorage.removeItem('tokenAs');
          localStorage.removeItem('cpAfirme');
          localStorage.removeItem('quotationType');

        } else {
          // localStorage.removeItem('tokenAs');
          localStorage.removeItem('plToken');
          localStorage.removeItem('coverageSelected');
          localStorage.removeItem('payFormSelected');
          localStorage.removeItem('cotizacionSavedNew');
          sessionStorage.removeItem('totalP');
          localStorage.removeItem('cotizador');
          localStorage.removeItem('cpAfirme');
          localStorage.removeItem('quotationType');

        }
      }
    }
  }
}
