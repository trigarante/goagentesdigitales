import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EvetnsEmiterLocalService {
  navchangeMenu: EventEmitter<boolean> = new EventEmitter();
  resumeMenu: EventEmitter<number> = new EventEmitter();
  loader: EventEmitter<string> = new EventEmitter();

  constructor() { }

  emitNavChangeEvent(toggle: boolean): void {
    this.navchangeMenu.emit(toggle);
  }

  getNavChangeEmitter() {
    return this.navchangeMenu;
  }

  emitResumeChangeEvent(opt: number): void {
    this.resumeMenu.emit(opt);
  }

  getResumeChangeEmitter() {
    return this.resumeMenu;
  }

  emitLoader(opt: string): void {
    this.loader.emit(opt);
  }

  getLoader() {
    return this.loader;
  }
}
