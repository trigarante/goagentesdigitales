import { Injectable } from '@angular/core';

import { camelcaseKeysDeep } from 'camelcase-keys-deep';

@Injectable({
  providedIn: 'root'
})
export class ToLowerCaseKeyService {

  constructor() { }

  parseToLower(json: any): any {
    for (var prop in json) {
      if (typeof json[prop] === 'string') {
        json[prop] = json[prop].toLowerCase();
      }
      // if (typeof json[prop] === 'jsonect') {
      //   parseToLower(json[prop]);
      // }
    }
    return json;
  }

  parseToLower2(json: any): any {
    let _json = {
      "aseguradora": json.Aseguradora,
      "cliente": {
        "tipoPersona": json.Cliente.TipoPersona,
        "nombre": json.Cliente.Nombre,
        "apellidoPat": json.Cliente.ApellidoPat,
        "apellidoMat": json.Cliente.ApellidoMat,
        "rfc": json.Cliente.RFC,
        "fechaNacimiento": json.Cliente.FechaNacimiento,
        "ocupacion": json.Cliente.Ocupacion,
        "curp": json.Cliente.CURP,
        "direccion": {
          "calle": json.Cliente.Direccion.Calle,
          "noExt": json.Cliente.Direccion.NoExt,
          "noInt": json.Cliente.Direccion.NoInt,
          "colonia": json.Cliente.Direccion.Colonia,
          "codPostal": json.Cliente.Direccion.CodPostal,
          "poblacion": json.Cliente.Direccion.Poblacion,
          "ciudad": json.Cliente.Direccion.Ciudad,
          "pais": json.Cliente.Direccion.Pais
        },
        "edad": json.Cliente.Edad,
        "genero": json.Cliente.Genero,
        "telefono": json.Cliente.Telefono,
        "email": json.Cliente.Email
      },
      "vehiculo": {
        "uso": json.Vehiculo.Uso,
        "marca": json.Vehiculo.Marca,
        "modelo": json.Vehiculo.Modelo,
        "noMotor": json.Vehiculo.NoMotor,
        "noSerie": json.Vehiculo.NoSerie,
        "noPlacas": json.Vehiculo.NoPlacas,
        "descripcion": json.Vehiculo.Descripcion,
        "codMarca": json.Vehiculo.CodMarca,
        "codDescripcion": json.Vehiculo.CodDescripcion,
        "codUso": json.Vehiculo.CodUso,
        "clave": json.Vehiculo.Clave,
        "servicio": json.Vehiculo.Servicio,
        "subMarca": json.Vehiculo.SubMarca
      },
      "coberturas": json.Coberturas,
      "paquete": "All",
      "descuento": json.Descuento,
      "periodicidadDePago": json.PeriodicidadDePago,
      "cotizacion": json.Cotizacion,
      "emision": {
        "primaTotal": json.Emision.PrimaTotal,
        "primaNeta": json.Emision.PrimaNeta,
        "derechos": json.Emision.Derechos,
        "impuesto": json.Emision.Impuesto,
        "recargos": json.Emision.Recargos,
        "primerPago": json.Emision.PrimerPago,
        "pagosSubsecuentes": json.Emision.PagosSubsecuentes,
        "idCotizacion": json.Emision.IDCotizacion,
        "terminal": json.Emision.Terminal,
        "documento": json.Emision.Documento,
        "poliza": json.Emision.Poliza,
        "resultado": false
      },
      "pago": {
        "medioPago": "",
        "nombreTarjeta": "",
        "banco": "",
        "noTarjeta": "",
        "mesExp": "",
        "anioExp": "",
        "codigoSeguridad": "",
        "noClabe": "",
        "carrier": 0,
        "msi": ""
      },
      "codigoError": "",
      "urlRedireccion": ""
    }

    return _json;
  }
}
