import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxIndexedDBService, ObjectStoreMeta } from 'ngx-indexed-db';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { T2020AreasInterface, T2020FilesByAreasInterface } from '../_interfaces';

@Injectable({
  providedIn: 'root'
})
export class T2020Service {

  constructor(
    private http: HttpClient,
    private dbService: NgxIndexedDBService
  ) { }

  getAreas(): Observable<T2020AreasInterface[]> {
    return this.http.get<T2020AreasInterface[]>(environment.endpoint_t2020 + 'areas');
  }

  getFilesByArea(areaID: number): Observable<T2020FilesByAreasInterface[]> {
    return this.http.get<T2020FilesByAreasInterface[]>(environment.endpoint_t2020 + 'archivos-area/' + areaID);
  }

  storeAreasObject(object: T2020AreasInterface[]) {
    const storeSchema: ObjectStoreMeta = {
      store: 'areasT2020',
      storeConfig: { keyPath: 'localID', autoIncrement: true },
      storeSchema: [
        { name: 'activo', keypath: 'activo', options: { unique: false } },
        { name: 'fechaCreacion', keypath: 'fechaCreacion', options: { unique: false } },
        { name: 'descripcion', keypath: 'descripcion', options: { unique: false } },
        { name: 'id', keypath: 'id', options: { unique: false } }
      ]
    };

    this.dbService.createObjectStore(storeSchema);

    const storeSchema2: ObjectStoreMeta = {
      store: 'filesByAreasT2020',
      storeConfig: { keyPath: 'localID', autoIncrement: true },
      storeSchema: [
        { name: 'activo', keypath: 'activo', options: { unique: false } },
        { name: 'fechaRegistro', keypath: 'fechaRegistro', options: { unique: false } },
        { name: 'tipoArchivo', keypath: 'tipoArchivo', options: { unique: false } },
        { name: 'url', keypath: 'url', options: { unique: false } },
        { name: 'nombre', keypath: 'nombre', options: { unique: false } },
        { name: 'idArea', keypath: 'idArea', options: { unique: false } },
        { name: 'id', keypath: 'id', options: { unique: false } }
      ]
    };

    this.dbService.createObjectStore(storeSchema2);

    this._insertAreas(object);
  }

  getBySpecificIndex(dbName: string, index: string, value: string): any {
    this.dbService.getByIndex(dbName, index, value).subscribe(
      element => {
        return element
      }
    );
  }

  private _insertAreas(object: T2020AreasInterface[]) {
    object.forEach(el => {
      let _insert: boolean = false;
      this._insertFilesByAreas(el.id);
      this.dbService.getByIndex('areasT2020', 'id', el.id).subscribe((people) => {
        _insert = (people) ? false : true;

        if (_insert) {
          this.dbService.add(
            'areasT2020',
            {
              id: el.id,
              descripcion: el.descripcion,
              fechaCreacion: el.fechaCreacion,
              activo: el.activo
            }
          ).subscribe((key) => {
            // console.log('key: ', key);
          });
        }
      });
    });
  }

  private _insertFilesByAreas(areaID: number) {
    this.getFilesByArea(areaID).subscribe(
      succ => {
        succ.forEach(el => {
          let _insert: boolean = false;
          this._insertFilesByAreas(el.id);
          this.dbService.getByIndex('filesByAreasT2020', 'id', el.id).subscribe((people) => {
            _insert = (people) ? false : true;

            if (_insert) {
              this.dbService.add(
                'filesByAreasT2020',
                {
                  id: el.id,
                  idArea: el.idArea,
                  nombre: el.nombre,
                  url: el.url,
                  tipoArchivo: el.tipoArchivo,
                  fechaRegistro: el.fechaRegistro,
                  activo: el.activo
                }
              ).subscribe((key) => {
                // console.log('key: ', key);
              });
            }
          });
        });
      },
      err => {
        console.error(err.messageError);
      }
    )
  }
}

