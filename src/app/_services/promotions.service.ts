import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { environment as GENERALS } from 'src/environments/environment.generals';

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  constructor(
    private http: HttpClient,
  ) { }

  getCurrentPromotions() {
    let _return = [];
    GENERALS.current_promotions.forEach(el => {
      if(el.active)
        _return.push(el)
    });

    return _return;
  }
}
