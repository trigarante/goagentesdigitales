import {HttpClient, HttpContext} from '@angular/common/http';
import {EventEmitter, Injectable, OnDestroy, Output} from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CatalogosInterface, CodPostalInterface,CotizacionRequestInterface, CotizacionResponseInterface, EmisionRequestInterface } from '../_interfaces';
import * as moment from 'moment';
import {BYPASS_JW_TOKEN, tokenGo, sinToken} from "../_interceptors";

@Injectable({
    providedIn: 'root'
})
export class CatalogosService {

    @Output() disparadorDescuentos: EventEmitter<any> = new EventEmitter()

    constructor(
        private http: HttpClient
    ) { }

    urlTypeService:any;

    /**
       * Metodo para devolver el catalogo de marcas de acuerdo a la aseguradora
       * @param aseguradora
       */
    getMarcas(aseguradora: string, urlType: string): Observable<CatalogosInterface[]> {
        if (urlType === 'particular'|| urlType==='valor-factura') {
            
            //if(aseguradora.toUpperCase() === 'MIGO'){ aseguradora = "general_de_seguros"; }
            return this.http.get<CatalogosInterface[]>(environment.ENDCOTIZACION + '/' + aseguradora + '/brand', { context: new HttpContext().set(tokenGo, true)});
        } else { //privateDriver
            return this.http.get<CatalogosInterface[]>( `${environment.ENDPOINTAUTOSTESTV2}/${aseguradora}/private/brand`);
        }
    }

    getMarcasComparador(): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTBASE + 'v1/comparador/marcas');
    }

    /**
     * Metodo para devolver el catalogo de modelos de acuerdo a la aseguradora y marca
     * @param aseguradora
     * @param marca
     */
    getModelos(aseguradora: string, marca: string, urlType: string): Observable<CatalogosInterface[]> {
        //se valida el urlType que sea igual a valor factura o particular ya que se consumen los mismos servicos para los catalogos
        if (urlType === 'particular' || urlType === 'valor-factura' ) {
            this.urlTypeService = urlType//TODO
            return this.http.get<CatalogosInterface[]>(environment.ENDCOTIZACION + '/' + aseguradora + '/year', { context: new HttpContext().set(tokenGo, true),params: { brand: marca } });
        } else { //privateDriver
            return this.http.get<CatalogosInterface[]>( `${environment.ENDPOINTAUTOSTESTV2}/${aseguradora}/private/year`, { params: { brand: marca } });
        }
    }

    getModelosComparador(marca: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTBASE + 'v1/comparador/modelos', { params: { marca: marca } });
    }

    /** */
    /**
     * Metodo para devolver el catalogo de descripciones de acuerdo a la aseguradora, marca y modelo
     * @param aseguradora
     * @param marca
     * @param modelo
     *
     */
    getDescripciones(aseguradora: string, marca: string, modelo: string, urlType: string): Observable<CatalogosInterface[]> {
        //se valida el urlType que sea igual a valor factura o particular ya que se consumen los mismos servicos para los catalogos
        if (urlType === 'particular'|| urlType === 'valor-factura') {
            //if(aseguradora.toUpperCase() === 'MIGO'){ aseguradora = "general_de_seguros"; }
                return this.http.get<CatalogosInterface[]>(environment.ENDCOTIZACION + '/' + aseguradora + '/model', {context: new HttpContext().set(tokenGo, true), params: { brand: marca, year: modelo } });
            } else { //privateDriver
            return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTESTV2 + '/' + aseguradora + '/private/model', { params: { brand: marca, year: modelo } });
        }
    }

    getSubmarcaComparador(marca: string, modelo: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTBASE + 'v1/comparador/submarcas', { params: { marca: marca, modelo: modelo } });
    }

    getSubdescripcionesComparador(marca: string, modelo: string, submarca: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/comparador/descripciones', { params: { marca: marca, modelo: modelo, submarca: submarca } });
    }


    getDetallesComparador(marca: string, modelo: string, submarca: string, descripcion: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTBASE + 'v1/comparador/detalles', { params: { marca: marca, modelo: modelo, submarca: submarca, descripcion: descripcion } });
    }

    /**
     * Metodo para devolver el catalogo de subdescripciones  de acuerdo a la aseguradora, marca, modelo y descripcion
     * @param aseguradora
     * @param marca
     * @param modelo
     * @param descripcion
     */
    getSubdescripciones(aseguradora: string, marca: string, modelo: string, submarca: string, urlType: string): Observable<CatalogosInterface[]> {
        //se valida el urlType que sea igual a valor factura o particular ya que se consumen los mismos servicos para los catalogos
        if (urlType === 'particular' || urlType === 'valor-factura') {

                return this.http.get<CatalogosInterface[]>(environment.ENDCOTIZACION + '/' + aseguradora + '/variant', {context: new HttpContext().set(tokenGo, true),  params: { brand: marca, year: modelo, model: submarca } });
        } else { //privateDriver
            return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTESTV2 + '/' + aseguradora + '/private/variant', { params: { brand: marca, year: modelo, model: submarca } });
        }
    }

    /**
     * Metodo para devolver el catalogo de subdescripciones  de acuerdo a la aseguradora, marca, modelo y descripcion
     * @param aseguradora
     * @param marca
     * @param modelo
     * @param descripcion
     */
    getDescripcionesComparador(marca: string, modelo: string, submarca: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTBASE + 'v1/comparador/descripciones', { params: { marca: marca, modelo: modelo, submarca: submarca } });
    }

    /**
     * Metodo para devolver el catalogo de detalles  de acuerdo a la aseguradora, marca, modelo, descripcion y subdescripcion
     * @param aseguradora
     * @param marca
     * @param modelo
     * @param descripcion
     * @param subdescripcion
     */
    getdetalles(aseguradora: string, marca: string, modelo: string, descripcion: string, subdescripcion: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/migo/detalles_aseguradora', { params: { aseguradora: aseguradora, marca: marca, modelo: modelo, descripcion: descripcion, subdescripcion: subdescripcion } });
    }

    cotizar(c: CotizacionRequestInterface, urlType: string): Observable<CotizacionResponseInterface> {
        
        if (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS' || c.aseguradora.toUpperCase() === 'LA_LATINO' || c.aseguradora.toUpperCase() === 'EL_POTOSI') {
            c.fechaNacimiento = moment(c.fechaNacimiento).format('YYYY-MM-DD');
            c.aseguradora = (c.aseguradora.toUpperCase() === 'LA_LATINO') ? 'LATINO' : (c.aseguradora.toUpperCase() === 'EL_POTOSI') ? 'ELPOTOSI' : (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS')?'GENERALDESEGUROS':c.aseguradora.toUpperCase();
        }       
        if (urlType === 'particular') {
            if(urlType === 'particular' && localStorage.getItem('tokenAs') === 'Aba'){
                return this.http.post<CotizacionResponseInterface>(environment.ENDCOTIZACION + '/' + 'aba' + '/quotation', c,{ context: new HttpContext().set(tokenGo, true)});
            }else{
                // return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/cotizacion', c);
                return this.http.post<CotizacionResponseInterface>('', c)
            }
            // return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/cotizacion', c);

        } else {
            if(c.aseguradora === 'AXA'){
                const partes = c.fechaNacimiento.split('-')
                const fechaTransformada = `${partes[2]}-${partes[1]}-${partes[0]}`
                c.fechaNacimiento = fechaTransformada
            }
            console.log('privado ------>',urlType);
            localStorage.setItem('servicio',urlType)
            c.servicio = 'APP'
            
            return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTESTV2 + '/' + c.aseguradora.toLowerCase() + '/private/quotation', c);
        }
    }
    cotizarValorFactura(c: CotizacionRequestInterface, urlType: string): Observable<CotizacionResponseInterface> {
        if (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS' || c.aseguradora.toUpperCase() === 'LA_LATINO' || c.aseguradora.toUpperCase() === 'EL_POTOSI' || c.aseguradora.toUpperCase() === 'AXA') {
            c.fechaNacimiento = moment(c.fechaNacimiento).format('YYYY-MM-DD');
            c.aseguradora = (c.aseguradora.toUpperCase() === 'LA_LATINO') ? 'LATINO' : (c.aseguradora.toUpperCase() === 'EL_POTOSI') ? 'ELPOTOSI' : (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS')?'GENERALDESEGUROS':c.aseguradora.toUpperCase();
        }
        return this.http.post<CotizacionResponseInterface>( environment.ENDPOINTAUTOSTEST +'/' +c.aseguradora.toLowerCase() + '/cotizaciones_vf', c);

    }

    cotizar3Ways(c: any): Observable<CotizacionResponseInterface> {

        if (c.aseguradora.toUpperCase() === 'MAPFRE' || c.aseguradora.toUpperCase() === 'ZURICH' || c.aseguradora.toUpperCase() === 'EL_POTOSI' || c.aseguradora.toUpperCase() === 'MIGO') {
            c.fechaNacimiento = moment(c.fechaNacimiento).format('YYYY-MM-DD');
        }
        if (c.aseguradora.toUpperCase() === 'AXA' || c.aseguradora.toUpperCase() === 'HDI') {//AQUÍ SE AGREGA DD/MM/YYYY PARA QUE COTICE AXA EN PROD
            c.fechaNacimiento = moment(c.fechaNacimiento).format('DD/MM/YYYY');
        }
        if (c.aseguradora.toUpperCase() === 'BANORTE' ) {
             c.fechaNacimiento = moment(c.fechaNacimiento).format('DD/MM/YYYY');
        }
        if (c.aseguradora.toUpperCase() === 'EL_POTOSI') {
            c.aseguradora = 'ELPOTOSI';
        }
        if (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS') {
            c.aseguradora = 'GENERALDESEGUROS';
        }
      
        //TODO
        /*Caso especial para que migo cotice como general de seguros*/
        let _insurance = (c.aseguradora.toLowerCase() === 'elpotosi') ? 'el_potosi' : (c.aseguradora.toLowerCase() === 'primero') ? 'primero_seguros' : (c.aseguradora.toLowerCase() === 'generaldeseguros') ? 'general_de_seguros' : c.aseguradora.toLowerCase();
        if(c.aseguradora ==='QUALITAS' ){

            c.tipoValor = "COMERCIAL"
            c.peridiocidadPago = "ANUAL"

            function cambiarFormatoFecha(fechaNacimiento) {
                // Dividir la cadena de fecha en partes
                const partes = fechaNacimiento.split('-');
              
                // Crear un objeto Date con las partes de la cadena
                const fecha = new Date(partes[2], partes[1] - 1, partes[0]);
              
                // Obtener los componentes de la fecha
                const dia = fecha.getDate().toString().padStart(2, '0');
                const mes = (fecha.getMonth() + 1).toString().padStart(2, '0'); // ¡Recuerda que los meses comienzan desde 0!
                const año = fecha.getFullYear();
              
                // Formar la nueva cadena de fecha en el formato dd/mm/aaaa
                const nuevoFormato = `${dia}/${mes}/${año}`;
              
                return nuevoFormato;
              }
              const  cambio = cambiarFormatoFecha(c.fechaNacimiento)
              c.fechaNacimiento = cambio
           
              c.submarca = ""

           
            return this.http.post<CotizacionResponseInterface>(environment.COTIZACIONEMICIONQUALITAS + '/' + _insurance + '/quotation', c,{ context: new HttpContext().set(tokenGo, true)});

        }else{

            return this.http.post<CotizacionResponseInterface>(environment.ENDCOTIZACION + '/' + _insurance + '/quotation', c,{ context: new HttpContext().set(tokenGo, true)});
        }
    }

    pagar(e: CotizacionResponseInterface): Observable<CotizacionResponseInterface> {
        if(localStorage.getItem('tokenAs')){
            return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/' +localStorage.getItem('tokenAs').toLowerCase()+ '/pagar', e);
        }else if(localStorage.getItem('tmpMultiSelected')){
            return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/' +localStorage.getItem('tmpMultiSelected').toLowerCase()+ '/pagar', e);
        }
        else {
            return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/pagar', e);
        }
    }

    emitir(e: CotizacionResponseInterface, _cotizacionSaved?: any): Observable<CotizacionResponseInterface> {
        let _send, _url: string = 'emitir';
        /*
        * NOTA:
        * Si la url cambia or aseguradora sera necesario agregar un swtch o agregar la configuracion desde los environments
        * */
        if(typeof e.Aseguradora !== 'undefined'){
            if(e.Aseguradora.toUpperCase() === 'QUALITAS'){_url = 'qualitas/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'MIGO'){_url = 'migo/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'GNP'){_url = 'gnp/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'ANA'){_url = 'ana/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'MAPFRE'){_url = 'mapfre/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'HDI'){_url = 'hdi/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'PRIMERO'){_url = 'primero_seguros/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'ABA'){_url = 'aba/emisiones'}
            if(e.Aseguradora.toUpperCase() === 'ABA'){_url = 'aba/emisiones'}

        }else{
            if(e.aseguradora.toUpperCase() === 'QUALITAS'){ _url = 'qualitas/emisiones'}
            if(e.aseguradora.toUpperCase() === 'MIGO'){ _url = 'migo/emisiones'}
            if(e.aseguradora.toUpperCase() === 'GNP'){ _url = 'gnp/emisiones'}
            if(e.aseguradora.toUpperCase() === 'ANA'){ _url = 'ana/emisiones'}
            if(e.aseguradora.toUpperCase() === 'MAPFRE'){ _url = 'mapfre/emisiones'}
            if(e.aseguradora.toUpperCase() === 'HDI'){ _url = 'hdi/emisiones'}
            if(e.aseguradora.toUpperCase() === 'PRIMERO'){ _url = 'primero_seguros/emisiones'}
            if(e.aseguradora.toUpperCase() === 'ABA'){_url = 'aba/emisiones'}
            if(e.aseguradora.toUpperCase() === 'AFIRME'){_url = 'afirme/emisiones'}
            if(e.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS'){_url = 'general_de_seguros/emisiones'}
        }


        if (_cotizacionSaved) {
            if (localStorage.getItem('tmpMultiSelected')) {
                _send = e;
            } else {
                let _insurance = JSON.parse(_cotizacionSaved.peticion).aseguradora.toUpperCase();
                //_send = this.makeJSON(e, _cotizacionSaved);
                _send = e;
            }
        } else {
            _send = e;
        }
        return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/' + _url, _send);
    }

    private makeJSON(e, _cotizacionSaved): any {
        let _tmp = JSON.parse(localStorage.getItem('tmpQuotation'))
        let _multiQuote = JSON.parse(localStorage.getItem('multiQuote')) || null
        let _multiInsurance = localStorage.getItem('tmpMultiSelected') || null
        let _return;

        if (_multiInsurance) {
            let _filter = _multiQuote.filter(el => el.aseguradora.toLowerCase() === _multiInsurance.toLocaleLowerCase())
            _return = {
                Aseguradora: e.Aseguradora,
                Cliente: e.Cliente,
                CodigoError: e.CodigoError,
                Descuento: e.Descuento,
                Paquete: "Amplia", // e.Paquete
                PeriodicidadDePago: e.PeriodicidadDePago,
                UrlRedireccion: e.UrlRedireccion,
                Vehiculo: e.Vehiculo,
                Coberturas: _tmp.Coberturas,
                Cotizacion: _tmp.Cotizacion,
                Emision: e.Emision,
                Pago: e.Pago,
                urlRedireccion: e.urlRedireccion
            }
        } else {
            _return = {
                Aseguradora: e.Aseguradora,
                Cliente: e.Cliente,
                CodigoError: e.CodigoError,
                Descuento: e.Descuento,
                Paquete: "Amplia", // e.Paquete
                PeriodicidadDePago: e.PeriodicidadDePago,
                UrlRedireccion: e.UrlRedireccion,
                Vehiculo: e.Vehiculo,
                Coberturas: _tmp.Coberturas,
                Cotizacion: _tmp.Cotizacion,
                Emision: _tmp.Emision,
                Pago: e.Pago
            }
        }
        return _return
    }

    emitir_Save(e: EmisionRequestInterface, idAgente: string, idCot: string, payForm:string): Observable<EmisionRequestInterface> {
        return this.http.post<EmisionRequestInterface>(environment.ENDPOINTAUTOSTEST + '/emitir/guardar/' + idAgente + '/' + idCot+'/'+payForm, e);
    }

    guardarPagoQualitas(e: EmisionRequestInterface, idAgente: string, idRegistro: string): Observable<EmisionRequestInterface> {
        return this.http.post<EmisionRequestInterface>(environment.ENDPOINTAUTOSTEST + '/emitir/guardar_pago/' + idAgente + '/' + idRegistro, e);
    }

    getColonias(cp: string): Observable<CodPostalInterface[]> {
        return this.http.get<CodPostalInterface[]>(environment.ENDPOINTAUTOSTEST + '/sepomex', { params: { cp: cp } });
    }
    getColoniasAfirme(cp: string): Observable<CodPostalInterface[]> {
        return this.http.get<CodPostalInterface[]>(environment.ENDPOINTBASE + 'v2/afirme/address', { params: { zipcode: cp } });
    }
    getDescuentoAfirme(ciudad:string): Observable<any>  {
        // return this.http.get<any>(environment.ENDPOINTAUTOSTEST + '/afirme/descuentos',{ params: {  estado: ciudad } });
        return this.http.get(`${environment.ENDCOTIZACION}/afirme/discount`,
            {  params: {  state: ciudad }, context: new HttpContext().set(tokenGo, true) });
    }
    getColoniasMigo(cp: string): Observable<CodPostalInterface[]> {
        return this.http.get<CodPostalInterface[]>(environment.ENDPOINTAUTOSTEST + '/migo/colonias', { params: { cp: cp } });
    }

    sendLogin(dataUser: any): Observable<any> {
        return this.http.post<any>(environment.apiUrlDataRoot + 'authenticate', dataUser);
    }

    guardarCotizacion(c: any): Observable<any> {
        return this.http.post<any>(environment.ENDPOINTAUTOSTEST + '/cotizacion/guardar', c);
    }

    guardarNewFormat(idQuote: number, c: any): Observable<any> {
        console.log('------------------>',idQuote,c);
        idQuote = parseInt(localStorage.getItem('id'))
        return this.http.put<any>(environment.ENDPOINTAUTOSTEST + '/cotizacion/guardar-comparador/' + idQuote, c)
    }

    getVersionesByAseguradoras(marca: string, modelo: string, submarca: string): Observable<CatalogosInterface[]> {
        return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/comparador/versiones_by_aseguradoras', { params: { marca: marca, modelo: modelo, submarca: submarca } });
    }

    getPlacasVheiculo(placas: string): Observable<any> {
        return this.http.get<any>(`https://core-persistance-service.com/v1/repuve`, {params: {placa: placas} ,context: new HttpContext().set(BYPASS_JW_TOKEN, true)});
        // return this.http.get<any>(`https://core-persistance-service.com`, {params: {placa: placas} ,context: new HttpContext().set(BYPASS_JW_TOKEN, true)});
    }

    getBancos() {
        return this.http.get('https://dev.core-goagentesdigitales.com/v2/mapfre/bank', {context: new HttpContext().set(tokenGo, true)})
    }

    getBancosAfirme() {
        return this.http.get(`https://ws-afirme.com/v1/afirme-car/bank`, {context: new HttpContext().set(tokenGo, true)})
    }

    getBancosGnp(){
            return this.http.get(`http://core-gnp-prod.eba-tskjpfym.us-east-1.elasticbeanstalk.com/datos_pago_gnp/get_bancos`, {context: new HttpContext().set(tokenGo, true)})
    }

    getBancosQualitas(): Observable<any>{
        return this.http.get(`https://ws-qualitas.com/catalogos/bancos`, {context: new HttpContext().set(tokenGo, true)})
    }

    gettipoTarjeta(){
        return this.http.get('https://dev.core-goagentesdigitales.com/v2/gnp/cardType', {context: new HttpContext().set(tokenGo, true)})
    }

    postPagoMapfre(body: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/mapfre/payment`, body, { context: new HttpContext().set(tokenGo, true)})
    }

    postEmisionMapfre(bodyEmision: any): Observable<any>{

        return this.http.post(`${environment.ENDCOTIZACION}/mapfre/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }

    postPagoQualitas(body: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/qualitas/payment`, body, { context: new HttpContext().set(tokenGo, true)})
    }

    postEmisionQualitas(bodyEmision: any): Observable<any>{
        debugger
        if (this.urlTypeService === 'particular' || this.urlTypeService === 'valor-factura' ) {
            
            return this.http.post(`${environment.COTIZACIONEMICIONQUALITAS}/qualitas/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
        } else { //privateDriver
            bodyEmision.servicio = 'APP'
            bodyEmision.vehiculo.servicio = 'APP'
            bodyEmision.vehiculo.uso = 'APP'

            return this.http.post(`${environment.COTIZACIONEMICIONQUALITAS}/qualitas/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
        }
        
    }
    //TODO
    postEmisionAxa(bodyEmision: any): Observable<any>{

      

        if (this.urlTypeService === 'particular' || this.urlTypeService === 'valor-factura' ) {
            
            return this.http.post(`${environment.COTIZACIONEMICIONQUALITAS}/axa/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
        } else { //privateDriver
            bodyEmision.servicio = 'APP'
            bodyEmision.vehiculo.servicio = 'APP'
            bodyEmision.vehiculo.uso = 'APP'

            return this.http.post(`${environment.COTIZACIONEMICIONQUALITAS}/axa/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
        }
        
    }

    // preguntar
    postEmisionGNP(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/gnp/issue-and-pay`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }

    postEmisionAfirme(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/afirme/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }

    postRecalculateBanorte(bodyEmision: any): Observable<any>{
        return this.http.post(`http://dev.core-banorte-prod.us-east-1.elasticbeanstalk.com/v1/banorte-car/recalculate`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }

    postRecalculateDebitoBanorte(): Observable<any>{
        return this.http.get(`${environment.ENDCOTIZACION}/banorte/paymethod`, { context: new HttpContext().set(tokenGo, true)})
    }
    postEmisionBanorte(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/banorte/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }



    postPagoAxa(body: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/axa/payment`, body, { context: new HttpContext().set(tokenGo, true)})
    }

    postEmisionAba(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/aba/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }
    postEmisionMigo(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/aba/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }
    postEmisionLatino(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/la_latino/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }
    postEmisionAig(bodyEmision: any): Observable<any>{
        return this.http.post(`${environment.ENDCOTIZACION}/aig/issue`, bodyEmision, { context: new HttpContext().set(tokenGo, true)})
    }


    getDescuentosGenerales(aseguradora): Observable<any>{
        return this.http.get<any>(`https://core-persistance-service.com/v1/discount/${aseguradora}?business=GO`,{context: new HttpContext().set(sinToken, true)});
    }

    validDataPago(idPolicy,idReceipt): Observable<any>{
        return this.http.get<any>(`https://dev.ws-afirme.com/v1/afirme-car/payment-validate`, { params: { idPolicy: idPolicy, idReceipt: idReceipt} })
    }

}
