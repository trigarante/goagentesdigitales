import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment';
import {RamoInterface} from "../_interfaces";
var axios = require("axios").default;


@Injectable({
  providedIn: 'root'
})
export class UserService {
  helper: any;

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  getInfo(): Observable<any> {
    let _id = localStorage.getItem('id') || '';
    return this.http.get<any>(environment.ENDPOINTAUTOSTEST + '/agente', { params: { id: _id } });
  }


  destroySession(): Observable<any> {
    let _id = localStorage.getItem('id') || '';

    return axios({
      method: "get",
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      url: environment.ENDPOINTBASE+  'destroy?idAgente='+_id,
    })
        .then(res => {console.log("destroy => "+res.data)})
        .catch(err => {console.log(err)});
  }

  isLoggedIn(): boolean {
    return (localStorage.getItem('token')) ? this.parseJwt(localStorage.getItem('token')) : false;
  }

  signOut(): any {
    let _rememberme = localStorage.getItem('remember');
    localStorage.clear();
    sessionStorage.clear();
    localStorage.setItem('remember', _rememberme);
    this.router.navigate(['/login']);
  }

  parseJwt(token: any): boolean {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    let _l = JSON.parse(jsonPayload);
    let diff = moment().diff(moment.unix(_l.exp).format("MM/DD/YYYY H:m:s"), 'm');

    return (diff >= 0) ? false : true;
  };
}

