import { Injectable } from '@angular/core';
import {HttpClient, HttpContext} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {tokenGo} from "../_interceptors";


@Injectable({
  providedIn: 'root'
})
export class OtrosRamosServicesService {

  constructor(private http:HttpClient) { }
  // SERVICIO PRINCIPLA DE GUARDAR
  /* guardardatosRamos(BodyRamo): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/cotizacion/guardar', BodyRamo,{ context: new HttpContext().set(tokenGo, true)});
  }*/
  guardarDataGmm(bodyGmm): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/gastos-medicos-mayores/save', bodyGmm)
  }
  guardarDatosVida(bodyVida): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/seguro-vida/save', bodyVida)
  }

  guardarDatosGrupal(bodyGrupal): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/seguro-grupal/save', bodyGrupal)
  }

  guardarDatosHogar(bodyHogar): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/seguro-hogar/save', bodyHogar)
  }

  guardarDatosViaje(bodyHogar): Observable<any>{
    return this.http.post(environment.OTROSRAM + '/seguro-viaje/save', bodyHogar)
  }

  verOtrosRamos(goId, paginaActual, resultadosPorPaginas, ramo){
    // return this.http.get('https://dev.core-goagentesdigitales.com/v2/agente/view-quotesOthers?goId=rruiz@ahorra.io&paginaActual=0&resultadosPorPaginas=2&ramo=86',
    return this.http.get(environment.OTROSRAMOSVIEW + '/agente/view-quotesOthers',
        {params: {goId: goId, paginaActual: paginaActual, resultadosPorPaginas:resultadosPorPaginas, ramo:ramo}})
  }

}
