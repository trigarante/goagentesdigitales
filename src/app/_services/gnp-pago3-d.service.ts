import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {CotizacionResponseInterface} from "../_interfaces";

@Injectable({
  providedIn: 'root'
})
export class GnpPago3DService {

  constructor(
      private http: HttpClient
  ) { }

  getBancos(): Observable<any> {
    return this.http.get<any>(environment.ENDPOINTBASE + 'v1/gnp/findbancos');
  }

  getTipoTargetaValida(): Observable<any> {
    return this.http.get<any>(environment.ENDPOINTBASE + 'v1/gnp/get_tipo_tarjeta_valida');
  }


  getPayPans(numTargeta: string): Observable<any> {
    // return this.http.get<any>(environment.ENDPOINTBASE + 'v1/gnp/obtener_planes_pago?numTarjeta='+numTargeta);
    return this.http.get<any>('https://web-gnp.mx/pago_gnp_3D/v2/validate-card', {params: {card:numTargeta}} );
  }

  formaDePagoGnp(bin): Observable<any>{
    return this.http.get<any>(`https://web-gnp.mx/pago_gnp_3D/v2/check-msi`, {params: {bin : bin}})
  }


  validateCard(request: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTBASE + 'v1/gnp/validacion_tarjeta', request);
  }


  emision_uat(request: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTBASE + 'v1/gnp/emisiones_full', request);
  }


  crear_id_pago(request: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTBASE + 'v1/gnp/crear_id_pago_3d', request);
  }


  obtener_emision(request: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTBASE + 'v1/gnp/obtener_emision', request);
  }


}
