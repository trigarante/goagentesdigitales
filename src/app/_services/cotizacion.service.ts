import { HttpClient, HttpContext, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { CatalogosInterface, CodPostalInterface, CotizacionRequestInterface, CotizacionResponseInterface, EmisionRequestInterface } from '../_interfaces';
import * as moment from 'moment';
import {AseguradorasOnOffService} from "./aseguradoras-on-off.service";
import { tokenGo } from '../_interceptors';

@Injectable({
  providedIn: 'root'
})
export class CotizacionService {
  form = {
    aseguradora: '',
    clave: '',
    cp: '',
    descripcion: '',
    detalle: '',
    descuento: 0,
    edad: '',
    fechaNacimiento: '',
      submarca: '',
    genero: '',
    marca: '',
    modelo: '',
    movimiento: 'cotizacion',
    paquete: 'AMPLIA',
    servicio: 'PARTICULAR'
  };
 AseguradoraDisbale
    insuranceCarriers: any[] = [];

    constructor(
    private http: HttpClient,
    private servicioAseguradora : AseguradorasOnOffService,
  ) {
        GENERALS.particual_quote.forEach(el => {
            if (el.private.has) {
                this.insuranceCarriers.push(el)
            }
        });
      this.servicioAseguradora.ViewEnableAseg(2201574).subscribe((dataAseg) => {
          this.AseguradoraDisbale = dataAseg
          for (let recorAseg = 0; recorAseg < GENERALS.particual_quote.length; recorAseg++) {
              if (GENERALS.particual_quote[recorAseg].insurance_carrier && this.AseguradoraDisbale[recorAseg].isEnabledGeneral === false) {
                  GENERALS.particual_quote[recorAseg].disabled = !this.AseguradoraDisbale[recorAseg].isEnabledGeneral;
              }
          }
      })

    }

  getMarcas(aseguradora: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/' + aseguradora + '/marcas');
  }

  getModelos(aseguradora: string, marca: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/' + aseguradora + '/modelos', { params: { marca: marca } });
  }

  getDescripciones(aseguradora: string, marca: string, modelo: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/' + aseguradora + '/submarcas', { params: { marca: marca, modelo: modelo } });
  }

  getSubdescripciones(aseguradora: string, marca: string, modelo: string, submarca: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/' + aseguradora + '/descripciones', { params: { marca: marca, modelo: modelo, submarca: submarca } });
  }

  getSubdescripcionesComparador(marca: string, modelo: string, submarca: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/comparador/descripciones', { params: { marca: marca, modelo: modelo, submarca: submarca } });
  }

  getdetalles(aseguradora: string, marca: string, modelo: string, descripcion: string, subdescripcion: string): Observable<CatalogosInterface[]> {
    return this.http.get<CatalogosInterface[]>(environment.ENDPOINTAUTOSTEST + '/migo/detalles_aseguradora', { params: { aseguradora: aseguradora, marca: marca, modelo: modelo, descripcion: descripcion } });
  }

  cotizar(c: any): Observable<CotizacionResponseInterface> {
    if(c.aseguradora === 'QUALITAS'){
      c.tipoValor = "COMERCIAL"
      c.peridiocidadPago = "ANUAL"

      function cambiarFormatoFecha(fechaNacimiento) {
          // Dividir la cadena de fecha en partes
          const partes = fechaNacimiento.split('-');
        
          // Crear un objeto Date con las partes de la cadena
          const fecha = new Date(partes[2], partes[1] - 1, partes[0]);
        
          // Obtener los componentes de la fecha
          const dia = fecha.getDate().toString().padStart(2, '0');
          const mes = (fecha.getMonth() + 1).toString().padStart(2, '0'); // ¡Recuerda que los meses comienzan desde 0!
          const año = fecha.getFullYear();
        
          // Formar la nueva cadena de fecha en el formato dd/mm/aaaa
          const nuevoFormato = `${dia}/${mes}/${año}`;
        
          return nuevoFormato;
        }
        const  cambio = cambiarFormatoFecha(c.fechaNacimiento)
        c.fechaNacimiento = cambio
     
        c.submarca = ""
    }
    if (c.aseguradora.toUpperCase() === 'GENERAL_DE_SEGUROS' || c.aseguradora.toUpperCase() === 'LA_LATINO' || c.aseguradora.toUpperCase() === 'EL_POTOSI' || c.aseguradora.toUpperCase() === 'AIG') {
      c.fechaNacimiento = moment(c.fechaNacimiento).format('YYYY-MM-DD');
      c.aseguradora = (c.aseguradora.toUpperCase() === 'LA_LATINO') ? 'LATINO' : (c.aseguradora.toUpperCase() === 'EL_POTOSI') ? 'ELPOTOSI' : (c.aseguradora.toUpperCase() === 'AIG') ? 'AIG' : 'GENERALDESEGUROS'
    }
  //(APAGAR COTIZACION) COMENTAR AQUÍ CON //*
    if(    c.aseguradora.toUpperCase() === 'MIGO'          && GENERALS.particual_quote[18].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'GNP'           && GENERALS.particual_quote[1].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'MAPFRE'        && GENERALS.particual_quote[17].disabled === false//ok
        || c.aseguradora.toUpperCase() === 'ANA'           && GENERALS.particual_quote[7].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'ABA'           && GENERALS.particual_quote[14].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'BANORTE'       && GENERALS.particual_quote[3].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'BXMAS'         && GENERALS.particual_quote[13].disabled === false//detalles en la cotizacion
        || c.aseguradora.toUpperCase() === 'HDI'           && GENERALS.particual_quote[5].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'ELPOTOSI'      && GENERALS.particual_quote[16].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'ZURICH'        && GENERALS.particual_quote[4].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'SURA'          && GENERALS.particual_quote[15].disabled === false// OK
        || c.aseguradora.toUpperCase() === 'LATINO'        && GENERALS.particual_quote[11].disabled === false// - OK
        || c.aseguradora.toUpperCase() === 'QUALITAS'      && GENERALS.particual_quote[0].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'ATLAS'         && GENERALS.particual_quote[8].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'AXA'           && GENERALS.particual_quote[2].disabled === false//- OK
        // || c.aseguradora.toUpperCase() === 'CRABI'         && GENERALS.particual_quote[0].disabled === false//- NO DEBE MOSTRARSE
        || c.aseguradora.toUpperCase() === 'GS'            && GENERALS.particual_quote[12].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'PRIMEROSEGUROS'&& GENERALS.particual_quote[19].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'AFIRME'        && GENERALS.particual_quote[9].disabled === false//- Detalles en la cotizacion
        || c.aseguradora.toUpperCase() === 'AIG'           && GENERALS.particual_quote[10].disabled === false//- OK
        || c.aseguradora.toUpperCase() === 'INBURSA'       && GENERALS.particual_quote[20].disabled === false//-
        || c.aseguradora.toUpperCase() === 'ELAGUILA'      && GENERALS.particual_quote[6].disabled === false//- Sin cotizacion aun (COMPARADOR)
        || c.aseguradora.toUpperCase() === 'SPT'           && GENERALS.particual_quote[21].disabled === false
    ){

    
      //return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/migo/cotizaciones', c);
      let _url = this.getUrlCotizacionComparador(c.aseguradora);
      return this.http.post<CotizacionResponseInterface>(_url , c);
    }else{
        // return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/cotizacion', c);
        return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/apagada', c);
     }

  }

  pagar(aseguradora: any, e: CotizacionResponseInterface): Observable<CotizacionResponseInterface> {
    return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/pagar', e);
  }

  saveFake(quotationID: number, c: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTAUTOSTEST + '/emitir/guardar-fake/' + quotationID, c);
  }

  printPolizaBanorte(cotId: any, poliza:any, rfc:any): Observable<any> {
    return this.http.post<any>(environment.ENDCOTIZACION +'/banorte/print?idCot='+cotId+'&poliza='+poliza+'&rfc='+rfc , { context: new HttpContext().set(tokenGo, true) });
  }

  savePago(body: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTAUTOSTEST + '/general/savePago', body);
  }

  updateFake(emisionID: number, c: any, idEstadoEmision: number):any {
    console.log('lase que semanda',c);
    if(c.aseguradora === 'MIGO'|| c.aseguradora === 'AIG'){
      localStorage.setItem('laC',JSON.stringify(c));
    }
    var servicio = localStorage.getItem("servicio");
    if(servicio !== 'privado'){
      return this.http.put<any>(environment.ENDPOINTBASE + 'v1/emitir/update-fake/' + emisionID+'/'+idEstadoEmision, c);
      // return this.http.put<any>(environment.ENDPOINTBASE + 'v1/emitir/' + emisionID+'/'+idEstadoEmision, c);
    }else{
      c.vehiculo.servicio = "APP"
      c.vehiculo.uso = "APP"
      return this.http.put<any>(environment.ENDPOINTBASE + 'v1/emitir/update-fake/' + emisionID+'/'+idEstadoEmision, c);
    }
  }

  emitir(e: CotizacionResponseInterface): Observable<CotizacionResponseInterface> {
    return this.http.post<CotizacionResponseInterface>(environment.ENDPOINTAUTOSTEST + '/emitir', e);
  }

  emitir_Save(e: EmisionRequestInterface, idAgente: number, idCot: number): Observable<EmisionRequestInterface> {
    return this.http.post<EmisionRequestInterface>(environment.ENDPOINTAUTOSTEST + '/emitir/guardar/' + idAgente + '/' + idCot, e);
  }

  getPoliza(quotationID: number): Observable<any> {
    return this.http.get<any>(environment.ENDPOINTAUTOSTEST + 'v1/gnp/gnp_impresiones', { params: { idPoliza: quotationID.toString() } })
  }

  getColonias(cp: string): Observable<CodPostalInterface[]> {
    return this.http.get<CodPostalInterface[]>(environment.ENDPOINTAUTOSTEST + '/sepomex', { params: { cp: cp } });
  }

  sendLogin(dataUser: any): Observable<any> {
    return this.http.post<any>(environment.apiUrlDataRoot + 'authenticate', dataUser);
  }

  checkPlaca(dataPlaca: any): Observable<any> {
    return this.http.get<any>(environment.apiUrlData + 'api_vehicular/search/' + dataPlaca);
  }

  guardarCotizacion(c: any): Observable<any> {
    return this.http.post<any>(environment.ENDPOINTAUTOSTEST + '/cotizacion/guardar', c);
  }

  async armarRequest(form: any, detalles: any): Promise<CotizacionRequestInterface[]> {
    const req: CotizacionRequestInterface[] = [];
    detalles.forEach((aseg: any) => {
        const r = Object.assign({}, this.form);
        r.paquete = 'AMPLIA';
        r.aseguradora = (typeof aseg.Aseguradora !== 'undefined') ? aseg.Aseguradora : aseg.aseguradora;
        r.clave = aseg.id;
        r.descripcion = form['selectDescripcion'].value;
        r.cp = form['cp'].value;
        r.marca = form['selectMarca'].value;
        r.modelo = form['selectModelo'].value;
        r.edad = form['edad'].value;
        r.genero = form['genero'].value;
        r.fechaNacimiento = '01/01/' + (new Date().getFullYear() - Number(form['edad'].value));
        if(r.aseguradora === 'ABA'){
          r.detalle = aseg.text
          req.push(r)
        }else if(r.aseguradora !== 'ABA'){
            r.detalle = aseg.text;

            req.push(r);
        }
    });
    localStorage.setItem('tmpMultAskQuotation', JSON.stringify(req));
    return req;
  }
//PARA QUE NO COTICE EN EL MULTICOTIZADOR: (APAGAR COTIZACION)
  // @ts-ignore
  getUrlCotizacionComparador(aseguradora: string): string {
    //debugger
      switch (aseguradora.toUpperCase()){
        case 'MIGO':
          return environment.ENDCOTIZACION + '/migo/quotation'
          break;
        case 'GNP':
          return environment.ENDCOTIZACION + '/gnp/quotation'
          break;
          //debugger
        case 'QUALITAS':
          return environment.COTIZACIONEMICIONQUALITAS + '/qualitas/quotation'
          break;
        case 'MAPFRE':
          return environment.ENDCOTIZACION + '/mapfre/quotation'
          break;
        case 'ANA':
          return environment.ENDCOTIZACION + '/ana/quotation'
          break;
        case 'ABA':
          return environment.ENDCOTIZACION + '/aba/quotation' //'/aba/quotation'
          break;
        case 'BANORTE':
          return environment.ENDCOTIZACION + '/banorte/quotation'
          break;
        case 'BXMAS':
          return environment.ENDCOTIZACION + '/bxmas/quotation'
          break;
        case 'LATINO':
          return environment.ENDCOTIZACION + '/la_latino/quotation'
          break;
        case 'HDI':
          return environment.ENDCOTIZACION + '/hdi/quotation'
          break;
          case 'ELPOTOSI':
           return environment.ENDCOTIZACION + '/el_potosi/quotation'
           break;
        case 'ATLAS':
          return environment.ENDCOTIZACION + '/atlas/quotation'
          break;
          case 'ZURICH':
            return environment.ENDCOTIZACION + '/zurich/quotation'
            break;
        case 'SURA':
          return environment.ENDCOTIZACION + '/sura/quotation'
          break;
        case 'ATLAS':
          return environment.ENDCOTIZACION + '/atlas/quotation'
          break;
        case 'AXA':
          return environment.ENDCOTIZACION + '/axa/quotation'
          break;
        case 'CRABI':
          return environment.ENDCOTIZACION + '/crabi/quotation'
          break;
        case 'GS':
          // return environment.ENDCOTIZACION + '/general_de_seguros/quotation'
          return environment.ENDCOTIZACION + '/general_de_seguros/quotation'
          break;
        case 'PRIMEROSEGUROS':
          return environment.ENDCOTIZACION + '/primero_seguros/quotation'
          break;

        case 'AIG':
          return environment.ENDCOTIZACION + '/aig/quotation'
          break;
        case 'AFIRME':
          // return environment.ENDCOTIZACION + '/afirme/quotation'
          return environment.ENDCOTIZACION + '/afirme/quotation'
          break;
        case 'ELAGUILA':
          return environment.ENDCOTIZACION + '/el_aguila/quotation'
          break;
        case 'SPT':
          return environment.ENDCOTIZACION + '/spt/quotation'
          break;

        default:
          return "cotizar"
          break;
      }
    // }
  }

}
