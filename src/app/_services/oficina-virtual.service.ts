import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {CatalogosInterface} from "../_interfaces";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class OficinaVirtualService {
  private _refresh$=new Subject<void>();
  constructor( private http:HttpClient) { }
  get refresh$(){
    return this._refresh$;
  }
  getMarcas(aseguradora: string): Observable<CatalogosInterface[]> {
    return this.http.get<any>(environment.ENDPOINTAUTOSTEST + '/' + aseguradora + '/marcas');
  }

  //SERVICO DE COBRANZA
  getCobranza(idAgente:number):Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/cobranza/portal/'+ idAgente)

  }
  //SERVICO SUBSECUENTE
  getSubsecuente(idAgente:number):Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/subsecuentes/portal/'+ idAgente)
  }
  //SERVICIO POR ID DE REGISTRO(TRAE LOS DE LOS PAGOS)
  getIdRegistro(idAgente:number):Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/emisiones/recibos/portal/by-id-registro/'+ idAgente)
  }

  //Servicio pólizas totales y prima neta por fecha
  getTotalesFecha(idAgente:number,fechaInicio:any, fechaFin:any):Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/reportes-portal/prima-total/get-by-fecha'+'/'+ idAgente+'/'+fechaInicio+'/'+fechaFin)
  }
  //Servicios mis resultados
  getTotalesPrima(idAgente:number):Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/reportes-portal/prima-total/'+ idAgente)
  }
  getMisResulTablaFiltro(idAgente:number, fechaInicio:any, fechaFinal:any){
    return this.http.get<any>(environment.endpoint_t2022 + '/reportes-portal/prima-total/' +idAgente+'/'+fechaInicio +'/'+fechaFinal);
  }
  //Servicio compensacion por id
  getCompensacionId(idAgente:number){
    return this.http.get<any>(environment.endpoint_t2022 + '/reportes-portal/compensacion/' +idAgente);
  }
  //Servicio compensacion por fecha
  getCompensacionFecha(idAgente:number, fechaInicio:any, fechaFinal:any){
    return this.http.get<any>(environment.endpoint_t2022 + '/reportes-portal/compensacion/' +idAgente+'/'+fechaInicio +'/'+fechaFinal);
  }

  //Servicio comprobante de pago
  getComprobantePago(idRegistro:number){
    return this.http.get<any>(environment.endpoint_t2022 + '/emisiones/registro/getreciboByPago/' + idRegistro);
  }
  //Servicio recibos de pago
  getRecibosPago(idRegistro:number){
    return this.http.get<any>(environment.endpoint_t2022 + '/finanzas/administracion-recibos/recibosByIdRegistro/' + idRegistro);
  }
  //Servicio mis leads
  getLeads(idAgente : string){ //TODO
    // return this.http.get<any>(environment.endpoint_t2022 + '/emision/solicitudesEmision/' + 2205662);
    return this.http.get<any>(environment.endpoint_t2022 + '/emision/solicitudesEmisionOrigen/' + idAgente);
    // return this.http.get<any>('https://go-dev-core.trigarante2022.com/emision/solicitudesEmision/' + idAgente);
  }
  //Servicio detalles leads
  getLeadsDetalles(idEmision:number){
    return this.http.get<any>(environment.endpoint_t2022 + '/emision/emisionFake/' + idEmision);
  }
  //Servicio detalles leads
  getLeadsDetallesCotizacion(idCotizacion:number){
    return this.http.get<any>(environment.endpoint_t2022 + '/emision/byIdCotizacion/' + idCotizacion);
  }
  //Servicio saveLog
  guardarLog(log: any): Observable<any> {
    return this.http.post<any>(environment.endpoint_t2022 + '/cobranza/saveLog',log );
  }
  //Servicio imagen poliza- sube la imagen y genera la url
  poliza(idRegistro: any, formData): Observable<any> {
    return this.http.post<any>(environment.endpoint_t2022 + '/driveApi/uploadPoliza/'+ idRegistro ,formData)
  }
  //Servicio imagen documentos  sube la imagen y genera la url
  art492(idCliente: any, formData): Observable<any> {
    return this.http.post<any>(environment.endpoint_t2022 + '/driveApi/uploadCliente/'+ idCliente,formData )
  }
  //Servicio imagen documentos  sube la imagen y genera la url
  recibo(idRecibo: number,numero:number, formData): Observable<any> {
    return this.http.post<any>(environment.endpoint_t2022 +  '/files/uploadRecibo/'+idRecibo + '/' + numero ,formData )
  }
  //Servicio actualizar imagen poliza
  updateFile(idRegistro: number, data: any, ): Observable<any> {
    return this.http.put<any>(environment.endpoint_t2022 + '/emisiones/registro/archivo/'+idRegistro, data)
        .pipe(
            tap(()=>{
              this._refresh$.next();
            })
        )
  }
  updateCliente(idCliente: number, data: any, ): Observable<any> {
    return this.http.put<any>(environment.endpoint_t2022 + '/clientes/archivo/'+idCliente, data)
        .pipe(
            tap(()=>{
              this._refresh$.next();
            })
        )
  }
  updateRecibo(idRecibo: number, data: any, ): Observable<any> {
        return this.http.put<any>(environment.endpoint_t2022 + '/recibos/archivo/'+idRecibo, data)
        .pipe(
            tap(()=>{
              this._refresh$.next();
            })
        )
  }
  //Servicio actualiza el estado venta
  estadoVenta(idEmision: any, data): Observable<any> {
    return this.http.post<any>(environment.endpoint_t2022 + '/emision/statusSeguimiento/'+ idEmision ,data )
        .pipe(
            tap(()=>{
              this._refresh$.next();
            })
        )
  }
}
