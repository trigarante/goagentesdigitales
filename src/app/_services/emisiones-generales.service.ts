import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EmisionesGeneralesService {

  constructor(private http: HttpClient) { }


  vistaTablaEmisiones(goId, paginaActual, resultadosPorPaginas): Observable<any>{
   // return this.http.get(environment.OTROSRAMOSVIEW + '/agente/view-issued-policies?goId=2201574&paginaActual=2&resultadosPorPaginas=50',
    return this.http.get<any>(environment.OTROSRAMOSVIEW + '/agente/view-issued-policies',
        {params:{goId: goId, paginaActual: paginaActual, resultadosPorPaginas: resultadosPorPaginas}})
  }
}
