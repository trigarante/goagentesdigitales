import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CapacitacionService {

  constructor(private http:HttpClient) { }
  //Servicio imagenes cobranzaq
  getImgCobranza():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/presentaciones-e-instructivos/cobranza-go')
  }
  //Servicio imagenes portalGO
  getImgPortalGo():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/presentaciones-e-instructivos/portal-go')
  }

  //Servicio imagenes metodo de pago
  getImgMetodoPago():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/presentaciones-e-instructivos/metodo-de-pago')
  }
  //Servicio materiales para redes
  getMaterialesRedes():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/publicidad-go/materiales-para-redes')
  }
  //Servicio producto
  getProductos():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/aseguradoras/producto')
  }
  //Servicio formatos y condiciones
  getFormatosYCondiciones():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/aseguradoras/formatos-y-condiciones')
  }
  //Argumentos de Venta
  getArgumentosVentas():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/presentaciones-e-instructivos/argumento-de-venta')
  }
  //Clientes
  getClientes():Observable<any>{
    return this.http.get<any>( environment.endpoint_t2022 + '/capacitacion/presentaciones-e-instructivos/cliente')
  }
  //Vistual Card
  getVirtualCard(id:any):Observable<any>{
    return this.http.get<any>(environment.endpoint_t2022 + '/rrhh/agentes/get-by-go-id-concesion/'+ id)
  }


}
