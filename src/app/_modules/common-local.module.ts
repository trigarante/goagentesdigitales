import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MaterialModule } from './material.module';

import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { NgOverlayContainerModule } from 'ng-overlay-container';

import { AlertComponent, FooterComponent, GenericPayComponent, HeaderComponent, LoaderComponent, MessagesComponent, ResumePayComponent, SidebarleftComponent, SubirArchivoRegistroComponentComponent, TableInfoQuotationComponent } from '../_components/generals';

import { AuthBarrierInterceptor } from '../_interceptors';
import { AesEDecodeService, CatalogosService, DecodeJWTService, T2020Service } from '../_services';

import { ShowDeducibleComparadorPipe, ShowSumaAseguradaPipe,ShowDeduciblePipe, ShowNamePipe, ShowSumaPipe, ShowSumaRcPipe, CardHidePipe, DiscountInsurancePipe, DisplayInsuranceImagePipe, SafePipe, RemoveMimePipe } from '../_pipes';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
// import { ModalDialogModule } from '../_components/generals/modal-dialog/modal-dialog.module';

// import { AuthInterceptorService, TranslateComponentService, ModalActionsService } from '../_helpers';

const dbConfig: DBConfig = {
  name: 'goagentes',
  version: 1,
  objectStoresMeta: [
    // {
    //   store: 'brandMulty',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
    // {
    //   store: 'modelMulty',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'brand', keypath: 'brand', options: { unique: false } },
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
    // {
    //   store: 'subBrandMulty',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'brand', keypath: 'brand', options: { unique: false } },
    //     { name: 'model', keypath: 'model', options: { unique: false } },
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
    // // {
    // //   store: 'descriptionMulty',
    // //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    // //   storeSchema: [
    // //     { name: 'brand', keypath: 'brand', options: { unique: false } },
    // //     { name: 'model', keypath: 'model', options: { unique: false } },
    // //     { name: 'subBrand', keypath: 'subBrand', options: { unique: false } },
    // //     { name: 'text', keypath: 'text', options: { unique: false } },
    // //     { name: 'id', keypath: 'id', options: { unique: false } },
    // //   ]
    // // },
    // {
    //   store: 'brandQualitas',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
    // {
    //   store: 'modelQualitas',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'brand', keypath: 'brand', options: { unique: false } },
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
    // {
    //   store: 'subBrandQualitas',
    //   storeConfig: { keyPath: 'localID', autoIncrement: true },
    //   storeSchema: [
    //     { name: 'brand', keypath: 'brand', options: { unique: false } },
    //     { name: 'model', keypath: 'model', options: { unique: false } },
    //     { name: 'text', keypath: 'text', options: { unique: false } },
    //     { name: 'id', keypath: 'id', options: { unique: false } },
    //   ]
    // },
  ]
};

@NgModule({
    declarations: [
        // AlertComponent
        HeaderComponent,
        SidebarleftComponent,
        FooterComponent,
        GenericPayComponent,
        SubirArchivoRegistroComponentComponent,
        ResumePayComponent,
        MessagesComponent,
        LoaderComponent,
        TableInfoQuotationComponent,
        ShowDeduciblePipe,
        ShowSumaPipe,
        ShowNamePipe,
        ShowSumaRcPipe,
        CardHidePipe,
        DiscountInsurancePipe,
        DisplayInsuranceImagePipe,
        SafePipe,
        RemoveMimePipe,
        ShowSumaAseguradaPipe,
        ShowDeducibleComparadorPipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        HttpClientModule,
        RouterModule,
        NgxExtendedPdfViewerModule,
        ClipboardModule,
        CdkStepperModule,
        CdkTableModule,
        CdkTreeModule,
        DragDropModule,
        PortalModule,
        ScrollingModule,
        ShowHidePasswordModule,
        // ModalDialogModule,
        NgxIndexedDBModule.forRoot(dbConfig),
        YouTubePlayerModule,
        NgOverlayContainerModule
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        HttpClientModule,
        RouterModule,
        NgxExtendedPdfViewerModule,
        ClipboardModule,
        CdkStepperModule,
        CdkTableModule,
        CdkTreeModule,
        DragDropModule,
        PortalModule,
        ScrollingModule,
        MessagesComponent,
        ShowHidePasswordModule,
        LoaderComponent,
        // ModalDialogModule,
        // NgxIndexedDBModule.forRoot(dbConfig),
        YouTubePlayerModule,
        NgOverlayContainerModule,
        // AlertComponent,
        HeaderComponent,
        SidebarleftComponent,
        FooterComponent,
        GenericPayComponent,
        SubirArchivoRegistroComponentComponent,
        ResumePayComponent,
        TableInfoQuotationComponent,
        ShowDeduciblePipe,
        ShowSumaPipe,
        ShowSumaAseguradaPipe,
        ShowDeducibleComparadorPipe,
        ShowNamePipe,
        ShowSumaRcPipe,
        CardHidePipe,
        DiscountInsurancePipe,
        DisplayInsuranceImagePipe,
        SafePipe,
        RemoveMimePipe
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthBarrierInterceptor,
            multi: true
        },
        DecodeJWTService,
        CatalogosService,
        AesEDecodeService,
        CurrencyPipe,
        T2020Service
    ]
})
export class CommonLocalModule { }
