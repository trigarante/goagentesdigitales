import { TestBed } from '@angular/core/testing';

import { AuthBarrierInterceptor } from './auth-barrier.interceptor';

describe('AuthBarrierInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      AuthBarrierInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: AuthBarrierInterceptor = TestBed.inject(AuthBarrierInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
