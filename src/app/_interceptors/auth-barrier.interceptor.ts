import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient, HttpContextToken, HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import {LoginService} from "../_services";

export const BYPASS_JW_TOKEN = new HttpContextToken(() => false);
export const tokenGo = new HttpContextToken(() => false);
export const sinToken = new HttpContextToken(() => false);

@Injectable()
export class AuthBarrierInterceptor implements HttpInterceptor {

  constructor(private loginService: LoginService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = localStorage.getItem('token') || null;
    let tokenBranding = localStorage.getItem('tokenBranding') || null;
    let _request = request.url.split('/');

    if (token != null) {
      let _token = (request.url.includes('branding')) ? tokenBranding : token;
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${_token}`
        }
      });
    }
    // if (request.context.get(BYPASS_JW_TOKEN) === true) {
    //   let tokemPlacas = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiZ3VhZGFycmFtYUBhaG9ycmEuaW86NCIsImV4cCI6MjY1MjMyNTczMCwiaWF0IjoxNjUyMzI1NzMxfQ.qCgfLs148JNQdCde8fF_i0Hpj6xzbrosK_pEzx8P0rUFEQXSTjezY8NIHObkzGnJTXhsVJW-901awDLRW6zwOA'
    //   const headers = new HttpHeaders().append('Authorization', 'Bearer ' + tokemPlacas);
    //   const reqClone = request.clone({headers})
    //   return next.handle(reqClone)
    // }
    if (request.context.get(BYPASS_JW_TOKEN) === true) {
        let data = localStorage.getItem('plToken')
        const headers = new HttpHeaders().append('Authorization', 'Bearer ' + data);
        const reqClone = request.clone({headers})
        return next.handle(reqClone)
    }
    if (request.context.get(sinToken) === true) {
      let sinTokenDes = ''
      const headers = new HttpHeaders().append('Authorization', 'Bearer ' + sinTokenDes);
      const reqClone = request.clone({headers})
      return next.handle(reqClone)
    }
    if (request.context.get(tokenGo) === true) {
      this.loginService.disparadorDeTokenGo.subscribe( data => {
        const headers = new HttpHeaders().append('Authorization', 'Bearer ' + data);
        const reqClone = request.clone({headers})
        return next.handle(reqClone)
      })
    }

    return next.handle(request);
  }
}
