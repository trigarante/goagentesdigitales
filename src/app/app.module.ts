import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonLocalModule } from './_modules/common-local.module';

import { LoginComponent } from './_components/public';
import { AlertComponent } from './_components/generals';
import {ForgotPasswordComponent} from './_components/public'

import { LoginService } from './_services';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthBarrierInterceptor } from './_interceptors';
import {QRCodeModule} from "angularx-qrcode";
import {BusinessCardsComponent} from "./_components/oficinavirtual/business-cards/business-cards.component";
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PayMapfreComponent } from './_components/generals/generic-pay/pay-mapfre/pay-mapfre.component';
import { PayQualitasComponent } from './_components/generals/generic-pay/pay-qualitas/pay-qualitas.component';
import { PayGnpComponent } from './_components/generals/generic-pay/pay-gnp/pay-gnp.component';
import { GraciasPagoComponent } from './_components/generals/generic-pay/gracias-pago/gracias-pago.component';
import { PayBanorteComponent } from './_components/generals/generic-pay/pay-banorte/pay-banorte.component';
import { PayAfirmeComponent } from './_components/generals/generic-pay/pay-afirme/pay-afirme.component';
import { PaySuraComponent } from './_components/generals/generic-pay/pay-sura/pay-sura.component';
import { PayAxaComponent } from './_components/generals/generic-pay/pay-axa/pay-axa.component';
import { PayAbaComponent } from './_components/generals/generic-pay/pay-aba/pay-aba.component';
import { OtrosRamosComponent } from './_components/otros-ramos/otros-ramos.component';
import { AseguradorasOnOffComponent } from './_components/aseguradoras-on-off/aseguradoras-on-off.component';
import { EmisionesGeneralesComponent } from './_components/emisiones-generales/emisiones-generales.component';
import { init as initApm } from "@elastic/apm-rum";
initApm({
    // serviceName: "Search UI Sandbox",
    // serverUrl: "https://68a4e94cc8b640e6a77b3da09dd7df30.apm.us-central1.gcp.cloud.es.io:443",
    serviceName: "goagentesdigitalesMxDevelopS3WebsiteUsEast1amazonawsCom",
    serverUrl: "https://trackingfront.com",
    // Set the service version (required for source map feature)
    serviceVersion: "",
    environment: process.env.NODE_ENV
});
@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        AlertComponent,
        ForgotPasswordComponent,
        BusinessCardsComponent,
        PayMapfreComponent,
        PayQualitasComponent,
        PayGnpComponent,
        GraciasPagoComponent,
        PayBanorteComponent,
        PayAfirmeComponent,
        PaySuraComponent,
        PayAxaComponent,
        PayAbaComponent,
        OtrosRamosComponent,
        AseguradorasOnOffComponent,
        EmisionesGeneralesComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        CommonLocalModule,
        NgbModule,
        QRCodeModule,
        PdfViewerModule,
        // <- Add PdfViewerModule to imports
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthBarrierInterceptor,
            multi: true
        },
        LoginService
    ],
    exports: [

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
