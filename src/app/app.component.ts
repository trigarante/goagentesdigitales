import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { T2020Service } from './_services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'goagentesdigitales';

  constructor(
    private route: ActivatedRoute,
    private location: LocationStrategy,
    private t2020Service: T2020Service
  ) {
    // history.pushState(null, null, window.location.href);
    // this.location.onPopState(() => {
    //   history.pushState(null, null, window.location.href);
    // });

    // window.addEventListener("beforeunload", function (e) {
    //   var confirmationMessage = "\o/";
    // console.log('close event: 1');
    //   (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    //   console.log('close event:', e);
    //   return confirmationMessage;                            //Webkit, Safari, Chrome
    // });

    this.route.params.subscribe(params => {
    });
  }

  ngOnInit() {
    // this._getT2020Areas();
  }
  src = 'https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf';

  private _getT2020Areas(): void {
    this.t2020Service.getAreas().subscribe(
      succ => {
        this.t2020Service.storeAreasObject(succ);
      },
      err => {
        console.error(err.messageError);
      }
    );
  }
}
