import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showDeducibleComparador'
})
export class ShowDeducibleComparadorPipe implements PipeTransform {

  transform(value: any , ...args: any[]): any {
    let deducible: any='';
    deducible = value;
    var regExp = /[123456789]/;
    var result = regExp.exec(deducible);
    if (deducible === 0 || deducible === "" || deducible == "0%"|| deducible == "0" || deducible === 0.0 || deducible === "0.0") {
      deducible = "NO APLICA";
    } else if (deducible.includes("$") == true) {
      if (deducible.includes(".0") == true) {
        if (deducible.includes(".00") == false) {
          deducible = deducible + "0";
        }
      } else {
        deducible = deducible + ".00";
      }
    } else if (result != null) {
      //tiene numeros 1-9
        if(deducible.includes("%") == false && deducible.length>2){
            // deducible= '$' + deducible;
            deducible=this.currency(deducible);

        }
      if (deducible.includes("%") == false && deducible.length<=2) {
        deducible = deducible + " %";
      }
    }
    return deducible;
  }
  currency(number){
        return new Intl.NumberFormat('en-US', {style: 'currency',currency: 'USD', minimumFractionDigits: 2}).format(number);
    };
}
