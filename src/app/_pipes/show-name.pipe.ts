import { Pipe, PipeTransform } from '@angular/core';
import { ToLowerCaseKeyService } from '../_services';

@Pipe({
  name: 'showName'
})
export class ShowNamePipe implements PipeTransform {

  constructor(
    private toLowerCaseKeyService: ToLowerCaseKeyService
  ) { }

  transform(value: any, type?: string) {
    if (!value) return;
    if (type) {
      this.toLowerCaseKeyService.parseToLower(value);
      if(value.nombre.charAt(0) === ' '){
         value.nombre = value.nombre.toString().substring(2,value.nombre.substring().length);
      }

      return value.nombre.charAt(0).toUpperCase() + value.nombre.slice(1);
    } else {
      return value.substring(value.lastIndexOf('-N') + 2, value.lastIndexOf('-S'));
    }
  }

}
