import { Pipe, PipeTransform } from '@angular/core';
import { environment as GENERALS } from 'src/environments/environment.generals';
import { DiscountByInsuranceInterface } from '../_interfaces';
import * as moment from 'moment';

@Pipe({
  name: 'discountInsurance'
})
export class DiscountInsurancePipe implements PipeTransform {
  // descuentos: DiscountByInsuranceInterface[] = GENERALS.discount;
  descuentos;

  transform(value: any, descuentoAseguradora: number, ...args: any[]): any {

    console.log(descuentoAseguradora);
    this.descuentos = descuentoAseguradora
    if (!value) return;
    let _isSpecial, _special;
    // if (!this.isNumber(value)) {
    //   _special = (this.descuentos.find(el => el.insurance === value.toUpperCase()).special.length > 0) ? this.descuentos.find(el => el.insurance === value.toUpperCase()).special[0] : null;
    //   if (_special) {
    //     let _end = moment(_special.end).add(1, 'days')
    //     _isSpecial = (moment().isBetween(_special.start, _end)) ? true : false;
    //   }
    // } else {
    //   _special = (this.descuentos.find(el => el.insurance === args[1].toUpperCase()).special.length > 0) ? this.descuentos.find(el => el.insurance === args[1].toUpperCase()).special[0] : null;
    //   if (_special) {
    //     let _end = moment(_special.end).add(1, 'days')
    //     _isSpecial = (moment().isBetween(_special.start, _end)) ? true : false;
    //   }
    // }
    let _return: any;
    if(value === 'GS'){ value = 'general_De_Seguros'; }else if(value === 'PRIMEROSEGUROS'){ value = 'PRIMERO';}
    switch (args[0]) {
      case 1:
        // _return = (_isSpecial) ? _special.discount : this.descuentos.find(el => el.insurance === value.toUpperCase()).discount;
        value = (value === 'general_De_Seguros') ? 'general' : (value === 'la_latino') ? 'latino' : value;
        // _return = this.descuentos.find(el => el.insurance === value.split('_').join('').toUpperCase()).discount;
        _return = this.descuentos.discount
        break;
      case 2:
        // let _i = 100 - (_isSpecial) ? _special.discount : this.descuentos.find(el => el.insurance === args[1].toString().toUpperCase()).discount;
        args[1] = (args[1] === 'GS') ? 'general' : (args[1] === 'la_latino') ? 'latino' : (args[1] === 'PRIMEROSEGUROS') ? 'PRIMERO' : args[1];
        // let _i = 100 - this.descuentos.find(el => el.insurance === args[1].split('_').join('').toString().toUpperCase()).discount;
        let _i = 100 - this.descuentos.discount
        console.log('this.descuentos.discount', this.descuentos.discount)
        _return = (_i === 100) ? value : Number(value) / Number((_i < 10) ? ('0.0' + _i) : ('0.' + _i));
        break;
      case 3:
        // _return = (_isSpecial) ? _special.msi : this.descuentos.find(el => el.insurance === value.toUpperCase()).msi;
        value = (value === 'general_De_Seguros') ? 'general' : (value === 'la_latino') ? 'latino' : value;
        // _return = this.descuentos.find(el => el.insurance === value.split('_').join('').toUpperCase()).msi;
        _return = this.descuentos.msi;
        break;
      case 4:
        // _return = (_isSpecial) ? _special.discount : this.descuentos.find(el => el.insurance === value.toUpperCase()).discount;
        value = (value === 'general_De_Seguros') ? 'general' : (value === 'la_latino') ? 'latino' : value;
        // _return = this.descuentos.find(el => el.insurance.toUpperCase() === value.split('_').join('').toUpperCase()).discount;
        _return = this.descuentos.discount;
        _return = (_return == 0) ? false : true;
        break;
    }
    return _return;
  }

  private isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); } 
}
