import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showSumaAsegurada'
})
export class ShowSumaAseguradaPipe implements PipeTransform {

  transform(value:any, ...args: any[]): any {
    let sumaAseg:any = '';
    let regExp =  /^[0-9]*$/;
    var regExp2 = /[a-zA-Z]/g;
    let result = regExp.test(sumaAseg);
    let result2= regExp2.test(sumaAseg);
    sumaAseg = value;
    if (sumaAseg === 0 || sumaAseg === ""|| sumaAseg === "0") {
      sumaAseg = "NO APLICA";
    } else if (sumaAseg === 1 || sumaAseg ==='1') {
      sumaAseg = "AMPARADA";
    } else if (!isNaN(sumaAseg)) {
      if(sumaAseg.length <=2){
        sumaAseg=sumaAseg + '%';
      }else{
        sumaAseg=this.currency(sumaAseg);
      }

      //tiene numeros 1-9


      // var result2 = regExp2.exec(sumaAseg);


    }
    return sumaAseg;
  }
  // transform(value:any, ...args: any[]): any {
  //   let sumaAseg:any = '';
  //   let regExp =  /^[0-9]*$/;
  //   let result = regExp.exec(sumaAseg);
  //   sumaAseg = value;
  //   if (sumaAseg === 0 || sumaAseg === ""|| sumaAseg === "0") {
  //     sumaAseg = "NO APLICA";
  //   } else if (sumaAseg === 1 || sumaAseg ==='1') {
  //     sumaAseg = "AMPARADA";
  //   } else if (result != null) {
  //
  //     //tiene numeros 1-9
  //     var regExp2 = /[a-zA-Z]/g;
  //
  //     var result2 = regExp2.exec(sumaAseg);
  //
  //     if (sumaAseg.includes("$") == false) {
  //       if (result2 != null) {
  //         //V. COMERCIAL50000
  //         sumaAseg =
  //             sumaAseg.substring(0, result.index) +
  //             " $ " +
  //             sumaAseg.substring(result.index, sumaAseg.length);
  //       } else {
  //         sumaAseg = "$ " + sumaAseg;
  //       }
  //     }
  //     if (sumaAseg.includes(".0") == true) {
  //       if (sumaAseg.includes(".00") == false) {
  //         sumaAseg = sumaAseg + "0";
  //       }
  //     } else {
  //       sumaAseg = sumaAseg + ".00";
  //     }
  //     //Comas version2
  //     if (!sumaAseg.includes(",")) {
  //       var result3 = regExp.exec(sumaAseg);
  //       var inicio = result3.index;
  //       var regExp3 = /\.00/;
  //       var result4 = regExp3.exec(sumaAseg);
  //       var fin = result4.index;
  //       var n = (fin - inicio) / 3;
  //       if (n > 1 && n <= 2) {
  //         //1,000->100,000
  //         var z = sumaAseg.substring(fin - 3, fin);
  //         var y = sumaAseg.substring(inicio, fin - 3);
  //         var x = sumaAseg.substring(0, inicio);
  //         var zz = sumaAseg.substring(fin, sumaAseg.length);
  //         sumaAseg = x + y + "," + z + zz;
  //       } else if (n > 2 && n <= 3) {
  //         //1,000,000->100,000,000
  //         var z = sumaAseg.substring(fin - 3, fin);
  //         var z2 = sumaAseg.substring(fin - 6, fin - 3);
  //         var y2 = sumaAseg.substring(inicio, fin - 6);
  //         var x = sumaAseg.substring(0, inicio);
  //         var zz = sumaAseg.substring(fin, sumaAseg.length);
  //         sumaAseg = x + y2 + "," + z2 + "," + z + zz;
  //       }
  //     }
  //   }
  //   return sumaAseg;
  // }
  currency(number){
    return new Intl.NumberFormat('en-US', {style: 'currency',currency: 'USD', minimumFractionDigits: 0}).format(number);
  };
  getNumbersInString(string) {
    const tmp = string.split("");
    const map = tmp.map(function(current) {
      if (!isNaN(parseInt(current))) {
        return current;
      }
    });

    const numbers = map.filter(function(value) {
      return value != undefined;
    });

    return numbers.join("");
  }
}
