export * from './show-deducible.pipe';
export * from './show-suma.pipe';
export * from './show-name.pipe';
export * from './show-suma-rc.pipe';
export * from './card-hide.pipe';
export * from './discount-insurance.pipe';
export * from './display-insurance-image.pipe';
export * from './safe.pipe';
export * from './remove-mime.pipe';
export * from './show-suma-asegurada.pipe';
export * from './show-deducible-comparador.pipe';
