import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cardHide'
})
export class CardHidePipe implements PipeTransform {

  transform(value: string): string {
    // let _card = value.substring(12,16);
    return '****-****-****-' + value.substring(12,16);
  }

}
