import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeMime'
})
export class RemoveMimePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    value = value.replace(new RegExp('^[0-9{1,}]- '), '');
    return value.replace('_', ' ').split('.').slice(0, -1).join('.');
  }

}
