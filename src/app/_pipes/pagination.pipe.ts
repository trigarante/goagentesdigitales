import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  transform(data:any,page:number,search: string): any[] {
    if (search.length === 0)
      return data.slice(page, page + 5);
    const filteredData = data.filter((data: any) => data.poliza.includes(search));
    return filteredData.slice(page, page + 5);
  }

}
