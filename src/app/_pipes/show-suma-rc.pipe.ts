import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { ToLowerCaseKeyService } from '../_services';
import {environment as GENERALS} from "../../environments/environment.generals";

@Pipe({
    name: 'showSumaRc'
})
export class ShowSumaRcPipe implements PipeTransform {
    numero: RegExp = GENERALS.regex_onlyNumber;
    numLet: RegExp = GENERALS.regex_numLetters;
    numSig: RegExp = GENERALS.regex_numSig;

    constructor(
        private currencyPipe: CurrencyPipe,
        private toLowerCaseKeyService: ToLowerCaseKeyService
    ) { }

    transform(value: any, type?: string) {
        // const currencyPipe = new CurrencyPipe(null);
        if (!value) return;

        if (type) {
            if (type === 'multy') {
                if (typeof value.sumaAsegurada) {
                    let regExpNum = this.numero;
                    let valiNum = regExpNum.exec(value.sumaAsegurada);//valida que sean solo numeros
                    // console.log('valiNum',valiNum);
                    if(valiNum!= undefined){
                        return ((parseInt(valiNum[0]) === 0 ) ? 'NO APLICA' : parseInt(valiNum[0]) > 0 ? this.currencyPipe.transform(valiNum[0], 'USD', 'symbol', '1.0-0') : value.sumaAsegurada);

                    }else {
                        return (Number(value.sumaAsegurada) === 0) ? 'NO APLICA' : value.sumaAsegurada;
                    }


                }
                if (typeof value.sumaAsegurada) {
                    let regExpNum = this.numero;
                    let regExpNumLett = this.numLet;
                    let regExpNumSig = this.numSig;
                    let valiNum = regExpNum.test(value.sumaAsegurada);//valida que sean solo numeros
                    let valiNumLet =regExpNumLett.test(value.sumaAsegurada);//valida que sean numeros y letras
                    let valiNumSig =regExpNumSig.test(value.sumaAsegurada);//valida que sean numeros y signos
                    if (valiNum=== true){
                        // console.log(this.currencyPipe.transform(value.sumaAsegurada, 'USD', 'symbol', '1.0-0'))
                        return this.currencyPipe.transform(value.sumaAsegurada, 'USD', 'symbol', '1.0-0')
                    }else if(valiNumLet=== true){
                        console.log('value.sumaAsegurada',value.sumaAsegurada);
                        return value.sumaAsegurada;
                    }else if(valiNumSig === true){
                        console.log('$'+value.sumaAsegurada);
                        return '$'+value.sumaAsegurada;
                    }else{
                        return (Number(value.sumaAsegurada) === 0) ? 'NO APLICA' : value.sumaAsegurada;
                    }
// return (value.SumaAsegurada == 0) ? 'NO APLICA' : this.currencyPipe.transform(value.sumaAsegurada, 'USD', 'symbol', '1.0-0');
                }

                let _sum = this.currencyPipe.transform(value.sumaAsegurada, 'USD', 'symbol', '1.0-0');
                return (value.SumaAsegurada == 0) ? 'NO APLICA' : this.currencyPipe.transform(value.sumaAsegurada, 'USD', 'symbol', '1.0-0');
            } else { // 'USD':'symbol':'1.0-0'
                let _value = this.toLowerCaseKeyService.parseToLower(value);
                let _digits = _value.sumaAsegurada.substring(1).trim().replace(',', '');
                let _pipe = (this._isNumber(_digits)) ? this.currencyPipe.transform(_digits, 'USD', 'symbol', '1.0-0') : _value.sumaAsegurada;

                return (_pipe.includes('$') && _pipe !== '$0') ? _pipe : 'NO APLICA';
            }
        } else {
            let value_aux = value.substring(value.lastIndexOf('-S') + 2, value.lastIndexOf('-D'));
            if (value_aux.indexOf('.') !== -1) { // Verifica si existe un punto en la suma
                if (value_aux.indexOf('.') === 1) { // Si el punto está en la posición 1 es V. Comercial y se sustituye por VALOR COMERCIAL
                    return 'NO APLICA'; // 'V. COMERCIAL';
                } else if (value_aux.indexOf('.') !== value_aux.length - 2) { // Quita los decimales de Banorte
                    return value_aux.substring(0, value_aux.indexOf('.')) + '\nPOR EVENTO';
                }
                value_aux = value.substring(value.lastIndexOf('-S') + 2, value.lastIndexOf('-D') - 2); // Quita los decimales de AFIRME
            }
            if (value_aux) {
                value_aux = +value_aux + ''; // Quita los espacios en qualitas por el mal response de WEBSSERVICE
            }
            // Evita las palabras y los precios de GNP debido a que ya tienen el formato requerido
            if (!isNaN(parseInt(value_aux[0], 10)) && value_aux.indexOf('$') === -1) {
                if (value_aux.indexOf(',') !== -1) { // Evita los precios sin formato de comas
                    return '$ ' + value_aux;
                }
                if (value_aux.length > 3) { // Evita los montos que no son lo suficientemente largos para tener comas
                    if (value_aux.length / 3 > 1 && value_aux.length / 3 <= 2) { // Se le agrega una coma
                        value_aux = value_aux.substring(0, value_aux.length - 3) + ',' + value_aux.substring(value_aux.length - 3, value_aux.length);
                    } else if (value_aux.length / 3 > 2 && value_aux.length / 3 <= 3) { // Se le agrega dos comas
                        value_aux = value_aux.substring(0, value_aux.length - 6) + ',' + value_aux.substring(value_aux.length - 6, value_aux.length - 3)
                            + ',' + value_aux.substring(value_aux.length - 3, value_aux.length);
                    }
                } else if (value_aux.length === 1) {
                    if (value_aux[0] === '1') { // Cambia los 1's de MAPFRE por AMPARADA
                        return 'AMPARADA';
                    } else if (value_aux[0] === '0') { // Cambia los 0's por N/A
                        return 'NO APLICA'; // 'Valor Comercial';
                    }
                }
                return '$ ' + value_aux; // Regresa los precios con el formato requerido
            }
            return value_aux.toString().toUpperCase(); // Regresa las palabras en mayúsculas
        }
    }

    private _isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }
}
