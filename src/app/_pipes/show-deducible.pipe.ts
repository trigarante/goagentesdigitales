import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showDeducible'
})
export class ShowDeduciblePipe implements PipeTransform {

  transform(value: any, type?: string) {
    if (type) {
      if(value.deducible.includes('.00')){
        return value.deducible.split('.')[0] +  '%';
      }else if(value.deducible === 'uma'){
        return value.deducible=== 'uma'? 'AMPARADA': value.deducible;
      }
      return (Number(value.deducible) === 0) ? 'NO APLICA' :(value.deducible === '0%') ? 'NO APLICA' : (value.deducible.toUpperCase() === 'NO APLICA' || value.deducible.toUpperCase() === 'N/A') ? 'NO APLICA' : (value.deducible.includes('$')) ? value.deducible : (value.deducible.includes('%')) ? value.deducible : (value.deducible > 0 ) ? value.deducible + '%': value.deducible ;
    } else {
      let value_aux = value.substring(value.lastIndexOf('-D') + 2, value.lastIndex);
      if (value_aux.indexOf('.') !== -1) { // En caso de que haya decimales en el porcentaje
        value_aux = value_aux.substring(0, value_aux.indexOf('.'));
        return (value_aux.includes('%')) ? value_aux : value_aux + '%';
      }
      if (value_aux[0] !== '$' && !isNaN(parseInt(value_aux[1], 10))) { // Evita todos los que son dinero y los que son palabras
        if (value_aux.length > 3) {
          if (value_aux[0] === '0' && value_aux[1] === '0' && value_aux[2] === '0') { // Evita los tres ceros de Qualitas
            value_aux = value.substring(value.lastIndexOf('-D') + 5, value.lastIndex);
            if (value_aux[0] === '0') { // Si es cero se evita poner el signo de porcentaje
              return 'NO APLICA'; // '0';
            }
            return value_aux + ' %';
          }
        } else if (value_aux[value_aux.length - 1] === '%') { // Evita colocar dos veces el signo de porcentaje
          return value_aux;
        } else {
          return value_aux + ' %';
        }
      } else if (value_aux.length === 1) { // Verifica si solo es un caracter
        if (value_aux[0] === '0') { // Si es cero se sustituye por N/A
          return 'NO APLICA'; // '0 %';
        }
        return value_aux + ' %';
      } else if (value_aux.length === 0) { // Sustituye los espacios en blanco por N/A
        return '';
      } else if (value_aux[0] !== '$') {
        if (value_aux.toString().toUpperCase() === 'NO APLICA') {
          return '';
        }
        return value_aux.toString().toUpperCase();
      }
      return value_aux + ' %';
    }
  }
}
