import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'showSuma'
})
export class ShowSumaPipe implements PipeTransform {

    transform(value: any, type?: string) {
        if (!value) return;

        if (type) {
            return (value.SumaAsegurada == 0) ? 'NO APLICA' : value.SumaAsegurada;
        } else {
            let value_aux = value.substring(value.lastIndexOf('-S') + 2, value.lastIndexOf('-D'));
            if (value_aux.indexOf('.') !== -1) { // Verifica si existe un punto en la suma
                if (value_aux.indexOf('.') === 1) { // Si el punto está en la posición 1 es V. Comercial y se sustituye por VALOR COMERCIAL
                    return 'NO APLICA'; // 'V. COMERCIAL';
                } else if (value_aux.indexOf('.') !== value_aux.length - 2) { // Quita los decimales de Banorte
                    return value_aux.substring(0, value_aux.indexOf('.')) + '\nPOR EVENTO';
                }
                value_aux = value.substring(value.lastIndexOf('-S') + 2, value.lastIndexOf('-D') - 2); // Quita los decimales de AFIRME
            }
            // if (value_aux) {
            //     value_aux = +value_aux; // Quita los espacios en qualitas por el mal response de WEBSSERVICE
            // }
            // Evita las palabras y los precios de GNP debido a que ya tienen el formato requerido
            if (!isNaN(parseInt(value_aux[0], 10)) && value_aux.indexOf('$') === -1) {
                if (value_aux.indexOf(',') !== -1) { // Evita los precios sin formato de comas
                    return '$ ' + value_aux;
                }
                if (value_aux.length > 3) { // Evita los montos que no son lo suficientemente largos para tener comas
                    if (value_aux.length / 3 > 1 && value_aux.length / 3 <= 2) { // Se le agrega una coma
                        value_aux = value_aux.substring(0, value_aux.length - 3) + ',' + value_aux.substring(value_aux.length - 3, value_aux.length);
                    } else if (value_aux.length / 3 > 2 && value_aux.length / 3 <= 3) { // Se le agrega dos comas
                        value_aux = value_aux.substring(0, value_aux.length - 6) + ',' + value_aux.substring(value_aux.length - 6, value_aux.length - 3)
                            + ',' + value_aux.substring(value_aux.length - 3, value_aux.length);
                    }
                } else if (value_aux.length === 1) {
                    if (value_aux[0] === '1') { // Cambia los 1's de MAPFRE por AMPARADA
                        return 'AMPARADA';
                    } else if (value_aux[0] === '0') { // Cambia los 0's por N/A
                        return 'NO APLICA'; // 'Valor Comercial';
                    }
                }
                return '$ ' + value_aux; // Regresa los precios con el formato requerido
            }
            return value_aux.toString().toUpperCase(); // Regresa las palabras en mayúsculas
        }
    }

}
