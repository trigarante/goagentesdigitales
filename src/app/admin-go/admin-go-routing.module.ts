import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessagesComponent } from '../_components/generals';
import { AuthGuardGuard } from '../_guards/auth-guard.guard';

import { AdminGoComponent } from './admin-go.component';

const routes: Routes = [
  {
    path: '', component: AdminGoComponent, children: [
      { path: 'dashboard', loadChildren: () => import('./../_components/dashboard/dashboard.module').then(m => m.DashboardModule), canActivate: [AuthGuardGuard] },
      { path: 'mensajes', component: MessagesComponent, canActivate: [AuthGuardGuard] },
      { path: 'mensajes/:option', component: MessagesComponent, canActivate: [AuthGuardGuard] },
      { path: 'inicio', loadChildren: () => import('./../_components/inicio/inicio.module').then(m => m.InicioModule), canActivate: [AuthGuardGuard] },
      { path: 'oficina-virtual/dasboard', loadChildren: () => import('./../_components/oficinavirtual/oficinavirtual.module').then(m => m.OficinavirtualModule), canActivate: [AuthGuardGuard] },
      { path: 'quienes-somos', loadChildren: () => import('./../_components/us/us.module').then(m => m.UsModule), canActivate: [AuthGuardGuard] },
      { path: 'cotiza-go', loadChildren: () => import('./../_components/go-quotes/go-quotes.module').then(m => m.GoQuotesModule), canActivate: [AuthGuardGuard] },
      { path: 'usuario', loadChildren: () => import('./../_components/user/user.module').then(m => m.UserModule), canActivate: [AuthGuardGuard] },
      { path: 'descargar-formatos', loadChildren: () => import('./../_components/download-formats/download-formats.module').then(m => m.DownloadFormatsModule), canActivate: [AuthGuardGuard] },
      { path: 'capacitacion', loadChildren: () => import('./../_components/training/training.module').then(m => m.TrainingModule), canActivate: [AuthGuardGuard] },
      { path: 'otros-ramos', loadChildren: () => import('./../_components/otros-ramos/otros-ramos.module').then(m => m.OtrosRamosModule), canActivate: [AuthGuardGuard] },
      { path: 'desactivarAseg', loadChildren: () => import('./../_components/aseguradoras-on-off/aseguradoras-on-off.module').then(m => m.AseguradorasOnOffModule), canActivate: [AuthGuardGuard] },
      { path: 'cotizaciones', loadChildren: () => import('./../_components/cotizaciones-generales/cotizaciones-generales.module').then(m => m.CotizacionesGeneralesModule), canActivate: [AuthGuardGuard] },
      { path: 'emisionesGenerales', loadChildren: () => import('./../_components/emisiones-generales/emisiones-generales.module').then(m => m.EmisionesGeneralesModule), canActivate: [AuthGuardGuard] },
      { path: '**', redirectTo: '/admin/dashboard', pathMatch: 'full' }
    ]
  }
];
// [{ path: '', component: AdminGoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminGoRoutingModule { }
