import { Component, HostListener, OnInit } from '@angular/core';
import { CatalogosService, MenuToggleService } from '../_services';
import { NgxIndexedDBService } from 'ngx-indexed-db';



@Component({
  selector: 'app-admin-go',
  templateUrl: './admin-go.component.html',
  styleUrls: ['./admin-go.component.css']
})
export class AdminGoComponent implements OnInit {
  toggleMenu: boolean = false;
  scrHeight: any;
  scrWidth: any;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
  }


  constructor(
    private menuToggleService: MenuToggleService,
    private dbService: NgxIndexedDBService,
    private catalogosService: CatalogosService
  ) { 
    this.getScreenSize();
  }
  ngOnInit(): void {
    this.toggleMenu = false
    this.menuToggleService.getNavChangeEmitter().subscribe((item: any) => { this.toggleMenu = item; });
    // this._getBrands();
  }

  private _getBrands(): void {
    if (!localStorage.getItem('brandsMulty')) {
      this.catalogosService.getMarcasComparador().subscribe(
        resp => {
          resp.forEach(el => {
            this._getModels(el.text);
            this.dbService
              .add('brandMulty', {
                id: el.id,
                text: el.text
              })
              .subscribe((key) => {
                // console.log('key: ', key);
              });
          });
          localStorage.setItem('brandsMulty', 'true');
        }
      );
    }
  }

  private _getModels(brand: string): void {
    this.catalogosService.getModelosComparador(brand).subscribe(
      resp => {
        resp.forEach(el => {
          this._getSubBrands(brand, el.text);
          this.dbService
            .add('modelMulty', {
              id: el.id,
              text: el.text,
              brand: brand
            })
            .subscribe((key) => {
              // console.log('key: ', key);
            });
        });
      }
    );
  }

  private _getSubBrands(brand: string, model: string): void {
    this.catalogosService.getSubmarcaComparador(brand, model).subscribe(
      resp => {
        resp.forEach(el => {
          // this._getDescription(brand, model, el.text);
          this.dbService
            .add('subBrandMulty', {
              id: el.id,
              text: el.text,
              brand: brand,
              model: model
            })
            .subscribe((key) => {
              // console.log('key: ', key);
            });
        });
      }
    );
  }

  private _getDescription(brand: string, model: string, subBrand: string): void {
    this.catalogosService.getDescripcionesComparador(brand, model, subBrand).subscribe(
      resp => {
        resp.forEach(el => {
          this.dbService
            .add('descriptionMulty', {
              id: el.id,
              text: el.text,
              brand: brand,
              model: model,
              subBrand: subBrand
            })
            .subscribe((key) => {
              // console.log('key: ', key);
            });
        });
      }
    );
  }
}
