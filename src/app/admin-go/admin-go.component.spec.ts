import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGoComponent } from './admin-go.component';

describe('AdminGoComponent', () => {
  let component: AdminGoComponent;
  let fixture: ComponentFixture<AdminGoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminGoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
