import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminGoRoutingModule } from './admin-go-routing.module';
import { AdminGoComponent } from './admin-go.component';

import { CommonLocalModule } from '../_modules/common-local.module';
import { ModalDialogModule } from '../_components/generals/modal-dialog/modal-dialog.module';
import { SidebarleftmvlComponent } from '../_components/generals/sidebarleftmvl/sidebarleftmvl.component';

@NgModule({
  declarations: [
    AdminGoComponent,
    SidebarleftmvlComponent
   
  ],
  imports: [
    CommonModule,
    AdminGoRoutingModule,

    CommonLocalModule,
    ModalDialogModule
  ]
})
export class AdminGoModule { }
