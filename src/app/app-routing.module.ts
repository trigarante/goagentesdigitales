import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './_components/public';
import { ForgotPasswordComponent } from './_components/public';
import { AuthGuardGuard } from './_guards/auth-guard.guard';
import {BusinessCardsComponent} from "./_components/oficinavirtual/business-cards/business-cards.component";


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'forgotPass', component: ForgotPasswordComponent },
  { path: 'virtual-card', component: BusinessCardsComponent },

  // validate
  { path: 'admin', loadChildren: () => import('./admin-go/admin-go.module').then(m => m.AdminGoModule), canActivate: [AuthGuardGuard] },

  { path: '', redirectTo: '/admin', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
