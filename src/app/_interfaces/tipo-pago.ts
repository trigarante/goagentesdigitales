export interface TipoPagoInterface {
    id: number;
    cantidadPagos: number;
    tipoPago: string;
    activo: number;
}
