export interface ClientesInterface {
    id: number;
    idPais: number;
    personaMoral: number;
    nombre: string;
    paterno: string;
    materno: string;
    cp: number;
    calle: string;
    numInt: string;
    numExt: string;
    idColonia: number;
    colonia: string;
    genero: string;
    telefonoFijo: string;
    telefonoMovil: string;
    correo: string;
    fechaNacimiento: string;
    fechaRegistro: string;
    curp: string;
    rfc: string;
    razonSocial: string;
    idFase: string;
}
