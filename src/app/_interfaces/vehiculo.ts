export interface VehiculoInterface {
    Uso?: string;
    Marca?: string;
    Modelo?: string;
    NoMotor?: string;
    NoSerie?: string;
    NoPlacas?: string;
    Descripcion?: string;
    CodMarca?: string;
    CodDescripcion?: string;
    CodUso?: string;
    Clave?: string;
    Servicio?: string;
    SubMarca?: string;

    uso?: string;
    marca?: string;
    modelo?: string;
    noMotor?: string;
    noSerie?: string;
    noPlacas?: string;
    descripcion?: string;
    codMarca?: string;
    codDescripcion?: string;
    codUso?: string;
    clave?: string;
    servicio?: string;
    subMarca?: string;
}
