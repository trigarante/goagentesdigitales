export interface T2020AreasInterface {
    id: number;
    descripcion: string;
    fechaCreacion: string;
    activo: boolean;
}
