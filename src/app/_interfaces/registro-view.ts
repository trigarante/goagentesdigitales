export interface RegistroViewInterface {
 id: number;
 poliza: string;
 alias: string;
 fechaInicio: string;
 nombreCliente: string;
 telefonoMovil: string;
 correo: string;
 nombreAgente: string;
 estado: string;
 tiempoPoliza: number;
 archivoCliente?: string;
 archivoRegistro?: string;
 idCliente: number;
 idRecibo: number;
 numero: number;
 activo: number;
}
