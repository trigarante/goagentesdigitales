export interface ProductoSocioInterface {
    id: number;
    idSubRamo: number;
    idTipoProducto: number;
    prioridad: number;
    nombre: string;
    activo: number;
    tipoProducto: string;
}
