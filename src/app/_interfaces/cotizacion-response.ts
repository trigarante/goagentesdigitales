import { ClienteInerface } from "./cliente";
import { CotizacionInterface } from "./cotizacion";
import { EmisionInterface } from "./emision";
import { PagoInterface } from "./pago";
import { VehiculoInterface } from "./vehiculo";
import { CoverageInterface } from './coverage';

export class CotizacionResponseInterface {
 Aseguradora?: string;
 Cliente?: ClienteInerface;
 Vehiculo?: VehiculoInterface;
 Coberturas?: any[]; // CoverageInterface[];
 Paquete?: string;
 Descuento?: string;
 PeriodicidadDePago?: string;
 Cotizacion?: CotizacionInterface;
 Emision?: EmisionInterface;
 Pago?: PagoInterface;
 CodigoError?: string;
 //urlRedireccion?: any;
 UrlRedireccion?: any;
 Submarca?: string;

 aseguradora?: string;
//  cotizacion?: any;
 versiones?: any[];
 cliente?: ClienteInerface;
 vehiculo?: VehiculoInterface;
 coberturas?: any[]; // CoverageInterface[];
 paquete?: string;
 descuento?: string;
 periodicidadDePago?: string;
 cotizacion?: CotizacionInterface;
 emision?: EmisionInterface;
 pago?: PagoInterface;
 codigoError?: string;
 submarca?: string;

 constructor(aseguradora: string, cotizacion: object, versiones: any[]) {
    this.aseguradora = aseguradora;
    this.cotizacion = cotizacion;
    this.versiones = versiones;
    }
}
