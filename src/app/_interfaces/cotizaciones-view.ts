export interface CotizacionesViewInterface {
 id: number;
 alias: string;
 fechaCreacion: string;
 cp: string;
 edad: number;
 genero: string;
 clave: string;
 marca: string;
 descripcion: string;
 paquete: string;
 servicio: string;
 descuento: string;
 fechaNacimiento: string;
 nombreAgente: string;
}
