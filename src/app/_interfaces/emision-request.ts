import { ClienteInerface } from "./cliente";
import { CotizacionInterface } from "./cotizacion";
import { EmisionInterface } from "./emision";
import { PagoInterface } from "./pago";
import { VehiculoInterface } from "./vehiculo";

export interface EmisionRequestInterface {
    Aseguradora?: string;
    Cliente?: ClienteInerface;
    Vehiculo?: VehiculoInterface;
    Coberturas?: any;
    Paquete?: string;
    Descuento?: string;
    PeriodicidadDePago?: string;
    Cotizacion?: CotizacionInterface;
    Emision?: EmisionInterface;
    Pago?: PagoInterface;
    CodigoError?: string;
    UrlRedireccion?: string;
    urlRedireccion?: any;

    aseguradora?: string;
    cliente?: ClienteInerface;
    vehiculo?: VehiculoInterface;
    coberturas?: any;
    paquete?: string;
    descuento?: string;
    periodicidadDePago?: string;
    cotizacion?: CotizacionInterface;
    emision?: EmisionInterface;
    pago?: PagoInterface;
    codigoError?: string;

    idRegistro?:any;
}
