    export interface Leads{
        idEmision:         null | string;
        goId:              string;
        idCotizacion:      string;
        fechaCotizaciones: Date;
        idAgente:          number;
        nombreAgente:      string;
        estadoEmision:     null | string;
        idEstadoEmision:   number | null;
        idAgente1:         string;
        marca:             string;
        modelo:            string;
        nombre:            null | string;
        telefono:          null | string;
        correo:            null | string;
        descripcion:       string;
        primaNeta:         null;
        contadorstatus:    null;
    }
