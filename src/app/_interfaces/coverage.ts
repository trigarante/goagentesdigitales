export interface CoverageInterface {
    Nombre: string;
    SumaAsegurada: string;
    Deducible: string | number;
}
