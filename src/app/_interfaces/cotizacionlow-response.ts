import { ClienteInerface } from "./cliente";
import { CotizacionInterface } from "./cotizacion";
import {VehiculoInterface} from "./vehiculo";
import {EmisionInterface} from "./emision";
import {PagoInterface} from "./pago";

export class CotizacionlowResponseInterface {
    aseguradora?: string;
    cliente?: ClienteInerface;
    vehiculo?: VehiculoInterface;
    coberturas?: any[];
    paquete?: string;
    descuento?: string;
    periodicidadDePago?: number;
    cotizacion?: CotizacionInterface;
    emision?: EmisionInterface;
    pago?: PagoInterface;
    codigoError?: string;
    urlRedireccion?: any;
    UrlRedireccion?: any;

    Aseguradora?: string;
    Cotizacion?: any;
    Versiones?: any[];

    constructor(Aseguradora:string, Cotizacion:object, Versiones: any[]) {
        this.Aseguradora = Aseguradora;
        this.Cotizacion  = Cotizacion;
        this.Versiones   = Versiones;
    }
}
