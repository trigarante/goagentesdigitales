export interface SubRamoInterface {
    id: number;
    idRamo: number;
    idTipoSubRamo: number;
    descripcion: string;
    prioridad: number;
    activo: number;
    tipoSubRamo: string;
}
