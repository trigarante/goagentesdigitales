export interface SociosInterface {
    id: number;
    prioridad: number;
    nombreComercial: string;
    rfc: string;
    razonSocial: string;
    alias: string;
    idEstadoSocio: string;
    activo: number;
    idPais: number;
}
