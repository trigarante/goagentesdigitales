export interface ResponsiveInterface {
    height?: number;
    width?: number;
}
