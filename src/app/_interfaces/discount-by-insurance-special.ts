export interface DiscountByInsuranceSpecialInterface {
    start?: string;
    end?: string;
    discount?: number;
    msi?: string;
}
