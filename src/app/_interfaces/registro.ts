export interface RegistroInterface {
    id: number;
    idAgente: number;
    idProducto: number;
    idTipoPago: number;
    idEstadoPoliza: number;
    idSocio: number;
    idProductoSocio: number;
    idPeriodicidad: number;
    poliza: string;
    fechaInicio: string;
    primaNeta: number;
    archivo: string;
}
