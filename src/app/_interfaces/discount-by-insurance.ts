import { DiscountByInsuranceSpecialInterface } from "./discount-by-insurance-special";

export interface DiscountByInsuranceInterface {
    insurance: string;
    discount: number;
    active?: boolean;
    msi: string;
    special?: DiscountByInsuranceSpecialInterface[];
}
