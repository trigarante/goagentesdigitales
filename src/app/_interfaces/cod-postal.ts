export interface CodPostalInterface {
 ciudad?: any;
 delMun: string;
 idCp: number;
 cp: number;
 asenta: string;
 estado: string;
 descripcion?: string;
 codigoPostal?: string;
 codigoColonia?: string;
 id?:number;
}
