export interface CardTypeInterface {
    label: string;
    value: string;
}
