export interface CarrierInterface {
    id: number;
    carrier: string;
    activo: number;
}
