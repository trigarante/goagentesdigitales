export interface PagosInterface {
    id: number;
    idAgente: number;
    idRecibo: number;
    idFormaPago: number;
    idEstadoPago: number;
    fechaPago: string;
    cantidad: number;
    archivo: string;
    datos: any;
}
