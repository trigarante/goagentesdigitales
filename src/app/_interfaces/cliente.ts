import { DireccionInterface } from "./direccion";

export interface ClienteInerface {
    TipoPersona?: any;
    Nombre?: any;
    ApellidoPat?: any;
    ApellidoMat?: any;
    BeneficiarioPreferente?: any;
    RFC?: any;
    FechaNacimiento?: string;
    Ocupacion?: any;
    CURP?: any;
    Direccion?: DireccionInterface;
    Edad?: number;
    Genero?: string;
    Telefono?: string;
    Email?: any;

    tipoPersona?: any;
    nombre?: any;
    apellidoPat?: any;
    apellidoMat?: any;
    beneficiarioPreferente?: any;
    rfc?: any;
    fechaNacimiento?: string;
    ocupacion?: any;
    curp?: any;
    direccion?: DireccionInterface;
    edad?: number;
    genero?: string;
    telefono?: string;
    email?: any;
}
