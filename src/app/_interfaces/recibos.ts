export interface RecibosInterface {
    id: number;
    idRegistro: number;
    idAgente: number;
    idEstadoRecibos: number;
    numero: number;
    cantidad: number;
    fechaVigencia: string;
    activo: number;
    poliza: string;
    estado: string;
    fechaLiquidacion?: string;
    idPago?: number;
    idEstadoPago?: number;
    fechaPago?: string;
    fechaRegistro?: string;
    archivo?: string;
}
