export interface AseguradoraInterface {
 selectAseguradora?: any;
  selectDescripcion?: any;
  selectSubDescripcion?: any;
  selectMarca?: any;
  selectModelo?: any;
  selectDetalle?: any;
  nombre?: any;
  email?: any;
  telefono?: any;
  edad?: any;
  genero?: any;
  cp?: any;
  descuento?: any;
  folio?: any;
  poliza?: any;
  recibo?: any;
  aviso?: any;
  numPoliza?: number;
  responseCot?: any;
}
