export interface T2020FilesByAreasInterface {
    id: number;
    idArea: number;
    nombre: string;
    url: string;
    tipoArchivo: string;
    fechaRegistro: string;
    activo: boolean;
}
