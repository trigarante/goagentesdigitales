export interface FormaPagoInterface {
    id: number;
    description: string;
    activo: number;
}
