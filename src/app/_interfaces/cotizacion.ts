export interface CotizacionInterface {
    PrimaTotal?: string;
    PrimaNeta?: string;
    Derechos?: string;
    Impuesto?: string;
    Recargos?: any;
    PrimerPago?: string;
    PagosSubsecuentes?: any;
    IDCotizacion?: string;
    CotID?: string;
    VerID?: string;
    CotIncID?: string;
    VerIncID?: string;
    Resultado?: string;

    primaTotal?: string;
    primaNeta?: string;
    derechos?: string;
    impuesto?: string;
    recargos?: any;
    primerPago?: string;
    pagosSubsecuentes?: any;
    idCotizacion?: string;
    cotID?: string;
    verID?: string;
    cotIncID?: string;
    verIncID?: string;
    resultado?: string;
}
