export interface CoberturaInterface {
 DanosMateriales: any;
 DanosMaterialesPP: any;
 RoboTotal: any;
 RCBienes: any;
 RCPersonas: any;
 RC: any;
 RCFamiliar: any;
 RCExtension: any;
 RCExtranjero: any;
 RCPExtra: any;
 AsitenciaCompleta: any;
 DefensaJuridica: any;
 GastosMedicosOcupantes: any;
 MuerteAccidental: any;
 GastosMedicosEvento: any;
 Cristales: any;
}
