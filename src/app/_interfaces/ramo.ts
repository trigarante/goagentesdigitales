export interface RamoInterface {
    id: number;
    idSocio: number;
    idTipoRamo: number;
    descripcion: string;
    prioridad: number;
    activo: number;
    tipoRamo: string;
}
