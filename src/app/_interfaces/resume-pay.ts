export interface ResumePayInterface {
    personal?: PersonalDataInterface;
    address?: AddressDataInterface;
    car?: CarDataInterface
    card?: CardDataInterface
}

export interface PersonalDataInterface {
    nombre?: string;
    aPaterno?: string;
    aMaterno?: string;
    correo?: string;
    telefono?: string;
    nacimientoEstado?: string;
    fechaNacimiento?: string;
    rfc?: string;
    curp?: string;
}

export interface AddressDataInterface {
    estadoMun?: string;
    colonia?: string;
    calleNum?: string;
    numero?: number | string;
    numInterior?: number | string;
}

export interface CarDataInterface {
    niv?: string;
    motor?: string;
    placa?: string;
}

export interface CardDataInterface {
    tipoTarjeta?: string | number;
    // tdc?: string;
    nombreTarjeta?: string;
    banco?: string;
    metodoPago?: string;
    formaPago?: string;
    numTarjeta?: string;
    mesVencimiento?: number;
    anioVencimiento?: number;
    cvv?: number;
}

// export interface GenericResponseInterface {
//     new(...args: any[]): any;
// }
