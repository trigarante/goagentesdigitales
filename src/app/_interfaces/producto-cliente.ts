export interface ProductoClienteInterface {
    id: number;
    idCotizacion: number;
    idSubRamo: number;
    datos: any;
    idCliente: number;
}
