export interface CotizacionRequestInterface {
 aseguradora?: string;
 clave?: string;
 cp?: string;
 descripcion?: string;
 descuento?: number;
 detalle?: string,
 edad?: string;
 fechaNacimiento?: string;
 genero?: string;
 marca?: string;
 modelo?: string;
 movimiento?: string;
 paquete?: string;
 servicio?: string;
 valorFactura?: number;
 periodicidadDePago?: string;
 nombre?: string;
 submarca?: string;
 coberturas?: any;
 cotizacion?: any;
}
