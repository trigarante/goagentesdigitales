import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../_services';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  helper: any;

  constructor(
    private router: Router,
    private userService: UserService,
    private rutaActiva: ActivatedRoute
  ) { }

  public async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    try {
      if (this.userService.isLoggedIn()) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    } catch (err) {
      return false;
    }
  }

  public isLoggedIn(): boolean {
    return new Date().getTime() <= this.helper.getTokenExpirationDate(localStorage.getItem('token')).getTime();
  }
}
