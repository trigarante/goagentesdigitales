// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,

    base_url: 'https://b54335fbbf97.ngrok.io/',
    // 'http://goagentesdigitales.mx-develop.s3-website-us-east-1.amazonaws.com/',

    //
    ENDPOINTBASE:      'https://dev.core-goagentesdigitales.com/',
    ENDPOINTPERSISTANCE: 'https://core-persistance-service.com/',
    ENDPOINTAUTOSTEST: 'https://dev.core-goagentesdigitales.com/v1',
    // ENDPOINTBASE:      'http://dev.core-goagentes-service-dev.us-east-1.elasticbeanstalk.com/',
    // ENDPOINTAUTOSTEST: 'http://dev.core-goagentes-service-dev.us-east-1.elasticbeanstalk.com/v1',
    ENDCOTIZACION : 'https://dev.core-goagentesdigitales.com/v2', //TODO

    OTROSRAM : 'https://dev.core-goagentesdigitales.com/v1',
    OTROSRAMOSVIEW : 'https://dev.core-goagentesdigitales.com/v2',

    //ENDPOINTGNP3D: 'https://dev.web-gnp.mx/',
    // ENDPOINTBASE: 'http://192.168.46.8:5000/',
    // ENDPOINTAUTOSTEST: 'http://192.168.46.8:5000/v1',
    //ENDPOINTGNP3D: 'http://core-gnp-dev.us-east-1.elasticbeanstalk.com/',
    apiUrlDataRoot: 'https://api-vehicular.theapiworld.com/',
    apiUrlData: 'https://api-vehicular.theapiworld.com/v1/',
    // oficinaServices: 'http://localhost:8080'
    endpoint_privat_driver: 'https://core-brandingservice.com/',
    // endpoint_privat_driver: 'http://192.168.36.146:5000/',
    // ENDPOINTBASE: 'https://core-goagentesdigitales.com/',
    // ENDPOINTAUTOSTEST: 'https://core-goagentesdigitales.com/v1',
    // ENDPOINTGNP3D: 'http://core-gnp-prod.eba-tskjpfym.us-east-1.elasticbeanstalk.com/',
    ENDPOINTGNP3D: 'https://web-gnp.mx/',
    //ENDPOINTAUTOSTEST: 'http://192.168.44.245:5001/v1',
    //ENDPOINTBASE: 'http://192.168.44.245:5001/',
    // ENDPOINTAUTOSTEST: 'http://localhost:5000/v1',
    // ENDPOINTBASE: 'http://localhost:5000/',
    //apiUrlDataRoot: 'https://api-vehicular.theapiworld.com/',
    //apiUrlData: 'https://api-vehicular.theapiworld.com/v1/',
    oficinaServices: 'https://go.mark-43.net',
    //oficinaServices: 'http://localhost:8080',
    endpoint_t2020: 'https://goagentes-core.trigarante2022.com/retail/',
    endpoint_t2020_drive: 'https://drive.google.com/uc?export=view&id=',
    endpoint_t2022: 'https://go-dev-core.trigarante2022.com',
    
    ENDPOINTAUTOSTESTV2:'https://dev.core-goagentesdigitales.com/v2',
    COTIZACIONEMICIONQUALITAS: 'https://dev.core-goagentesdigitales.com/v2',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
