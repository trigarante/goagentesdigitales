export const environment = {
  production: true,

  base_url: 'https://portal.goagentesdigitales.mx/',

  ENDPOINTBASE: 'https://core-goagentesdigitales.com/',
  ENDPOINTPERSISTANCE: 'https://core-persistance-service.com/',
  ENDPOINTAUTOSTEST: 'https://core-goagentesdigitales.com/v1',
  ENDPOINTGNP3D: 'https://web-gnp.mx/',
  // ENDPOINTGNP3D: 'http://core-gnp-prod.eba-tskjpfym.us-east-1.elasticbeanstalk.com/',
  ENDCOTIZACION : 'https://core-goagentesdigitales.com/v2',
  OTROSRAM : 'https://core-goagentesdigitales.com/v1',
  OTROSRAMOSVIEW : 'https://core-goagentesdigitales.com/v2',

  //  ENDPOINTAUTOSTEST: 'http://192.168.36.179:5000/v1',
  //  ENDPOINTBASE: 'http://192.168.36.179:5000/',
  apiUrlDataRoot: 'https://api-vehicular.theapiworld.com/',
  apiUrlData: 'https://api-vehicular.theapiworld.com/v1/',
  oficinaServices: 'https://goagentes-core.trigarante2022.com',
  endpoint_privat_driver: 'https://core-brandingservice.com/',

  endpoint_t2020: 'https://goagentes-core.trigarante2022.com/retail/',
  endpoint_t2020_drive: 'https://drive.google.com/uc?export=view&id=',
  endpoint_t2022: 'https://goagentes-core.trigarante2022.com',

  ENDPOINTAUTOSTESTV2:'https://core-goagentesdigitales.com/v2',
  COTIZACIONEMICIONQUALITAS:'https://core-goagentesdigitales.com/v2'
  
};
